<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();            
            $table->string('email')->unique();
            $table->string('skype')->nullable();
            $table->string('phone')->nullable();
            $table->string('country')->nullable();
            $table->string('password')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();            
            $table->string('business_name')->nullable();
            $table->string('business_address')->nullable();
            $table->string('business_phone')->nullable();
            $table->string('platform')->nullable();
            $table->string('storeruntime')->nullable();
            $table->string('ordersno')->nullable();
            $table->string('productsno')->nullable();
            $table->string('ordersperday')->nullable();
            $table->string('verifyToken')->nullable();
            $table->enum('status', ['0','1'])->default('0')->comment('deactive->0, active->1');
            $table->enum('role', ['0','1'])->default('1')->comment('admin->0, user->0');
            $table->integer('approve')->default('0');
            $table->timestamp('email_verified_at')->nullable();            
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
