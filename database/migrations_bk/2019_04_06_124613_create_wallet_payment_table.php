<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->nullable();
            $table->integer('user_id')->nullable();            
            $table->string('payment_method')->nullable();
            $table->string('amount_without_fee')->nullable();
            $table->string('amount')->nullable();
            $table->string('currency_code')->nullable();
            $table->string('txn_id')->nullable();
            $table->string('payment_status')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('proof_of_payment')->nullable();                         
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallet_payment');
    }
}
