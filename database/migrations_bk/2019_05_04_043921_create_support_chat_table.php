<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportChatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_chat', function (Blueprint $table) {
            $table->bigIncrements('chat_id');
            $table->bigInteger('ticket_id');
            $table->longText('message');
            $table->integer('reply_by_admin');
            $table->integer('reply_by_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_chat');
    }
}
