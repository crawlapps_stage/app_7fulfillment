<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('line_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->string('shopify_order_line_item_id')->nullable()->comment("shopify LineItem id");
            $table->string('shopify_product_id')->nullable()->comment("shopify Product id");
            $table->string('shopify_variant_id')->nullable()->comment("shopify variant id");
            $table->string('product_title')->nullable()->comment("shopify product title");
            $table->float('product_price')->nullable()->comment("comes frpm portal");
            $table->string('sku')->nullable();
            $table->integer('qty')->nullable();
            $table->float('weight')->nullable();
            $table->integer('shipping_provider')->nullable();
            $table->boolean('status')->nullable()->comment('0:SKU not matched; 1:SKU matched');
            $table->foreign('order_id')->references('id')->on('orders')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('line_items');
    }
}
