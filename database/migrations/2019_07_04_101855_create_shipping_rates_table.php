<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('shipping_provider_name')->nullable();
            $table->integer('provider_id')->nullable();
            $table->float('cny_per_piece', 8,4)->nullable();
            $table->float('cny_per_gram', 8,4)->nullable();
            $table->float('price_less_than_500', 8,4)->nullable();
            $table->float('price_more_than_500', 8,4)->nullable();
            $table->integer('country_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_rates');
    }
}
