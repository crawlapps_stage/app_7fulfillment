<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBulkSourcingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bulk_sourcing', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('q_link')->nullable();
			$table->string('pieces')->nullable();
			$table->string('shipping_location')->nullable();
			$table->string('time_frame')->nullable();
			$table->double('quote',8,2)->nullable();
			$table->integer('status')->default(0);
			$table->integer('quote_approve')->default(0);
			$table->integer('user_id')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bulk_sourcing');
	}

}
