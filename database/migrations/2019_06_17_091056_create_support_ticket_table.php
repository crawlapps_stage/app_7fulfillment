<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSupportTicketTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('support_ticket', function(Blueprint $table)
		{
			$table->bigInteger('ticket_id', true);
			$table->integer('order_id');
			$table->integer('user_id');
			$table->string('user_email');
			$table->string('subject');
			$table->text('message');
			$table->integer('status');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('support_ticket');
	}

}
