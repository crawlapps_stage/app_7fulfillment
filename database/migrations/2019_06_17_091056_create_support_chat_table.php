<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSupportChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('support_chat', function(Blueprint $table)
		{
			$table->bigInteger('chat_id', true);
			$table->bigInteger('ticket_id');
			$table->text('message');
			$table->integer('reply_by_admin');
			$table->integer('reply_by_user');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('support_chat');
	}

}
