<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('shop_id')->unsigned();
            $table->string('customer_id')->nullable()->comment("Shopify customer id");
            $table->string('order_id')->comment('shopify order id');
            $table->string('order_number')->nullable()->comment('shopify order number');
            $table->string('order_status')->nullable()->comment('shopify order status');
            $table->string('no_line_item')->default(0);
            $table->string('order_buyer_country')->nullable();
            $table->dateTime('order_date')->nullable();

            //vgs changes
            $table->bigIncrements('number_of_parcel')->default(0)->comment('7fulfillment Number of parcel');
            $table->double('shipping_cost',8,2)->nullable()->comment('7fulfillment Shipping Cost');
            $table->double('product_fee',8,2)->nullable()->comment('7fulfillment Product Fee');
            $table->double('handling_fee',8,2)->nullable()->comment('7fulfillment Handling Fee');
            $table->double('total_cost',8,2)->nullable()->comment('7fulfillment Total Cost');
            $table->string('shipping_status',100)->nullable()->comment('7fulfillment shipping status');
            //vgs changes end 

            $table->timestamps();

            $table->foreign('shop_id')->references('id')->on('shops')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
