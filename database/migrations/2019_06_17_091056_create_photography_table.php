<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePhotographyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('photography', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('product_link')->nullable();
			$table->float('product_price', 8,2)->nullable();
			$table->float('total_price', 8,2)->nullable();
			$table->integer('status')->default(0);
			$table->integer('user_id')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photography');
	}

}
