<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('fname', 191)->nullable();
			$table->string('lname', 191)->nullable();
			$table->string('email', 191)->unique();
			$table->string('skype', 191)->nullable();
			$table->string('phone', 191)->nullable();
			$table->string('country', 191)->nullable();
			$table->string('password', 191)->nullable();
			$table->string('state')->nullable();
			$table->string('city')->nullable();
			$table->string('business_name')->nullable();
			$table->string('business_address')->nullable();
			$table->string('business_phone')->nullable();
			$table->string('platform')->nullable();
			$table->string('storeruntime')->nullable();
			$table->string('ordersno')->nullable();
			$table->string('productsno')->nullable();
			$table->string('ordersperday')->nullable();
			$table->string('verifyToken', 191)->nullable();
			$table->enum('status', array('0','1'))->default('0')->comment('0=>\'unverified\', 1=>\'Verified\'');
			$table->enum('role', array('0','1','2'))->default('1')->comment('0=>\'Admin\',1=>\'User\', 2=>\'Sub Admin\'');
			$table->enum('approve', array('0','1'))->default('0')->comment('0=>\'Not approved by admin\', 1=>\'Approved by admin\'');
			$table->dateTime('email_verified_at')->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
