<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('number_of_parcel');->nullable();
			$table->double('shipping_cost_cny',8,2)->nullable();
			$table->double('shipping_cost_usd',8,2)->nullable();
			$table->double('handling_fee',8,2)->nullable();
			$table->double('product_cost',8,2)->nullable();
			$table->double('total_cost',8,2)->nullable();
			$table->double('total_product_cost',8,2)->nullable();
			$table->integer('user_id');
			$table->string('email', 60);
			$table->string('shipping_type', 100)->nullable();
			$table->enum('status', array('0','1'))->default('1')->comment('0=>\'unverified\', 1=>\'verified\'');
			$table->string('created_by')->nullable();
			$table->integer('created_by_id')->default(0);
			$table->primary(['id','created_by_id']);
			$table->integer('role');->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoices');
	}

}
