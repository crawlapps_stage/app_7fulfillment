<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInventoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inventory', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('product_name')->nullable();
			$table->double('price',8,2)->nullable();
			$table->integer('quantity')->unsigned()->nullable();
			$table->integer('weight')->nullable();
			$table->string('sku')->nullable();
			$table->integer('shipping_provider')->nullable();	
			$table->integer('user_id')->nullable();
			$table->string('email')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inventory');
	}

}
