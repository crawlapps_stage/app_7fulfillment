<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use OhMyBrew\ShopifyApp\Traits\ShopModelTrait;
use OhMyBrew\ShopifyApp\Models\Shop as BaseShop;
/**
 * Responsible for reprecenting a shop record.
 */
class Shop extends BaseShop
{
    protected $table = 'shops';

    public function user(){
        return $this->belongsTo(User::class);
    }
}
