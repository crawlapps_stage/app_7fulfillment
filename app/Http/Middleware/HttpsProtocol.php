<?php

namespace App\Http\Middleware;

use Closure;


class HttpsProtocol
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->url() == 'https://7fulfillment.com/portal') {
            return redirect('https://app.7fulfillment.com');
        }
         if (!$request->secure()) {
             echo $url = '';
             return redirect()->secure($url);
         }
        return $next($request);
    }
}
