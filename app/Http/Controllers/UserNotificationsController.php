<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\WalletPayment;
use DB;
use App\usersnotifications;

class UserNotificationsController extends Controller
{
    
	/**
	* Create a new controller instance.
	*
	* @return void
	*/	
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() {
	
		if(Auth::user()->role == 1) {
		    
			$unread_notifications = usersnotifications::where('user_id', Auth::user()->id)->where('read_status', 0)->orderBy('id','desc')->get();
			$read_notifications = usersnotifications::where('user_id', Auth::user()->id)->where('read_status', 1)->orderBy('id','desc')->get();
 			
			return view('user-notification.index',compact('unread_notifications','read_notifications'));
		} 
		else if(Auth::user()->role == 0) {
			return back();
		}
		else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
		else {
			return view('/'); 
		}
	}
	
	public function message ($id) {
	
		$notification = usersnotifications::find($id);
		$time = $notification->created_at;
		if(empty($notification)){
		    return back();
		}
		if((Auth::user()->id) != $notification->user_id){
		    return back();
		}
		
		if($notification->read_status == 0) {
		
			$notification->read_status = 1;
			$notification->created_at = $time;
			$notification->save();
		}
		//print_r($notification);die;
		
		return view('user-notification.message',compact('notification'));	
	}
}
