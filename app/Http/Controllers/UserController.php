<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use Mail;
use App\Mail\forgotEmail;

class UserController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		//$this->middleware('isAdmin');
	}
	
    public function index() {
    
    	if (Auth::check()) {
    	
         	if(Auth::user()->role == 1) {
    		 // Get the currently authenticated user...
	        $user = Auth::user();
	              
	    	return view('profile', compact('user'));
    		
	    	} 
	    	else if(Auth::user()->role == 0) {
	    		return back();
	    		
	    	}
	    	else if(Auth::user()->role == 2) {
        	    return back();
        	    
        	}
	    	else {
	    		return view('/'); 
	        }            
        } 
        else {
        	\Session::flash('message', "Please Login First");
        	return redirect('/');
        }
    	
       
    }

    public function storeProfile(Request $request) {
    //print_r($request->all());die;
    	
	    $rules = array(
		  'fname'     			=> 	'required',
		  'lname'   			=> 	'required',
		  'skype' 			=> 	'required',
		  'phone' 			=> 	'required',
		  'country' 			=> 	'required',
		  'state'     			=> 	'required',
		  'city'    			=> 	'required',
		  'business_name' 		=> 	'required',
		  'business_address' 	        => 	'required',
		  'business_phone' 		=> 	'required',
		  'platform'     		=> 	'required',
		  'storeruntime'    	        => 	'required',
		  'ordersno' 			=> 	'required',
		  'productsno' 			=> 	'required',
		  'ordersperday' 		=> 	'required',
		);

		$validator = Validator::make($request->all(), $rules);
	
	if ($validator->fails()) 
	{
	  $failedRules = $validator->getMessageBag()->toArray();
	  $errorMsg = "";
	 
		if(isset($failedRules['ordersperday']))
	    $errorMsg = $failedRules['ordersperday'][0] . "\n";
	    if(isset($failedRules['productsno']))
	    $errorMsg = $failedRules['productsno'][0] . "\n";
    	if(isset($failedRules['ordersno']))
		$errorMsg = $failedRules['ordersno'][0] . "\n";
		if(isset($failedRules['storeruntime']))
		$errorMsg = $failedRules['storeruntime'][0] . "\n";
		if(isset($failedRules['platform']))
		$errorMsg = $failedRules['platform'][0] . "\n";
	  	if(isset($failedRules['business_phone']))
	    $errorMsg = $failedRules['business_phone'][0] . "\n";
	    if(isset($failedRules['business_address']))
	    $errorMsg = $failedRules['business_address'][0] . "\n";
    	if(isset($failedRules['business_name']))
		$errorMsg = $failedRules['business_name'][0] . "\n";
		if(isset($failedRules['city']))
		$errorMsg = $failedRules['city'][0] . "\n";
		if(isset($failedRules['state']))
		$errorMsg = $failedRules['state'][0] . "\n";
	    if(isset($failedRules['country']))
	    $errorMsg = $failedRules['country'][0] . "\n";
	    if(isset($failedRules['phone']))
	    $errorMsg = $failedRules['phone'][0] . "\n";
    	if(isset($failedRules['skype']))
		$errorMsg = $failedRules['skype'][0] . "\n";
		if(isset($failedRules['lname']))
		$errorMsg = $failedRules['lname'][0] . "\n";
		if(isset($failedRules['fname']))
		$errorMsg = $failedRules['fname'][0] . "\n";	  
	
	  return back()->with('status',$errorMsg."\n") ;
	} 
	else {
		// Get the currently authenticated user...
	        
	        // Get the currently authenticated user's ID...
	        $id = Auth::id();
	        $user = User::find($id);
	
	    	$user->fname = $request->fname;
	    	$user->lname = $request->lname;
	    	$user->email = $request->email;
	    	$user->skype = $request->skype;
	    	$user->phone = $request->phone;
	    	$user->country = $request->country;
	    	$user->state = $request->state;
	    	$user->city = $request->city;
	    	$user->business_name = $request->business_name;
	    	$user->business_address = $request->business_address;
	    	$user->business_phone = $request->business_phone;
	    	$user->platform = $request->platform;
	    	$user->storeruntime = $request->storeruntime;
	    	$user->ordersno = $request->ordersno;
	    	$user->productsno = $request->productsno;
	    	$user->ordersperday = $request->ordersperday;
	    	
	    	if($user->save()) {
	    		return back()->with('status', 'Profile updated!');
	    	}
	        else {
	         return back()->with('status', 'Profile Not updated! PLease try again Later.');
	        }		
	}       

    }
    
    public function changePasswordView() {
    
	if (Auth::check()) {
	
	    	if(Auth::user()->role == 1) {
	    		return view('changePassword');
	    		
	    	} 
	    	else if(Auth::user()->role == 0) {
	    		return back();
	    		
	    	}
	    	else if(Auth::user()->role == 2) {
        	    return back();
        	    
        	}
	    	else {
	    		return view('/'); 
	        }
	} 
	else {
		\Session::flash('message', "Please Login First");
		return redirect('/');
	}               
    	
    }

    public function storechangePassword(Request $request) {      
    
	$rules = array(
	  'cpassword'     => 'required',
	  'npassword'    =>  'required',
	  'confirmpassword' => 'required',
	);
	$validator = Validator::make($request->all(), $rules);
	
	if ($validator->fails()) 
	{
	  $failedRules = $validator->getMessageBag()->toArray();
	  $errorMsg = "";
	 
	  
	    if(isset($failedRules['confirmpassword']))
	    $errorMsg = $failedRules['confirmpassword'][0] . "\n";
		    if(isset($failedRules['npassword']))
		    $errorMsg = $failedRules['npassword'][0] . "\n";
		    	if(isset($failedRules['cpassword']))
	    		$errorMsg = $failedRules['cpassword'][0] . "\n";	  
	
	  return back()->with('status',$errorMsg."\n") ;
	}
	else 
	{
		if($request->cpassword != '' && $request->opassword != '') {
    	    
		    	$opassword = $request->opassword;
	    		$cpassword = $request->cpassword;
    		}

	   	if($request->cpassword != '') {   	
	    	
		    	if(Hash::check($cpassword, $opassword))
			{
				if($request->npassword == '') {
					return back()->with('status', 'Please Fill the New Password!.');
				}
				else {
					if($request->npassword == $request->confirmpassword) {
					
					
				 
				     // Get the currently authenticated user...
				        $user = Auth::user();
				
				        // Get the currently authenticated user's ID...
				        $id = Auth::id();        
				        
				        $user = User::find($id);
				        $user->password = Hash::make($request->npassword);
			
				    	if($user->save()) {
				    		return back()->with('status', 'Password updated!');
				    	}
				        else {
				         return back()->with('status', 'Password Not updated! PLease try again Later.');
				        }
				        }
					else {
					return back()->with('status', 'New Password and Confirm new Password Not matched! PLease try again Later.');
					}
				}
			}
			else
			{
			    return back()->with('status', 'Current Password is wrong!');
			}
		} 
	    	else 
	    	{
	    		return back()->with('status', 'Current Password is Required.!');
	    	}	
	}
    }
    
    public function forgotPassword(Request $request) {
    	if($request->email == '') {
    		return back()->with('status', 'Please enter the Email!');
    	} else {
		$email = $request->email;	    
		$data = array('email'=>$email);	
		Mail::send('forgotMail', $data, function ($m) use ($data) {
			$m->from('notifications@7fulfillment.com', '7fulfillment');
			
			$m->to($data['email'])->subject('Reset your password');
		});	
		return redirect()->route('loginurl')->with('status', 'Password reset link sent to your mail. Check your inbox.');    	
    	}    
    }
    
    public function resetPassword($email) {
        
        $email= \Illuminate\Support\Facades\Crypt::decryptString($email);
    
    	$userDetail = User::select('id','email')->where('email', $email)->first();    	
        if(isset($userDetail) ){
        	$id = $userDetail->id;        	
        	$user = User::find($id);        	
        	return view('resetPassword',compact('userDetail'));        	           
        }else{
            return redirect('/')->with('warning', "Sorry your email cannot be identified.");
        }
        return redirect('/')->with('status', $status);   
    }
    
    public function changeresetPassword(Request $request) {
    
    	$npassword = $request->password;
    	$cpassword = $request->cpassword;    	  
    	
	if($npassword == '') {
		return back()->with('status', 'Please Fill the New Password!.');
	}
	else {
		if($npassword == $cpassword) {
		
			$userDetail = User::select('id')->where('email', $request->email)->first(); 
			 
			$id = $userDetail->id;      
			
			$user = User::find($id);
			
			
			$user->password = Hash::make($npassword);
			
			if($user->save()) {
				return \Redirect::to('/')->with('status','Your Password Recovered. Please login.');
				//return back()->with('status', 'Password Changed!');
			}
			else {
			 return back()->with('status', 'Password Not Changed! PLease try again Later.');
			}
		}
		else {
			return back()->with('status', 'New Password and Confirm new Password Not matched! PLease try again Later.');
		}
	}    
    }
        
}
