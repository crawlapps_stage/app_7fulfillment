<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\Payments;
use App\WiredPayment;
use App\Orders;
use App\WalletPayment;
use App\WalletBalance;
use DB;
use App\Invoice;
use App\RecentActivities;
use App\usersnotifications;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware('isAdmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	if(Auth::user()->role == 0) {
    		return redirect('/admin/home');
    		
    	} 
    	else if(Auth::user()->role == 1) {
    		$id = Auth::user()->id;
    
	    	//WalletController::walletBalanceUpdate();
	    
	    	$balance_history = WalletBalance::select('amount')->where('user_id',$id)->first();
	        // 	$total_points = Invoice::where([['user_id','=', $id],['status','=', 1]])->sum('number_of_parcel');
	   
	        $total_points = Invoice::where('user_id', $id)->where('status', '1')->sum('number_of_parcel');
	    	$total_parcels_shipped = Invoice::where('user_id', $id)->where('status', '1')->sum('number_of_parcel');
	    	
	    	$notifications = usersnotifications::where('user_id', $id)->where('read_status', 0)->count('read_status');
	    	
	    	$activity_messages = [];
	    	$recent_activities = RecentActivities::select('id','message','created_at')->where('user_id',$id)->orderBy('created_at','desc')->limit(10)->get();
        	//$recent_activities = RecentActivities::select('id','message','created_at')->where('user_id',$id)->orderBy()->get()->slice(0,2);
        	
        	if(!empty($recent_activities)) {
        		foreach($recent_activities as $activities) {
        			$activity_messages[] = $activities;
        		}
        	} 
        	//else {
        	//	$activity_messages = 'No Activities.';	
        	//}
        	
    		return view('home',compact('balance_history','total_points','total_parcels_shipped','recent_activities','notifications'));
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return redirect('/subadmin/home');
    	    
    	}
    	else {
    		return view('/'); 
        }
    }
}
