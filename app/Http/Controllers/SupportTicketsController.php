<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use DB;
use App\Quotation;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\Invoice;
use App\TicketCreate;
use App\ChatCreate;
use Mail;

class SupportTicketsController extends Controller
{
    /**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() {
	
	if(Auth::user()->role == 1) {
		return view('support-tickets.index');
		
	} 
	else if(Auth::user()->role == 0) {
		return back();
		
	}
	else if(Auth::user()->role == 2) {
	    return back();
	    
	}
	else {
		return view('/'); 
	}			    
	}

	public function show() {
        
        try
        { 
    		$offset = 0;            
        
        	DB::statement(DB::raw('set @rownumber='.$offset.''));
       		// $invoices = new invoiceCreate();
           
            $support_ticket = DB::table('support_ticket')
            ->where('support_ticket.user_id','=',Auth::user()->id);         
            
            /*$invoices = DB::table('invoices')
                   // ->where(array('invoices.id'=>'5'));
            ->where('id','<>', 0);
            */          
            $support_tickets   =  $support_ticket->orderby('support_ticket.created_at','desc');
            $total      	   =  $support_ticket->count();
            $support_tickets   =  $support_ticket->get();           
            
            return Response::json(array('data'=>  $support_tickets));
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
        }
    }

    public function view($id) 
   {

      if(Auth::user()->role == 1) 
    {
        try
      { 
       
        /*$ticket_details =  DB::table('support_ticket')
        					->where('ticket_id','=',$id)
        					->first();
		*/
        $ticket_details = DB::select('select * from support_ticket where ticket_id = :id AND user_id = :user_id', ['id' => $id,'user_id' => Auth::user()->id]);	

        //print_R($ticket_details);die;				
        
        $count_ticket	=  count($ticket_details);
       
        if(is_array($ticket_details)){
	        if($count_ticket > 0)
	        {
	         
	         $ticket_details = (object)$ticket_details[0];	
	         $chat_details   =  DB::table('support_chat')->where('ticket_id','=',$id)->get();	

	         return view('support-tickets.view-ticket',compact('ticket_details','chat_details'));
	        
	        }else{
	          return redirect("support-tickets/")->with('status-error', 'No such ticket found! Please try again Later.');
	        }
    	}else{
    		return redirect("support-tickets/")->with('status-error', 'No such ticket found! Please try again Later.');
	    }
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
          return (json_encode(array('status',$ex->getMessage()))) ;
      }       
      } 
      else if(Auth::user()->role == 0) 
      {
        return back();
      }
      else if(Auth::user()->role == 2) {
	        return back();
	    
	    }
      else 
      {
        return view('/'); 
      }
    }

    public function store(Request $request) 
  	{
    	$email 			   = 	Auth::user()->email;
    	$user_id 		   = 	Auth::user()->id;

    	$post_data 		   =	$request->all();
    	$message		   =	$post_data['support_message']; 
    	$ticket_id 		   =    $post_data['ticket_id']; 

    	if(Auth::user()->role == 1) 
    	{
	     	if(empty($message)){
	          
	          return back()->with('status-error', 'Please fill the message');

	        }else{
	          
	           	   $reply_by_user   =   0;
	               $reply_by_admin  =   0;

	               if(Auth::user()->role == 1){
	                	$reply_by_user  = 1;
	               }else{
	                	$reply_by_admin = 1;
	               }

	               $ChatCreate                  =   new ChatCreate(); 
	               $ChatCreate->ticket_id       =   $ticket_id;
	               $ChatCreate->message         =   $message;
	               $ChatCreate->reply_by_admin  =   $reply_by_admin;
	               $ChatCreate->reply_by_user   =   $reply_by_user;

	               try{
	                  if($ChatCreate->save()){
	                      return redirect("support-ticket/view/$ticket_id")->with('status', 'Ticket Updated');
	                  }
	               }catch(Exception $exception2){
	                  return back()->with('status-error', $exception2->getMessage());
	               }  
		    }
		}
		else if(Auth::user()->role == 0) 
	    {
	      return back();
	    }
	    else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
      	else 
      	{
          return view('/'); 
        }
	}
}
