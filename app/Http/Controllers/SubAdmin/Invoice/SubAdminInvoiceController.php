<?php

namespace App\Http\Controllers\SubAdmin\Invoice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\WalletBalance;
use DB;

class SubAdminInvoiceController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	/**
	* Displays the index page.
	*
	* @return void
	*/
	
    public function index() {
    
        if(Auth::user()->role == 0) {
            return back();	
        } 
        else if(Auth::user()->role == 1) {
            return back();
        }
        else if(Auth::user()->role == 2) {
            return view('subadmin.invoices.invoice');
        }
        else {
            return view('/'); 
        }
    }
    
    /**
     *  This function shows all the invoices.
     *
     *  @return response
     */

    public function show() 
    {
        try
        {                
            $offset = 0;
            DB::statement(DB::raw('set @rownumber='.$offset.''));
            $invoices = DB::table('invoices')
                    ->join('users', 'invoices.user_id', '=', 'users.id')
                    ->select('invoices.*', 'users.fname', 'users.lname','users.role')
                    ->where('invoices.id','<>', 0)->where('invoices.role','=',2)->where('invoices.created_by_id','=',Auth::user()->id);         
                      
            $invoices   =  $invoices->orderby('invoices.id','desc');
            $total      =  $invoices->count();
            $invoices   =  $invoices->get();
            
            return Response::json(array('data'=>  $invoices));   
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
        }
    }
}

