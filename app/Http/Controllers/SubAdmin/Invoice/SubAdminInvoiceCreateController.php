<?php

namespace App\Http\Controllers\SubAdmin\Invoice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\invoiceCreate;
use DB;
use App\Quotation;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\WalletBalance;
use App\Inventory;
use App\Invoice;
use Mail;
use App\InvoiceProduct;
use App\RecentActivities;
use App\Points;
use App\InventoryActivity;
use App\HandlingFee;
use App\DiscountRate;
use App\ExchangeRate;

class SubAdminInvoiceCreateController extends Controller
{
   /**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() 
	{
		if(Auth::user()->role == 0) {
            return back();
	    } 
	    else if(Auth::user()->role == 1) 
	    {
	    	return back();	    		
	    }
	    else if(Auth::user()->role == 2) {
    	    $allusers       =  DB::table('users')->where('id','<>', 0)->get();
            $user_details   =  array();
            $user_count     =  count($allusers);
			if($user_count > 0){
				foreach($allusers as $key => $user){
                    $walletBalance      =   WalletBalance::where('user_id',$user->id)->first();	        
                    $walletamount       =   $walletBalance['amount'];
                    $user_details[]     =   array('user_id' => $user->id, 'email' => $user->email, 'name' =>$user->fname." ".$user->lname, 'wallet_amount' => $walletamount) ;	        
				}
			}
            $handlingFee = HandlingFee::first();
            $discount = DiscountRate::first();
            $exchange_rate = ExchangeRate::first();
            if(!empty($handlingFee)){
                return view('subadmin.invoices.invoice-create',compact('user_details','handlingFee','discount','exchange_rate'));    
            }

		    return view('subadmin.invoices.invoice-create',compact('user_details'));
    	    
    	}
	    else 
	    {
	    	return view('/'); 
	    }    
	}
    
    public function store(Request $request) 
    {
    if($request->invoice_id) 
    {
        if(empty($request->uemail))
        {
            return back()->with('status-error', 'Please fill all fields value.');
        }
        preg_match('#\((.*?)\)#', $request->uemail, $user_email);
        $request->uemail = $user_email[1];
    }

    $users     =    DB::table('users')->where('email', $request->uemail)->first();
    $c_user_id =    $users->id;
    if($request->shipping_type == 'shipping_invoice') {
        if(empty($request->number_of_parcel) || empty($request->shipping_cost) || empty($request->handling_fee) || empty($request->uemail)) {
            return back()->with('status-error', 'Please fill all fields value.');    
        }
    }
    $count_user = count((array)$users);
    if($count_user < 1) 
    {
        return back()->with('status-error', 'No such user found! Please try again Later.');
    }

    $allproduct = [];
    $sum = 0;
    $quantity=0;
    $price=0;
    $result = 1;
    $result1=[];
    $resuted_price = 0;
    $allproductstock = [];
    $product_arr_to_json= [];
    $product_arr_to_json_edit= [];
    $other_cost = $request->shipping_cost + $request->handling_fee;

    if(isset($request->products) && !empty($request->products)) 
    {
        $allproduct = explode(',', $request->products);
    }

    foreach($allproduct as $productval)
    {        
        foreach($request[$productval] as $val){
            $result = $result * $val;        		  	        		
        }
        $result = $result+$sum;        	
        array_push($result1,$result);        	
        $result=1;
    }
    if(empty($allproduct))
    {
        return back()->with('status-error', 'Unable to create invoice! No any Items Found');
    }

    foreach($allproduct as $productval)
    {        
        foreach($request[$productval] as $key=>$loop) 
        {
            if($key == "'quantity'" && $loop != 0) {
                $allproductstock[] = $productval;
                $product_arr_to_json [] = $request[$productval];
                $product_arr_to_json_edit [] = $request["original_".$productval];		    	
            }
        }	    
    }
    $form_type = $request->form_type;
    $invoice_approved = Invoice::find($request->invoice_id);
    
    for($i=0;$i<(count($allproductstock));  $i=($i+1))
    {
        $product_arr_to_json[$i]['name'] = str_ireplace("_", " ", $allproductstock[$i]);
        $product_arr_to_json_edit [$i]['name'] = str_ireplace("_", " ", $allproductstock[$i]);
        if($product_arr_to_json[$i]['name'] == $product_arr_to_json_edit [$i]['name']) {
            
            if($request->product_invoice_id){
                $on_edit        =   (int)$product_arr_to_json[$i]["'quantity'"];
                //$before_edit    =   (int)$product_arr_to_json_edit[$i]["'quantity'"];
                $before_edit = ($form_type == 'create_invoice') ? 0 : (int)$product_arr_to_json_edit[$i]["'quantity'"];
            //    if($invoice_approved->status == '1') {
                    /*
                    $stock_check    =   0;
                    $inventory_activity                 = new InventoryActivity();
        
                    if($before_edit > $on_edit)
                    {
                        //reduce quantity on edit
                        $stock_check = (int)$before_edit - $on_edit;
                        $inventory_activity->user_id        = $c_user_id;
                        $inventory_activity->product_name   = $product_arr_to_json[$i]['name'];
                        $inventory_activity->activity       = $stock_check." item (s) added to inventory.";
                        $inventory_activity->save();
                    }
                    else if($before_edit < $on_edit)
                    {
                        //increase quantity on edit
                        $stock_check = (int)$on_edit - $before_edit;
                        $inventory_activity->user_id        = $c_user_id;
                        $inventory_activity->product_name   = $product_arr_to_json[$i]['name'];
                        $inventory_activity->activity       = $stock_check." item (s) consumed from inventory.";
                        $inventory_activity->save();
                    }
                    */    
             //   }    
            }    
        }
    }
    $myJSON = json_encode($product_arr_to_json);
    if(empty($product_arr_to_json))
    {
        return back()->with('status-error', 'Unable to create invoice! No any Products selected');
    }

    for($i=0;$i<count($result1);$i++)
    {
        $resuted_price = $resuted_price + $result1[$i];
    }
    if($request->invoice_id) 
    {
        $total_price = $resuted_price + $request->shipping_cost + $request->handling_fee;
        
        $total_price_cost = 0;
        if($total_price > $request->original_total_product_cost) {
            $total_price_cost = $total_price - $request->original_total_product_cost;

        } elseif($total_price < $request->original_total_product_cost) {
            $total_price_cost = $request->original_total_product_cost - $total_price;
        } elseif($total_price == $request->original_total_product_cost) {
            $total_price_cost = 0;
        }

    } else {
        $total_price = $resuted_price + $request->shipping_cost + $request->handling_fee;
    }
    
    $user_id                    =   $users->id;
    $fname                      =   $users->fname;
    $lname                      =   $users->lname;
    $wallet_get                 =   DB::table('wallet_balance')->where('user_id', $user_id)->first();
    /*if(empty($wallet_get))
    {
    return back()->with('status-error', 'Invoice Not Created! User '.$users->fname.' have 0 balance.');
    }*/
    $wallet_balance             =   $wallet_get->amount;
    if($request->invoice_id) 
    {
        $invoice                =   invoiceCreate::find($request->invoice_id);
    } 
    else 
    {
        $invoice                =   new invoiceCreate();
    }

    $invoice->number_of_parcel  =   $request->number_of_parcel;
    $invoice->shipping_cost_cny     =   $request->shipping_cost_cny;
    $invoice->shipping_cost_usd     =   $request->shipping_cost;
    $invoice->handling_fee      =   $request->handling_fee;
    $invoice->total_product_cost=   $total_price;        
    $invoice->user_id           =   $user_id;
    $invoice->email             =   $request->uemail;
    $invoice->shipping_type     =   $request->shipping_type;
    if($request->invoice_id) 
    {
        $invoice->status            =   '1';
    } else {
        $invoice->status            =   '0';
    }
    
    $invoice->created_by        =   Auth::user()->fname.' '.Auth::user()->lname;
    $invoice->created_by_id     =   Auth::user()->id;
    $invoice->role     =   '2';
    $amount_sortage             =   $invoice->total_cost - $wallet_balance;
    /*
    if($invoice->total_product_cost > $wallet_balance)
    {
    return back()->with('status-error', 'Invoice can not be created due to less wallet balance, To create Invoice add balance into wallet.');
    }
    */
    if($invoice->save()) 
    {
       // if($invoice_approved->status == '1') {
            /*
            // RecentActivities functionality
            $recent_activities = new RecentActivities;
            $recent_activities->user_id = $invoice->user_id;
    
            if($request->shipping_type =='sourcing_invoice')
            {
                //  $recent_activities->message = $invoice->number_of_parcel." products were shipped and $".$invoice->total_product_cost." was deducted from your wallet";
                $recent_activities->message = "Product(s) were shipped and $".$invoice->total_product_cost." was deducted from your wallet";
            }
            else 
            {
                $recent_activities->message = $invoice->number_of_parcel." parcels were shipped and $".$invoice->total_product_cost." was deducted from your wallet";   
            }
            $recent_activities->save();
            // End RecentActivities functionality
            */
       // }

        if($request->product_invoice_id) 
        {
            $invoice_product                =   InvoiceProduct::find($request->product_invoice_id);
        } 
        else 
        {
            $invoice_product                =   new InvoiceProduct();
        }

        $invoice_product->invoice_id		=	$invoice->id;
        $invoice_product->product_details	=	$myJSON;

        if($invoice_product->save())
        {
            /*********** Point Activity functionallity ****/

            if($request->invoice_id) 
            {
              //  if($invoice_approved->status == '1') {
                    /*********** Edit Point Activity functionallity on edit Invoice ****/
                    /*
                    if($request->shipping_type !='sourcing_invoice')
                    {
                        $resulted_point = 0;
                        $point_data  = Points::where('invoice_id',$request->invoice_id)->first();
                        $point_id    = $point_data->id;
                        $point      = Points::find($point_id);
    
                        if(empty($point)) 
                        {
                            return back()->with('status-error', 'Server error.Please try agin later.');
                        }
                        else 
                        {
                            if($request['original_number_of_parcel'] > $request['number_of_parcel']) 
                            {
                                $resulted_point         =   $request['original_number_of_parcel'] - $request['number_of_parcel'];
                                $points = new Points();
                                $points->invoice_id     =   $invoice->id;
                                $points->user_id        =   $invoice->user_id;
                                $points->point	        =   $point_data->point - $resulted_point;
                                $points->parcel	        =   $request['number_of_parcel'];
                                $points->message        =   $resulted_point." point (s) were substracted.";
                                $points->save();
                            } 
                            elseif($request['original_number_of_parcel'] < $request['number_of_parcel'])
                            {
                                $resulted_point         =   $request['number_of_parcel'] - $request['original_number_of_parcel'];
                                $points = new Points();
                                $points->invoice_id     =   $invoice->id;
                                $points->user_id        =   $invoice->user_id;
                                $points->point	        =   $point_data->point + $resulted_point;
                                $points->parcel	        =   $request['number_of_parcel'];
                                $points->message        =   $resulted_point." point (s) were added.";
                                $points->save();
                            }
                            elseif($request['original_number_of_parcel'] == $request['number_of_parcel'])
                            {
                                $resulted_point         =   $request['original_number_of_parcel'] - $request['number_of_parcel'];
                                $points = new Points();
                                $points->invoice_id     =   $invoice->id;
                                $points->user_id        =   $invoice->user_id;
                                $points->point	        =   $point_data->point - $resulted_point;
                                $points->parcel	        =   $request['number_of_parcel'];
                                $points->message        =   $resulted_point." point (s) were substracted.";
                            }
                        } 
                    }
                    */
                    
                    /*********** End Edit Point Activity functionallity on edit Invoice ****/
               // }
            } 
            else 
            {
                /*********** Add Point Activity functionallity on Add Invoice ****/
                /*
                if($request->shipping_type !='sourcing_invoice')
                {
                    $points = new Points();
                    $points->invoice_id =   $invoice->id;
                    $points->user_id    =   $invoice->user_id;
                    $points->point      =   $request->number_of_parcel;
                    $points->parcel     =   $request->number_of_parcel;
                    $points->message    =   $request->number_of_parcel." point (s) were added.";
                    $points->save();
                }
                */
                /*********** End Add Point Activity functionallity on Add Invoice **/
            }
            /*********** End Point Activity functionallity ****/
        }

        $inventory_stock            =  array();
        $inventory_stock_count     	=  count($allproductstock);
        $resulted_quantity = 0;

        if($inventory_stock_count > 0)
        {
            foreach($allproductstock as $key => $stock)
            {
                $inventory_stock[]     =   array('product_name' => $stock, 'email' => $request->uemail);
            }
        }

        if($request->product_invoice_id) 
        {
          //  if($invoice_approved->status == '1') {
                /*
                for($i =0 ; $i<count($product_arr_to_json_edit); $i++ )
                {
                    foreach($product_arr_to_json_edit[$i] as $key1 => $value1)
                    {
                        if($key1 == "name") 
                        {
                            $inventory_products	=   Inventory::select('id','quantity')->where('email', $request->uemail)->Where('product_name',$value1)->first();
                            $inventory_update	=   Inventory::find($inventory_products->id);
    
                            if($product_arr_to_json_edit[$i]["'quantity'"] > $product_arr_to_json[$i]["'quantity'"] || $product_arr_to_json_edit[$i]["'quantity'"] == $product_arr_to_json[$i]["'quantity'"]) 
                            {
                                $resulted_quantity = $product_arr_to_json_edit[$i]["'quantity'"] - $product_arr_to_json[$i]["'quantity'"];
                                $inventory_update->quantity	=   $inventory_products->quantity + $resulted_quantity;	
                            } 
                            elseif($product_arr_to_json_edit[$i]["'quantity'"] < $product_arr_to_json[$i]["'quantity'"])
                            {
                                $resulted_quantity = $product_arr_to_json[$i]["'quantity'"] - $product_arr_to_json_edit[$i]["'quantity'"];
                                $inventory_update->quantity	=   $inventory_products->quantity - $resulted_quantity;	
                            }
                            $inventory_update->save();    
                        }
                    }
                }
                */
         //   }
        } 
        else 
        {
            // foreach($product_arr_to_json as $key => $value)
            // {
            //     foreach($value as $key1 => $value1)
            //     {
            //         if($key1 == "name") 
            //         {
            //             $inventory_products	        =   Inventory::select('id','quantity')->where('email', $request->uemail)->Where('product_name',$value1)->first();
            //             $inventory_update	        =   Inventory::find($inventory_products->id);
            //             $inventory_update->quantity	=   $inventory_products->quantity - $value["'quantity'"];
            //             $inventory_update->save();
            //         }    
            //     }
            // }
        }

        $walletBalance = WalletBalance::where('user_id',$user_id)->first();
        if(!empty($walletBalance)) 
        {
            
            /*
            $wallet = WalletBalance::find($walletBalance->id);
            $wallet->amount = $walletBalance->amount - $invoice->total_product_cost;

            //check for wallet minimum amount 
            $row_get                   =   DB::table('walletlimitamount')->where('id','<>', 0)->get();
            $count_row                 =   count($row_get);
            $wallet_minimum_amount     =   ($count_row > 0) ? $row_get[0]->wallet_minimum_amount : 0;//check for minimum balance
            $username                  =   $fname.' '.$lname;

            if($wallet->amount < $wallet_minimum_amount)
            {
            $email    =   $invoice->email; 
            //$email    =   'dineshkashera5@gmail.com';//test amount remove in future
            $data     =   array('email'=>$email,'wallet_amount' => $wallet->amount,'name' => $username,'wallet_minimum_amount' => $wallet_minimum_amount  ); 

            Mail::send('email.walletminimum', $data, function ($m) use ($data) 
            {
            $m->from('notifications@7fulfillment.com', '7fulfillment');
            $m->to($data['email'])->subject('Warning!!! Wallet Balance Low.');
            });
            }
            //wallet minimum amount End

            */

            if($request->product_invoice_id) 
            {
               // if($invoice_approved->status == '1') {
                    /*
                    for($i =0 ; $i<count($product_arr_to_json_edit); $i++ )
                    {
                        foreach($product_arr_to_json_edit[$i] as $key1 => $value1)
                        {
                            if($key1 == "name") 
                            {
                                $inventory_products	=   Inventory::select('id','quantity')->where('email', $request->uemail)->Where('product_name',$value1)->first();
                                $inventory_update	=   Inventory::find($inventory_products->id);
                                $inventory_update->quantity	    =   $inventory_products->quantity;
                                $inventory_update->id           =   $inventory_products->id;
                                //check for minimum inventory
                                $row_get1                       =   DB::table('minimum_inventory')->where('id','<>', 0)->get();
                                $count_row1                     =   count($row_get1);
                                $inventory_minimum_quantity     =   ($count_row1 > 0) ? $row_get1[0]->inventory_minimum_quantity : 0;//check for minimum balance
                                $username                       =   $fname.' '.$lname;
    
                                if($inventory_update->quantity < $inventory_minimum_quantity)
                                {
                                    $email    =   $invoice->email; 
                                    //$email    =   'dineshkashera5@gmail.com';//test amount remove in future
                                    $data     =   array('email'=>$email,'inventory_quantity' => $inventory_update->quantity,'name' => $username,'inventory_minimum_quantity' => $inventory_minimum_quantity  ,'product_name' => $value1,'product_id' => $inventory_update->id); 
    
                                    Mail::send('email.inventoryminimum', $data, function ($m) use ($data) 
                                    {
                                        $m->from('notifications@7fulfillment.com', '7fulfillment');
                                        $m->to($data['email'])->subject('Warning!!! Inventory Low.');
                                    });
                                }
                                //wallet minimum inventory End
                            }
                        }
                    }
                    */
               // }
            } 
            else 
            {
                /*
                foreach($product_arr_to_json as $key => $value)
                {
                    foreach($value as $key1 => $value1)
                    {
                        if($key1 == "name") 
                        {
                            $inventory_products	            =   Inventory::select('id','quantity')->where('email', $request->uemail)->Where('product_name',$value1)->first();
                            $inventory_update	            =   Inventory::find($inventory_products->id);
                            $inventory_update->quantity	    =   $inventory_products->quantity;
                            $inventory_update->id           =   $inventory_products->id;
                            //check for minimum inventory
                            $row_get1                       =   DB::table('minimum_inventory')->where('id','<>', 0)->get();
                            $count_row1                     =   count($row_get1);
                            $inventory_minimum_quantity     =   ($count_row1 > 0) ? $row_get1[0]->inventory_minimum_quantity : 0;//check for minimum balance
                            $username                       =   $fname.' '.$lname;

                            if($inventory_update->quantity < $inventory_minimum_quantity)
                            {
                                $email    =   $invoice->email; 
                                //$email    =   'dineshkashera5@gmail.com';//test amount remove in future
                                $data     =   array('email'=>$email,'inventory_quantity' => $inventory_update->quantity,'name' => $username,'inventory_minimum_quantity' => $inventory_minimum_quantity  ,'product_name' => $value1,'product_id' => $inventory_update->id); 

                                Mail::send('email.inventoryminimum', $data, function ($m) use ($data) 
                                {
                                    $m->from('notifications@7fulfillment.com', '7fulfillment');
                                    $m->to($data['email'])->subject('Warning!!! Inventory Low.');
                                });
                            }
                            //wallet minimum inventory End
                        }
                    }
                }
                */
            }
            // 	$wallet->save();
            if($request->invoice_id) 
            {
                return redirect('/subadmin/invoices')->with('status', 'Invoice Updated Successfully.');
            } 
            else 
            {
                return redirect('/subadmin/invoices/create')->with('status', 'Invoice Created.Pending for admin approval.');
            }    
        } 
    }
    else 
    {
        return back()->with('status-error', 'Invoice Not Created! Please try again Later.');
    }
}

	public function invoiceProduct ($id) 
	{
		$allusers       =  DB::table('users')->where('id','<>', 0)->get();
		$user_details   =  array();
        $user_count     =  count($allusers);
        $user_data = DB::table('users')->select('email')->where('id','=', $id)->first();
        $email = $user_data->email;
		$shipping_type = $_GET['shipping_type'];

		if($user_count > 0)
		{
			foreach($allusers as $key => $user)
			{
			    $walletBalance      =   WalletBalance::where('user_id',$user->id)->first();	        
                $walletamount       =   $walletBalance['amount'];
			    $user_details[]     =   array('user_id' => $user->id, 'email' => $user->email, 'name'=> $user->fname.' '.$user->lname,  'wallet_amount' => $walletamount);	        
			}
		}
        
		$invoice_product = Inventory::select('id','product_name','quantity','email','user_id')->where('user_id',$id)->get();
		$product_details   =  array();
		$product_count     =  count($invoice_product);
	
		if($product_count > 0)
		{
			foreach($invoice_product as $key => $product)
			{
		        $product_details[]     =   array('product_name' => $product->product_name, 'quantity' => $product->quantity);
			}
		}
        $handlingFee = HandlingFee::first();
        $discount = DiscountRate::first();
        $exchange_rate = ExchangeRate::first();
            
        if(!empty($handlingFee)){
            // return view('admin.invoices.invoice-create',compact('product_details','user_details','handlingFee','email','shipping_type'));
            return view('subadmin.invoices.invoice-create',compact('product_details','user_details','handlingFee','email','shipping_type','discount','exchange_rate'));    
        }

		return view('subadmin.invoices.invoice-create',compact('product_details','user_details','email','shipping_type'));
	}
	
	public function edit($id) 
	{
	    if(Auth::user()->role == 0) 
		{
		    return back();    		
	    } 
    	else if(Auth::user()->role == 1) 
    	{
    		return back();
    	}
    	else if(Auth::user()->role == 2) {
    	   try
			{
				$allusers       =  DB::table('users')->where('id','<>', 0)->get();
				$user_details   =  array();
				$user_count     =  count($allusers);
				//$user_data = DB::table('users')->select('email')->where('id','=', $id)->first();
				//$email = $user_data->email;
				
				if($user_count > 0)
				{
					foreach($allusers as $key => $user)
					{
					    $walletBalance      =   WalletBalance::where('user_id',$user->id)->first();	        
					    $walletamount       =   $walletBalance['amount'];
					    //$user_details[]     =   array('user_id' => $user->id, 'email' => $user->email, 'wallet_amount' => $walletamount) ;
					    $user_details[]     =   array('user_id' => $user->id,'email' => $user->email,'wallet_amount' => $walletamount) ;	        
					}
				}
				$invoice = Invoice::find($id);
				$first_name = User::select('fname')->where('id',$invoice->user_id)->first();
				$last_name = User::select('lname')->where('id',$invoice->user_id)->first();
				if(empty($invoice)) 
				{
					return back()->with('status', 'No such invoice found! PLease try again Later.');
				}
				
				$invoice_product = InvoiceProduct::where('invoice_id', $invoice->id)->first();
				
				if(empty($invoice_product)) 
				{
					return back()->with('status', 'No such invoice product found! PLease try again Later.');
				}
				
				$product_details_json = $invoice_product->product_details;
				$product_details_arr = json_decode($product_details_json, true);
				
				$inventory_invoice_product = Inventory::select('id','product_name','quantity','email','user_id')->where('user_id',$invoice->user_id)->get();
				
				for($i=0;$i<(count($inventory_invoice_product));  $i=($i+1))
				{
					for($j=0;$j<(count($product_details_arr));  $j=($j+1))
					{
						if($inventory_invoice_product[$i]['product_name'] == $product_details_arr[$j]['name']) 
						{
							$product_details_arr[$j]['original_quantity'] = $inventory_invoice_product[$i]['quantity'];
						}	
					}	
				}
				
				$product_details   =  array();
				$product_count     =  count($inventory_invoice_product);
			
				if($product_count > 0)
				{
					foreach($inventory_invoice_product as $key => $product)
					{
					    $product_details[]     =   array('product_name' => $product->product_name, 'quantity' => $product->quantity);   	
					}
				}
                $handlingFee = HandlingFee::first();
                $discount = DiscountRate::first();
                $exchange_rate = ExchangeRate::first();
                    
                if(!empty($handlingFee)){
                    return view('subadmin.invoices.invoice-edit',compact('handlingFee','invoice','invoice_product','product_details_arr','first_name','last_name','discount','exchange_rate'));    
                }
				return view('subadmin.invoices.invoice-edit',compact('invoice','invoice_product','product_details_arr','first_name','last_name'));			
			}
			catch(\Illuminate\Database\QueryException $ex)
			{
			    return (json_encode(array('status',$ex->getMessage()))) ;
			}
    	    
    	}
    	else 
    	{
    		return view('/'); 
        }
	}
	
	public function view($id) 
	{
	    
	    if(Auth::user()->role == 0) 
		{
		     return back();  		
	    } 
	    else if(Auth::user()->role == 1) 
	    {
	    	return back();
	    }
	    else if(Auth::user()->role == 2) {
    	    try
			{
				// Fetching Invoice details of an invoice with id $id
				$invoice = Invoice::find($id);
				
				if(empty($invoice)) 
				{
					return back()->with('status', 'No such invoice found! PLease try again Later.');
				}
				$first_name = User::select('fname')->where('id',$invoice->user_id)->first();
				$last_name = User::select('lname')->where('id',$invoice->user_id)->first();
				
				// Fetching Invoice Product details of an invoice with id $invoice->id
				$invoice_product = InvoiceProduct::where('invoice_id', $invoice->id)->first();
				if(empty($invoice_product)) 
				{
					return back()->with('status', 'No such invoice product found! PLease try again Later.');
				}
				
				$product_details_json = $invoice_product->product_details;
				
				// Converting product details json to array
				$product_details_arr = json_decode($product_details_json, true);
				return view('subadmin.invoices.invoice-view',compact('invoice','invoice_product','product_details_arr','first_name','last_name'));			
			}
			catch(\Illuminate\Database\QueryException $ex)
			{
			    return (json_encode(array('status',$ex->getMessage()))) ;
			}   
    	}
	    else 
	    {
	    	return view('/'); 
	    }
    }
}
