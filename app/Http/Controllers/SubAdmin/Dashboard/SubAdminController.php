<?php

namespace App\Http\Controllers\SubAdmin\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SubAdminController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
     
    public function index()
    {
        
    	if(Auth::user()->role == 2) {
    	    return view('subadmin.subadminhome');	
    	} 
    	else if(Auth::user()->role == 1) {
    		return redirect('/home');
    		
    	}
    	else if(Auth::user()->role == 0) {
    	    return view('admin.home');	
    	}
    	else {
    		return view('/'); 
        }
    }
}
