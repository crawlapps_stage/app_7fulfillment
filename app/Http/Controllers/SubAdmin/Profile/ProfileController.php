<?php

namespace App\Http\Controllers\SubAdmin\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;

class ProfileController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
    public function index() {
       	
       	if(Auth::user()->role == 0) {
    		return back();	
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();	
    	}
    	else if(Auth::user()->role == 2) {
    	    $user = Auth::user();              
    		return view('subadmin.Profile.profile', compact('user'));   
    	}
    	else {
    		return view('/'); 
        } 
        
    }
    
    public function storeProfile(Request $request) {
        // Get the currently authenticated user...
        
        // Get the currently authenticated user's ID...
        $id = Auth::id();
        $user = User::find($id);

    	$user->fname                =   $request->fname;
    	$user->lname                =   $request->lname;
    	$user->email                =   $request->email;
    // 	$user->skype                =   $request->skype;
    // 	$user->phone                =   $request->phone;
    // 	$user->country              =   $request->country;
    //     $user->state 				= 	$request->state;
    // 	$user->city 				= 	$request->city;
    // 	$user->business_name        = 	$request->business_name;
    // 	$user->business_address 	= 	$request->business_address;
    // 	$user->business_phone 		= 	$request->business_phone;
    // 	$user->platform 			= 	$request->platform;
    // 	$user->storeruntime 		= 	$request->storeruntime;
    // 	$user->ordersno 			= 	$request->ordersno;
    // 	$user->productsno 			= 	$request->productsno;
    // 	$user->ordersperday 		= 	$request->ordersperday;
    	if($user->save()) {
    		return back()->with('status', 'Profile updated!');
    	}
        else {
         return back()->with('status', 'Profile Not updated! PLease try again Later.');
        }

    }
    
    public function changePasswordView() {
    
    if(Auth::user()->role == 0) {
    		return back();	
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return view('subadmin.Profile.changePassword');   
    	}
    	else {
    		return view('/'); 
        }	
    }

    public function storechangePassword(Request $request) {
    
    	if($request->cpassword != '' && $request->opassword != '') {
    	    
	    	$opassword = $request->opassword;
	    	$cpassword = $request->cpassword;
    	}
    	
    	if($request->cpassword != '') {
		if(Hash::check($cpassword, $opassword))
		{
			if($request->npassword == '') {
				return back()->with('status', 'Please Fill the New Password!.');
			}
			else {	
				if($request->npassword == $request->confirmpassword) {
							 
				     	// Get the currently authenticated user...
				        $user = Auth::user();
				
				        // Get the currently authenticated user's ID...
				        $id = Auth::id();        
				        
				        $user = User::find($id);
				
				    	
				    	$user->password = Hash::make($request->npassword);
			
				    	if($user->save()) {
				    		return back()->with('status', 'Password updated!');
				    	}
				        else {
				        	return back()->with('status', 'Password Not updated! PLease try again Later.');
				        }    
			        }
				        
			else {
			return back()->with('status', 'New Password and Confirm New Password Not matched! PLease try again Later.');
			}
			}
		}
		else
		{
			return back()->with('status', 'Current Password is wrong!');
		}
    	} 
    	else 
    	{
    		return back()->with('status', 'Current Password is Required.!');
    	}   	
    	
    	
    }
}
