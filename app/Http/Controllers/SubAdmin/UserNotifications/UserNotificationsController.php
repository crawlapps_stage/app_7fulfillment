<?php

namespace App\Http\Controllers\SubAdmin\UserNotifications;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Quotation;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Mail;
use App\usersnotifications;


class UserNotificationsController extends Controller
{
    
    /**
	* Create a user notification
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() {
    
		if(Auth::user()->role == 0) {
		    	return back();
	    	} 
	    	else if(Auth::user()->role == 1) {
	    		return back();	    		
	    	}
	    	else if(Auth::user()->role == 2) {
                $allusers       =  DB::table('users')->where('id','<>', 0)->get();
                $user_details   =  array();
                $user_count     =  count($allusers);
                
                if($user_count > 0){
                    foreach($allusers as $key => $user){
                        $user_details[]     =   array('user_id' => $user->id, 'email' => $user->email, 'name' =>$user->fname." ".$user->lname) ;	        
                    }
                }
                return view('subadmin.UserNotifications.usernotification-create',compact('user_details'));
        	}
	    	else {
	    		return view('/'); 
	        }    
	}

	public function store(Request $request) { 

		$posted_data 		=  	$request->all();
		$all_users   		= 	isset($posted_data['uemail']) ? $posted_data['uemail'] : array();
		$users_count		=	count($all_users);
		$user_messages   	=  	$posted_data['user_messages'];

		if($users_count < 1){
			return back()->with('status-error', 'Select atleast one user.'); 
		}elseif(empty($user_messages)){
			return back()->with('status-error', 'Message field can\'t be empty!');  
		} else {

			//create usernotification model object
			
			if(in_array('all_users',$all_users)){
				$all_users       	=  DB::table('users')->where('id','<>', 0)->get();
				$users_count      	=  count($all_users);

				if($users_count > 0){
					foreach($all_users as $key => $single_user){
						
						$UserNotifications 	=  new usersnotifications();
						$UserNotifications->user_id 		= $single_user->id;
	    				$UserNotifications->read_status 	= 0;
	    				$UserNotifications->messages 		= $user_messages;
	    				$UserNotifications->save();
					}
				}
	    		
			}else{
				$users_count      	=  count($all_users);
				if($users_count > 0){
					foreach($all_users as $key => $single_user){
						$getData 		= 	explode(",",$single_user);

						$user_email 	=	$getData[0];
						$user_id 		=	$getData[1];

						$UserNotifications 	=  new usersnotifications();
						$UserNotifications->user_id 		= $user_id;
	    				$UserNotifications->read_status 	= 0;
	    				$UserNotifications->messages 		= $user_messages;
	    				$UserNotifications->save();
					}
				}		
				
			}
			return redirect('/subadmin/usernotifications')->with('status', 'User \'s Notifications Send Successfully.');
		}
	}
}
