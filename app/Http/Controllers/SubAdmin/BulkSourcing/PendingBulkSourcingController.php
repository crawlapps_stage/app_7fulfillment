<?php

namespace App\Http\Controllers\SubAdmin\BulkSourcing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\WalletPayment;
use App\WalletBalance;
use DB;
use Mail;
use App\RecentActivities;
use App\BulkSourcing;

use App\Http\Controllers\Controller;

class PendingBulkSourcingController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
    public function index() {
    	
    	if(Auth::user()->role == 0) {
    		return back();
    		
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return view('subadmin.BulkSourcing.pending-BulkSourcing');
    	    
    	}
    	else {
    		return view('/'); 
        }
    }
    
    public function show() 
    {
    	try
    	{	
    		$offset = 0;		
    		DB::statement(DB::raw('set @rownumber='.$offset.''));
    		
    		$bulkSourcing = new BulkSourcing();				
    		
    		$bulkSourcing = $bulkSourcing
    		          ->join('users', 'bulk_sourcing.user_id', '=', 'users.id')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'bulk_sourcing.id','bulk_sourcing.q_link','bulk_sourcing.pieces','bulk_sourcing.shipping_location','bulk_sourcing.time_frame','bulk_sourcing.quote','bulk_sourcing.status','bulk_sourcing.quote_approve','bulk_sourcing.user_id','bulk_sourcing.created_at','users.fname','users.lname','users.email')	          
    		           ->where(array('bulk_sourcing.status'=>'0'));
    		
    		$bulkSourcing = $bulkSourcing->orderby('bulk_sourcing.created_at','desc');
    		$total = $bulkSourcing->count();
    		
    		$bulkSourcing = $bulkSourcing->get();
    		
    		return Response::json(array('data'=> $bulkSourcing));
    	       
    	}
    	catch(\Illuminate\Database\QueryException $ex)
    	{
    		return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
    	}
    }
    
    public function approveRequest(Request $request) {
    		
	try
	{
		if($request->ajax())
		{
			if($request->id){
				
				$photography = Photography::where('id',$request->id)->first();
				
				if(!empty($photography)) {
					
					$photography = Photography::find($request->id);
					
					$photography->status = 1;		
					
					if($photography->save()) {
						
					    //Email code start
					    $user  = User::where('id',$photography->user_id)->first();
		    			$customer_email =  $user->email;
		    			$cmailbody 		=  'Your Request Phototgraphy has been approved.';

		    			//$getadmin 		=  	DB::table('users')->where('id','=',$walletPayment->user_id)->first();
		    			//$getadmin		=	(array)$getadmin;
				        // 	$customerName		=  	$getadmin['fname'].' '.$getadmin['lname'];
			        	$customerName		=  	$user->fname.' '.$user->lname;
			        	
			        	$balance_id = WalletBalance::select('id','amount')->where('user_id',$photography->user_id)->get()->first();
						
						if(!empty($balance_id)) {
		        	
			        			$balance_amount = $balance_id->amount;		        	
				        }

		    			$data     		=   array(
	    									'customer_email'	=>	$customer_email,
	    									'customer_name' 	=> 	$customerName,
	    									'mail_body'		    =>	$cmailbody,
	    									'wallet_amount'		=>	$balance_amount
		    							); 

		    			//send mail to customers
		    	 		Mail::send('email.walletapprove', $data, function ($m) use ($data) {
	                        $m->from('notifications@7fulfillment.com', '7fulfillment');
	                        $m->to($data['customer_email'])->subject('Request Phototgraphy has been approved on 7fulfillment');
	                    });
		    	 		//Email code end

						return ('Request Phototgraphy Approved successfully');
						
					} else {
						return ('Request Phototgraphy Not Approved. Please try again Later');
					}									
				}				   				  
			}
			return ('No such record found');
		}
	}
	catch(\Illuminate\Database\QueryException $ex)
	{
	return (json_encode(array('status',$ex->getMessage()))) ;
	}
    }
    
    public function makeQuote($id) 
	{
	    if(Auth::user()->role == 0) 
		{
		    return back();    		
	    } 
	    else if(Auth::user()->role == 1) 
	    {
	    	return back();
	    }
	    else if(Auth::user()->role == 2) {
    	    try
			{
				// Fetching Invoice details of an invoice with id $id
				$bulkSourcing = BulkSourcing::find($id);
				
				if(empty($bulkSourcing)) 
				{
					return back()->with('status', 'No such bulkSourcing found! PLease try again Later.');
				}
				$first_name = User::select('fname')->where('id',$bulkSourcing->user_id)->first();
				$last_name = User::select('lname')->where('id',$bulkSourcing->user_id)->first();
				
				return view('subadmin.BulkSourcing.BulkSourcing-view',compact('bulkSourcing','first_name','last_name'));			
			}
			catch(\Illuminate\Database\QueryException $ex)
			{
			    return (json_encode(array('status',$ex->getMessage()))) ;
			}
    	    
    	}
	    else 
	    {
	    	return view('/'); 
	    }
    }
    
    public function makeQuoteStore(Request $request) {
        
        $rules = [
            'quote'  =>  'required|numeric',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            
            return back()->withErrors($validator)->withInput();
        }
        
        if($request->quote == '' ) {
            return back()->with('status','Please fill all the Quote Field.');
        }
        
        $bulkSourcing = BulkSourcing::find($request->id);
        
        
        $bulkSourcing->quote = $request->quote;
        $bulkSourcing->status = 1;
        
        if($bulkSourcing->save()) {
            return back()->with('status','Quote successfully saved!');    
        } else {
            return back()->with('status','Quote not saved. Try again later.');
        }
        
    }
}
