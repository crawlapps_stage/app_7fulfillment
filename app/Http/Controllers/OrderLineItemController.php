<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use DB;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\Shop;
use App\Order;
use App\LineItem;



use App\WalletBalance;
use Mail;
use App\RecentActivities;
use App\Points;


class OrderLineItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    public function index(Request $request, Order $order)
    {

        $id = $order->id;

        //dd($shipping, $handling,$order);
        if(Auth::user()->role == 1)
        {
            return view('orders.details',compact('id'));
        }
        else if(Auth::user()->role == 0)
        {
            return back();
        }
        else if(Auth::user()->role == 2)
        {
            return back();
        }
        else {
            return view('/');
        }
    }

    public function show(Request $request, Order $order)
    {
        try
        {


            $offset = 0;
            DB::statement(DB::raw('set @rownumber='.$offset.''));
            //$lineItem = LineItem::where('order_id',$order->id)->get();
            $id = Auth::user()->id;

            $shop = Shop::where('user_id',$id)->first();

           $lineItem = Order::with('line_item')->where('id',$order->id)->where('shop_id','=',$shop->id)->orderBy('order_date','desc')->first()->toArray();

            if(!$lineItem){

                return 'no orders';
            }

            $handling = \App\HandlingFee::first()->toArray();
            $exchangeRate = \App\ExchangeRate::first()->toArray();

            $shipping = \App\ShippingProvider::get()->toArray();
            $shipping = array_column($shipping,'name','id');

            $country = @$order->country()->first()->id;
            $shippingRate = \App\ShippingRates::where('country_id',$country)->get(['id','provider_id', 'cny_per_piece', 'cny_per_gram'])->toArray();
            foreach($shippingRate as $key  => $val){
                $rates[$val['provider_id']] = $val;
            }

            $ships = [];
            $totalweight = $totalpiece = $totalgram = $total_shipping = $product_fee = $num_percel = 0;
            $final_rates = $data = [];

            foreach($lineItem['line_item'] as $key => $val){

                $temp = [];
                $temp['id'] = $val['id'];
                $temp['sku'] = @$val['sku']?$val['sku']:"---";
                $temp['product_name'] = $val['product_title'];
                $temp['product_price'] = @$val['product_price']?"$".$val['product_price']:"---";
                $temp['qty'] = $val['qty'];
                $temp['weight'] = @$val['weight']?$val['weight']:"---";
                $temp['shipping_provider'] = @$shipping[$val['shipping_provider']]?$shipping[$val['shipping_provider']]:"---";
                $temp['shipping_provider_id'] = @$val['shipping_provider']?$val['shipping_provider']:null;
                $temp['status'] = $val['status'];
                $data[] = $temp;
                if(@array_key_exists($val['shipping_provider'], $rates)){
                    $rate = $rates[$val['shipping_provider']];
                    $final_rates[$val['shipping_provider']] = $val['weight'] * $val['qty'];
                    $total_shipping += ($val['weight'] * $val['qty']) + $rate['cny_per_piece'] + $rate['cny_per_gram'];
                    $totalweight += $val['weight'] * $val['qty'];
                    $totalgram = $rate['cny_per_gram'];
                    $totalpiece = $rate['cny_per_piece'];


                    $ships[$val['shipping_provider']]['cny_per_piece'] = $rate['cny_per_piece'];
                    $ships[$val['shipping_provider']]['cny_per_gram'] = $rate['cny_per_gram'];
                    @$ships[$val['shipping_provider']]['weight_qty'] += $val['weight'] * $val['qty'];

                    if(count($final_rates) == 1) {
                        $num_percel += $val['weight'] * $val['qty'];
                    }else{
                        $num_percel = count($final_rates);
                    }
                    //$num_percel += $val['weight'] * $val['qty'];
                    if(@$val['product_price']){
                        $product_fee += $val['product_price'] * $val['qty'];
                    }
                }

            }

            $num_percel = $final_parcel = 0;
            $t_shipping = [];
            foreach($ships as $key => $val){

                $final_parcel = ceil($val['weight_qty'] / 2000);
                $t_shipping[$key] = ($val['weight_qty'] * $val['cny_per_gram']) + $final_parcel * $val['cny_per_piece'];
                $num_percel += $final_parcel;
            }
            array_sum($t_shipping);
            //$t_shipping = $t_shipping * $exchangeRate['usd_value'];
            $t_shipping = array_sum($t_shipping);
            /*if(count($final_rates) > 1 && array_sum($final_rates) >= 2000){
                $num_percel = array_sum($final_rates);
            }

            if($num_percel >= 2000){
                $num_percel = ceil($num_percel / 2000);
            }else{
                $num_percel = count($final_rates);
            }
            //dd($totalweight, $totalgram, $num_percel, $totalpiece);
            if(count($final_rates) == 1 && $num_percel > 1){
                $total_shipping = ($totalweight * $totalgram) + $num_percel * $totalpiece;
            }*/
            //dd(count($final_rates),$num_percel,$total_shipping);


            
            $shipping_status = $order->shipping_status;

            if($shipping_status == 'Paid'){
                
                $number_of_parcel   =   $order->number_of_parcel;
                $shipping_cost      =   $order->shipping_cost;
                $product_fee        =   $order->product_fee;
                $handling_fee       =   $order->handling_fee;
                $total_cost         =   $order->total_cost;

                $prepare = [
                    'data'=>  $data,
                    'num_parcel' => ceil($number_of_parcel),
                    'product_fee' => "$".$product_fee,
                    'total_shipping' => "$".number_format((float)$shipping_cost,2,".",""),
                    'handling_fee' => ($product_fee > 0)?"$".number_format((float)$handling_fee,2,".","") :"$"."0",
                    'total_cost' => ($product_fee > 0)?"$".number_format((float)$total_cost,2,".",""):"$"."0",
                    'button_hide' => 'yes',
                ];
            }
            else{

                $total_shipping = $t_shipping * $exchangeRate['usd_value'];
                $totalCost = $total_shipping + $product_fee;
                $total = $totalCost + ($num_percel * $handling['handling_fee']);
                $handling_fee = $num_percel * $handling['handling_fee'];
                $prepare = [
                    'data'=>  $data,
                    'num_parcel' => ceil($num_percel),
                    'product_fee' => "$".$product_fee,
                    'total_shipping' => "$".number_format((float)$total_shipping,2,".",""),
                    'handling_fee' => ($product_fee > 0)?"$".number_format((float)$handling_fee,2,".","") :"$"."0",
                    'total_cost' => ($product_fee > 0)?"$".number_format((float)$total,2,".",""):"$"."0",
                    'button_hide' => 'no',
                ];
            }

            return Response::json($prepare);
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
        }
    }


     public function payShipping(Request $request, Order $order)
    {

         $postdata               =   $request->all();
         $is_actual_pay_now      =   $postdata['paynow'];

        if(isset($is_actual_pay_now)){
            try
            {
                $id = Auth::user()->id;
                $shop = Shop::where('user_id',$id)->first();

                $lineItem = Order::with('line_item')->where('id',$order->id)->where('shop_id','=',$shop->id)->orderBy('order_date','desc')->first()->toArray();

                if(!$lineItem){
                    return 'no orders';
                }

                $handling = \App\HandlingFee::first()->toArray();
                $exchangeRate = \App\ExchangeRate::first()->toArray();

                $shipping = \App\ShippingProvider::get()->toArray();
                $shipping = array_column($shipping,'name','id');

                $country = @$order->country()->first()->id;
                $shippingRate = \App\ShippingRates::where('country_id',$country)->get(['id','provider_id', 'cny_per_piece', 'cny_per_gram'])->toArray();
                foreach($shippingRate as $key  => $val){
                    $rates[$val['provider_id']] = $val;
                }

                $ships = [];
                $totalweight = $totalpiece = $totalgram = $total_shipping = $product_fee = $num_percel = 0;
                $final_rates = $data = [];
                foreach($lineItem['line_item'] as $key => $val){
                    $temp = [];
                    $temp['id'] = $val['id'];
                    $temp['sku'] = @$val['sku']?$val['sku']:"---";
                    $temp['product_name'] = $val['product_title'];
                    $temp['product_price'] = @$val['product_price']?"$".$val['product_price']:"---";
                    $temp['qty'] = $val['qty'];
                    $temp['weight'] = @$val['weight']?$val['weight']:"---";
                    $temp['shipping_provider'] = @$shipping[$val['shipping_provider']]?$shipping[$val['shipping_provider']]:"---";
                    $temp['shipping_provider_id'] = @$val['shipping_provider']?$val['shipping_provider']:null;
                    $temp['status'] = $val['status'];
                    $data[] = $temp;
                    if(@array_key_exists($val['shipping_provider'], $rates)){
                        $rate = $rates[$val['shipping_provider']];
                        $final_rates[$val['shipping_provider']] = $val['weight'] * $val['qty'];
                        $total_shipping += ($val['weight'] * $val['qty']) + $rate['cny_per_piece'] + $rate['cny_per_gram'];
                        $totalweight += $val['weight'] * $val['qty'];
                        $totalgram = $rate['cny_per_gram'];
                        $totalpiece = $rate['cny_per_piece'];


                        $ships[$val['shipping_provider']]['cny_per_piece'] = $rate['cny_per_piece'];
                        $ships[$val['shipping_provider']]['cny_per_gram'] = $rate['cny_per_gram'];
                        @$ships[$val['shipping_provider']]['weight_qty'] += $val['weight'] * $val['qty'];

                        if(count($final_rates) == 1) {
                            $num_percel += $val['weight'] * $val['qty'];
                        }else{
                            $num_percel = count($final_rates);
                        }
                        //$num_percel += $val['weight'] * $val['qty'];
                        if(@$val['product_price']){
                            $product_fee += $val['product_price'] * $val['qty'];
                        }
                    }

                }

                $num_percel = $final_parcel = 0;
                $t_shipping = [];
                foreach($ships as $key => $val){

                    $final_parcel = ceil($val['weight_qty'] / 2000);
                    $t_shipping[$key] = ($val['weight_qty'] * $val['cny_per_gram']) + $final_parcel * $val['cny_per_piece'];
                    $num_percel += $final_parcel;
                }
                array_sum($t_shipping);
                $t_shipping = array_sum($t_shipping);
               

                $total_shipping = $t_shipping * $exchangeRate['usd_value'];
                $totalCost = $total_shipping + $product_fee;
                $total = $totalCost + ($num_percel * $handling['handling_fee']);
                $handling_fee = $num_percel * $handling['handling_fee'];

                $num_of_parcel = (int)$num_percel;

                /*echo $num_percel.'</br>';
                echo $total_shipping.'</br>';
                echo $product_fee.'</br>';
                echo $handling_fee.'</br>';
                echo $total.'</br>';*/
                
                //save into order tables
                DB::table('orders')
                ->where('id', $order->id)
                ->update(['number_of_parcel' => $num_of_parcel,'shipping_cost' => $total_shipping,'product_fee' => $product_fee,'handling_fee' => $handling_fee, 'total_cost' => $total, 'shipping_status' => 'Paid' ]);


                //update wallet balance
                $user_id = Auth::user()->id;

                $walletBalance = WalletBalance::where('user_id',$user_id)->first();
                if(!empty($walletBalance)) 
                {
                    $user_data = User::select('fname','lname')->where('id',$user_id)->first();
                    
                    $wallet = WalletBalance::find($walletBalance->id);
                    $wallet->amount = $walletBalance->amount - $total;
                    
                    //check for wallet minimum amount 
                    $row_get                   =   DB::table('walletlimitamount')->where('id','<>', 0)->get();
                    $count_row                 =   count($row_get);
                    $wallet_minimum_amount     =   ($count_row > 0) ? $row_get[0]->wallet_minimum_amount : 0;//check for minimum balance
                    $username                  =   $user_data->fname.' '.$user_data->lname;
                    
                    if($wallet->amount < $wallet_minimum_amount)
                    {
                        $email    =   Auth::user()->email; 
                        //$email    =   'dineshkashera5@gmail.com';//test amount remove in future
                        $data     =   array('email'=>$email,'wallet_amount' => $wallet->amount,'name' => $username,'wallet_minimum_amount' => $wallet_minimum_amount  ); 
                    
                        Mail::send('email.walletminimum', $data, function ($m) use ($data) 
                        {
                            $m->from('notifications@7fulfillment.com', '7fulfillment');
                            $m->to($data['email'])->subject('Warning!!! Wallet Balance Low.');
                        });
                    }
                    //wallet minimum amount End
                    $wallet->save();
                    
                }
            return redirect('/order-details/'.$order->id)->with('status','Order Updated Successfully.');

            }
            catch(\Illuminate\Database\QueryException $ex)
            {
                return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
            }
        }
    }

}
