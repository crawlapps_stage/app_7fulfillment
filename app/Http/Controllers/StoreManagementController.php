<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\storemanagement;
use Illuminate\Support\Facades\Auth;

class StoreManagementController extends Controller
{

	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function storeManagement() {
	    
		if(Auth::user()->role == 1) {
			return view('store.StoreManagement');
			
		} 
		else if(Auth::user()->role == 0) {
			return back();
			
		}
		else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
		else {
			return view('/'); 
		}
	
	}
	    
	public function storeShopurl(Request $request) {
		$id = Auth::id();
		echo $id;die;
		$shop = storemanagement::find($id);
		
		$shop->storename = $request->storename;
		$shop->lname = $request->lname;
		
		if($shop->save()) {
			return back()->with('status', 'Your sote inserted!');
		}
		else {
		 return back()->with('status', 'Your sote inserted Not updated! PLease try again Later.');
		}
	
	}
}

