<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;
use Mail;
use App\Mail\verifyEmail;
use App\Product;
use Session;
use Illuminate\Http\Request;
use App\WalletBalance;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

/*
    protected function validator(array $data)
    {
    
        return Validator::make($data, [
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],         
            'email' => ['required', 'unique:users'],
            'skype' => ['required'],
            'phone' => ['required','numeric'],
            'country' => ['required'],            
            'password' => ['required', 'string', 'confirmed'],
            'g-recaptcha-response' => ['required','captcha'],
            'agree' => ['required'],
        ]);
    }
*/

   protected function validator(array $data)
    { 
        return Validator::make($data, [
            'platform' => ['required'],
            'storeruntime' => ['required'],         
            'ordersno' => ['required'],
            'productsno' => ['required'],
            'ordersperday' => ['required'],
            //'g-recaptcha-response' => ['required','captcha'],            
            'agree' => ['required'],
            
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

/*
    protected function create(array $data)
    {
    	
        $user = User::create([
            'fname' => $data['fname'],
            'lname' => $data['lname'],           
            'email' => $data['email'],
            'skype' => $data['skype'],
            'phone' => $data['phone'],
            'country' => $data['country'],
            'password' => Hash::make($data['password']),
            'verifyToken' => Str::random(40),
        ]);
        
        return $user;
    }

*/

protected function create(array $data)
    {
        // $user = User::firstOrCreate(['email' => $data['email']],
        // [$data['fname'],$data['lname'], $data['skype'],$data['phone'],$data['country'],Hash::make($data['password']),$data['state'],$data['city'],$data['business_name'],$data['business_address'],$data['business_phone'],$data['platform'],$data['storeruntime'],$data['ordersno'],$data['productsno'],$data['ordersperday'],Str::random(40),]);
        
            
        $user = User::create([
            'fname'             =>  $data['fname'],
            'lname'             =>  $data['lname'],           
            'email'             =>  $data['email'],
            'skype'             =>  $data['skype'],
            'phone'             =>  $data['phone'],
            'country'           =>  $data['country'],
            'password'          =>  Hash::make($data['password']),
            'state'             =>  $data['state'],
            'city'              =>  $data['city'],
            'business_name'     =>  $data['business_name'],
            'business_address'  =>  $data['business_address'],
            'business_phone'    =>  $data['business_phone'],
            'platform'          =>  $data['platform'],
            'storeruntime'      =>  $data['storeruntime'],
            'ordersno'          =>  $data['ordersno'],
            'productsno'        =>  $data['productsno'],
            'ordersperday'      =>  $data['ordersperday'],
            'verifyToken'       =>  Str::random(40),                
        ]);
        
        $balance = new WalletBalance;
		$balance->user_id = $user->id;
		$balance->amount = 0;
		$balance->status = '1';
		$balance->save();
	
       
       /* 	
        $balance_id = WalletBalance::select('id','amount')->where('id',$lastInsertId())->get()->first();
	if(!empty($balance_id)) {

		$balance_amount = $balance_id->amount;

	} else {
	
		$balance_amount = 0;		        	
	}			        
	
	if(!empty($balance_id)) {
		$balance_amount_id = $balance_id['id'];
		$balance = WalletBalance::find($balance_amount_id);
		
		$balance->user_id = $id;
        	$balance->amount = $balance_amount;
        	$balance->status = '1';
        	$balance->save();			        		
	
	} else {
	
		$balance = new WalletBalance;
        	$balance->user_id = $id;
        	$balance->amount = $balance_amount;
        	$balance->status = '1';
        	$balance->save();
	
	}
	*/
        
        return $user;
    }
    
    public function html_email(array $data) {
    
    $name = $data['fname'];
    $email = $data['email'];
    
      $data = array('name'=>$name ,'email'=>$email);
           
      Mail::send('mail', $data, function ($m) use ($data) {
            $m->from('notifications@7fulfillment.com', '7fulfillment');

            $m->to($data['email'], $data['name'])->subject('Confirm Your account in order to activate.!');
        });
      
      return redirect()->route('loginurl')->with('status', 'Thanks very much for taking the time to fill all the fields, we value your business and we will get back to you soon. Verification Email Sent. Check your inbox.');
      
   }
    
   // public function sendEmail($thisUser) {
    //	Mail::to($thisUser['email'])->send(new VerifyEmail($thisUser));
   // }
    
   // public function verifyEmailFirst() {
   // 	return view('email.verifyEmail');
   // }
    
   // public function sendEmailDone() {
   // }

/**
    * Show the step 1 Form for creating a new product.
    *
    * @return \Illuminate\Http\Response
    */
   public function createStep1(Request $request)
   {
       $product = $request->session()->get('product');
       return view('products.create-step1',compact('product', $product));
   }
   /**
    * Post Request to store step1 info in session
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function postCreateStep1(Request $request)
   {
       $validatedData = $request->validate([
            'email'                     =>  'required|unique:users',
            'password'                  =>  ['required', 'string', 'confirmed'],
            'password_confirmation'     =>  'required',
       ]);
       if(empty($request->session()->get('product'))){
           $product = new Product();
           $product->fill($validatedData);
           $request->session()->put('product', $product);
       }
       else
       {
           $product = $request->session()->get('product');
           $product->fill($validatedData);
           $request->session()->put('product', $product);
       }
       return redirect('/step2');
   }
   /**
    * Show the step 2 Form for creating a new product.
    *
    * @return \Illuminate\Http\Response
    */
   public function createStep2(Request $request)
   {
       $product1 = $request->session()->get('product1');
       
       return view('products.create-step2',compact('product1', $product1));
   }
   /**
    * Post Request to store step1 info in session
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function postCreateStep2(Request $request)
   {   
        $product = $request->session()->get('product');
        
        $validatedData = $request->validate([
           'fname'      =>  'required|regex:/^[\pL\s\-]+$/u',
           'lname'      =>  'required|regex:/^[\pL\s\-]+$/u',
           'skype'      =>  'required',
           'phone'      =>  'required|numeric|digits_between:10,12',
           'country'    =>  'required',
           'state'      =>  'required|regex:/^[\pL\s\-]+$/u',
           'city'       =>  'required|regex:/^[\pL\s\-]+$/u',
        ]);
        $product1 = new Product();
        $product1->fill($validatedData);
        $request->session()->put('product1', $product1);
        
        
           $fileName = "productImage";
           //$request->productimg->storeAs('productimg', $fileName);
           $product = $request->session()->get('product');
           
           $product->productImg = $fileName;
           //$request->session()->put('product', $product);
           
        
        return redirect('/step3');
   }

   public function createStep3(Request $request)
   {

       $product2 = $request->session()->get('product2');

       return view('products.create-step3',compact('product2', $product2));
   }
   /**
    * Post Request to store step1 info in session
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function postCreateStep3(Request $request)
   {            
       
       $product = $request->session()->get('product');

       $validatedData = $request->validate([
               'business_name' => 'required',
               'business_address' => 'required',
               'business_phone' => 'required|numeric|digits_between:10,12',                
           ]);
       $product2 = new Product();
       $product2->fill($validatedData);
       $request->session()->put('product2', $product2);

       $product = $request->session()->get('product');
       $product1 = $request->session()->get('product1');
       
      
       return redirect('/step4');
   }    
   
   /**
    * Show the Product Review page
    *
    * @return \Illuminate\Http\Response
    */
   public function createStep4(Request $request)
   {        
       $product3 = $request->session()->get('product3');
    //   Session::get('variableName');
    
       
       return view('products.create-step4',compact('product3',$product3));
   }
   /**
    * Store product
    *
    * @return \Illuminate\Http\Response
    */
   public function signupstore(Request $request)
   {           
        $product = $request->session()->get('product');
        $validatedData = $request->validate([
            'platform' => 'required',
            'storeruntime' => 'required',
            'ordersno' => 'required',
            'productsno' => 'required',
            'ordersperday' => 'required',
            'g-recaptcha-response' => ['required','captcha'],            
            'agree' => ['required'],                
        ]);
       $product3 = new Product();
       $product3->fill($validatedData);
       $request->session()->put('product3', $product3);
       $data = Session::get('product');
       $data1 = Session::get('product1');
       $data2 = Session::get('product2');
       $data3 = Session::get('product3');
       $product->save();
       return redirect('/products');
   }

}
