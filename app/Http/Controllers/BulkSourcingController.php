<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\Photography;
use App\invoiceCreate;
use DB;
use App\Quotation;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\WalletBalance;
use App\Inventory;
use App\Invoice;
use Mail;
use App\InvoiceProduct;
use App\RecentActivities;
use App\Points;
use App\InventoryActivity;
use App\BulkSourcing;

class BulkSourcingController extends Controller
{
    /**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() {
	
	if(Auth::user()->role == 1) {
		return view('bulk-sourcing.index');
		
	} 
	else if(Auth::user()->role == 0) {
		return back();
		
	}
	else if(Auth::user()->role == 2) {
    	return back();
    	    
    }
	else {
		return view('/'); 
	}			    
	}
	
	/**
     * To display the create Quote view.
     *
     * @return void
     */
    
    public function createQuote() {
        if(Auth::user()->role == 1) {
            return view('bulk-sourcing.create-quote');
            
        } else if(Auth::user()->role == 0) {
            return back();
        }
        else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
            return view('/');
        }
    }
    
    /**
     * To show all the Bulk-sourcing data.
     *
     * @param  Request $request
     * @return Response
     */
     
    public function show(Request $request) {
        $photography = BulkSourcing::select('id','q_link','pieces','shipping_location','time_frame','quote','quote_approve','user_id','status','created_at')->where('user_id',Auth::user()->id)->orderBy('id','desc')->get();
        return Response::json(array('data'=>$photography));
    }
    
    /**
     * To show all the Photography data.
     *
     * @param  Request $request
     * @return Response
     */
     
    public function store(Request $request)  {
        
        $rules = [
            'q_link'  =>  'required',
            'pieces' =>  'required|numeric',
            'shipping_location' =>  'required',
            'time_frame'    =>  'required',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            
            return back()->withErrors($validator)->withInput();
        }
        
        if($request->q_link == '' || $request->pieces == '' || $request->shipping_location == '' || $request->time_frame == '') {
            return back()->with('status','Please fill all the fields!');
        }
        
        $bulkSourcing = new BulkSourcing();
        
        $bulkSourcing->q_link = $request->q_link;
        $bulkSourcing->pieces = $request->pieces;
        $bulkSourcing->shipping_location = $request->shipping_location;
        $bulkSourcing->time_frame = $request->time_frame;
        //$bulkSourcing->quote = '';
        $bulkSourcing->status = 0;
        
        $bulkSourcing->user_id = Auth::user()->id;
        
        
        if($bulkSourcing->save()) {
            
            // $walletBalance = WalletBalance::where('user_id',Auth::user()->id)->first();
            // if(!empty($walletBalance)) 
            // {
            // 	$wallet = WalletBalance::find($walletBalance->id);
            // 	$wallet->amount = $walletBalance->amount - $photography->total_price;
            	
            // 	//check for wallet minimum amount 
            // 	$row_get                   =   DB::table('walletlimitamount')->where('id','<>', 0)->get();
            // 	$count_row                 =   count($row_get);
            // 	$wallet_minimum_amount     =   ($count_row > 0) ? $row_get[0]->wallet_minimum_amount : 0;//check for minimum balance
            // 	$username                  =   Auth::user()->fname.' '. Auth::user()->lname;
            	
            // 	if($wallet->amount < $wallet_minimum_amount)
            // 	{
            // 	    $email    =   Auth::user()->email; 
            // 	    //$email    =   'dineshkashera5@gmail.com';//test amount remove in future
            // 	    $data     =   array('email'=>$email,'wallet_amount' => $wallet->amount,'name' => $username,'wallet_minimum_amount' => $wallet_minimum_amount  ); 
            	
            // 	    Mail::send('email.walletminimum', $data, function ($m) use ($data) 
            // 	    {
            // 	        $m->from('notifications@7fulfillment.com', '7fulfillment');
            // 	        $m->to($data['email'])->subject('Warning!!! Wallet Balance Low.');
            // 	    });
            // 	}
            // 	//wallet minimum amount End 
            	
            // 	$wallet->save();    
            // }
            return back()->with('status','Bulk-Sourcing Request successfully send!');    
        } else {
            return back()->with('status','Bulk-Sourcing Request not send. Try again later.');
        }    
    }
    
    public function approveQuote(Request $request) {
    		
	try
	{
		if($request->ajax())
		{
			if($request->id){
				
				$bulkSourcing = BulkSourcing::where('id',$request->id)->first();
				
				if(!empty($bulkSourcing)) {
					
					$bulkSourcing = BulkSourcing::find($request->id);
					
					$bulkSourcing->quote_approve = 1;		
					
					if($bulkSourcing->save()) {
					    
					    $walletBalance = WalletBalance::where('user_id',Auth::user()->id)->first();
                        if(!empty($walletBalance)) 
                        {
                        	$wallet = WalletBalance::find($walletBalance->id);
                        	$wallet->amount = $walletBalance->amount - $bulkSourcing->quote;
                        	
                        	//check for wallet minimum amount 
                        	$row_get                   =   DB::table('walletlimitamount')->where('id','<>', 0)->get();
                        	$count_row                 =   count($row_get);
                        	$wallet_minimum_amount     =   ($count_row > 0) ? $row_get[0]->wallet_minimum_amount : 0;//check for minimum balance
                        	$username                  =   Auth::user()->fname.' '. Auth::user()->lname;
                        	
                        	if($wallet->amount < $wallet_minimum_amount)
                        	{
                        	    $email    =   Auth::user()->email; 
                        	    //$email    =   'dineshkashera5@gmail.com';//test amount remove in future
                        	    $data     =   array('email'=>$email,'wallet_amount' => $wallet->amount,'name' => $username,'wallet_minimum_amount' => $wallet_minimum_amount  ); 
                        	
                        	    Mail::send('email.walletminimum', $data, function ($m) use ($data) 
                        	    {
                        	        $m->from('notifications@7fulfillment.com', '7fulfillment');
                        	        $m->to($data['email'])->subject('Warning!!! Wallet Balance Low.');
                        	    });
                        	}
                        	//wallet minimum amount End 
                        	
                        	$wallet->save();    
                        }
					    
						/*
					    //Email code start
					    $user  = User::where('id',$bulkSourcing->user_id)->first();
		    			$customer_email =  $user->email;
		    			$cmailbody 		=  'Your Request Quote has been approved.';

		    			//$getadmin 		=  	DB::table('users')->where('id','=',$walletPayment->user_id)->first();
		    			//$getadmin		=	(array)$getadmin;
				        // 	$customerName		=  	$getadmin['fname'].' '.$getadmin['lname'];
			        	$customerName		=  	$user->fname.' '.$user->lname;
			        	
			        	$balance_id = WalletBalance::select('id','amount')->where('user_id',$photography->user_id)->get()->first();
						
						if(!empty($balance_id)) {
		        	
			        			$balance_amount = $balance_id->amount;		        	
				        }

		    			$data     		=   array(
	    									'customer_email'	=>	$customer_email,
	    									'customer_name' 	=> 	$customerName,
	    									'mail_body'		    =>	$cmailbody,
	    									'wallet_amount'		=>	$balance_amount
		    							); 

		    			//send mail to customers
		    	 		Mail::send('email.walletapprove', $data, function ($m) use ($data) {
	                        $m->from('notifications@7fulfillment.com', '7fulfillment');
	                        $m->to($data['customer_email'])->subject('Request Phototgraphy has been approved on 7fulfillment');
	                    });
		    	 		//Email code end
		    	 		*/

						return ('Request Quote Approved successfully');
						
					} else {
						return ('Request Quote Not Approved. Please try again Later');
					}									
				}				   				  
			}
			return ('No such record found');
		}
	}
	catch(\Illuminate\Database\QueryException $ex)
	{
	return (json_encode(array('status',$ex->getMessage()))) ;
	}
    }
}
