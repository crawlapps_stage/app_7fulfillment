<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
// use Validator;
// use URL;
// use Session;
// use Redirect;
// use Input;
// use App\User;
// use Stripe\Error\Card;
// use Cartalyst\Stripe\Stripe;
// use App\Payments;
// use App\WiredPayment;
// use Illuminate\Support\Facades\Auth;
// use App\Orders;
// use App\WalletPayment;
// use App\WalletBalance;
// use Mail;
// use DB;

use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;
use App\Payments;
use App\WiredPayment;
use Illuminate\Support\Facades\Auth;
use App\Orders;
use App\WalletPayment;
use App\WalletBalance;
use Mail;
use DB;
use App\RecentActivities;


class WalletController extends Controller
{

	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index(){
    	if(Auth::user()->role == 1) {
    		$id = Auth::user()->id;
    
	    	//WalletController::walletBalanceUpdate();
	    
	    	$balance_history = WalletBalance::select('amount')->where('user_id',$id)->first();
	    	//echo "<pre>";
	    	//print_r($balance_history);die;
	    	return view('wallet.index',compact('balance_history'));
    		
    	} 
    	else if(Auth::user()->role == 0) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }
 	
    }
    
    
    public function walletpaypal() {
    	if(Auth::user()->role == 1) {
    		$amount= 100;
    		return view ('wallet.paypalViewPage' ,compact('amount'));
    		
    	} 
    	else if(Auth::user()->role == 0) {
    		return back();
    		
    	}
    	else {
    		return view('/'); 
        }
    	
    	
    }
    
    public function walletstripe() {
    	if(Auth::user()->role == 1) {
    		$amount= 100;
    		return view ('wallet.stripeViewPage' ,compact('amount'));
    		
    	} 
    	else if(Auth::user()->role == 0) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }
        
    	
    }
    
    public function walletwired() {
    	if(Auth::user()->role == 1) {
    		return view ('wallet.wiredViewPage');
    		
    	} 
    	else if(Auth::user()->role == 0) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }	
    }
    
    public function walletwiredpayment(Request $request) {
        
        $rules = [
            'moneyamount' => 'required|numeric',
            'bankname' => 'required',
            'txn_id' => 'required|alpha_dash',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            
            return back()->withErrors($validator)->withInput();
        }
    
	    if($request->moneyamount == '' || $request->bankname=='' || $request->txn_id=='') {
	    	return back()->with('status', 'Please Fill the values');
	    }
	    else {
	    
	    	if($request->moneyamount < 3000) {
	    		$fee = 30;
	    		$amount_without_fee = $request->moneyamount-$fee;
	    	
	    	} else {
	    		$fee = 0;
	    		$amount_without_fee = $request->moneyamount-$fee;
	    	}
	    
	    	$email = Auth::user()->email;
			$id = Auth::user()->id;
				
	    	$payment = new WalletPayment;
	    	$payment->email = $email;
			$payment->user_id = $id;
			$payment->payment_method = 'wired';
			$payment->amount_without_fee = $amount_without_fee;
	    	$payment->amount = $request->moneyamount;
	    	$payment->currency_code = 'USD';
	    	$payment->txn_id = $request->txn_id;
	    	$payment->payment_status = 'pending';
	    	$payment->bank_name = $request->bankname;
	    		    	
	    	
		if($request->hasfile('file'))
		{	        
			$file = array_get($request,'file');
			
			
			$file_size = $file->getSize();
			
			if($file_size > 5000000) {
				return back()->with('status', 'File must be less than 5 mb size.');
			}
			
			$file_type = $file->getMimeType();			
			
			if($file_type == 'application/msword' || $file_type == 'application/pdf' || $file_type == 'image/jpeg' || $file_type == 'text/plain') {			
				// SET UPLOAD PATH 
				$destinationPath = 'public/image/'; // upload path
				// GET THE FILE EXTENSION
				$extension = $file->getClientOriginalExtension(); 			 
				//$fileName = $file->getClientOriginalName() . '.' . $extension;
				$fileName = $file->getClientOriginalName(); 
				// MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
				$upload_success = $file->move($destinationPath, $fileName);
				
				$payment->proof_of_payment = $fileName;
			} 
			else 
			{
				return back()->with('status', 'File should be a image/doc/text or pdf file.');
			}  
		}
				      	    	
	    	if($payment->save()) {
	    	
	    			//Email code start 
	    			$customer_email =  Auth::user()->email;
	    			$customeName    =  Auth::user()->fname.' '.Auth::user()->lname;
	    			$cmailbody 		=  'We have received your request for Bnak transfer. Our team will verify it and will get back to you. Below are the details you have submitted';

	    			$getadmin 		=  DB::table('users')->where('role','=', '0')->first();
	    			//$admin_email	=  $getadmin->email;
	    			$admin_email	=  'notifications@7fulfillment.com';
	    			
	    			$data     =   array(
	    								'customer_email'			=>	$customer_email,
	    								'customer_name' 		=> 	$customeName,
	    								'mail_body'				=>	$cmailbody,
	    								'payment_method'		=>	'Wired Transfer',
	    								'amount_without_fee'	=>	$amount_without_fee,
	    								'amount_with_fee'		=>	$request->moneyamount,
	    								'txn_id'				=>	$request->txn_id,
	    								'payment_status'		=>	'pending',
	    								'bank_name'				=>	$request->bankname,
	    								'admin_email'			=>	$admin_email,
	    								'mail_send'				=>	'wired'  
	    							); 

	    			//send mail to customers
	    	 		Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
                        $m->from('notifications@7fulfillment.com', '7fulfillment');
                        $m->to($data['customer_email'])->subject('Bank transfer payment request made on 7 fulfillment');
                    });

                    //send mail to admin
                    $data['mail_send']	= 'admin';
                    
                    $data['mail_body']  = $customeName.' has submitted bank transfer payment. Please check the details and approve his payment from admin dashboard payment details.';
                    Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
                        $m->from('notifications@7fulfillment.com', '7fulfillment');
                        $m->to($data['admin_email'])->subject('Bank transfer payment received');
                    });

                    //Email code ends
	    	/*
	    		
		        $sum=0;
			
			$allAmount = WalletPayment::where('user_id',$id)
			->whereNotIn('payment_status', array('pending'))->get()->toArray();
					        
			
			// if(empty($allAmount)) {
			
			//  $sum=0;
			 
			// } else {
			
			// 	$sum=0;
			// 	foreach($allAmount as $amount) {
			// 		$sum = $sum+$amount['amount_without_fee'];
			// 	}
				
			// }
			
			$balance_id = WalletBalance::select('id','amount')->where('user_id',$id)->get()->first();
			$balance_amount = $balance_id->amount + $amount_without_fee;						
			
			if(!empty($balance_id)) {
				$balance_amount_id = $balance_id['id'];
				$balance = WalletBalance::find($balance_amount_id);				
				$balance->user_id = $id;
				$balance->amount = $balance_amount;
				$balance->status = '1';
				$balance->save();
				
			
			} else {
			
				$balance = new WalletBalance;
				$balance->user_id = $id;
				$balance->amount = $balance_amount;
				$balance->status = '1';
				$balance->save();
			
			}
			*/
	
	    		return back()->with('status', 'We have recieved your request , our team will review it and credit the amount in your account.');
	    	}
	    	else {
	    		return back()->with('status', 'Your Transaction not completed. Please try again Later');
	    	}       
	    }
    	 	
    }
    
    public function paymentmethod(Request $request) {
    
    	
    	if(($request->moneyamount == '') || ($request->paymentmethod == '' )){
    	
    		return back()->with('status', 'Please Fill the values!');
    	}
    	else {
    		if($request->paymentmethod == "stripe") {
    			$amount= $request->moneyamount;
    			
    			
    			return view ('wallet.stripeViewPage',compact('amount'));
    		}
    		elseif($request->paymentmethod == "paypal") {
    			$amount= $request->moneyamount;
    			return view ('wallet.paypalViewPage',compact('amount'));
    		}
    		elseif($request->paymentmethod == "wired") {
    			return view ('wallet.wiredViewPage');
    		}
    	}
    	
    }
    
    public function stripeViewIndex(){
    	if(Auth::user()->role == 1) {
    		return view ('wallet.stripeViewPage');
    		
    	} 
    	else if(Auth::user()->role == 0) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }
    	
    }
    
    public function payWithStripe()
    {
    	if(Auth::user()->role == 1) {
    		return view('wallet.stripeViewPage');
    		
    	} 
    	else if(Auth::user()->role == 0) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }
    	
    }
    
    public function postPaymentWithStripe(Request $request)
    {
        
        $rules = [
            
            'amountAdd' => 'required|numeric',
            
        ];
        
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            
            return back()->withErrors($validator)->withInput();
        }
        
        require_once('stripe-php/init.php');
     
     
	$input = $request->all();
		 
	$amount 	= 	$input['amount'];
	$source 	= 	$input['stripeToken'];

	\Stripe\Stripe::setApiKey('sk_test_HSQ7BO2R4QabqeKb69qsFKBC');

	$charge = \Stripe\Charge::create(['amount' => ($amount*100), 'currency' => 'usd', 'source' => $source]);
	//echo $charge;
	//print_r($request->all());die;     
	$status =  $charge->status;
     
	try{
			
			 
			 if($status == 'succeeded') {
				 /**
				 * Write Here Your Database insert logic.
				 */
				 
				 $amount 	=  $charge->amount;
     			         $tid 	 	=  $charge->id;
				 $email 	=  Auth::user()->email;
				 $id 		=  Auth::user()->id;
				 
				 //$addFee	=  (((($request->amount)*(4.4))/100)+0.3);
				 
				 $order = new WalletPayment;
				 $order->email = $email;
				 $order->user_id = $id;
				 $order->payment_method = 'stripe';
				 $order->txn_id = $charge->balance_transaction;
				 $order->payment_status = $charge->status;
				 $order->amount_without_fee = $request->amountAdd;//$request->amountAdd;
				 //$order->amount = $request->amount+$addFee;
				 $order->amount = $request->amount;
				 $order->currency_code = $charge->currency;
				 
				 if($order->save()) {
				 
				    //Email code start 
	    			$customer_email =  Auth::user()->email;
	    			$customeName    =  Auth::user()->fname.' '.Auth::user()->lname;
	    			$cmailbody 		=  'Thanks for making transaction, Below are your transaction details.';

	    			$getadmin 		=  DB::table('users')->where('role','=', '0')->first();
	    			//$admin_email	=  $getadmin->email;
	    			$admin_email	=  'notifications@7fulfillment.com';
	    			
	    			$data     =   array(
	    								'customer_email'		=>	$customer_email,
	    								'customer_name' 		=> 	$customeName,
	    								'mail_body'				=>	$cmailbody,
	    								'payment_method'		=>	'Stripe',
	    								'amount_without_fee'	=>	$request->amountAdd,//$request->amountAdd,
	    								'amount_with_fee'		=>	$request->amount,
	    								'txn_id'				=>	$charge->balance_transaction,
	    								'payment_status'		=>	$charge->status,
	    								'admin_email'			=>	$admin_email,
	    								'mail_send'				=>	'stripe',
	    								'bank_name'				=>	''  
	    							); 

	    			//send mail to customers
	    	 		Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
                        $m->from('notifications@7fulfillment.com', '7fulfillment');
                        $m->to($data['customer_email'])->subject('Payment has been received');
                    });

                    //send mail to admin
                     $data['mail_send']	= 'admin';
                    
                    $data['mail_body']  = $customeName.' has made payment from stripe. Please check the details.';

                    Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
                        $m->from('notifications@7fulfillment.com', '7fulfillment');
                        $m->to($data['admin_email'])->subject('Payment has been made on 7 fulfillment');
                    });

			        	$balance_id = WalletBalance::select('id','amount')->where('user_id',$id)->get()->first();
			        	if(!empty($balance_id)) {
		        	
		        		$balance_amount = $balance_id->amount + $request->amountAdd;//$request->amountAdd;
		        	
			        	} else {
			        	
			        		$balance_amount = $request->amountAdd;//$request->amountAdd;		        	
			        	}
			        	
			        	//$amount_diff = ($amount['amount_without_fee']) - ($sum - $balance_id['amount']);			        
			        	
			        	if(!empty($balance_id)) {
			        		$balance_amount_id = $balance_id['id'];
			        		$balance = WalletBalance::find($balance_amount_id);
			        		
			        		$balance->user_id = $id;
				        	$balance->amount = $balance_amount;
				        	$balance->status = '1';
				        	$balance->save();			        		
			        	
			        	} else {
			        	
			        		$balance = new WalletBalance;
				        	$balance->user_id = $id;
				        	$balance->amount = $balance_amount;
				        	$balance->status = '1';
				        	$balance->save();
			        	
			        	}
			        	// Points functionality
			    		$recent_activities = new RecentActivities;
			    		$recent_activities->user_id = $id;
			    		$recent_activities->message = "$".$request->amountAdd." was added to your wallet";
			    		$recent_activities->save();
			        	// End Points functionality
			        	
	
				  	return back()->with('status', 'Your Transaction has completed.');
				 }
				 else {				 
				 	return back()->with('status', 'Your Transaction not completed. Please try again Later');
				 }				 
				 //echo "<pre>";
				 //print_r($charge);die();
				 return redirect()->route('addmoney.paywithstripe');
			 }
		 } catch (Exception $e) {
		 	\Session::put('error',$e->getMessage());
		 	return redirect()->route('addmoney.paywithstripe');
		 } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
		 	\Session::put('error',$e->getMessage());
		 	return redirect()->route('addmoney.paywithstripe');
		 } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
		 	\Session::put('error',$e->getMessage());
		 	return redirect()->route('addmoney.paywithstripe');
		 }
	 }

     
     public function paypalsuccess(Request $request) {

		//Get payment information from PayPal
		$item_number =$request['item_number']; 
		$txn_id = $request['tx'];
		$payment_gross = $request['amt'];
		$currency_code = $request['cc'];
		$payment_status = $request['st'];
		$amount_without_fee =$request['cm'];
		
		if(!empty($txn_id)){
		    //Check if payment data exists with the same TXN ID.
		    $prevPaymentResult = Payments::select('payment_id')->where('txn_id',$txn_id);
		    
	    		$email = Auth::user()->email;
			$id = Auth::user()->id;						
						
		        //Insert tansaction data into the database
		        
		        $insert = new WalletPayment;
		        $insert->email = $email;
				$insert->user_id = $id;
				$insert->payment_method = 'paypal';
				$insert->amount_without_fee = $amount_without_fee;
				$insert->amount = $payment_gross;
		        $insert->currency_code = $currency_code;
		        $insert->txn_id = $txn_id;		        
		        $insert->payment_status = $payment_status;

		        if($insert->save()) {
		        
		        	//Email code start 
	    			$customer_email =  Auth::user()->email;
	    			$customeName    =  Auth::user()->fname.' '.Auth::user()->lname;
	    			$cmailbody 		=  'Thanks for making transaction, Below are your transaction details.';

	    			$getadmin 		=  DB::table('users')->where('role','=', '0')->first();
	    			//$admin_email	=  $getadmin->email;
	    			$admin_email	=  'notifications@7fulfillment.com';
	    			$data     =   array(
	    								'customer_email'		=>	$customer_email,
	    								'customer_name' 		=> 	$customeName,
	    								'mail_body'				=>	$cmailbody,
	    								'payment_method'		=>	'Paypal',
	    								'amount_without_fee'	=>	$amount_without_fee,
	    								'amount_with_fee'		=>	$payment_gross,
	    								'txn_id'				=>	$txn_id,
	    								'payment_status'		=>	$payment_status,
	    								'admin_email'			=>	$admin_email,
	    								'mail_send'				=>	'paypal',
	    								'bank_name'				=>	''  
	    							); 

	    			//send mail to customers
	    	 		Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
                        $m->from('notifications@7fulfillment.com', '7fulfillment');
                        $m->to($data['customer_email'])->subject('Payment has been received');
                    });

                    //send mail to admin
                     $data['mail_send']	= 'admin';
                    
                    $data['mail_body']  = $customeName.' has making payment from stripe. Please check the details.';

                    Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
                        $m->from('notifications@7fulfillment.com', '7fulfillment');
                        $m->to($data['admin_email'])->subject('Payment has been made on 7 fulfillment');
                    });

                    //Email code ends
		        	//$sum=0;
		        	
		        	// $allAmount = WalletPayment::where('user_id',$id)
		        	// ->whereNotIn('payment_status', array('pending'))->get()->toArray();		        
		        	
		        	// if(empty($allAmount)) {
		        	
		        	//  $sum=0;
		        	 
		        	// } else {
		        	
		        	// 	$sum=0;
			        // 	foreach($allAmount as $amount) {
			        // 		$sum = $sum+$amount['amount_without_fee'];
			        // 	}
		        	// }
		        	
		        	$balance_id = WalletBalance::select('id','amount')->where('user_id',$id)->get()->first();
		        	if(!empty($balance_id)) {
		        	
		        		$balance_amount = $balance_id->amount + $amount_without_fee;
		        	
		        	} else {
		        	
		        		$balance_amount = $amount_without_fee;		        	
		        	}		        	
		        	
		        	if(!empty($balance_id)) {
		        		$balance_amount_id = $balance_id['id'];
		        		$balance = WalletBalance::find($balance_amount_id);
		        		$balance->user_id = $id;
			        	$balance->amount = $balance_amount;
			        	$balance->status = '1';
			        	if($balance->save()){
			        		// Points functionality
				    		$recent_activities = new RecentActivities;
				    		$recent_activities->user_id = $id;
				    		$recent_activities->message = "$".$amount_without_fee." was added to your wallet";
				    		$recent_activities->save();
				        	// End Points functionality
			        	}
			        	
		        	} else {
		        	
		        		$balance = new WalletBalance;
			        	$balance->user_id = $id;
			        	$balance->amount = $balance_amount;
			        	$balance->status = '1';
			        	if($balance->save()){
			        		// Points functionality
				    		$recent_activities = new RecentActivities;
				    		$recent_activities->user_id = $id;
				    		$recent_activities->message = "$ ".$amount_without_fee." was added to your balance";
				    		$recent_activities->save();
				        	// End Points functionality
			        	}
		        	}		        	
		        	
		        	return redirect('wallet/paypal')->with('status', 'Your payment has been successful.');
		        } else {
		        	return redirect('wallet/paypal')->with('status', 'Your payment has been failed.');
		        }
		       
		    	
		}
		else{
			return redirect('wallet/paypal')->with('status', 'Your payment has failed.');
    		 }
    	}
    	
    	public function walletsuccess() {
    		if(Auth::user()->role == 1) {
    		return view('wallet.success');
    		
	    	} 
	    	else if(Auth::user()->role == 0) {
	    		return back();
	    		
	    	}
	    	else if(Auth::user()->role == 2) {
        	    return back();
        	    
        	}
	    	else {
	    		return view('/'); 
	        }
    		
    	}
    	
    	public function walletBalanceUpdate() {
    		
    	
	    	$id = Auth::user()->id;
	    	
	    	$sum=0;
			        	
		$allAmount = WalletPayment::where('user_id',$id)
		->whereNotIn('payment_status', array('pending'))->get()->toArray();		        
		
		if(empty($allAmount)) {
		
		 $sum=0;
		 
		} else {
		
			$sum=0;
	        	foreach($allAmount as $amount) {
	        		$sum = $sum+$amount['amount_without_fee'];
	        		
	        	}
		}
		
		$balance_id = WalletBalance::select('id')->where('user_id',$id)->get()->first();
		
		
		if(!empty($balance_id)) {
			$balance_amount_id = $balance_id['id'];
			$balance = WalletBalance::find($balance_amount_id);
			$balance->user_id = $id;
	        	$balance->amount = $sum;
	        	$balance->status = '1';
	        	$balance->save();
			
		
		} else {
		
			$balance = new WalletBalance;
	        	$balance->user_id = $id;
	        	$balance->amount = $sum;
	        	$balance->status = '1';
	        	$balance->save();	
		}
    	
    	}
    	
    	public function walletpayoneer() {
        	if(Auth::user()->role == 1) {
        		$amount= 100;
        		return view ('wallet.payoneerViewPage' ,compact('amount'));
        		
        	} 
        	else if(Auth::user()->role == 0) {
        		return back();
        		
        	}
        	else if(Auth::user()->role == 2) {
        	    return back();
        	    
        	}
        	else {
        		return view('/'); 
            }	
        }
    
    public function payoneerpayment(Request $request) 
    {
        
        $rules = [
            'moneyamount' => 'required|numeric',
            'payoneer_account' => 'required|alpha_num',
            'txn_id' => 'required|alpha_dash',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            
            return back()->withErrors($validator)->withInput();
        }
    
	    if($request->moneyamount == '' || $request->payoneer_account=='' || $request->txn_id=='') {
	    	return back()->with('status', 'Please Fill the values');
	    }
	    else {
	    
	    	if($request->moneyamount < 3000) {
	    		$fee = 30;
	    		$amount_without_fee = $request->moneyamount-$fee;
	    	
	    	} else {
	    		$fee = 0;
	    		$amount_without_fee = $request->moneyamount-$fee;
	    	}
	    
	    	$email = Auth::user()->email;
		$id = Auth::user()->id;
				
	    	$payment = new WalletPayment;
	    	$payment->email = $email;
		$payment->user_id = $id;
		$payment->payment_method = 'payoneer';
		$payment->amount_without_fee = $amount_without_fee;
	    	$payment->amount = $request->moneyamount;
	    	$payment->currency_code = 'USD';
	    	$payment->txn_id = $request->txn_id;
	    	$payment->payment_status = 'pending';
	    	$payment->payoneer_account = $request->payoneer_account;
	    		    	
	    	
		if($request->hasfile('file'))
		{	        
			$file = array_get($request,'file');
			
			
			$file_size = $file->getSize();
			
			if($file_size > 5000000) {
				return back()->with('status', 'File must be less than 5 mb size.');
			}
			
			$file_type = $file->getMimeType();			
			
			if($file_type == 'application/msword' || $file_type == 'application/pdf' || $file_type == 'image/jpeg' || $file_type == 'text/plain') {			
				// SET UPLOAD PATH 
				$destinationPath = 'public/image/'; // upload path
				// GET THE FILE EXTENSION
				$extension = $file->getClientOriginalExtension(); 			 
				//$fileName = $file->getClientOriginalName() . '.' . $extension;
				$fileName = $file->getClientOriginalName(); 
				// MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
				$upload_success = $file->move($destinationPath, $fileName);
				
				$payment->proof_of_payment = $fileName;
			} 
			else 
			{
				return back()->with('status', 'File should be a image/doc/text or pdf file.');
			}  
		}
				      	    	
	    	if($payment->save()) {
	    	
	    			//Email code start 
	    			$customer_email =  Auth::user()->email;
	    			$customeName    =  Auth::user()->fname.' '.Auth::user()->lname;
	    			$cmailbody 		=  'We have received your request for Payoneer transfer. Our team will verify it and will get back to you. Below are the details you have submitted';

	    			$getadmin 		=  DB::table('users')->where('role','=', '0')->first();
	    			//$admin_email	=  $getadmin->email;
	    			$admin_email	=  'notifications@7fulfillment.com';
	    			$data     =   array(
							'customer_email'		=>	$customer_email,
							'customer_name' 		=> 	$customeName,
							'mail_body'			=>	$cmailbody,
							'payment_method'		=>	'Payoneer Transfer',
							'amount_without_fee'		=>	$amount_without_fee,
							'amount_with_fee'		=>	$request->moneyamount,
							'txn_id'			=>	$request->txn_id,
							'payment_status'		=>	'pending',
							//'payoneer_account'		=>	$request->payoneer_account,
							'bank_name'			=>	'',
							'admin_email'			=>	$admin_email,
							'mail_send'			=>	'payoneer'
							  
	    					); 

	    			//send mail to customers
	    	 		Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
                        $m->from('notifications@7fulfillment.com', '7fulfillment');
                        $m->to($data['customer_email'])->subject('Payoneer transfer payment request made on 7 fulfillment');
                    });

                    //send mail to admin
                     $data['mail_send']	= 'admin';
                    
                    $data['mail_body']  = $customeName.' has submitted payoneer transfer payment. Please check the details and approve his payment from admin dashboard payment details.';
                    Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
                        $m->from('notifications@7fulfillment.com', '7fulfillment');
                        $m->to($data['admin_email'])->subject('Payoneer transfer payment received');
                    });

                    //Email code ends
	    		return back()->with('status', 'We have recieved your request , our team will review it and credit the amount in your account.');
	    	}
	    	else {
	    		return back()->with('status', 'Your Transaction not completed. Please try again Later');
	    	}       
	    }
    	 	
    }
	
	
	
	/************ this file(old) functions******************/
	    
	
	
//     public function index(){
//     	if(Auth::user()->role == 1) {
//     		$id = Auth::user()->id;
    
// 	    	//WalletController::walletBalanceUpdate();
	    
// 	    	$balance_history = WalletBalance::select('amount')->where('user_id',$id)->first();
// 	    	//echo "<pre>";
// 	    	//print_r($balance_history);die;
// 	    	return view('wallet.index',compact('balance_history'));
    		
//     	} 
//     	else if(Auth::user()->role == 0) {
//     		return back();
    		
//     	}
//     	else {
//     		return view('/'); 
//         }
 	
//     }
    
    
//     public function walletpaypal() {
//     	if(Auth::user()->role == 1) {
//     		$amount= 100;
//     		return view ('wallet.paypalViewPage' ,compact('amount'));
    		
//     	} 
//     	else if(Auth::user()->role == 0) {
//     		return back();
    		
//     	}
//     	else {
//     		return view('/'); 
//         }
    	
    	
//     }
    
//     public function walletstripe() {
//     	if(Auth::user()->role == 1) {
//     		$amount= 100;
//     		return view ('wallet.stripeViewPage' ,compact('amount'));
    		
//     	} 
//     	else if(Auth::user()->role == 0) {
//     		return back();
    		
//     	}
//     	else {
//     		return view('/'); 
//         }
        
    	
//     }
    
//     public function walletwired() {
//     	if(Auth::user()->role == 1) {
//     		return view ('wallet.wiredViewPage');
    		
//     	} 
//     	else if(Auth::user()->role == 0) {
//     		return back();
    		
//     	}
//     	else {
//     		return view('/'); 
//         }
    	
//     }
    
//     public function walletwiredpayment(Request $request) {   
    
// 	    if($request->moneyamount == '' || $request->bankname=='' || $request->txn_id=='') {
// 	    	return back()->with('status', 'Please Fill the values');
// 	    }
// 	    else {
	    
// 	    	if($request->moneyamount < 3000) {
// 	    		$fee = 30;
// 	    		$amount_without_fee = $request->moneyamount-$fee;
	    	
// 	    	} else {
// 	    		$fee = 0;
// 	    		$amount_without_fee = $request->moneyamount-$fee;
// 	    	}
	    
// 	    	$email = Auth::user()->email;
// 		$id = Auth::user()->id;
				
// 	    	$payment = new WalletPayment;
// 	    	$payment->email = $email;
// 			$payment->user_id = $id;
// 			$payment->payment_method = 'wired';
// 			$payment->amount_without_fee = $amount_without_fee;
// 	    	$payment->amount = $request->moneyamount;
// 	    	$payment->currency_code = 'USD';
// 	    	$payment->txn_id = $request->txn_id;
// 	    	$payment->payment_status = 'pending';
// 	    	$payment->bank_name = $request->bankname;
	    		    	
	    	
// 		if($request->hasfile('file'))
// 		{	        
// 			$file = array_get($request,'file');
			
			
// 			$file_size = $file->getSize();
			
// 			if($file_size > 5000000) {
// 				return back()->with('status', 'File must be less than 5 mb size.');
// 			}
			
// 			$file_type = $file->getMimeType();			
			
// 			if($file_type == 'application/msword' || $file_type == 'application/pdf' || $file_type == 'image/jpeg' || $file_type == 'text/plain') {			
// 				// SET UPLOAD PATH 
// 				$destinationPath = 'public/image/'; // upload path
// 				// GET THE FILE EXTENSION
// 				$extension = $file->getClientOriginalExtension(); 			 
// 				//$fileName = $file->getClientOriginalName() . '.' . $extension;
// 				$fileName = $file->getClientOriginalName(); 
// 				// MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
// 				$upload_success = $file->move($destinationPath, $fileName);
				
// 				$payment->proof_of_payment = $fileName;
// 			} 
// 			else 
// 			{
// 				return back()->with('status', 'File should be a image/doc/text or pdf file.');
// 			}  
// 		}
				      	    	
// 	    	if($payment->save()) {
	    	
// 	    			//Email code start 
// 	    			$customer_email =  Auth::user()->email;
// 	    			$customeName    =  Auth::user()->fname.' '.Auth::user()->lname;
// 	    			$cmailbody 		=  'We have received your request for Bnak transfer. Our team will verify it and will get back to you. Below are the details you have submitted';

// 	    			$getadmin 		=  DB::table('users')->where('role','=', '0')->first();
// 	    			//$admin_email	=  $getadmin->email;
// 	    			$admin_email	=  'notifications@7fulfillment.com';
// 	    			$data     =   array(
// 	    								'customer_email'		=>	$customer_email,
// 	    								'customer_name' 		=> 	$customeName,
// 	    								'mail_body'				=>	$cmailbody,
// 	    								'payment_method'		=>	'Wired Transfer',
// 	    								'amount_without_fee'	=>	$amount_without_fee,
// 	    								'amount_with_fee'		=>	$request->moneyamount,
// 	    								'txn_id'				=>	$request->txn_id,
// 	    								'payment_status'		=>	'pending',
// 	    								'bank_name'				=>	$request->bankname,
// 	    								'admin_email'			=>	$admin_email,
// 	    								'mail_send'				=>	'wired'  
// 	    							); 

// 	    			//send mail to customers
// 	    	 		Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
//                         $m->from('notifications@7fulfillment.com', '7fulfillment');
//                         $m->to($data['customer_email'])->subject('Bank transfer payment request made on 7 fulfillment');
//                     });

//                     //send mail to admin
//                     $data['mail_send']	= 'admin';	
//                     $data['mail_body']  = $customeName.' has submitted bank transfer payment. Please check the details and approve his payment from admin dashboard payment details.';
//                     Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
//                         $m->from('notifications@7fulfillment.com', '7fulfillment');
//                         $m->to($data['admin_email'])->subject('Bank transfer payment received');
//                     });

//                     //Email code ends
// 	    	/*
	    		
// 		        $sum=0;
			
// 			$allAmount = WalletPayment::where('user_id',$id)
// 			->whereNotIn('payment_status', array('pending'))->get()->toArray();
					        
			
// 			// if(empty($allAmount)) {
			
// 			//  $sum=0;
			 
// 			// } else {
			
// 			// 	$sum=0;
// 			// 	foreach($allAmount as $amount) {
// 			// 		$sum = $sum+$amount['amount_without_fee'];
// 			// 	}
				
// 			// }
			
// 			$balance_id = WalletBalance::select('id','amount')->where('user_id',$id)->get()->first();
// 			$balance_amount = $balance_id->amount + $amount_without_fee;						
			
// 			if(!empty($balance_id)) {
// 				$balance_amount_id = $balance_id['id'];
// 				$balance = WalletBalance::find($balance_amount_id);				
// 				$balance->user_id = $id;
// 				$balance->amount = $balance_amount;
// 				$balance->status = '1';
// 				$balance->save();
				
			
// 			} else {
			
// 				$balance = new WalletBalance;
// 				$balance->user_id = $id;
// 				$balance->amount = $balance_amount;
// 				$balance->status = '1';
// 				$balance->save();
			
// 			}
// 			*/
	
// 	    		return back()->with('status', 'We have recieved your request , our team will review it and credit the amount in your account.');
// 	    	}
// 	    	else {
// 	    		return back()->with('status', 'Your Transaction not completed. Please try again Later');
// 	    	}       
// 	    }
    	 	
//     }
    
//     public function paymentmethod(Request $request) {
    
    	
//     	if(($request->moneyamount == '') || ($request->paymentmethod == '' )){
    	
//     		return back()->with('status', 'Please Fill the values!');
//     	}
//     	else {
//     		if($request->paymentmethod == "stripe") {
//     			$amount= $request->moneyamount;
    			
    			
//     			return view ('wallet.stripeViewPage',compact('amount'));
//     		}
//     		elseif($request->paymentmethod == "paypal") {
//     			$amount= $request->moneyamount;
//     			return view ('wallet.paypalViewPage',compact('amount'));
//     		}
//     		elseif($request->paymentmethod == "wired") {
//     			return view ('wallet.wiredViewPage');
//     		}
//     	}
    	
//     }
    
//     public function stripeViewIndex(){
//     	if(Auth::user()->role == 1) {
//     		return view ('wallet.stripeViewPage');
    		
//     	} 
//     	else if(Auth::user()->role == 0) {
//     		return back();
    		
//     	}
//     	else {
//     		return view('/'); 
//         }
    	
//     }
    
//     public function payWithStripe()
//     {
//     	if(Auth::user()->role == 1) {
//     		return view('wallet.stripeViewPage');
    		
//     	} 
//     	else if(Auth::user()->role == 0) {
//     		return back();
    		
//     	}
//     	else {
//     		return view('/'); 
//         }
    	
//     }
    
//     public function postPaymentWithStripe(Request $request)
//     {
//       	$validator = Validator::make($request->all(), [
// 		 'card_no' => 'required',
// 		 'ccExpiryMonth' => 'required',
// 		 'ccExpiryYear' => 'required',
// 		 'cvvNumber' => 'required',	 
// 	 ]);
	 
// 	 $input = $request->all();
	 
// 	 if ($validator->passes()) { 
// 		 $input = array_except($input,array('_token'));
// 		 $stripe = Stripe::make('sk_test_PVXtzkhKGE6eV0iuxTqgh4iZ');
// 		 try {
// 			 $token = $stripe->tokens()->create([
// 				 'card' => [
// 					 'number' => $request->get('card_no'),
// 					 'exp_month' => $request->get('ccExpiryMonth'),
// 					 'exp_year' => $request->get('ccExpiryYear'),
// 					 'cvc' => $request->get('cvvNumber'),
// 				 ],
// 			 ]);			 			 
			 
// 			if (!isset($token['id'])) {
// 			 	return redirect()->route('addmoney.paywithstripe');
// 			 }
// 			 $charge = $stripe->charges()->create([
// 				 'card' => $token['id'],
// 				 'currency' => 'USD',
// 				 'amount' => $request->amount,
// 				 'description' => 'Add in wallet',
// 			 ]);
			 
// 			 if($charge['status'] == 'succeeded') {
// 				 /**
// 				 * Write Here Your Database insert logic.
// 				 */
				 
// 				 $email = Auth::user()->email;
// 				 $id = Auth::user()->id;
				 
// 				 $order = new WalletPayment;
// 				 $order->email = $email;
// 				 $order->user_id = $id;
// 				 $order->payment_method = 'stripe';
// 				 $order->txn_id = $charge['balance_transaction'];
// 				 $order->payment_status = $charge['status'];
// 				 $order->amount_without_fee = $request->amountAdd;
// 				 $order->amount = $request->amount;
// 				 $order->currency_code = $charge['currency'];
				 
// 				 if($order->save()) {
				 
// 				 		//Email code start 
// 		    			$customer_email =  Auth::user()->email;
// 		    			$customeName    =  Auth::user()->fname.' '.Auth::user()->lname;
// 		    			$cmailbody 		=  'Thanks for making transaction, Below are your transaction details.';

// 		    			$getadmin 		=  DB::table('users')->where('role','=', '0')->first();
// 		    			$admin_email	=  $getadmin->email;
		    			
// 		    			$data     =   array(
// 		    								'customer_email'		=>	$customer_email,
// 		    								'customer_name' 		=> 	$customeName,
// 		    								'mail_body'				=>	$cmailbody,
// 		    								'payment_method'		=>	'Stripe',
// 		    								'amount_without_fee'	=>	$request->amountAdd,
// 		    								'amount_with_fee'		=>	$request->amount,
// 		    								'txn_id'				=>	$charge['balance_transaction'],
// 		    								'payment_status'		=>	$charge['status'],
// 		    								'admin_email'			=>	$admin_email,
// 		    								'mail_send'				=>	'stripe',
// 		    								'bank_name'				=>	''  
// 		    							); 

// 		    			//send mail to customers
// 		    	 		Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
// 	                        $m->from('notifications@7fulfillment.com', '7fulfillment');
// 	                        $m->to($data['customer_email'])->subject('Payment has been received');
// 	                    });

// 	                    //send mail to admin
// 	                    $data['mail_send']	= 'admin';	
// 	                    $data['mail_body']  = $customeName.' has making payment from stripe. Please check the details.';

// 	                    Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
// 	                        $m->from('notifications@7fulfillment.com', '7fulfillment');
// 	                        $m->to($data['admin_email'])->subject('Payment has been made on 7 fulfillment');
// 	                    });

//                     	//Email code ends
// 			        	//$sum=0;
		        	
// 			        	//$allAmount = WalletPayment::where('user_id',$id)
// 			        	//->whereNotIn('payment_status', array('pending'))->get()->toArray();
			        			        
			        	
// 			        	// if(empty($allAmount)) {
			        	
// 			        	//  $sum=0;
			        	 
// 			        	// } else {
			        	
// 			        	// 	$sum=0;
// 				        // 	foreach($allAmount as $amount) {
// 				        // 		$sum = $sum+$amount['amount_without_fee'];
// 				        // 	}
				        	
// 			        	// }
			        	
// 			        	$balance_id = WalletBalance::select('id','amount')->where('user_id',$id)->get()->first();
// 			        	if(!empty($balance_id)) {
		        	
// 		        		$balance_amount = $balance_id->amount + $request->amountAdd;
		        	
// 			        	} else {
			        	
// 			        		$balance_amount = $request->amountAdd;		        	
// 			        	}
			        	
// 			        	//$amount_diff = ($amount['amount_without_fee']) - ($sum - $balance_id['amount']);			        
			        	
// 			        	if(!empty($balance_id)) {
// 			        		$balance_amount_id = $balance_id['id'];
// 			        		$balance = WalletBalance::find($balance_amount_id);
			        		
// 			        		$balance->user_id = $id;
// 				        	$balance->amount = $balance_amount;
// 				        	$balance->status = '1';
// 				        	$balance->save();			        		
			        	
// 			        	} else {
			        	
// 			        		$balance = new WalletBalance;
// 				        	$balance->user_id = $id;
// 				        	$balance->amount = $balance_amount;
// 				        	$balance->status = '1';
// 				        	$balance->save();
			        	
// 			        	}
			        	
	
// 				  	return back()->with('status', 'Your Transaction has completed.');
// 				 }
// 				 else {				 
// 				 	return back()->with('status', 'Your Transaction not completed. Please try again Later');
// 				 }				 
// 				 //echo "<pre>";
// 				 //print_r($charge);die();
// 				 return redirect()->route('addmoney.paywithstripe');
// 			 } else {
// 			 	\Session::put('error','Money not add in wallet!!');
// 			 	return redirect()->route('addmoney.paywithstripe');
// 			 }
// 		 } catch (Exception $e) {
// 		 	\Session::put('error',$e->getMessage());
// 		 	return redirect()->route('addmoney.paywithstripe');
// 		 } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
// 		 	\Session::put('error',$e->getMessage());
// 		 	return redirect()->route('addmoney.paywithstripe');
// 		 } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
// 		 	\Session::put('error',$e->getMessage());
// 		 	return redirect()->route('addmoney.paywithstripe');
// 		 }
// 	 }
//      }

// public function postPaymentWithStripe2(Request $request)
// 	{
// //print_r($request->all());die;	
// 		$input = $request->all();			 
// 		//$input = array_except($input,array('_token'));
// 		//echo $input;die;
// 		$stripe = Stripe::make('sk_test_qv1K4eIRc55DvYyESEnOHUES');
// 		$token = $request->stripeToken;
// //$token = $request->_token;
          

// 		$amountInCents= $request->amountInCents;
		
// 		try 
// 		{
// 			if (!isset($token)) {
				
// 				return redirect()->route('addmoney.paywithstripe');
// 			}
// 			//echo $token;die;



// $charge = Charge::create(array(
//             'customer' => $customer->id,
//             'amount'   => 1999,
//             'currency' => 'usd'
//         ));

// /*
// 			$charge = $stripe->charges()->create([
// 				'card' => $token,
// 				'currency' => 'USD',
// 				'amount' => $amountInCents,
// 				'description' => 'Add in wallet',
// 			]);
// */

// 			print_r($charge);die;

// 			if($charge['status'] == 'succeeded') {
// 			/**
// 			* Write Here Your Database insert logic.
// 			*/			

// 			$email = Auth::user()->email;
// 			$id = Auth::user()->id;

// 			$order = new WalletPayment;
// 			$order->email = $email;
// 			$order->user_id = $id;
// 			$order->payment_method = 'stripe';
// 			$order->txn_id = $charge['balance_transaction'];
// 			$order->payment_status = $charge['status'];
// 			$order->amount_without_fee = $request->amountAdd;
// 			$order->amount = $request->amount;
// 			$order->currency_code = $charge['currency'];

// 			if($order->save()) {

// 			//Email code start 
// 			$customer_email =  Auth::user()->email;
// 			$customeName    =  Auth::user()->fname.' '.Auth::user()->lname;
// 			$cmailbody 		=  'Thanks for making transaction, Below are your transaction details.';

// 			$getadmin 		=  DB::table('users')->where('role','=', '0')->first();
// 			$admin_email	=  $getadmin->email;

// 			$data     =   array(
// 						'customer_email'		=>	$customer_email,
// 						'customer_name' 		=> 	$customeName,
// 						'mail_body'				=>	$cmailbody,
// 						'payment_method'		=>	'Stripe',
// 						'amount_without_fee'	=>	$request->amountAdd,
// 						'amount_with_fee'		=>	$request->amount,
// 						'txn_id'				=>	$charge['balance_transaction'],
// 						'payment_status'		=>	$charge['status'],
// 						'admin_email'			=>	$admin_email,
// 						'mail_send'				=>	'stripe',
// 						'bank_name'				=>	''  
// 					); 

// 			//send mail to customers
// 			Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
// 			$m->from('notifications@7fulfillment.com', '7fulfillment');
// 			$m->to($data['customer_email'])->subject('Payment has been received');
// 			});

// 			//send mail to admin
// 			$data['mail_send']	= 'admin';	
// 			$data['mail_body']  = $customeName.' has making payment from stripe. Please check the details.';

// 			Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
// 			$m->from('notifications@7fulfillment.com', '7fulfillment');
// 			$m->to($data['admin_email'])->subject('Payment has been made on 7 fulfillment');
// 			});		

// 			$balance_id = WalletBalance::select('id','amount')->where('user_id',$id)->get()->first();
// 			if(!empty($balance_id)) {

// 			$balance_amount = $balance_id->amount + $request->amountAdd;

// 			} else {

// 			$balance_amount = $request->amountAdd;		        	
// 			}					        

// 			if(!empty($balance_id)) {
// 				$balance_amount_id = $balance_id['id'];
// 				$balance = WalletBalance::find($balance_amount_id);

// 				$balance->user_id = $id;
// 				$balance->amount = $balance_amount;
// 				$balance->status = '1';
// 				$balance->save();			        		

// 			} else {

// 				$balance = new WalletBalance;
// 				$balance->user_id = $id;
// 				$balance->amount = $balance_amount;
// 				$balance->status = '1';
// 				$balance->save();
// 			}

// 			return back()->with('status', 'Your Transaction has completed.');
// 			}
// 			else {				 
// 				return back()->with('status', 'Your Transaction not completed. Please try again Later');
// 			}				
// 			return redirect()->route('addmoney.paywithstripe');
// 			} else {
// 				\Session::put('error','Money not add in wallet!!');
// 				return redirect()->route('addmoney.paywithstripe');
// 			}
// 		} 
// 		catch (Exception $e) {
// 			print_r($e->getMessage());die("1");
// 			\Session::put('error',$e->getMessage());
// 			return redirect()->route('addmoney.paywithstripe');
// 		} 
// 		catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
// 			print_r($e->getMessage());die("2");
// 			\Session::put('error',$e->getMessage());
// 			return redirect()->route('addmoney.paywithstripe');
// 		} 
// 		catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
// 			print_r($e->getMessage());die("3");
// 			\Session::put('error',$e->getMessage());
// 			return redirect()->route('addmoney.paywithstripe');
// 		}	
// 	}
     
//      public function paypalsuccess(Request $request) {    	

// 		//Get payment information from PayPal
// 		$item_number =$request['item_number']; 
// 		$txn_id = $request['tx'];
// 		$payment_gross = $request['amt'];
// 		$currency_code = $request['cc'];
// 		$payment_status = $request['st'];
// 		$amount_without_fee =$request['cm'];
		
// 		if(!empty($txn_id)){
// 		    //Check if payment data exists with the same TXN ID.
// 		    $prevPaymentResult = Payments::select('payment_id')->where('txn_id',$txn_id);
		    
// 	    		$email = Auth::user()->email;
// 			$id = Auth::user()->id;						
						
// 		        //Insert tansaction data into the database
		        
// 		        $insert = new WalletPayment;
// 		        $insert->email = $email;
// 			$insert->user_id = $id;
// 			$insert->payment_method = 'paypal';
// 			$insert->amount_without_fee = $amount_without_fee;
// 			$insert->amount = $payment_gross;
// 		        $insert->currency_code = $currency_code;
// 		        $insert->txn_id = $txn_id;		        
// 		        $insert->payment_status = $payment_status;
// 		        if($insert->save()) {
		        
// 		        	//Email code start 
// 	    			$customer_email =  Auth::user()->email;
// 	    			$customeName    =  Auth::user()->fname.' '.Auth::user()->lname;
// 	    			$cmailbody 		=  'Thanks for making transaction, Below are your transaction details.';

// 	    			$getadmin 		=  DB::table('users')->where('role','=', '0')->first();
// 	    			$admin_email	=  $getadmin->email;
	    			
// 	    			$data     =   array(
// 	    								'customer_email'		=>	$customer_email,
// 	    								'customer_name' 		=> 	$customeName,
// 	    								'mail_body'				=>	$cmailbody,
// 	    								'payment_method'		=>	'Paypal',
// 	    								'amount_without_fee'	=>	$amount_without_fee,
// 	    								'amount_with_fee'		=>	$payment_gross,
// 	    								'txn_id'				=>	$txn_id,
// 	    								'payment_status'		=>	$payment_status,
// 	    								'admin_email'			=>	$admin_email,
// 	    								'mail_send'				=>	'paypal',
// 	    								'bank_name'				=>	''  
// 	    							); 

// 	    			//send mail to customers
// 	    	 		Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
//                         $m->from('notifications@7fulfillment.com', '7fulfillment');
//                         $m->to($data['customer_email'])->subject('Payment has been received');
//                     });

//                     //send mail to admin
//                     $data['mail_send']	= 'admin';	
//                     $data['mail_body']  = $customeName.' has making payment from stripe. Please check the details.';

//                     Mail::send('email.walletrecharge', $data, function ($m) use ($data) {
//                         $m->from('notifications@7fulfillment.com', '7fulfillment');
//                         $m->to($data['admin_email'])->subject('Payment has been made on 7 fulfillment');
//                     });

//                     //Email code ends
// 		        	//$sum=0;
		        	
// 		        	// $allAmount = WalletPayment::where('user_id',$id)
// 		        	// ->whereNotIn('payment_status', array('pending'))->get()->toArray();		        
		        	
// 		        	// if(empty($allAmount)) {
		        	
// 		        	//  $sum=0;
		        	 
// 		        	// } else {
		        	
// 		        	// 	$sum=0;
// 			        // 	foreach($allAmount as $amount) {
// 			        // 		$sum = $sum+$amount['amount_without_fee'];
// 			        // 	}
// 		        	// }
		        	
// 		        	$balance_id = WalletBalance::select('id','amount')->where('user_id',$id)->get()->first();
// 		        	if(!empty($balance_id)) {
		        	
// 		        		$balance_amount = $balance_id->amount + $amount_without_fee;
		        	
// 		        	} else {
		        	
// 		        		$balance_amount = $amount_without_fee;		        	
// 		        	}		        	
		        	
		        			        	
		        	
// 		        	if(!empty($balance_id)) {
// 		        		$balance_amount_id = $balance_id['id'];
// 		        		$balance = WalletBalance::find($balance_amount_id);
// 		        		$balance->user_id = $id;
// 			        	$balance->amount = $balance_amount;
// 			        	$balance->status = '1';
// 			        	$balance->save();
		        		
		        	
// 		        	} else {
		        	
// 		        		$balance = new WalletBalance;
// 			        	$balance->user_id = $id;
// 			        	$balance->amount = $balance_amount;
// 			        	$balance->status = '1';
// 			        	$balance->save();
		        	
// 		        	}
		        	
		        			        	
		        	
// 		        	return redirect('wallet/paypal')->with('status', 'Your payment has been successful.');
// 		        } else {
// 		        	return redirect('wallet/paypal')->with('status', 'Your payment has been failed.');
// 		        }
		       
		    	
// 		}
// 		else{
// 			return redirect('wallet/paypal')->with('status', 'Your payment has failed.');
//     		 }
//     	}
    	
//     	public function walletsuccess() {
//     		if(Auth::user()->role == 1) {
//     		return view('wallet.success');
    		
// 	    	} 
// 	    	else if(Auth::user()->role == 0) {
// 	    		return back();
	    		
// 	    	}
// 	    	else {
// 	    		return view('/'); 
// 	        }
    		
//     	}
    	
//     	public function walletBalanceUpdate() {
    		
    	
// 	    	$id = Auth::user()->id;
	    	
// 	    	$sum=0;
			        	
// 		$allAmount = WalletPayment::where('user_id',$id)
// 		->whereNotIn('payment_status', array('pending'))->get()->toArray();		        
		
// 		if(empty($allAmount)) {
		
// 		 $sum=0;
		 
// 		} else {
		
// 			$sum=0;
// 	        	foreach($allAmount as $amount) {
// 	        		$sum = $sum+$amount['amount_without_fee'];
// 	        	}
// 		}
		
// 		$balance_id = WalletBalance::select('id')->where('user_id',$id)->get()->first();
		
		
// 		if(!empty($balance_id)) {
// 			$balance_amount_id = $balance_id['id'];
// 			$balance = WalletBalance::find($balance_amount_id);
// 			$balance->user_id = $id;
// 	        	$balance->amount = $sum;
// 	        	$balance->status = '1';
// 	        	$balance->save();
			
		
// 		} else {
		
// 			$balance = new WalletBalance;
// 	        	$balance->user_id = $id;
// 	        	$balance->amount = $sum;
// 	        	$balance->status = '1';
// 	        	$balance->save();	
// 		}
    	
//     	}
    
}
