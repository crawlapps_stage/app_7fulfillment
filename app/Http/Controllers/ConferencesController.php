<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\SUpport\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class ConferencesController extends Controller
{
    /**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() {
	
	if(Auth::user()->role == 1) {
            
		return view('conferences.index');
		
	} 
	else if(Auth::user()->role == 0) {
		return back();
		
	}
	else if(Auth::user()->role == 2) {
	    return back();
	    
	}
	else {
		return view('/'); 
	}			    
	}
}
