<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


use DB;
use App\Quotation;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\Invoice;
use App\TicketCreate;
use App\ChatCreate;
use Mail;

class TicketCreateController extends Controller
{
    /**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() {
		
		if(Auth::user()->role == 1) {
			//$invoices			 =    DB::table('invoices')->where('user_id', Auth::user()->id)->get();
			//$all_user_invoices   =    $invoices; 
			
			//return view('support-tickets.create-ticket',compact('all_user_invoices'));
      return view('support-tickets.create-ticket');
			
		} 
		else if(Auth::user()->role == 0) {
			return back();
			
		}
		else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
		else {
			return view('/'); 
		}			    
	}

	public function store(Request $request) 
  {
    	$email 			   = 	Auth::user()->email;
    	$user_id 		   = 	Auth::user()->id;

    	$post_data 		 =	$request->all();
    	$subject		   =	$post_data['support_subject'];
    	$message		   =	$post_data['support_message']; 
    	$invoice_id		 = 	(int)$post_data['invoice_list'];

    	
     		if(empty($invoice_id)){

     			return back()->with('status-error', 'Please Select Invoice');

     		}	
      	else if(empty($subject))
        {
          return back()->with('status-error', 'Please fill the subject');

        }else if(empty($message)){
          
          return back()->with('status-error', 'Please fill the message');

        }else{
          
          $ticketcreate = new TicketCreate();

          $ticketcreate->order_id 	= 	$invoice_id;
          $ticketcreate->user_id 	  = 	$user_id;
          $ticketcreate->user_email = 	$email;
          $ticketcreate->subject 	  = 	$subject;
          $ticketcreate->message 	  = 	$message;
          $ticketcreate->status 	  = 	0;

          try{

          	if($ticketcreate->save()){

               $ticket_id       =   DB::getPdo()->lastInsertId();
               $reply_by_user   =   0;
               $reply_by_admin  =   0;

               if(Auth::user()->role == 1){
                $reply_by_user  = 1;
               }else{
                $reply_by_admin = 1;
               }

               $ChatCreate                  =   new ChatCreate(); 
               $ChatCreate->ticket_id       =   $ticket_id;
               $ChatCreate->message         =   $message;
               $ChatCreate->reply_by_admin  =   $reply_by_admin;
               $ChatCreate->reply_by_user   =   $reply_by_user;

               try{
                  if($ChatCreate->save()){
                      return redirect('/create-ticket')->with('status', 'Ticket Created.');
                  }
               }catch(Exception $exception2){
                  return back()->with('status-error', $exception2->getMessage());
               }  
            }
          }catch(Exception $exception){

      	  	return back()->with('status-error', $exception->getMessage());

      	  }
      
        }
    }
}
