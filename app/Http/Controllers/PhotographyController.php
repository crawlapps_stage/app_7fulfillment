<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\Photography;
use App\invoiceCreate;
use DB;
use App\Quotation;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\WalletBalance;
use App\Inventory;
use App\Invoice;
use Mail;
use App\InvoiceProduct;
use App\RecentActivities;
use App\Points;
use App\InventoryActivity;

class PhotographyController extends Controller
{
    /**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() {
	
    	if(Auth::user()->role == 1) {
    	    return view('photography.index');
    	    
    	} else if(Auth::user()->role == 0) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	} else {
    		return view('/'); 
    	}			    
	}
	
	/**
     * To show all the Photography data.
     *
     * @param  Request $request
     * @return Response
     */
     
    public function show(Request $request) {
        $photography = Photography::select('id','product_link','product_price','total_price','user_id','status','created_at')->where('user_id',Auth::user()->id)->orderBy('id','desc')->get();
        return Response::json(array('data'=>$photography));
    }
    
    /**
     * To display the create Photography view.
     *
     * @return void
     */
    
    public function createPhotography() {
        if(Auth::user()->role == 1) {
            return view('photography.create-photography');
            
        } else if(Auth::user()->role == 0) {
            return back();
        }
        else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	} else {
            return view('/');
        }
    }
    
    /**
     * To show all the Photography data.
     *
     * @param  Request $request
     * @return Response
     */
     
    public function store(Request $request)  {
        
        $rules = [
            'product_link'  =>  'required',
            'product_price' =>  'required|numeric',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            
            return back()->withErrors($validator)->withInput();
        }
        
        if($request->product_link == '' || $request->product_price == '') {
            return back()->with('status','Please fill all the fields!');
        }
        
        $photography = new Photography();
        
        $photography->product_link = $request->product_link;
        $photography->product_price = $request->product_price;
        $photography->total_price = $request->product_price+100;
        $photography->status = 0;
        $photography->user_id = Auth::user()->id;
        
        
        if($photography->save()) {
            
            $walletBalance = WalletBalance::where('user_id',Auth::user()->id)->first();
            if(!empty($walletBalance)) 
            {
            	$wallet = WalletBalance::find($walletBalance->id);
            	$wallet->amount = $walletBalance->amount - $photography->total_price;
            	
            	//check for wallet minimum amount 
            	$row_get                   =   DB::table('walletlimitamount')->where('id','<>', 0)->get();
            	$count_row                 =   count($row_get);
            	$wallet_minimum_amount     =   ($count_row > 0) ? $row_get[0]->wallet_minimum_amount : 0;//check for minimum balance
            	$username                  =   Auth::user()->fname.' '. Auth::user()->lname;
            	
            	if($wallet->amount < $wallet_minimum_amount)
            	{
            	    $email    =   Auth::user()->email; 
            	    //$email    =   'dineshkashera5@gmail.com';//test amount remove in future
            	    $data     =   array('email'=>$email,'wallet_amount' => $wallet->amount,'name' => $username,'wallet_minimum_amount' => $wallet_minimum_amount  ); 
            	
            	    Mail::send('email.walletminimum', $data, function ($m) use ($data) 
            	    {
            	        $m->from('notifications@7fulfillment.com', '7fulfillment');
            	        $m->to($data['email'])->subject('Warning!!! Wallet Balance Low.');
            	    });
            	}
            	//wallet minimum amount End 
            	
            	$wallet->save();    
            }
            return back()->with('status','Photography request sent successfully!');    
        } else {
            return back()->with('status','Photography request not sent successfully. Try again later.');
        }    
    }
}
