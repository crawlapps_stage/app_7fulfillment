<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\Payments;
use App\WiredPayment;
use App\Orders;
use App\WalletPayment;
use App\WalletBalance;
use App\Invoice;
use App\RecentActivities;
use DB;
use App\Points;

class PriorityController extends Controller
{
    /**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() {
	
		if(Auth::user()->role == 1) {
			$id = Auth::user()->id;
		    	$total_points = Invoice::where('user_id', $id)->sum('number_of_parcel');
			return view('7Priority.index', compact('total_points'));
		} 
		else if(Auth::user()->role == 0) {
			return back();
		} else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
		else {
			return view('/'); 
		}			    
	}
	
	public function show() {
	
// 		$offset = 0;
// 		DB::statement(DB::raw('set @rownumber='.$offset.''));
// 		$points = new Invoice();
// 		$id = Auth::user()->id;
		
// 		$points = $points
// 			->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'id','number_of_parcel as points','created_at')
// 			->where(array('user_id'=>$id));
// 		$points = $points->orderby('invoices.id','desc');
// 		$total = $points->count();
// 		$points = $points->get();

        $offset = 0;
		DB::statement(DB::raw('set @rownumber='.$offset.''));
		$points = new Points();
		$id = Auth::user()->id;
		
		$points = $points
			->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'id','point','parcel','message','created_at')
			->where(array('user_id'=>$id));
			
		$points = $points->orderby('id','desc');
		$total = $points->count();
		$points = $points->get();
		
		return Response::json(array('data'=> $points));
		
		
	}
}
