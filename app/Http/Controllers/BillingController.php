<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\WalletBalance;
use DB;
use App\invoiceCreate;
use App\Quotation;
use App\Inventory;
use App\Invoice;
use App\InvoiceProduct;

class BillingController extends Controller
{

	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() {
							
	    						
		if(Auth::user()->role == 1) {
			return view('billing.index');
			
		} 
		else if(Auth::user()->role == 0) {
			return back();
			
		}
		else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
		else {
			return view('/'); 
		}
	
	}

	public function show() {
        try
        {                
            $offset   =  0;            
            $user_id  = Auth::user()->id;
            
            if(empty($user_id)){
            	return (json_encode(array('status'=>'error','message'=> 'User Not Found'))) ;
            }
            DB::statement(DB::raw('set @rownumber='.$offset.''));
            $invoices = DB::table('invoices')->where('user_id','=', $user_id)->where('status','=','1');
                      
            $invoices   =  $invoices->orderby('invoices.id','desc');
            $total      =  $invoices->count();
            $invoices   =  $invoices->get();           
            
            return Response::json(array('data'=>  $invoices));
               
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
        }
    
    }
    
    public function view($id) {
    	
    	
    	if(Auth::user()->role == 1) 
		{
	    		try
			{ 
				/*
				$allusers       =  DB::table('users')->where('id','<>', 0)->get();
				$user_details   =  array();
				$user_count     =  count($allusers);
				
				if($user_count > 0){
					foreach($allusers as $key => $user){
					
						$walletBalance      =   WalletBalance::where('user_id',$user->id)->first();	        
					        $walletamount       =   $walletBalance['amount'];
					        $user_details[]     =   array('user_id' => $user->id,'email' => $user->email,'wallet_amount' => $walletamount) ;	        
					}
				}
				*/
				
				// Fetching Invoice details of an invoice with id $id
				$invoice = Invoice::find($id);
				
				if(empty($invoice)) {
					return back()->with('status', 'No such invoice found! PLease try again Later.');
				}
				$first_name = User::select('fname')->where('id',$invoice->user_id)->first();
				$last_name = User::select('lname')->where('id',$invoice->user_id)->first();
				
				// Fetching Invoice Product details of an invoice with id $invoice->id
				$invoice_product = InvoiceProduct::where('invoice_id', $invoice->id)->first();
				
				if(empty($invoice_product)) {
					return back()->with('status', 'No such invoice product found! PLease try again Later.');
				}
				
				$product_details_json = $invoice_product->product_details;
				
				// Converting product details json to array
				$product_details_arr = json_decode($product_details_json, true);
				
				return view('billing.billing-view',compact('invoice','invoice_product','product_details_arr','first_name','last_name'));			
			}
			catch(\Illuminate\Database\QueryException $ex)
			{
			return (json_encode(array('status',$ex->getMessage()))) ;
			}    		
	    	} 
	    	else if(Auth::user()->role == 0) {
	    		return back();
	    		
	    	}
	    	else if(Auth::user()->role == 2) {
        	    return back();
        	    
        	}
	    	else {
	    		return view('/'); 
	        }
    }
}
