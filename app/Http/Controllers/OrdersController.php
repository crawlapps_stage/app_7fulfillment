<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use DB;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\Shop;
use App\Order;

class OrdersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    public function index()
    {

        if(Auth::user()->role == 1)
        {
            return view('orders.index');
        }
        else if(Auth::user()->role == 0)
        {
            return back();
        }
        else if(Auth::user()->role == 2)
        {
            return back();
        }
        else {
            return view('/');
        }
    }

    public function show()
    {
        try
        {
            $offset = 0;
            DB::statement(DB::raw('set @rownumber='.$offset.''));
            $id = Auth::user()->id;
            $shop = Shop::where('user_id',$id)->get();

            if($shop->isEmpty()){
                return 'no orders';
            }
            $data = [];
            foreach($shop as $shop) {
                $data['orders'] = Order::where('shop_id','=',$shop->id)->orderBy('order_date','desc')->get()->toArray();
                $data['shop'] = $shop;
                $data_val[] = $data;
            }
            $data = [];
            foreach($data_val as $key => $val){
                foreach($val['orders'] as $l_key => $l_val){
                    $temp = [];
                    $temp['id'] = $l_val['id'];
                    $temp['store_name'] = $val['shop']->shopify_domain;
                    $temp['order_number'] = '#'.$l_val['order_number'];
                    $temp['payment_status'] = ucfirst($l_val['order_status']);
                    $temp['buyer'] = $l_val['order_buyer_country']?$l_val['order_buyer_country']:"---";
                    $temp['no_of_items'] = $l_val['no_line_item'];
                    $temp['order_time'] = $l_val['order_date'];
                    $temp['shipping_status'] = $l_val['shipping_status'];//vgs change
                    $data[] = $temp;
                }
            }
            return Response::json(array('data'=>  $data));
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
        }
    }
}
