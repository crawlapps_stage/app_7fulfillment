<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use Mail;

class MailController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
   public function basic_email() {
   
      $data = array('name'=>"Ashutosh",'token'=>Auth::user()->verifyToken);
   
      Mail::send(['text'=>'mail'], $data, function($message) {
         $message->to('notifications@7fulfillment.com', 'Mail check')->subject
            ('Laravel Basic Testing Mail');
         $message->from('notifications@7fulfillment.com','7fulfillment');
      });
      
      return back()->with('status', 'Verification Email Sent. Check your inbox.');
      
      //echo "Basic Email Sent. Check your inbox.";
   }
   
   public function html_email() {
      $data = array('name'=>"Ashutosh");
      Mail::send('mail', $data, function($message) {
         $message->to('notifications@7fulfillment.com', 'Mail check')->subject
            ('Laravel HTML Testing Mail');
         $message->from('notifications@7fulfillment.com','7fulfillment');
      });
      return back()->with('status', 'Verification Email Sent. Check your inbox.');
   }
   
   public function verify() {
   }
   
   /*public function attachment_email() {
      $data = array('name'=>"Virat Gandhi");
      Mail::send('mail', $data, function($message) {
         $message->to('abc@gmail.com', 'Tutorials Point')->subject
            ('Laravel Testing Mail with Attachment');
         $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
         $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
         $message->from('xyz@gmail.com','Virat Gandhi');
      });
      echo "Email Sent with attachment. Check your inbox.";
   }
   */
}
