<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\Inventory;
use DB;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\InventoryActivity;
use App\ShippingProvider;

class InventoryController extends Controller
{
    /**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() {
	
	if(Auth::user()->role == 1) {
		return view('inventory.index');
		
	} 
	else if(Auth::user()->role == 0) {
		return back();
		
	}
	else if(Auth::user()->role == 2) {
	    return back();
	    
	}
	else {
		return view('/'); 
	}
	    
	}

        public function show() {
		
		try
		{				 
			$offset = 0;			
			
			DB::statement(DB::raw('set @rownumber='.$offset.''));
			$inventory = new Inventory();
                        $id = Auth::user()->id;
    
    $inventory = Inventory::leftJoin('shipping_provider','shipping_provider.id','=','inventory.shipping_provider')
			->select(DB::raw('@rownumber:=@rownumber+1 as S_No'), 'inventory.id','inventory.product_name','inventory.price','inventory.quantity','inventory.weight','inventory.sku','inventory.shipping_provider','shipping_provider.name','inventory.email','inventory.user_id')
                                     ->where(array('inventory.user_id'=>$id));
			
// 			$inventory = \DB::table('inventory')
// 			->select(DB::raw('@rownumber:=@rownumber+1 as S_No'), 'inventory.id','inventory.product_name','inventory.quantity','inventory.weight','inventory.sku','inventory.email','inventory.user_id')
//                                      ->where(array('inventory.user_id'=>$id));
			            
			$inventory = $inventory->orderby('inventory.id','asc');
			$total = $inventory->count();
			$inventory = $inventory->get();
			
			return Response::json(array('data'=>  $inventory));
		       
		}
		catch(\Illuminate\Database\QueryException $ex)
		{
			return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
		}
	}
	
	public function activity() {
		
		try
		{				 
			$offset = 0;			
			
			DB::statement(DB::raw('set @rownumber='.$offset.''));
			$inventory_activity = new InventoryActivity();
            $id = Auth::user()->id;
			
			$inventory_activity = \DB::table('inventory_activity')
			->select(DB::raw('@rownumber:=@rownumber+1 as S_No'), 'id','product_name','activity','created_at')
                                     ->where(array('user_id'=>$id));
			            
			$inventory_activity = $inventory_activity->orderby('id','desc');
			$total = $inventory_activity->count();
			$inventory_activity = $inventory_activity->get();	
					
			
			return Response::json(array('data'=>  $inventory_activity));
		       
		}
		catch(\Illuminate\Database\QueryException $ex)
		{
			return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
		}
	
	}
}
