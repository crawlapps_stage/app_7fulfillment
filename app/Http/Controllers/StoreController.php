<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\shopmanagement;
use Shopify;
use App\Shop;
use DB;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use Mail;



class StoreController extends Controller
{
    
    // protected $shop = "gautamteststore.myshopify.com";
    protected  $shop = "";
    protected $foo;
    protected $scopes = ['read_products','read_themes','read_orders'];
  
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() 
	{
    	if(Auth::user()->role == 1) 
    	{
    		return view('store.index');
    	} 
    	else if(Auth::user()->role == 0) 
    	{
    		return back();
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else 
    	{
    		return view('/'); 
    	}
	}
	
	public function show() 
	{
	    try
		{				 
			$offset = 0;			
			
			DB::statement(DB::raw('set @rownumber='.$offset.''));
			$shops = new Shop();			
			
			$shops = \DB::table('shops')           
					->select(DB::raw('@rownumber:=@rownumber+1 as S_No'), 'shops.*')
					->where('user_id','=',Auth::user()->id);
			            
			$shops = $shops->orderby('shops.id','desc');
			$total = $shops->count();
			$shops = $shops->get();			
			
			return Response::json(array('data'=>  $shops));
		       
		}
		catch(\Illuminate\Database\QueryException $ex)
		{
			return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
		}
	}
	
	public function destroy(Request $request)
	{
		try
		{
			if($request->ajax())
			{
				if($request->id){
					  //$shop = Shop::find($request->id);
					 if( DB::table('shops')->where('id', '=', $request->id)->delete())
				// 	  if($shop->delete())
					    return ('Shop deleted successfully');
				}
				return (json_encode(array('status','No such record found')));
			}
		}
		catch(\Illuminate\Database\QueryException $ex)
		{
		return (json_encode(array('status',$ex->getMessage()))) ;
		}
	}
	
	public function getPermission(Request $request) {
	    
        $store      =   $request->store;
        $ext        =   '.myshopify.com';
        $shopName   =   $store.$ext;
        $store_shop = shopmanagement::select('shop_url','id','access_token')->where('shop_url',$shopName)->first();
        
        if(empty($store_shop)) 
        {
            $ext            =   '.myshopify.com';
            $this->shop     =   $store.$ext;
            $this->foo      =   Shopify::make($this->shop, $this->scopes);
            return $this->foo->redirect();    
        }
        else
        {
            //return response()->json( array('success' => true, 'data'=>'Store Already exist') );
            return back()->with('status', 'Store Already exist' );    
        }    
	}
	
	public function getResponse(Request $request) {
	   
        $query = array(
            "client_id"     => '0c4e2fe08400cfdbe65ec7e0e4311578', // Your API key
            "client_secret" => '4f5582e36813f0b5e72e8e74a1047dd5', // Your app credentials (secret key)
            "code"          =>  $_GET['code'] // Grab the access key from the URL
        );
        $shopName         =   $_GET['shop'];
        
        // Generate access token URL
        
        $access_token_url = "https://" . $shopName . "/admin/oauth/access_token";
        
        // Configure curl client and execute request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $access_token_url);
        curl_setopt($ch, CURLOPT_POST, count($query));
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
        $result = curl_exec($ch);
        curl_close($ch);
        
        // Store the access token
        $result       = json_decode($result, true);
        
        $access_token = $result['access_token']; //store access token in database for future call
        
        $store_shop = new shopmanagement();
        $store_shop->shop_url=$shopName;
        $store_shop->access_token=$access_token;      
        $store_shop->save();
	}
}
