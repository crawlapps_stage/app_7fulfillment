<?php
namespace App\Http\Controllers\Admin\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

use App\Http\Controllers\Controller;

use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\WalletBalance;
use Illuminate\Support\Facades\Auth;
use DB;
use Mail;

class UserController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() {
	   
	   	if(Auth::user()->role == 0) {
    			return view('admin.users.user');
    		
	    	} 
	    	else if(Auth::user()->role == 1) {
	    		return back();
	    		
	    	}
	    	else if(Auth::user()->role == 2) {
        	    return back();
        	    
        	}
	    	else {
	    		return view('/'); 
	        }
	}
		
	public function show() {
		
		try
		{				 
			$offset = 0;			
			
			DB::statement(DB::raw('set @rownumber='.$offset.''));
			$user = new User();			
			
			$user = \DB::table('users')
					->leftJoin('wallet_balance', 'users.id', '=', 'wallet_balance.user_id')            
					->select(DB::raw('@rownumber:=@rownumber+1 as S_No'), 'users.id','users.fname','users.lname',
								'users.email','users.skype','users.country','users.approve','wallet_balance.amount')
					->where(array('users.role'=>'1'));
			            
			$user = $user->orderby('users.id','desc');
			$total = $user->count();
			$user = $user->get();			
			
			return Response::json(array('data'=>  $user));
		       
		}
		catch(\Illuminate\Database\QueryException $ex)
		{
			return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
		}
	
	}

      public function delete(Request $request) 
    {
	print_r($request->all());die;  	
			
	$user = user::where('id',$id)->first();
			
	if(!empty($user)) {
		
		$wallet = user::find($id);
		
		$wallet->payment_status = 'success';
				
		
		if($wallet->delete()) {
			return back();
			//return back()->with('status', 'Payment Not Approved.');
		} else {
			return back()->with('status', 'Deleted successfully');
		}
		
	
	} 
	
    }
     
	public function destroy(Request $request)
	{		
		try
		{
			if($request->ajax())
			{
				if($request->id){
					  $user = User::find($request->id); 
					  if($user->delete())
					    return ('User deleted successfully');
				}
				return (json_encode(array('status','No such record found')));
			}
		}
		catch(\Illuminate\Database\QueryException $ex)
		{
		return (json_encode(array('status',$ex->getMessage()))) ;
		}
	}
	
	public function edit($id)
	{
		if(Auth::user()->role == 0) {
	    		try
			{
				$user = User::find($id);
				
				$amount = WalletBalance::where('user_id',$id)->first();				
				if(empty($user)) {
				 return back()->with('status', 'No such user found! PLease try again Later.');
				}				
				return view('admin.users.edit', compact('user','amount'));			
			}
			catch(\Illuminate\Database\QueryException $ex)
			{
			return (json_encode(array('status',$ex->getMessage()))) ;
			}    		
	    	} 
	    	else if(Auth::user()->role == 1) {
	    		return back();
	    		
	    	}
	    	else if(Auth::user()->role == 2) {
        	    return back();
        	    
        	}
	    	else {
	    		return view('/'); 
	        }			
		
	}
	
	public function store(Request $request) {

                $approveValue = $request->approveValue;
                $approve = $request->approve;			
		
		$user = User::find($request->id);
		if(empty($user)) {
			return back()->with('status', 'No such user found! PLease try again Later.');
		}	        
	    	$user->fname 				= 	$request->fname;
	    	$user->lname 				= 	$request->lname;
	    	$user->email 				= 	$request->email;
	    	$user->skype 				= 	$request->skype;
	    	$user->phone 				= 	$request->phone;
	    	$user->country 				= 	$request->country;
	    	$user->state 				= 	$request->state;
	    	$user->city 				= 	$request->city;
	    	$user->business_name 		        = 	$request->business_name;
	    	$user->business_address 	        = 	$request->business_address;
	    	$user->business_phone 		        = 	$request->business_phone;
	    	$user->platform 			= 	$request->platform;
	    	$user->storeruntime 		        = 	$request->storeruntime;
	    	$user->ordersno 			= 	$request->ordersno;
	    	$user->productsno 			= 	$request->productsno;
	    	$user->ordersperday 		        = 	$request->ordersperday;
                $user->approve 				= 	$request->approve;
	    	if($user->save()) {
	    		
	    		$walletBalance = WalletBalance::where('user_id',$request->id)->first();
	    		if(!empty($walletBalance)) {
	    			$wallet = WalletBalance::find($walletBalance->id);
	    			$wallet->amount = $request->walletAmount;
	    			$wallet->save();
	    			
	    		} else {
	    			$wallet = new WalletBalance();
	    			$wallet->user_id = $request->id;
	    			$wallet->amount = $request->walletAmount;
	    			$wallet->status = '1';
	    			$wallet->save();
	    		}

			if (($approveValue == '0') && ($approve == '1')) {
			  $email    =   $user->email;
			  $data     =   array('email'=>$email,'name' => $request->fname);
			  Mail::send('email.approveUser', $data, function ($m) use ($data) {
			    $m->from('notifications@7fulfillment.com', '7fulfillment');
			    $m->to($data['email'])->subject('Your account has been approved . Please login from here.');
			  });
			}
	    		
	    		return redirect('/admin/users')->with('status', 'User updated!');
	    	}
	        else {
	        	return back()->with('status', 'User Not updated! PLease try again Later.');
	        }
	
	}
	
}

