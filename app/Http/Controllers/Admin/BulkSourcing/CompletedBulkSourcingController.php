<?php
namespace App\Http\Controllers\Admin\BulkSourcing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\WalletPayment;
use DB;
use App\BulkSourcing;

use App\Http\Controllers\Controller;

class CompletedBulkSourcingController extends Controller
{

	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
    public function index() {
    
    	if(Auth::user()->role == 0) {
    		return view('admin.BulkSourcing.completed-BulkSourcing');
    		
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }
    }

    public function show() 
    {
    	try
    	{	
    		$offset = 0;
    				
    		DB::statement(DB::raw('set @rownumber='.$offset.''));
    		
    		$bulkSourcing = new BulkSourcing();				
    		
    		$bulkSourcing = $bulkSourcing
    		          ->join('users', 'bulk_sourcing.user_id', '=', 'users.id')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'bulk_sourcing.id','bulk_sourcing.q_link','bulk_sourcing.pieces','bulk_sourcing.shipping_location','bulk_sourcing.time_frame','bulk_sourcing.quote','bulk_sourcing.status','bulk_sourcing.quote_approve','bulk_sourcing.user_id','bulk_sourcing.created_at','users.fname','users.lname','users.email')	          
    		           ->where(array('bulk_sourcing.status'=>'1'));
    		$bulkSourcing = $bulkSourcing->orderby('bulk_sourcing.created_at','desc');
    		$total = $bulkSourcing->count();
    		
    		$bulkSourcing = $bulkSourcing->get();
    		
    		
    		return Response::json(array('data'=> $bulkSourcing));
    	       
    	}
    	catch(\Illuminate\Database\QueryException $ex)
    	{
    		return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
    	}
    }
}
