<?php

namespace App\Http\Controllers\Admin\ExchangeRate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\ExchangeRate;
use Illuminate\Support\Facades\Validator;

class ExchangeRateController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
    public function index() {
    
    	if(Auth::user()->role == 0) {
    	    $exchange_rate = ExchangeRate::first();
    		return view('admin.exchangeRate.index', compact('exchange_rate'));
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }	
    }
    
    public function store(Request $request) {
        $rules = array(
            'usd_value' =>  'required'
            );
            
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return back()->withErrors($validator)
                        ->withInput();    
        }
        $exchange_rate = ExchangeRate::first();
        if(!empty($exchange_rate)){
            $id = $exchange_rate->id;
            $exchange = ExchangeRate::find($id);
            
            //$exchange->usd_value = $request->usd_value;
            
        } else {
            $exchange = new ExchangeRate();
            //$exchange->usd_value = $request->usd_value;
        }
        
        $exchange->usd_value = $request->usd_value;
        if($exchange->save()) {
            return back()->with('status','Values successfully saved!');    
        } else {
             return back()->with('status','Values not saved. Please try again Later!');
        }    
    }
}
