<?php

namespace App\Http\Controllers\Admin\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\WalletBalance;
use Illuminate\Support\Facades\Auth;
use DB;
use Mail;
use App\Inventory;
use App\ShippingProvider;

class InventoryController extends Controller
{
    /**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() {
	   
	   	if(Auth::user()->role == 0) {
    		return view('admin.inventory.index');
    		
	    	} 
	    	else if(Auth::user()->role == 1) {
	    		return back();
	    		
	    	}
	    	else if(Auth::user()->role == 2) {
    	        return back();
    	    
    	    }
	    	else {
	    		return view('/'); 
	        }
	}
		
	public function show() {
		
		try
		{				 
			$offset = 0;			
			
			DB::statement(DB::raw('set @rownumber='.$offset.''));
			$inventory = new Inventory();
			
			$inventory = Inventory::leftJoin('shipping_provider','shipping_provider.id','=','inventory.shipping_provider')
			    ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'), 'inventory.id','inventory.product_name','inventory.price','inventory.quantity','inventory.weight','inventory.sku','inventory.shipping_provider','shipping_provider.name','inventory.email','inventory.user_id');
			
// 			$inventory = \DB::table('inventory')
// 					->select(DB::raw('@rownumber:=@rownumber+1 as S_No'), 'inventory.id','inventory.product_name','inventory.quantity','inventory.weight','inventory.sku','inventory.email','inventory.user_id');
			            
			$inventory = $inventory->orderby('inventory.id','asc');

			$total = $inventory->count();
			$inventory = $inventory->get();	
				
			
			return Response::json(array('data'=>  $inventory));
		       
		}
		catch(\Illuminate\Database\QueryException $ex)
		{
			return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
		}
	
	}

    public function addInventory() {
        
     if(Auth::user()->role == 0) { 
          //$allusers       =  DB::table('users')->where('id','<>', 0)->get();
            $allusers       =  DB::table('users')->where('role','<>', 1)->get();
    	    $user_details   =  array();
            $user_count     =  count($allusers);

            if($user_count > 0){
                foreach($allusers as $key => $user){
                        $name = $user->fname.' '.$user->lname;
                        $user_details[]     =   array('email' => $user->email, 'name' => $name) ; 
                }
            }
            
            $shipping_provider = ShippingProvider::get();
	   	    if(!$shipping_provider->isEmpty()){
	   	        return view('admin.inventory.addInventory', compact('shipping_provider','user_details'));
	   	    }

            return view('admin.inventory.addInventory',compact('user_details'));
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	        return back();
    	    
    	}
    	else {
    		return view('/'); 
        }

   }

   
   public function store(Request $request) {
       
        $rules = array(
                    'product_name'  =>  'required',
                    'price'         =>  'required',
                    'quantity'      =>  'required|numeric|digits_between:0,25',
                    'weight'        =>  'required',
                    'sku'           =>  'required',
                    'shipping_provider'      =>  'required',
                    'uemail'        =>  'required|email'
                    );
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();   
        }

        $users = DB::table('users')->where('email', $request->uemail)->first();
        
        // if(empty($request->product_name) || empty($request->quantity) || empty($request->uemail)){
        //     return back()->with('status-error', 'Please fill all fields value.');    
        // }

        $count_user = count((array)$users);;

        if($count_user < 1) {
            return back()->with('status-error', 'No such user found! Please try again Later.');
        }

        $user_id                    =   $users->id;
        if($request->id) {
            
           $inventory                       =   Inventory::find($request->id);
           $inventory->product_name         =   $request->product_name;
           $inventory->price                =   $request->price;
           $inventory->quantity             =   $request->quantity;
           $inventory->weight               =   $request->weight;
           $inventory->sku                  =   $request->sku;
           $inventory->shipping_provider    =   $request->shipping_provider;
           $inventory->user_id              =   $user_id;
           $inventory->email                =   $request->uemail;
        
          if($inventory->save()) {
            return redirect('/admin/inventory')->with('status', 'Inventory Updated Successfully.');
          }
          else {
            return back()->with('status', 'Inventory Not Updated Successfully! Please try again Later.');
          }

        } else {

           $inventory               =   new Inventory();
           
           $inventory->product_name     =   $request->product_name;
           $inventory->price            =   $request->price;
           $inventory->quantity         =   $request->quantity;
           $inventory->weight           =   $request->weight;
           $inventory->sku              =   $request->sku;
           $inventory->shipping_provider    =   $request->shipping_provider;
           $inventory->user_id          =   $user_id;
           $inventory->email            =   $request->uemail;
        
           if($inventory->save()) {
           
              return redirect('/admin/inventory')->with('status', 'Inventory Saved Successfully.');
           }
           else {
              return back()->with('status', 'Inventory Not Created! Please try again Later.');
           }
        }
	
       }
		
	public function edit($id)
	{
		if(Auth::user()->role == 0) {
	    		try
			{
				$inventory = Inventory::find($id);

				$allusers       =  DB::table('users')->where('role','<>', 1)->get();
    	                        $user_details   =  array();
                                $user_count     =  count($allusers);

			        if($user_count > 0){
			                foreach($allusers as $key => $user){
			                        $name = $user->fname.' '.$user->lname;
			                        $user_details[]     =   array('email' => $user->email, 'name' => $name) ; 
			                }
			         }
				
								
				if(empty($inventory)) {
				 return back()->with('status', 'No such inventory found! PLease try again Later.');
				}
				
				$shipping_provider = ShippingProvider::get();
    	   	    if(!$shipping_provider->isEmpty()){
    	   	        return view('admin.inventory.edit', compact('inventory','shipping_provider','user_details'));
    	   	    }
				return view('admin.inventory.edit', compact('inventory','user_details'));			
			}
			catch(\Illuminate\Database\QueryException $ex)
			{
			return (json_encode(array('status',$ex->getMessage()))) ;
			}    		
	    	} 
	    	else if(Auth::user()->role == 1) {
	    		return back();
	    		
	    	}
	    	else if(Auth::user()->role == 2) {
    	        return back();
    	    
    	   }
	    	else {
	    		return view('/'); 
	        }		
	}

        public function destroy(Request $request)
	{		
		try
		{
			if($request->ajax())
			{
				if($request->id){
					  $inventory = Inventory::find($request->id); 
					  if($inventory->delete())
					    return ('Inventory deleted successfully');
				}
				return (json_encode(array('status','No such record found')));
			}
		}
		catch(\Illuminate\Database\QueryException $ex)
		{
		return (json_encode(array('status',$ex->getMessage()))) ;
		}
	}
}


  
