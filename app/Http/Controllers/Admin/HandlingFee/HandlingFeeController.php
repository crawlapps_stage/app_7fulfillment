<?php

namespace App\Http\Controllers\Admin\HandlingFee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\HandlingFee;
use Illuminate\Support\Facades\Validator;

class HandlingFeeController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
    public function index() {
    
    	if(Auth::user()->role == 0) {
    	    $handlingFee = HandlingFee::first();
    		return view('admin.handlingFee.index', compact('handlingFee'));
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }	
    }
    
    public function store(Request $request) {
        $rules = array(
            'handling_fee' =>  'required'
            );
            
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return back()->withErrors($validator)
                        ->withInput();    
        }
        $handlingFee = HandlingFee::first();
        if(!empty($handlingFee)){
            $id = $handlingFee->id;
            $handlingFee = HandlingFee::find($id);
            
            //$exchange->usd_value = $request->usd_value;
            
        } else {
            $handlingFee = new HandlingFee();
            //$exchange->usd_value = $request->usd_value;
        }
        
        $handlingFee->handling_fee = $request->handling_fee;
        if($handlingFee->save()) {
            return back()->with('status','Handling Fee successfully saved!');    
        } else {
             return back()->with('status-error','Handling Fee not saved. Please try again Later!');
        }    
    }
}
