<?php
namespace App\Http\Controllers\Admin\Photography;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\WalletPayment;
use DB;
use App\Photography;

use App\Http\Controllers\Controller;

class CompletedPhotographyController extends Controller
{

	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
    public function index() {
    
    	if(Auth::user()->role == 0) {
    		return view('admin.photography.completed-photography');
    		
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }
    }

    public function show() 
    {
    	try
    	{	
    		$offset = 0;
    				
    		DB::statement(DB::raw('set @rownumber='.$offset.''));
    		
    		$photography = new Photography();				
    		
    		$photography = $photography
    		          ->join('users', 'photography.user_id', '=', 'users.id')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'photography.id','photography.product_link','photography.product_price','photography.total_price','photography.status','photography.user_id','photography.created_at','users.fname','users.lname','users.email')	          
    		           ->where(array('photography.status'=>'1'));
    		$photography = $photography->orderby('photography.created_at','desc');
    		$total = $photography->count();
    		
    		$photography = $photography->get();
    		
    		
    		return Response::json(array('data'=> $photography));
    	       
    	}
    	catch(\Illuminate\Database\QueryException $ex)
    	{
    		return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
    	}
    }
}
