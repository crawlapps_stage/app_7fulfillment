<?php

namespace App\Http\Controllers\Admin\Photography;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\WalletPayment;
use App\WalletBalance;
use DB;
use Mail;
use App\RecentActivities;
use App\Photography;

use App\Http\Controllers\Controller;

class PendingPhotographyController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
    public function index() {
    	
    	if(Auth::user()->role == 0) {
    		return view('admin.photography.pending-photography');
    		
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }
    }
    
    public function show() 
    {
    	try
    	{	
    		$offset = 0;		
    		DB::statement(DB::raw('set @rownumber='.$offset.''));
    		
    		$photography = new Photography();				
    		
    		$photography = $photography
    		          ->join('users', 'photography.user_id', '=', 'users.id')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'photography.id','photography.product_link','photography.product_price','photography.total_price','photography.status','photography.user_id','photography.created_at','users.fname','users.lname','users.email')
    		          ->where(array('photography.status'=>'0'));
    		
    		$photography = $photography->orderby('photography.created_at','desc');
    		$total = $photography->count();
    		
    		$photography = $photography->get();
    		
    		return Response::json(array('data'=> $photography));
    	       
    	}
    	catch(\Illuminate\Database\QueryException $ex)
    	{
    		return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
    	}
    }
    
    public function approveRequest(Request $request) {
    		
	try
	{
		if($request->ajax())
		{
			if($request->id){
				
				$photography = Photography::where('id',$request->id)->first();
				
				if(!empty($photography)) {
					
					$photography = Photography::find($request->id);
					
					$photography->status = 1;		
					
					if($photography->save()) {
						
					    //Email code start
					    $user  = User::where('id',$photography->user_id)->first();
		    			$customer_email =  $user->email;
		    			$cmailbody 		=  'Photography request has been completed.';
		    			$product_link   =   $photography->product_link;

		    			//$getadmin 		=  	DB::table('users')->where('id','=',$walletPayment->user_id)->first();
		    			//$getadmin		=	(array)$getadmin;
				        // 	$customerName		=  	$getadmin['fname'].' '.$getadmin['lname'];
			        	$customerName		=  	$user->fname.' '.$user->lname;
			        	
			        	$balance_id = WalletBalance::select('id','amount')->where('user_id',$photography->user_id)->get()->first();
						
						if(!empty($balance_id)) {
		        	
			        			$balance_amount = $balance_id->amount;		        	
				        }

		    			$data     		=   array(
	    									'customer_email'	=>	$customer_email,
	    									'customer_name' 	=> 	$customerName,
	    									'mail_body'		    =>	$cmailbody,
	    									'wallet_amount'		=>	$balance_amount,
	    									'product'           =>  $product_link,
	    									
		    							); 

		    			//send mail to customers
		    	 		Mail::send('email.walletapprove', $data, function ($m) use ($data) {
	                        $m->from('notifications@7fulfillment.com', '7fulfillment');
	                        $m->to($data['customer_email'])->subject('Photography request has been completed');
	                    });
		    	 		//Email code end

						return ('Request Phototgraphy completed successfully');
						
					} else {
						return ('Request Phototgraphy Not completed. Please try again Later');
					}									
				}				   				  
			}
			return ('No such record found');
		}
	}
	catch(\Illuminate\Database\QueryException $ex)
	{
	return (json_encode(array('status',$ex->getMessage()))) ;
	}
    }
}
