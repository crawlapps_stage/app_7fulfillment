<?php

namespace App\Http\Controllers\Admin\SubadminInvoice;
// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Response;
// use Validator;
// use URL;
// use Session;
// use Redirect;
// use Input;
// use App\User;
// use Illuminate\Support\Facades\Auth;
// use App\WalletPayment;
// use App\WalletBalance;
// use DB;
// use Mail;
// use App\RecentActivities;
// use App\Photography;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\WalletBalance;
use DB;
use Invoice;

use App\Http\Controllers\Controller;

class PendingInvoiceController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
    public function index() {
    	
    	if(Auth::user()->role == 0) {
    		return view('admin.subadmininvoice.pending-invoice');
    		
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }
    }
    
    public function show() 
    {
    	try
        {                
            $offset = 0;            
            
            DB::statement(DB::raw('set @rownumber='.$offset.''));
          // $invoices = new invoiceCreate();
           
          $invoices = DB::table('invoices')
            ->join('users', 'invoices.user_id', '=', 'users.id')
            ->select('invoices.*', 'users.fname', 'users.lname')
            ->where('invoices.status','=', 0);         
            
                      
            $invoices   =  $invoices->orderby('invoices.id','desc');
            $total      =  $invoices->count();
            $invoices   =  $invoices->get();           
            
            return Response::json(array('data'=>  $invoices));
               
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
        }
    }
    
    
    public function approveRequest(Request $request) {
    		
	try
	{
		if($request->ajax())
		{
			if($request->id){
				
				$invoice = Invoice::where('id',$request->id)->first();
				
				if(!empty($invoice)) {
					
					$invoice = Invoice::find($request->id);
					
					$invoice->status = 1;		
					
					if($invoice->save()) {
					    
					    /*
            	$wallet = WalletBalance::find($walletBalance->id);
            	$wallet->amount = $walletBalance->amount - $invoice->total_product_cost;
            	
            	//check for wallet minimum amount 
            	$row_get                   =   DB::table('walletlimitamount')->where('id','<>', 0)->get();
            	$count_row                 =   count($row_get);
            	$wallet_minimum_amount     =   ($count_row > 0) ? $row_get[0]->wallet_minimum_amount : 0;//check for minimum balance
            	$username                  =   $fname.' '.$lname;
            	
            	if($wallet->amount < $wallet_minimum_amount)
            	{
            	    $email    =   $invoice->email; 
            	    //$email    =   'dineshkashera5@gmail.com';//test amount remove in future
            	    $data     =   array('email'=>$email,'wallet_amount' => $wallet->amount,'name' => $username,'wallet_minimum_amount' => $wallet_minimum_amount  ); 
            	
            	    Mail::send('email.walletminimum', $data, function ($m) use ($data) 
            	    {
            	        $m->from('notifications@7fulfillment.com', '7fulfillment');
            	        $m->to($data['email'])->subject('Warning!!! Wallet Balance Low.');
            	    });
            	}
            	//wallet minimum amount End
            	$wallet->save();
            	
            	*/
						
				// 	    //Email code start
				// 	    $user  = User::where('id',$photography->user_id)->first();
		  //  			$customer_email =  $user->email;
		  //  			$cmailbody 		=  'Your Invoice has been approved.';

		  //  			//$getadmin 		=  	DB::table('users')->where('id','=',$walletPayment->user_id)->first();
		  //  			//$getadmin		=	(array)$getadmin;
				//         // 	$customerName		=  	$getadmin['fname'].' '.$getadmin['lname'];
			 //       	$customerName		=  	$user->fname.' '.$user->lname;
			        	
			 //       	$balance_id = WalletBalance::select('id','amount')->where('user_id',$photography->user_id)->get()->first();
						
				// 		if(!empty($balance_id)) {
		        	
			 //       			$balance_amount = $balance_id->amount;		        	
				//         }

		  //  			$data     		=   array(
	   // 									'customer_email'	=>	$customer_email,
	   // 									'customer_name' 	=> 	$customerName,
	   // 									'mail_body'		    =>	$cmailbody,
	   // 									'wallet_amount'		=>	$balance_amount
		  //  							); 

		  //  			//send mail to customers
		  //  	 		Mail::send('email.walletapprove', $data, function ($m) use ($data) {
	   //                     $m->from('notifications@7fulfillment.com', '7fulfillment');
	   //                     $m->to($data['customer_email'])->subject('Request Phototgraphy has been approved on 7fulfillment');
	   //                 });
		  //  	 		//Email code end

						return ('Invoice Approved successfully');
						
					} else {
						return ('Invoice Not Approved. Please try again Later');
					}									
				}				   				  
			}
			return ('No such record found');
		}
	}
	catch(\Illuminate\Database\QueryException $ex)
	{
	return (json_encode(array('status',$ex->getMessage()))) ;
	}
    }
}
