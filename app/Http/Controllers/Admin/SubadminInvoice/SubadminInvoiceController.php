<?php

namespace App\Http\Controllers\Admin\SubadminInvoice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\WalletBalance;
use DB;

class SubadminInvoiceController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
    public function index() {
    
    if(Auth::user()->role == 0) {
    		return view('admin.subadmininvoice.index');
    		
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }
    
    
    }

    // public function show() {
        
    //     try
    //     {                
    //         $offset = 0;            
            
    //         DB::statement(DB::raw('set @rownumber='.$offset.''));
    //       // $invoices = new invoiceCreate();
           
    //       $invoices = DB::table('invoices')
    //         ->join('users', 'invoices.user_id', '=', 'users.id')
    //         ->select('invoices.*', 'users.fname', 'users.lname')
    //         ->where('invoices.id','<>', 0);         
            
    //         /*$invoices = DB::table('invoices')
    //               // ->where(array('invoices.id'=>'5'));
    //         ->where('id','<>', 0);
    //         */          
    //         $invoices   =  $invoices->orderby('invoices.id','desc');
    //         $total      =  $invoices->count();
    //         $invoices   =  $invoices->get();           
            
    //         return Response::json(array('data'=>  $invoices));
               
    //     }
    //     catch(\Illuminate\Database\QueryException $ex)
    //     {
    //         return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
    //     }
    
    // }
}

