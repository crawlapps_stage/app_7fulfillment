<?php
namespace App\Http\Controllers\Admin\SubAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

use App\Http\Controllers\Controller;

use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use App\WalletBalance;
use Illuminate\Support\Facades\Auth;
use DB;
use Mail;
use Hash;
use Crypt;

class SubAdminController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
	public function index() {
	   	if(Auth::user()->role == 0) {
    			return view('admin.subadmin.index');
    		
	    	} 
	    	else if(Auth::user()->role == 1) {
	    		return back();
	    		
	    	}
	    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
	    	else {
	    		return view('/'); 
	        }
	}
	
	public function add() {
	    if(Auth::user()->role == 0) {
    			return view('admin.subadmin.add');
    		
	    	} 
	    	else if(Auth::user()->role == 1) {
	    		return back();
	    		
	    	}
	    	else if(Auth::user()->role == 2) {
    	        return back();
    	    
    	    }
	    	else {
	    		return view('/'); 
	        }
	}
	
	public function store (Request $request) {
	     
	    if($request->id){
	        
	        $rules = array(
	            'fname' => 'required',
	            'lname' => 'required',
	            'email'   =>  'required|email',
	        );
    	        
    	    $validator = Validator::make($request->all(), $rules);
            
            if ($validator->fails()) {
                
                return back()->withErrors($validator)->withInput();
            }
            
            // Check email exist or not
            
            $subadmin = User::find($request->id);
            $email_check = User::select('id')->where('email',$request->email)->get();
            $count = $email_check->count();
            if($count >= 1 ) {
                foreach($email_check as $email_check_id) {
                    if($email_check_id->id != $request->id) {
                        return back()->with('status', 'Email already existed!');
                    }  
                }   
            }
            
            // End check email exist or not
            
            $subadmin->fname = $request->fname;
            $subadmin->lname = $request->lname;
    	    $subadmin->email = $request->email;
    	    if($subadmin->save()) {
    	        return back()->with('status', 'SubAdmin updated successfully!');
    	    }
    	    else {
     	        return back()->with('status', 'SubAdmin not updated. Please try again later.');
    	    }
	        
	        
	    } else {
	        $rules = array(
	            'fname' => 'required',
	            'lname' => 'required',
	            'email'   =>  'required|unique:users|email',
                'password'    =>  ['required', 'string', 'confirmed'],
                'password_confirmation'   =>  'required',
	        );
	        
    	    $validator = Validator::make($request->all(), $rules);
            
            if ($validator->fails()) {
                
                return back()->withErrors($validator)->withInput();
            }
            
            $subadmin = new User();
            $subadmin->fname = $request->fname;
            $subadmin->lname = $request->lname;
    	    $subadmin->email = $request->email;
            $subadmin->password = Hash::make($request->password);
    	    $subadmin->status = '1';
    	    $subadmin->role = '2';
            $subadmin->approve = '1';
            if($subadmin->save()) {
                
			  $email    =   $request->email;
			  $data     =   array('email'=>$email,'name' => $request->fname);
			  Mail::send('email.subadmin', $data, function ($m) use ($data) {
			    $m->from('notifications@7fulfillment.com', '7fulfillment');
			    $m->to($data['email'])->subject('Your account has been created . Please login from here.');
			  });
			
    	        return back()->with('status', 'SubAdmin created successfully!');
    	    }
    	    else {
     	        return back()->with('status', 'SubAdmin not created. Please try again later.');
    	    }   
	    }   
	}
	
	public function show() {
	    try
		{				 
			$offset = 0;			
			
			DB::statement(DB::raw('set @rownumber='.$offset.''));
			$subadmins = new User();
	        $subadmins = $subadmins->select(DB::raw('@rownumber:=@rownumber+1 as S_No'), 'users.*')->where('role','=','2');
	        $subadmins = $subadmins->orderby('users.id','desc');
			$total = $subadmins->count();
			$subadmins = $subadmins->get();			
			
			return Response::json(array('data'=>  $subadmins));
		}	
		catch(\Illuminate\Database\QueryException $ex)
		{
			return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
		}
	}
	
	public function edit($id) 
	{
	    if(Auth::user()->role == 0) 
	    {
 	    	try
 			{
        	    if($id)
        	    {
        	        $subadmin = User::where('role','=','2')->find($id);
        	        if(empty($subadmin)) {
        	            return back()->with('status','No such Sub Admin found!');
        	        } else {
        	            return view('admin.subadmin.add', compact('subadmin'));
        	        }
        	        
        	    } else {
        	        return back()->with('status','Server error, Please try again later!');
        	    }
 			}
			catch(\Illuminate\Database\QueryException $ex)
			{
			    return (json_encode(array('status',$ex->getMessage()))) ;
			}    		
	    } 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }
	    
	}
	
	public function destroy(Request $request) {
	    try {
	        if($request->ajax()){
	            if($request->id){
	                $subadmin = User::find($request->id);
	                if($subadmin->delete()) {
	                    return ('Sub Admin deleted Successfully.');   
	                }
	                return (json_encode(array('status','No such record found!')));
	           }    
	        }    
	    } 
	    catch(\Illuminate\Database\QueryException $ex) {
	        return (json_encode(array('status',$ex->getMessage())));   
	    }  
	}
	
}

