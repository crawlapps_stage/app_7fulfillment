<?php

namespace App\Http\Controllers\Admin\DiscountRate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\DiscountRate;
use Illuminate\Support\Facades\Validator;

class DiscountRateController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {       
        $this->middleware('isAdmin');
    }
    
    public function index() {
    
        if(Auth::user()->role == 0) {
            $discount_rate = DiscountRate::first();
            return view('admin.discountRate.index', compact('discount_rate'));
        } 
        else if(Auth::user()->role == 1) {
            return back();
            
        }
        else if(Auth::user()->role == 2) {
            return back();
            
        }
        else {
            return view('/'); 
        }   
    }
    
    public function store(Request $request) {
        $rules = array(
            'discount_value' =>  'required'
            );
            
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return back()->withErrors($validator)
                        ->withInput();    
        }
        $discount_rate = DiscountRate::first();
        if(!empty($discount_rate)){
            $id = $discount_rate->id;
            $discount = DiscountRate::find($id);
            
            //$exchange->usd_value = $request->usd_value;
            
        } else {
            $discount = new DiscountRate();
            //$exchange->usd_value = $request->usd_value;
        }
        
        $discount->discount_value = $request->discount_value;
        if($discount->save()) {
            return back()->with('status','Discount Rate successfully saved!');    
        } else {
             return back()->with('status','Discount Rate not saved. Please try again Later!');
        }    
    }
}
