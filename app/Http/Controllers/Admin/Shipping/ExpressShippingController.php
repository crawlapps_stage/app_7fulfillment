<?php

namespace App\Http\Controllers\Admin\Shipping;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\walletlimitamount;
use DB;
use App\Quotation;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Mail;
use App\MinimumInventory;
use App\Shipping_provider1;
use App\Shipping_provider2;
use App\ShippingRates;
use App\Countries;
use Illuminate\Support\Facades\Response;

class ExpressShippingController extends Controller
{
    
    /**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
    public function index() {
       
       	if(Auth::user()->role == 0) {
    		return view('admin.shipping.expressShipping');
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();    
    	}
    	else {
    		return view('/'); 
        }    
    }
    
    public function show() {
	    try
		{				 
			$offset = 0;			
			
			DB::statement(DB::raw('set @rownumber='.$offset.''));
			$providers = new ShippingRates();
			$providers = $providers->join('countries','countries.id','=','shipping_rates.country_id');
			$providers = $providers->select(DB::raw('@rownumber:=@rownumber+1 as S_No'), 'shipping_rates.*','countries.name','countries.code')->where('shipping_rates.provider_id','=','3');
	        $providers = $providers->orderby('shipping_rates.id','asc');
			$total = $providers->count();
			$providers = $providers->get();			
			
			return Response::json(array('data'=>  $providers));
		}	
		catch(\Illuminate\Database\QueryException $ex)
		{
			return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
		}
	}
	
	public function edit($id){
	    
	    if(Auth::user()->role == 0) {
	        $provider = ShippingRates::where('id','=',$id)->first();
    		return view('admin.shipping.expressShipping-edit',compact('provider'));
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }
	}

    public function store(Request $request) {

        if($request->id){
            $rules = array(
                        'price_less_than_500'    =>  'required',
                        'price_more_than_500'    =>  'required'
                        );
            $validator = Validator::make($request->all(),$rules);
            if($validator->fails()) {
                return back()->withErrors($validator)->withInput();    
            }

            $providers  =  ShippingRates::find($request->id);
            $provider_id = '3';
            $shipping_provider_name = 'Express Shipping';
            // Check country exist or not
            $country_check = ShippingRates::select('id')->where('country_id',$request->country)->where('provider_id',$provider_id)->get();
            $count = $country_check->count();
            if($count >= 1 ) {
                foreach($country_check as $country_check_id) {
                    if($country_check->id != $request->id) {
                        return back()->with('status', 'Express Shipping already existed!');
                    }  
                }   
            }
            // End check country exist or not
            $providers->price_less_than_500  =   $request->price_less_than_500;
            $providers->price_more_than_500  =   $request->price_more_than_500;
            
            if($providers->save()) {
                //return redirect('/admin/shipping/eParcel')->with('status', 'Details updated!');
                return back()->with('status', 'Express Shipping updated!');
            }
            else {
                return back()->with('status-error', 'Express Shipping not updated!! Please try again Later.');
            }
        }
        else {
            $rules = array(
                        'country'                =>  'required',
                        'price_less_than_500'    =>  'required',
                        'price_more_than_500'    =>  'required'
                    );
            $validator = Validator::make($request->all(),$rules);
            if($validator->fails()) {
                return back()->withErrors($validator)->withInput();    
            }

            $provider_id = '3';
            $shipping_provider_name = 'Express Shipping';

            // Check country exist or not
            $country_check = ShippingRates::select('id')->where('country_id',$request->country)->where('provider_id',$provider_id)->get();
            $count = $country_check->count();
            if($count >= 1 ) {
                return back()->with('status', 'Express Shipping already existed!');  
            }
            // End check country exist or not

            $providers  =  new ShippingRates();
            
            $providers->shipping_provider_name  =   $shipping_provider_name;
            $providers->provider_id             =   $provider_id;
            $providers->price_less_than_500  =   $request->price_less_than_500;
            $providers->price_more_than_500  =   $request->price_more_than_500;
            $providers->country_id              =   $request->country;
            
            
            if($providers->save()) {
                return back()->with('status', 'Express Shipping saved Successfully!');
            }
            else {
                return back()->with('status-error', 'Express Shipping not saved!! Please try again Later.');
            }
        }   
    }
    
    public function delete(Request $request) {
	    try {
	        if($request->ajax()){
	            if($request->id){
	                $providers = ShippingRates::find($request->id);
	                if($providers->delete()) {
	                    return ('Country deleted from Express-Shipping Successfully.');   
	                }
	                return (json_encode(array('status','No such record found!')));
	           }    
	        }    
	    } 
	    catch(\Illuminate\Database\QueryException $ex) {
	        return (json_encode(array('status',$ex->getMessage())));   
	    }  
	}

    public function add() {
       if(Auth::user()->role == 0) {
            $countries = Countries::get();
            if(!$countries->isEmpty()){
              return view('admin.shipping.expressShipping-create',compact('countries'));
            }
            return view('admin.shipping.expressShipping-create');
        } 
        else if(Auth::user()->role == 1) {
            return back();
            
        }
        else if(Auth::user()->role == 2) {
            return back();
            
        }
        else {
            return view('/'); 
        } 
    }
   
}
