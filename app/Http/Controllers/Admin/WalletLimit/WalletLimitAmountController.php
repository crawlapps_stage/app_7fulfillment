<?php

namespace App\Http\Controllers\Admin\WalletLimit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\walletlimitamount;
use DB;
use App\Quotation;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Mail;


class WalletLimitAmountController extends Controller
{
    
    /**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
    public function index() {
       
       	if(Auth::user()->role == 0) {
    		$row_get        =  DB::table('walletlimitamount')->where('id','<>', 0)->get(); 
            $count_row      =  count($row_get); 
            
            if($count_row > 0){
                $wallet_minimum_amount     =   $row_get[0]->wallet_minimum_amount;
            }else{
                $wallet_minimum_amount   = '';
            }

    		return view('admin.WalletLimit.walletlimitamount', compact('wallet_minimum_amount'));
    		
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        } 
        
    }

    public function store(Request $request) {
        $rules = array(
                    'wallet_minimum_amount' =>  'required|numeric|between:0,999999.99'
                    );
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
       
        $wallet_minimum_amount =  $request->wallet_minimum_amount;

        // if(empty($wallet_minimum_amount)) {
        //     return back()->with('status-error', 'Minimum amount should not be empty');
        // }

        $minimumamount  =  new walletlimitamount();
        $minimumamount->wallet_minimum_amount  =   $wallet_minimum_amount;

        $row_get        =  DB::table('walletlimitamount')->where('id','<>', 0)->get();
        $count_row      =  count($row_get);
        
        if($count_row > 0){
                $row_id     =   $row_get[0]->id;
                $data['wallet_minimum_amount'] = $wallet_minimum_amount;  
                DB::table('walletlimitamount')->where("id", $row_id)->update($data);
                return redirect('/admin/wallet/limitamount')->with('status', 'Amount Updated!');
        }else{
            if($minimumamount->save()) {
                return redirect('/admin/wallet/limitamount')->with('status', 'Amount Added!');    
            }
            else {
                return back()->with('status-error', 'Minimum Amount can not be Added! Please try again Later.');
            }    
        }
    }
   
}
