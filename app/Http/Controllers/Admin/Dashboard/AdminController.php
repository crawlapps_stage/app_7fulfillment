<?php

namespace App\Http\Controllers\Admin\Dashboard;

//namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
	$this->middleware('isAdmin');
	
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	if(Auth::user()->role == 0) {
    		return view('admin.home');
    		
    	} 
    	else if(Auth::user()->role == 1) {
    		return redirect('/home');
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return view('subadmin.subadminhome');
    	    
    	}
    	else {
    		return view('/'); 
        }
    	
    	//return view('admin.home');
    	 
        
    }
}
