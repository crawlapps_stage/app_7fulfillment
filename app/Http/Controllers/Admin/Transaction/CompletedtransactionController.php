<?php
namespace App\Http\Controllers\Admin\transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\WalletPayment;
use DB;

use App\Http\Controllers\Controller;

class CompletedtransactionController extends Controller
{

	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
    public function index() {
    
    	if(Auth::user()->role == 0) {
    		return view('admin.transaction.completed-transaction');
    		
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }
    
    
    }

    public function show() {
    	try
	{	
		$offset = 0;
		// if($page=='1')
		// {
		//   $offset = 0;
		// }
		// else{
		//   $offset = ($page-1)*$perpage;
		// }
				
		DB::statement(DB::raw('set @rownumber='.$offset.''));
		
		$walletPayment = new WalletPayment();				
		
		$walletPayment = $walletPayment
		          ->join('users', 'wallet_payment.user_id', '=', 'users.id')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'wallet_payment.txn_id','wallet_payment.payment_status','wallet_payment.amount_without_fee','wallet_payment.amount','wallet_payment.payment_method','wallet_payment.id','wallet_payment.created_at','users.fname','users.lname','wallet_payment.email','wallet_payment.proof_of_payment')	          ->whereNotIn('wallet_payment.payment_status', array('pending'));
		          
		         
		
		$walletPayment = $walletPayment->orderby('wallet_payment.id','desc');
		$total = $walletPayment->count();
		
		$walletPayment = $walletPayment->get();
		
		
		return Response::json(array('data'=> $walletPayment));
	       
	}
	catch(\Illuminate\Database\QueryException $ex)
	{
		return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
	}
    }
}
