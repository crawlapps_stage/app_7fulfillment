<?php

namespace App\Http\Controllers\Admin\transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\WalletPayment;
use App\WalletBalance;
use DB;
use Mail;
use App\RecentActivities;

use App\Http\Controllers\Controller;

class PendingtransactionController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
    public function index() {
    	
    	if(Auth::user()->role == 0) {
    		return view('admin.transaction.pending-transaction');
    		
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        }
    
    
    }
    
    public function show() 
    {
    	try
	{	
		$offset = 0;		
		DB::statement(DB::raw('set @rownumber='.$offset.''));
		
		$walletPayment = new WalletPayment();				
		
		$walletPayment = $walletPayment
		          ->join('users', 'wallet_payment.user_id', '=', 'users.id')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'wallet_payment.txn_id','wallet_payment.payment_status','wallet_payment.amount_without_fee','wallet_payment.amount','wallet_payment.payment_method','wallet_payment.id','wallet_payment.created_at','users.fname','users.lname','wallet_payment.email','wallet_payment.proof_of_payment')
		          ->where(array('wallet_payment.payment_status'=>'pending'));
		
		$walletPayment = $walletPayment->orderby('wallet_payment.created_at','desc');
		$total = $walletPayment->count();
		
		$walletPayment = $walletPayment->get();
		
		return Response::json(array('data'=> $walletPayment));
	       
	}
	catch(\Illuminate\Database\QueryException $ex)
	{
		return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
	}
    }
    
    public function approvePayment(Request $request) {
    		
	try
	{
		if($request->ajax())
		{
			if($request->id){
				
				$walletPayment = WalletPayment::where('id',$request->id)->first();
				
				
				if(!empty($walletPayment)) {
				    $payment_method=$walletPayment->payment_method;
					
					$wallet = WalletPayment::find($request->id);
					
					$wallet->payment_status = 'succeeded';		
					
					if($wallet->save()) {
					
						$balance_id = WalletBalance::select('id','amount')->where('user_id',$walletPayment->user_id)->get()->first();
						
						if(!empty($balance_id)) {
		        	
			        			$balance_amount = $balance_id['amount'] + $walletPayment->amount_without_fee;
			        	
				        	} else {
				        	
				        		$balance_amount = $walletPayment->amount_without_fee;		        	
				        	}
												
						
						if(!empty($balance_id)) {
							$balance_amount_id = $balance_id['id'];
							$balance = WalletBalance::find($balance_amount_id);				
							$balance->user_id = $walletPayment->user_id;
							$balance->amount = $balance_amount;
							$balance->status = '1';
							$balance->save();
							
						} else {
						
							$balance = new WalletBalance;
							$balance->user_id = $walletPayment->user_id;
							$balance->amount = $balance_amount;
							$balance->status = '1';
							$balance->save();
						}
						
						// Points functionality
				    		$recent_activities = new RecentActivities;
				    		$recent_activities->user_id = $walletPayment->user_id;
				    		$recent_activities->message = "$".$walletPayment->amount_without_fee." was added to your wallet";
				    		$recent_activities->save();
				        	// End Points functionality
						
					//Email code start 
		    			$customer_email =  $walletPayment->email;
		    			$cmailbody 		=  'Your '.$payment_method.' Transfer Payment amount has been approved, Please check your wallet.';

		    			$getadmin 		=  	DB::table('users')->where('id','=',$walletPayment->user_id)->first();
		    			$getadmin		=	(array)$getadmin;
					$customerName		=  	$getadmin['fname'].' '.$getadmin['lname'];

		    			$data     		=   array(
	    									'customer_email'	=>	$customer_email,
	    									'customer_name' 	=> 	$customerName,
	    									'mail_body'		=>	$cmailbody,
	    									'wallet_amount'		=>	$balance_amount
		    							);
		    			if($payment_method == 'payoneer') {
		    			    //send mail to customers
		    	 		    Mail::send('email.walletapprove', $data, function ($m) use ($data) {
    	                        $m->from('notifications@7fulfillment.com', '7fulfillment');
    	                        $m->to($data['customer_email'])->subject('Payoneer Transfer Payment has been approved on 7fulfillment');
	                        });
		    	 		    //Email code end
		    			} else {
		    			    //send mail to customers
		    	 		    Mail::send('email.walletapprove', $data, function ($m) use ($data) {
    	                        $m->from('notifications@7fulfillment.com', '7fulfillment');
    	                        $m->to($data['customer_email'])->subject('Wired Transfer Payment has been approved on 7fulfillment');
	                        });
		    	 		    //Email code end
		    			}

		    			

						return ('Payment Approved successfully');
						
					} else {
						return ('Payment Not Approved. Please try again Later');
					}									
				}				   				  
			}
			return ('No such record found');
		}
	}
	catch(\Illuminate\Database\QueryException $ex)
	{
	return (json_encode(array('status',$ex->getMessage()))) ;
	}
    }
    
    
    
    public function approve($id) 
    {  		
	$walletPayment = WalletPayment::where('id',$id)->first();						
			
	if(!empty($walletPayment)) {
		
		$wallet = WalletPayment::find($id);
		
		$wallet->payment_status = 'succeeded';
		
		if($wallet->save()) {
			return back();
		} else {
			return back()->with('status', 'Payment Not Approved. Please try again Later');
		}
	}
    }
    
}
