<?php

namespace App\Http\Controllers\Admin\MinimumInventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\walletlimitamount;
use DB;
use App\Quotation;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Mail;
use App\MinimumInventory;


class MinimumInventoryController extends Controller
{
    
    /**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{		
		$this->middleware('isAdmin');
	}
	
    public function index() {
       
       	if(Auth::user()->role == 0) {
    		$row_get        =  DB::table('minimum_inventory')->where('id','<>', 0)->get();
            $count_row      =  count($row_get); 
            
            if($count_row > 0){
                $inventory_minimum_quantity     =   $row_get[0]->inventory_minimum_quantity;
            }else{
                $inventory_minimum_quantity   = '';
            }
    		return view('admin.MinimumInventory.index', compact('inventory_minimum_quantity'));
    		
    	} 
    	else if(Auth::user()->role == 1) {
    		return back();
    		
    	}
    	else if(Auth::user()->role == 2) {
    	    return back();
    	    
    	}
    	else {
    		return view('/'); 
        } 
        
    }

    public function store(Request $request) {
        
        $rules = array(
                    'inventory_minimum_quantity'    =>  'required|numeric|digits_between:0,25'
                    );
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();    
        }
       
        $inventory_minimum_quantity =  $request->inventory_minimum_quantity;

        // if(empty($inventory_minimum_quantity)) {
        //     return back()->with('status-error', 'Minimum quantity should not be empty');
        // }

        $minimuminventory  =  new MinimumInventory();
        $minimuminventory->inventory_minimum_quantity  =   $inventory_minimum_quantity;

        $row_get        =  DB::table('minimum_inventory')->where('id','<>', 0)->get();
        $count_row      =  count($row_get);
        
        if($count_row > 0){
                $row_id     =   $row_get[0]->id;
                $data['inventory_minimum_quantity'] = $inventory_minimum_quantity;  
                DB::table('minimum_inventory')->where("id", $row_id)->update($data);
                return redirect('/admin/inventory/limitinventory')->with('status', 'Quantity Updated!');
        }else{
            if($minimuminventory->save()) {
                return redirect('/admin/inventory/limitinventory')->with('status', 'Quantity Added!');    
            }
            else {
                return back()->with('status-error', 'Minimum Quantity can not be Added! Please try again Later.');
            }    
        }
    }
   
}
