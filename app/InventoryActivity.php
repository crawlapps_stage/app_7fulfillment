<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryActivity extends Model
{
    protected $table = 'inventory_activity';
}
