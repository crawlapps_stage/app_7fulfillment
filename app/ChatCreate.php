<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatCreate extends Model
{
    //
    protected $table = 'support_chat';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ticket_id','message','reply_by_admin','reply_by_user',
    ];
}
