<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MInimumInventory extends Model
{
    protected $table = 'minimum_inventory';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'inventory_minimum_quantity',
    ];
}
