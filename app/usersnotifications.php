<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usersnotifications extends Model
{
    //
    protected $table = 'users_notification';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','read_status','messages','created_at',
    ];
}
