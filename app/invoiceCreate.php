<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invoiceCreate extends Model
{
    //
    protected $table = 'invoices';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number_of_parcel','shipping_cost','handling_fee','product_cost','total_cost','user_id','email',
    ];
}
