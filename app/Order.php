<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //

    public function country(){
        return $this->hasOne(Countries::class,'code','order_buyer_country');
    }

    public function line_item(){
        return $this->hasMany(LineItem::class,'order_id','id');
    }
}
