<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketCreate extends Model
{
    //
    protected $table = 'support_ticket';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id','user_id','user_email','subject','message','status',
    ];
}
