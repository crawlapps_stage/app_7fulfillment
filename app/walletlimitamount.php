<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class walletlimitamount extends Model
{
    //
    //
    protected $table = 'walletlimitamount';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'wallet_minimum_amount',
    ];
}
