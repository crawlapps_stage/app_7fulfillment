<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        \Log::Info("========== call console ===============");
        //$schedule->exec('cd /var/www/html/7fulfillment & php artisan queue:work')->everyMinute();
        //$schedule->exec('cd /home/zhongmin/public_html/portal && php artisan queue:work')->everyMinute();
        $schedule->exec('cd /home/zhongmin/public_html/portal && flock -n /tmp/queue /usr/local/php72/bin/php-cli artisan queue:work --stop-when-empty')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
