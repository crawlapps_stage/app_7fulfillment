<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';
    
    /**
     * Get the user for invoice.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
