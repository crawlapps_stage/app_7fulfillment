<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecentActivities extends Model
{
    protected $table = 'recent_activities';
}
