<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineItem extends Model
{
    protected $table = "line_items";

    public function order(){
        return $this->belongsTo(Order::class,'id','order_id');
    }
}
