<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingRates extends Model
{
  protected $table = 'shipping_rates';
}
