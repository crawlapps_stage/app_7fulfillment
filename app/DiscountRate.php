<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountRate extends Model
{
  protected $table = 'discount_rate';
}
