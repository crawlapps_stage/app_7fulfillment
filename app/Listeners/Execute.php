<?php

namespace App\Listeners;

use App\Events\Webhook;
use App\Inventory;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Order;
use App\LineItem;
use App\Webhook as WebhookModel;
class Execute
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Webhook  $event
     * @return void
     */
    public function handle(Webhook $event)
    {
        \Log::Info("=============== Webhook Listner ==================");
        $webhook = $event->webhook;
        \Log::Info($webhook);
        if($webhook->is_executed == 0) {
            if ($webhook->topic == 'orders/create' || $webhook->topic == 'existing-order') {
                $this->createOrder($webhook);
            } else if ($webhook->topic == 'orders/update') {
                $this->updateOrder($webhook);
            }
        }
    }

    public function createOrder($webhook) {
        $order = json_decode($webhook->data,1 );
        $customer = @$order['customer'];
        $entity = new Order;
        $entity->shop_id = $webhook->shop_id;
        $entity->order_id = $order['id'];
        $entity->order_buyer_country = @$customer['default_address']?$customer['default_address']['country_code']:NULL;
        $entity->order_number = $order['order_number'];
        $entity->order_status = $order['financial_status'];
        $entity->no_line_item = count($order['line_items']);
        $entity->order_date = date('Y-m-d h:i:s',strtotime($order['created_at']));
        $entity->customer_id = @$customer['id'];
        $entity->save();
        $this->addLineItem($order, $entity);
        //WebhookModel::where('id',$webhook->id)->update(['is_executed' => 1]);
        $webhook->is_executed = 1;
        $webhook->save();
    }

    public function updateOrder($webhook) {
        $entity = Order::where('order_id', $webhook->order_id)->first();
        if($entity){
            $order = json_decode($webhook->data,1 );
            $entity->order_status = $order['financial_status'];
            $entity->no_line_item = count($order['line_items']);
            $entity->save();

            LineItem::where('order_id', $entity->id)->delete();
            $this->addLineItem($order, $entity);
            //WebhookModel::where('id',$webhook->id)->update(['is_executed' => 1]);
            $webhook->is_executed = 1;
            $webhook->save();
        }
    }

    public function addLineItem($order, $entity){
        foreach ($order['line_items'] as $key => $val){
            $inventory = Inventory::where('sku', $val['sku'])->first();
            $lineItem = new LineItem;
            $lineItem = $this->commonLineItem($lineItem, $val, $inventory);
            $lineItem->order_id = $entity->id;
            $lineItem->save();
        }
    }

    public function commonLineItem($lineItem, $data, $inventory){
        $lineItem->shopify_order_line_item_id = $data['id'];
        $lineItem->shopify_product_id = $data['product_id'];
        $lineItem->shopify_variant_id = $data['variant_id'];
        $lineItem->product_title = $data['name'];
        $lineItem->qty = $data['quantity'];
        $lineItem->product_price = @$inventory->price?$inventory->price:NULL;
        $lineItem->sku = @$inventory->sku?$inventory->sku:NULL;
        $lineItem->weight = @$inventory->weight?$inventory->weight:NULL;
        $lineItem->shipping_provider = @$inventory->shipping_provider?$inventory->shipping_provider:NULL;
        $lineItem->status = @$inventory->sku?1:0;
        return $lineItem;
    }

}
