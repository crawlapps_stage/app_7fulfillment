<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BulkSourcing extends Model
{
    protected $table = 'bulk_sourcing';
}
