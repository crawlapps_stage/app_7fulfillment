<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WalletBalance extends Model
{
    protected $table = 'wallet_balance';
}
