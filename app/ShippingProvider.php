<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingProvider extends Model
{
  protected $table = 'shipping_provider';
}
