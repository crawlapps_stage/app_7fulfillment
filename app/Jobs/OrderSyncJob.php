<?php
namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Webhook;

class OrderSyncJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $tries = 5;
    public $timeout = 0;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $shop;
    public function __construct($shop)
    {
        \Log::Info('============ dispatch __construct ==================');
        $this->shop = $shop;
        \Log::Info(config('queue.default'));
        \Log::Info('============ dispatch __construct End==================');

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::Info('============ dispatch called ==================');
        \Log::Info(json_encode($this->shop));
        $shop = $this->shop;
        $queryString['status'] = 'any';
        //        $queryString['limit'] = 250;
        \Log::Info(config('queue.default'));
        \Log::Info('===========  1 ==================');
	if(!empty($shop->shopify_token)){
                $count = $shop->api()->rest('GET', '/admin/orders/count.json',$queryString);
        \Log::Info('===========  2 ==================');
        $queryString['limit'] = 250;

                $totalPage = abs(ceil($count->body->count / 250));
        \Log::Info('===========  3 ==================');
                $data = [];
        \Log::Info('===========  4 ==================');
                for($i=1; $i<=$totalPage; $i++){
                    \Log::Info('===========  5 ==================');
                                $queryString['page'] = $i;
                    \Log::Info('===========  loop in ==================');
                    //           \Log::Info($queryString);
                    $data250 = $shop->api()->rest('GET', '/admin/orders.json',$queryString);
                    foreach ($data250->body->orders as $key => $value) {
                        $order = json_encode($value);
                        $shopify_id = json_decode($order)->id;

                        $entity = Webhook::updateOrCreate(
                            ['shopify_id' => $shopify_id, 'topic' => 'existing-order', 'shop_id' => $shop->id],
                            ['shopify_id' => $shopify_id, 'topic' => 'existing-order', 'shop_id' => $shop->id, 'data' => $order]
                        );
                        /*$entity = Webhook::where('shopify_id',$shopify_id)->where('topic','existing-order')->where('shop_id',$shop->id)->first();
                        if($entity){
                            $entity->data = $order;
                        \Log::info("update called");
                        }else{
                         \Log::info("Create Called");
                            $entity = new Webhook;
                            $entity->shop_id = $shop->id;
                            //$entity->is_executed = 0;
                            $entity->shopify_id = $shopify_id;
                            $entity->topic = "existing-order";
                            $entity->data = $order;
                        }
                        $entity->save();*/
                        \Log::info($shop->shopify_domain);
                    }
                }
	}
        \Log::Info("Dispatch called end");
    }
}


