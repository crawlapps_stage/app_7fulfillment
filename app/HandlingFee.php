<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HandlingFee extends Model
{
  protected $table = 'handling_fee';
}
