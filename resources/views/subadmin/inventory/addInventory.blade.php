@extends('layouts.subadmin')
@section('page_title')
Add Inventory
@endsection

@section('page_css')
<style>
#inv_total_cost{
	display:none;
}
div#inv_total_cost label {
    font-weight: 600;
    font-size: 14px;
}
</style>
@endsection
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/subadmin/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
<a href="{{url('/subadmin/inventory')}}" class="kt-subheader__breadcrumbs-link">
						   Inventory                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						    Add Inventory                    </span>						   						   
						</div>
					
					</div>	
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick='window.location.href ="{{url('subadmin/inventory')}}";' class="btn btn-outline-danger" style="width:100%;"> Back </button>
					</div>			  
				</div>
			</div>
		</div>
		  
            <div class="kt-widget14">
	            @if (session('status') != '')
					<div class="alert alert-success">
						{{ session('status') }}
		 			</div>
				@elseif (session('status-error') !='')
					<div class="alert alert-danger">
						{{ session('status-error') }}
					</div>
				@endif	
			
	            <div class="kt-widget14__header kt-margin-b-30"> 
		   			<form role="form" method="POST" action="{{url('subadmin/inventory/store')}}" name="storeProfile" id="storeProfile">
		        		@csrf
		        			        	    
			            <div class="form-group">
			                <label class="control-label">Product Name</label>
			                <input type="text" placeholder="Product Name" name="product_name" id="product_name" value="" class="form-control"> 
			            </div>
			            <div class="form-group">
			                <label class="control-label">Quantity</label>
			                <input type="number" placeholder="Quantity" name="quantity" id="quantity" value="" class="form-control"> 
			            </div>

                        <div class="form-group">
			                <label class="control-label">Select User</label>
			                <select name="uemail" id="uemail" class="form-control" style="font-weight:600;"> 
			                	<option value="">Select User</option>
			                	@if (!empty($user_details))
									@foreach($user_details as $user)
				                		<option value="{{$user['email']}}"> {{$user['name']}} <strong>({{$user['email']}})</strong></option>
		   							@endforeach
	   							@endif
			                </select>	
			            </div>

                        <div class="row">
							<div class="col-md-3" >
	                        	<button style="width:100%;" type="submit" class="btn btn-primary" name="addinventory" id="addinventory">Save</button>
							</div>
	                    	<div  class="col-md-3" style="text-align: left;">
								<button style="width:100%;" type="button" onClick="window.location.href ='{{url('/subadmin/inventory')}}';" class="btn btn-outline-danger">Cancel</button>
		                 	</div>
	                    </div>
			            
			         </form>                
				</div>
        	</div>
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
<script>
	$('#uemail').select2({
 		 placeholder: 'Select User'
	});
	jQuery(document).ready(function(){
		//keypress input propertychange paste change keyup keydown
		jQuery('#shipping_cost, #handling_fee, #pro_cost').on('change blur focus focusout keypress keyup keydown input', function() {
			
			var flag 			=   true;
			var thisval 	    =   jQuery(this).val();

			if(!jQuery.isNumeric(thisval)){	
				jQuery(this).val('');
				flag = false;
			}
		});

		jQuery(document).on("change", ".wrap_total", function() {
    		var sum = 0;
    		jQuery(".wrap_total").each(function(){
        		sum += +jQuery(this).val();
    		});

			var html 		= 	'<label class="control-label">Total Cost :  $'+ sum +'</label>';
			jQuery('#inv_total_cost').html(html);
			jQuery('#inv_total_cost').css('display','block');	
		});
	});

</script>
@endsection