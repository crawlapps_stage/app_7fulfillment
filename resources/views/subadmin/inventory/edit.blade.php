@extends('layouts.subadmin')
@section('page_title')
Sub Admin | Edit Inventory
@endsection

@section('page_css')
<style>

</style>
@endsection

@section('content')
<!-- begin:: Content Head -->
	
	<!--<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	    <div class="kt-subheader__main">
	        <h3 class="kt-subheader__title">Profile</h3>
	        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
	        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
	            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
	            <span class="kt-input-icon__icon kt-input-icon__icon--right">
	                <span><i class="flaticon2-search-1"></i></span>
	            </span>
	        </div>
	    </div>
	    <div class="kt-subheader__toolbar">
	        <div class="kt-subheader__wrapper">     
	        </div>
	    </div>
	</div>
	-->
	
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/subadmin/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <a href="{{url('/subadmin/inventory')}}" class="kt-subheader__breadcrumbs-link">
						   Inventory                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Edit Inventory                          </span>						   						   
						</div>
					</div>				  
				</div>
			</div>
		</div>
		<!--
            	<div class="kt-portlet__head kt-portlet__head--lg">
		    <div class="kt-portlet__head-label" style="width: 100%;">
		         <span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>
		         <div class="row" style="width: 100%;">
				  <div class="col-xs-12 col-md-9" style="text-align: left;">
					  <h3 class="kt-portlet__head-title" style="margin-top:6px;">
						Admin Profile
					 </h3>
				  </div>
				  
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick="window.location.href ='http://7fulfillment.com/portal/wallet';" class="btn btn-outline-danger" style="width:100%;"> Back</button>
					</div>
					
				</div>
		    </div>
		</div>
		-->
                <div class="kt-widget14">
                    <div class="kt-widget14__header kt-margin-b-30">
                        <!-- BEGIN PROFILE CONTENT -->
                        @if (session('status'))
					    <div class="alert alert-success">
					        {{ session('status') }}
					    </div>
						@endif
			
                        <form role="form" method="POST" action="{{url('subadmin/inventory/store')}}" name="storeProfile" id="storeProfile">
                        	@csrf                        	
							<input type="hidden"  name="id" id="id" value="{{isset($inventory->id) ?  $inventory->id : '' }}">

<div class="row">
							  	<div class="col-md-12">
									
                            <div class="form-group">
			                <label class="control-label">Product Name</label>
			                <input type="text" placeholder="Product Name" name="product_name" id="product_name" value="{{isset($inventory->product_name) ?  $inventory->product_name : '' }}" class="form-control"> 
			            </div>
			            <div class="form-group">
			                <label class="control-label">Quantity</label>
			                <input type="number" placeholder="Quantity" name="quantity" id="quantity" value="{{isset($inventory->quantity) ?  $inventory->quantity : '' }}" class="form-control"> 
			            </div>

                                    <div class="form-group">
			                <label class="control-label">Select User</label>
			                <select name="uemail" id="uemail" class="form-control" style="font-weight:600;"> 
			                	<option value="">Select User</option>
			                	@if (!empty($user_details))
									@foreach($user_details as $user)
				                		<option value="{{$user['email']}}" {{($inventory->email == $user['email']) ?  'selected="selected"' : '' }}> {{$user['name']}} <strong>({{$user['email']}})</strong></option>	
		   							@endforeach
	   							@endif
			                </select>	
			            </div>

<div class="row">
							<div class="col-md-3" >
	                        	<button style="width:100%;" type="submit" class="btn btn-primary" name="addinventory" id="addinventory">Save</button>
							</div>
	                    	<div  class="col-md-3" style="text-align: left;">
								<button style="width:100%;" type="button" onClick="window.location.href ='{{url('/subadmin/inventory')}}';" class="btn btn-outline-danger">Cancel</button>
		                 	</div>
	                     </div>			
                        </form>    
                        <!-- END PROFILE CONTENT -->
                    </div>
                </div>                    
            </div>
        </div>
        <!--end:: Widgets/Daily Sales-->
    </div>
    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
<script>
$('#uemail').select2({
 		 placeholder: 'Select User'
	});
</script>
@endsection