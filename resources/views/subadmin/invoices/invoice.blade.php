@extends('layouts.subadmin')
@section('page_title')
Invoice
@endsection

@section('page_css')
<style>

</style>
@endsection

@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/subadmin/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Invoices                    </span>						   						   
						</div>
					
					</div>	
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick='window.location.href ="{{url('subadmin/invoices/create')}}";' class="btn btn-outline-danger" style="width:100%;"> Create Invoice</button>
					</div>			  
				</div>
			</div>
		</div>  
                <div class="kt-widget14">
	                @if (session('status')=='Sucess' || session('status')=='Invoice Updated Successfully.')
			<div class="alert alert-success">
			{{ session('status') }}
			 </div>
			@elseif (session('status') !='')
			<div class="alert alert-danger">
			{{ session('status') }}
			</div>
			@endif

			<div class="">
		    <!--begin: Search Form -->
		    <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
		        <div class="row align-items-center">
		            <div class="col-xl-8 order-2 order-xl-1">
		                <div class="row align-items-center">
		                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
		                        <div class="kt-input-icon kt-input-icon--left">
		                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
		                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
									<span><i class="la la-search"></i></span>
		                            </span>
		                        </div>
		                    </div>                    
		                </div>
		            </div>            
		        </div>
		    </div>
		    <!--end: Search Form -->
		</div>

			<!--begin: Datatable -->
			<div class="com_tran_datatable" id="Completed_datatable"></div>
			<!--end: Datatable -->		
                
        	</div>
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
<script>


   var path = APP_URL+'/subadmin/invoices/show';
   var datatable = $('.com_tran_datatable').KTDatatable({
  // datasource definition
  data: {
    type: 'remote',
    source: {
      read: {
        url: path,
        method: 'GET',
        map: function(raw) {
          // sample data mapping
          var dataSet = raw;
          if (typeof raw.data !== 'undefined') {
            dataSet = raw.data;
          }
          return dataSet;
        },
      },
    },
    pageSize: 10,
    serverPaging: false,
    serverFiltering: false,
    serverSorting: false,
  },

  // layout definition
  layout: {
    scroll: false,
    footer: false,
  },

  // column sorting
  sortable: true,

  pagination: true,

  search: {
    input: $('#generalSearch'),
  },

  // columns definition
  columns: [
  	{
      field: 'email',
      title: 'Username',     
      width: 180,      
      textAlign: 'center',
      template: function(row, index, datatable) {
      
      	return "<strong>"+row.fname+" "+row.lname+"</strong>"+"\n"+ "("+row.email+")";              
      },
    },
    {
      field: 'shipping_type',
      title: 'Shipping Type',     
      width: 180,      
      textAlign: 'center',
      template: function(row, index, datatable) {
      
        if(row.shipping_type == 'sourcing_invoice'){
          return "<strong>Sourcing Invoice</strong>"; 
        }
        if(row.shipping_type == 'shipping_invoice'){
          return "<strong>Shipping Invoice</strong>"; 
        }
        return "-";              
      },
    },
    {
      field: 'total_product_cost',
      title: 'Total Cost ($)',     
      width: 120,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var total_product_cost =formatter.format(row.total_product_cost); 
      
			return total_product_cost;              
      },
    },
    {
		field: 'status',
		title: 'Status',
		width: 180,
		textAlign: 'center',
		template: function(row) {
		    var status = row.status;
		    if(status == 0) {
		        return '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--danger">Pending</span>';
		    } else {
		        return '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--success">Approved</span>';
		    }	
		},
	},
  {
      field: 'created_at',
      width: 80,
      title: 'Date',
      textAlign: 'center',
      template: function(row, index, datatable) {
      
	var dateTime = row.created_at;
	var date = dateTime.split(" ");
	var result = date[0];
	var resulted_date =  result.split("-");
	var date_to_show = resulted_date[2]+'-'+resulted_date[1]+'-'+resulted_date[0];    
	return date_to_show;              
      },
    },
    {
      field: 'actions',      
      title: 'Actions', 
      textAlign: 'center', 
      template: function(row, index, datatable) {
        return '<a href="{{url('/subadmin/invoices/edit')}}/'+row.id+'" data-key="'+row.id+'" class="cstm_edit btn btn-hover-brand btn-icon btn-pill" title="Edit details">\
                        <i class="la la-edit"></i>\
                    </a>\
                <a href="{{url('/subadmin/invoices/view')}}/'+row.id+'" data-key="'+row.id+'" class="cstm_edit btn btn-label-primary btn-pill" title="View Invoice">\
                        Invoice\
                    </a>';
                    
      },
         
    }
  
   ],

});
</script>
@endsection