@extends('layouts.subadmin')
@section('page_title')
Create User's Notifications
@endsection

@section('page_css')
<style>

#ui-helper-center {
    text-align: center;
}
</style>
@endsection
@section('content')

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/subadmin/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Create User's Notifications                   </span>						   						   
						</div>
					
					</div>	
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick='window.location.href ="{{url('subadmin/home')}}";' class="btn btn-outline-danger" style="width:100%;"> Back </button>
					</div>			  
				</div>
			</div>
		</div>
		  
            <div class="kt-widget14">
	            @if (session('status') != '')
					<div class="alert alert-success">
						{{ session('status') }}
		 			</div>
				@elseif (session('status-error') !='')
					<div class="alert alert-danger">
						{{ session('status-error') }}
					</div>
				@endif	
			
	            <div class="kt-widget14__header kt-margin-b-30"> 
		   			<form role="form" method="POST" action="{{url('subadmin/usernotifications/store')}}" name="storeMessages" id="storeMessages">
		        		@csrf
		        	
		        	    <div class="form-group">
			                <label class="control-label">Select User</label>
			                <select name="uemail[]" id="uemail" class="form-control" style="font-weight:600;" multiple="multiple"> 
			                	<option value="">Select User</option>
			                	<option value="all_users">All Users</option>
			                	@if (!empty($user_details))
									@foreach($user_details as $user)
				                		<option value="{{$user['email']}},{{$user['user_id']}}" id="{{$user['user_id']}}" {{(isset($email) && ($email == $user['email'])) ?  'selected="selected"' : '' }}>{{$user['name']}}&nbsp; ({{$user['email']}}) </option>
		   							@endforeach
	   							@endif
			                </select>	

			            </div>
			            <div class="form-group">
			                <label class="control-label">Message</label>
			                <textarea name="user_messages" id="user_messages" class="form-control" rows="12"> </textarea>
			            </div>
			           
						 <div class="row">
							<div class="col-md-3" >
	                        	<button style="width:100%;" type="submit" class="btn btn-primary" name="send_notifications" id="send_notifications">Send</button>
							</div>
	                    	<div  class="col-md-3" style="text-align: left;">
								<button style="width:100%;" type="button" onClick="window.location.href ='{{url('/subadmin/home')}}';" class="btn btn-outline-danger">Cancel</button>
		                 	</div>
	                     </div>  
			         </form>                
				</div>
        	</div>
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
<script>
	$('#uemail').select2({
 		 placeholder: 'Select User',	 
	});


</script>
@endsection

