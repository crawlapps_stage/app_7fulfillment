@extends('layouts.app')
@section('page_title')
7Priority
@endsection
@section('page_css')
<style>
.cst_point {
	padding-top: 25px;
	padding-left: 25px;
	padding-right: 25px;
}
</style>
@endsection
@section('content')
<!-- begin:: Content Head -->
<!-- <div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title"></h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>
    <div class="kt-subheader__toolbar ">
        <div class="kt-subheader__wrapper">     
        </div>
    </div>
</div> -->
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   7Priority                    </span>
						   <!--
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Wired Transfer                   </a>-->
						   <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						</div>
					</div>				  
				</div>
			</div>
		</div>
		<!--
            	<div class="kt-portlet__head kt-portlet__head--lg">
		    <div class="kt-portlet__head-label" style="width: 100%;">
		         <span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>
		         <div class="row" style="width: 100%;">
				  <div class="col-xs-12 col-md-9" style="text-align: left;">
					  <h3 class="kt-portlet__head-title" style="margin-top:6px;">
						7Priority
					 </h3>
				  </div>
				  
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick="window.location.href ='http://7fulfillment.com/portal/wallet';" class="btn btn-outline-danger" style="width:100%;"> Back</button>
					</div>
					
				</div>
		    </div>
		</div>
		-->
                <div class="">
			<div class='row cst_point'>
				<div class='col-md-6' style="text-align:left;">         
					<h2>Total Points : <span style="color:#5867dd;">{{isset($total_points) ? ' '.number_format($total_points) : ' 0' }}</span></h2>               
				</div>
				
			</div>
				
           		 <div class="kt-portlet__body">
			    <!--begin: Search Form -->
			    <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
			        <div class="row align-items-center">
			            <div class="col-xl-8 order-2 order-xl-1">
			                <div class="row align-items-center">
			                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
			                        <div class="kt-input-icon kt-input-icon--left">
			                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
			                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
							<span><i class="la la-search"></i></span>
			                            </span>
			                        </div>
			                    </div>                    
			                </div>
			            </div>            
			        </div>
			    </div>
			    <!--end: Search Form -->
			</div>
			<div class="kt-portlet__body kt-portlet__body--fit">
				<!--begin: Datatable -->
				<div class="my_datatable" id="kt_datatable"></div>
				<!--end: Datatable -->
			</div>
        	      
                </div> 
                
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>
<!-- end:: Content -->
@endsection
@section('page_script')

<script>

   var path = APP_URL+'/7Priority/show';
   var datatable = $('.my_datatable').KTDatatable({
  // datasource definition
  data: {
    type: 'remote',
    source: {
      read: {
        url: path,
        method: 'GET',
        map: function(raw) {
          // sample data mapping
          var dataSet = raw;
          if (typeof raw.data !== 'undefined') {
            dataSet = raw.data;
          }
          return dataSet;
        },
      },
    },
    pageSize: 10,
    serverPaging: false,
    serverFiltering: false,
    serverSorting: false,
  },

  // layout definition
  layout: {
    scroll: true,
    footer: false,
  },

  // column sorting
  sortable: true,

  pagination: true,

  search: {
    input: $('#generalSearch'),
  },

  // columns definition
  columns: [
  /*
    {
      field: 'id',
      title: 'ID',     
      width: 40,      
      textAlign: 'center',
    },
    
     {
      field: 'points',
      title: 'Points',
      textAlign: 'center',
      width: 200,
      template: function(row, index, datatable) {
      
      	const formatter = new Intl.NumberFormat('en-US', {
	  
	  minimumFractionDigits: 2
	})
	
	var point =formatter.format(row.points)+' points were added';
	    
	return point;              
      },
    },
     
    {
      field: '',
      width: 290,
      title: 'Activity',            
      textAlign: 'center',
      template: function(row, index, datatable) {
      
      	var dateTime = row.created_at;
	var date = dateTime.split(" ");
	var result = date[0];
	var resulted_date =  result.split("-");
	var date_to_show = resulted_date[2]+'-'+resulted_date[1]+'-'+resulted_date[0];
	
      	const formatter = new Intl.NumberFormat('en-US', {
	  
	  minimumFractionDigits: 2
	})
	
	var amount =formatter.format(row.points)+' points were added on '+date_to_show;
	    
	return amount;              
      },
    }, 
    */
    
    {
      field: 'message',
      title: 'Activity',
      textAlign: 'center',
      width: 200,
      
    },
    
     {
      field: 'created_at', 
      width: 150,
      title: 'Date',
      textAlign: 'center',
      template: function(row, index, datatable) {
      
	var dateTime = row.created_at;
	var date = dateTime.split(" ");
	var result = date[0];
	var resulted_date =  result.split("-");
	var date_to_show = resulted_date[2]+'-'+resulted_date[1]+'-'+resulted_date[0];    
	return date_to_show;              
      },      
    }
    ],
});
</script>
@endsection