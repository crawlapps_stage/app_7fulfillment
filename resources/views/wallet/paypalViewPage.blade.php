<?php

//Set useful variables for paypal form
$paypalURL = 'https://www.paypal.com/cgi-bin/webscr'; //Test PayPal API URL https://www.sandbox.paypal.com/cgi-bin/webscr
$paypalID = 'payments@7fulfillment.com'; //Business Email info@codexworld.com

?>
<?php

$amount = $amount;
    
?>

@extends('layouts.app')
@section('page_title')
Wallet | Paypal
@endsection

@section('page_css')
<style>

</style>
@endsection

@section('content')
<!-- begin:: Content Head -->

<!--
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Profile</h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>
    <div class="kt-subheader__toolbar">
        <div class="kt-subheader__wrapper">     
        </div>
    </div>
</div>
-->
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <a href="{{url('/wallet')}}" class="kt-subheader__breadcrumbs-link">
						   Wallet                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Paypal                    </a>
						   <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						</div>
					</div>				  
				</div>
			</div>
		</div>
		<!--
            	<div class="kt-portlet__head kt-portlet__head--lg">
		    <div class="kt-portlet__head-label" style="width: 100%;">
		         <span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>
		         <div class="row" style="width: 100%;">
				  <div class="col-xs-12 col-md-9" style="text-align: left;">
					  <h3 class="kt-portlet__head-title" style="margin-top:6px;">
						Paypal Payment
					 </h3>
				  </div>
				  
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick="window.location.href ='http://7fulfillment.com/portal/wallet';" class="btn btn-outline-danger" style="width:100%;"> Back</button>
					</div>
					
				</div>
		    </div>
		</div>
            	-->
                <div class="kt-widget14">
                	@if (session('status')=='Your payment has been successful.')
			<div class="alert alert-success">
			{{ session('status') }}
			</div>
			
			  @elseif (session('status') !='')
				<div class="alert alert-danger">
				{{ session('status') }}
				</div>
			   @endif
 
                    <div class="kt-widget14__header kt-margin-b-30">
                        <!-- BEGIN PROFILE CONTENT -->                       
   
			    <form action="{{url($paypalURL)}}" method="post">
			        <!-- Identify your business so that you can collect the payments. -->
			        <input type="hidden" name="business" value="<?php echo $paypalID; ?>">
			        
			        <!-- Specify a Buy Now button. -->
			        <input type="hidden" name="cmd" value="_xclick">
			       <input type="hidden" name="custom" value="" id="custom_data_send">
			        
			        <!-- Specify details about the item that buyers will purchase. -->
			        
			        
			        
			        <input type="hidden" name="item_number" id="item_number" value="">
			        <input type="hidden" name="amount_without_fee" id="amount_without_fee" value="">
			        <div class="form-group">
	                              <label class="control-label">Enter The Amount</label>
	                              <input type="text" placeholder="Amount" name="amountAdd" id="amountAdd"  value="" class="form-control"> 
	                        </div>
	                        <div class="form-group">
	                        	Note : The PayPal charges a fees of 4.4%+0.3 for each transaction.
	                        </div>
	                        <input type="hidden" name="amount" id="amount" value="">
			        <input type="hidden" name="currency_code" value="USD">
			        
			        <!-- Specify URLs -->
			        <input type='hidden' name='notify_url' value='http://www.codexworld.com/ipn.php'>
			         			        
			        <input type='hidden' name='cancel_return' value="{{url('/paypal_integration_php/cancel')}}">
			        <input type='hidden' name='return' value="{{url('/paypal_integration_php/success.php')}}">
			        
			        <!-- Display the payment button. -->
			     
			                       	<div class="row">
									<div class="col-xs-12 col-md-3">
									   <input style="width:100%;" type="submit" class="btn btn-primary" name="submit" id="submit" value="Make Payment">
									  </div>
				<div class="col-xs-12 col-md-3" style="text-align: left;">
					<button type="button" onclick="window.location.href ='{{url('/wallet')}}';" class="btn btn-outline-danger" style="width:100%;"> Back
	                            	</button>
	                 </div>
	                </div>
			        
			        
			    </form>
    
    <!-- END PROFILE CONTENT -->
                    </div>
                </div>                    
            </div>
        </div>
        <!--end:: Widgets/Daily Sales-->
    </div>
    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>

<!-- end:: Content -->
@endsection
@section('page_script')

 <script>
 
        $(document).ready(function() {
        
        $('#submit').click(function() {
        
        	var amountAdd = $('#amountAdd').val();
	         if(amountAdd == '') {
	          alert('Please fill the amount.');
	          return false;
	         }
        });
         
        var amountToAdd =0;
        var addFee = 0
          var result = 0;
         $('#amountAdd').on('input',function(e){
         	
         	 amountToAdd = Number($('#amountAdd').val());
         	 addFee = Number(((((amountToAdd) *(4.4))/100)+0.3));        	
         	 result = Number(amountToAdd) + Number(addFee);
	    $('#amount').val(result);
	    
	    //$('#item_number').val(amountToAdd);
	    $('#custom_data_send').val(amountToAdd);
	    $('#amount_without_fee').val(amountToAdd);
	    
	    
	});
          
        });
      </script> 
	
@endsection