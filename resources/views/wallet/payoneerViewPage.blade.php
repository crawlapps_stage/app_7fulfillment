@extends('layouts.app')
@section('page_title')
Wallet | Payoneer
@endsection

@section('page_css')
<style>
</style>
@endsection

@section('content')
<!-- begin:: Content Head -->
<!--
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title"></h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>
    <div class="kt-subheader__toolbar">
        <div class="kt-subheader__wrapper">     
        </div>
    </div>
</div>

-->
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <a href="{{url('/wallet')}}" class="kt-subheader__breadcrumbs-link">
						   Wallet                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Payoneer                   </a>
						   <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						</div>
					</div>				  
				</div>
			</div>
		</div>
		<!--
            	<div class="kt-portlet__head kt-portlet__head--lg">
		    <div class="kt-portlet__head-label" style="width: 100%;">
		         <span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>
		         <div class="row" style="width: 100%;">
				  <div class="col-xs-12 col-md-9" style="text-align: left;">
					  <h3 class="kt-portlet__head-title" style="margin-top:6px;">
						Payoneer
					 </h3>
				  </div>
				  
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick="window.location.href ='http://7fulfillment.com/portal/wallet';" class="btn btn-outline-danger" style="width:100%;"> Back</button>
					</div>
					
				</div>
		    </div>
		</div>
		-->
                <div class="kt-widget14">
                    <div class="kt-widget14__header kt-margin-b-30">
                        <!-- BEGIN PROFILE CONTENT -->  
                        @if (session('status')=='Your Transaction has completed.')
			    <div class="alert alert-success">
			        {{ session('status') }}
			    </div>
			
			@elseif (session('status') !='')
			    <div class="alert alert-danger">
			        {{ session('status') }}
			    </div>
			@endif
				                     
   
			    <form role="form" method="POST" action="{{url('wallet/payoneer-payment')}}" name="paymentmethod" id="paymentmethod" enctype="multipart/form-data">
	                        	@csrf
	                        	
					<div class="form-group">
						<h5><p>To make the deposit use the following bank account details:</p></h3>
						<p>Account name: Test Account</p>
						<p>Accounrt Number: SJHF3424xxxx</p>
						<p>Name of the bank: Test Bank</p>
						<p>MICR code:  23423xxxxxxx</p>
						Note : For  depositing less than $3000, we will charge the fees ( $30 ) and depositing above $3000 we will cover ( $30 ).
					</div>
	                            	                            
	                            <div class="form-group">
	                                <label class="control-label"> Deposit Amount</label>
	                                <input type="text" placeholder="Amount" name="moneyamount" id="moneyamount"  value="" class="form-control">
	                                @if ($errors->has('moneyamount'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('moneyamount') }}</strong>
                                        </span>
                                    @endif
	                                
	                            </div>

	                            <!--<div class="form-group">
	                                <label for="paymentmethod">Bank Name</label>
	                                <input type="text" placeholder="Bank Name" name="bankname" id="bankname"  value="" class="form-control">
					 
	                            </div>-->
	                            <div class="form-group">
	                                <label for="paymentmethod">Payoneer Account</label>
	                                <input type="text" placeholder="Payoneer Account No." name="payoneer_account" id="payoneer_account"  value="" class="form-control">
	                                @if ($errors->has('payoneer_account'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('payoneer_account') }}</strong>
                                        </span>
                                    @endif
					 
	                            </div>
	                            
	                            <div class="form-group">
	                                <label for="paymentmethod">Transaction ID</label>
	                                <input type="text" placeholder="Transaction ID" name="txn_id" id="txn_id"  value="" class="form-control">
	                                @if ($errors->has('txn_id'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('txn_id') }}</strong>
                                        </span>
                                    @endif
					 
	                            </div>          
	                             
	                            <div class="form-group">
	                              <label for="Paymentproof">Payment Proof</label>
 	                              <input type="file" name="file" id="file" class="form-control">
                                    </div>             	                               

	                          	<div class="row">
									<div class="col-xs-12 col-md-3">
									<button style="width:100%;" type="submit" class="btn btn-primary" name="addpaymentmethod" id="addpaymentmethod">Submit</button>
									</div>
									<div class="col-xs-12 col-md-3" style="text-align: left;">
										<button  style="width:100%;" type="button" onclick="window.location.href ='{{url('/wallet')}}';" class="btn btn-outline-danger"> Back</button>
									</div>
	                            </div>
	                     </form>
    
    <!-- END PROFILE CONTENT -->
                    </div>
                </div>                    
            </div>
        </div>
        <!--end:: Widgets/Daily Sales-->
    </div>
    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>

<!-- end:: Content -->
@endsection
@section('page_script')

 

	
@endsection