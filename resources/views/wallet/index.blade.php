@extends('layouts.app')
@section('page_title')
Wallet
@endsection

@section('page_css')
<style>

 .size_btn{
    height: 5rem !important;
    width: 12rem !important;
}
 .plus_icon{
    font-size:2.2rem !important;
}
.payment_mthd .row > div {
    margin-bottom: 15px;
}
.row.cstm_wallet > div {
    border:1px solid #eee;
    padding: 25px;
    display: table;
    margin: 0 auto;
}
.row.cstm_wallet {
    margin-top: 20px;
}
.row.cstm_wallet div:hover img {
    -moz-transform: scale(1.1);
    -webkit-transform: scale(1.1);
    transform: scale(1.1);
}
.row.cstm_wallet div img {
    -moz-transition: all 0.3s;
    -webkit-transition: all 0.3s;
    transition: all 0.3s;
}
.row.cstm_wallet > div:hover {
    box-shadow: 0px 0px 7px rgba(128,128,128,0.2);
}
</style>
@endsection

@section('content')
<!-- begin:: Content Head -->
<!-- <div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title"></h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>
    <div class="kt-subheader__toolbar ">
        <div class="kt-subheader__wrapper">     
        </div>
    </div>
</div> -->
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Wallet                    </a>
						   <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						</div>
					</div>				  
				</div>
			</div>
		</div>
		<!--
            	<div class="kt-portlet__head kt-portlet__head--lg">
		    <div class="kt-portlet__head-label" style="width: 100%;">
		         <span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>
		         <div class="row" style="width: 100%;">
				  <div class="col-xs-12 col-md-9" style="text-align: left;">
					  <h3 class="kt-portlet__head-title" style="margin-top:6px;">
						Wallet
					 </h3>
				  </div>
				  
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick="window.location.href ='http://7fulfillment.com/portal/wallet';" class="btn btn-outline-danger" style="width:100%;"> Back</button>
					</div>
					
				</div>
		    </div>
		</div>
		-->
                <div class="kt-widget14">
					<div class='row'>
		                <div class='col-md-6' style="text-align:left;">         
							<h2>Wallet Balance : <span style="color:#5867dd;">{{isset($balance_history->amount) ? '$ '.number_format($balance_history->amount,2) : '$ 0' }}</span></h2>               
						</div>
						<div class='col-md-6' style="text-align:right;">
							<button type="button" onclick="window.location.href ='{{url('/wallet/transaction-history')}}';" class="btn btn-primary">Transaction History</button>
						</div>
					</div>
                </div> 
                <div class="kt-widget14">
	                @if (session('status')=='Sucess')
				    <div class="alert alert-success">
				        {{ session('status') }}
				    </div>
					@elseif (session('status') !='')
						<div class="alert alert-danger">
							{{ session('status') }}
						</div>
					@endif	
				<div class="row">
                	<div class='col-md-12' style="text-align:center;">
						<h3>Select payment Method</h3> 
					</div>
                </div>
                <div class="col-xl-12">
                    <div class="kt-widget14 payment_mthd" style="">
                    	<div class="row cstm_wallet">
							<div class='col-xs-12 col-md-3 col-lg-3' style="text-align:center;">
                    		<a href="{{url('wallet/paypal')}}"><img class="kt img-responsive" alt="Pic" style="height: 90px; width: 160px; margin: 0 39px;" src="{{asset(config('app.asset_path').'/assets/media/users/paypal.jpg')}}" /></a>
							</div>
                    		<div class='col-xs-12 col-md-3 col-lg-3' style="text-align:center;">
							<a href="{{url('wallet/stripe')}}"><img class="kt img-responsive" alt="Pic" style="height: 90px; width: 160px; margin: 0 39px;" src="{{asset(config('app.asset_path').'/assets/media/users/stripe.png')}}" /></a>
							</div>
                    		<div class='col-xs-12  col-md-3 col-lg-3' style="text-align:center;">
							<a href="{{url('wallet/wired')}}"><img class="kt img-responsive" alt="Pic" style="height: 90px; width: 160px; margin: 0 39px;" src="{{asset(config('app.asset_path').'/assets/media/users/wired-logo.png')}}" /></a>
							</div>
							
							<div class="col-xs-12  col-md-3 col-lg-3" style="text-align:center;">
							<a href="https://app.7fulfillment.com/wallet/payoneer"><img class="kt img-responsive" alt="Pic" style="height: 90px; width: 160px; margin: 0 39px;" src="https://app.7fulfillment.com/public/assets/media/users/Payoneer_logo.png"></a>
							</div>
                    	</div>
                    
                    <!--
	                   <form role="form" method="POST" action="{{url('wallet/paymentmethod')}}" name="paymentmethod" id="paymentmethod">
	                        	@csrf
	                            	                            
	                            <div class="form-group">
	                                <label class="control-label">Add Money Amount</label>
	                                <input type="text" placeholder="Amount" name="moneyamount" id="moneyamount"  value="" class="form-control"> 
	                            </div>

	                            <div class="form-group">
	                                <label for="paymentmethod">Payment Method</label>
					<select class="form-control" id="paymentmethod" name="paymentmethod">
						<option value="">Select Payment Method</option>
						<option value="stripe">Stripe Payment</option>
						<option value="paypal">Paypal Payment</option>
						<option value="wired">Wired Payment</option>
					</select> 
	                            </div>	                               

	                            <div class="form-group">
	                            	<button type="submit" class="btn btn-outline-primary" name="addpaymentmethod" id="addpaymentmethod">Make Payment</button>
	                            	<button type="button" class="btn btn-outline-danger">Cancel
	                            	</button>
	                            </div>
	                     </form>
	                     
	                     -->
                     </div>
                   <div class="kt-widget14">
                   <div class="kt-widget14">
                    
                   <!--
                   Stripe Payment
                    <a href="{{url('/stripe/view')}}" class="btn btn-brand btn-icon size_btn"  aria-haspopup="true" aria-expanded="false">
                    <i class="flaticon2-plus plus_icon"> </i>
                </a>
                -->
                </div>
                </div>                     
            </div>
        </div>

        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>
<!-- end:: Content -->
@endsection
@section('page_script')
@endsection
