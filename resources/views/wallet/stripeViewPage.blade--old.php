@extends('layouts.app')
@section('page_title')
Wallet | Stripe
@endsection

@section('page_css')
<style>
.kt-user-card.kt-user-card--skin-dark .kt-user-card__name {
    color: #262222 !important;
}
 .size_btn{
    height: 5rem !important;
    width: 12rem !important;
}
 .plus_icon{
    font-size:2.2rem !important;
}

</style>
@endsection

@section('content')
<!-- begin:: Content Head -->
<!--
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title"></h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>
    <div class="kt-subheader__toolbar ">
        <div class="kt-subheader__wrapper">     
        </div>
    </div>
</div>
-->
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <a href="{{url('/wallet')}}" class="kt-subheader__breadcrumbs-link">
						   Wallet                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Stripe Payment                   </a>
						   <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						</div>
					</div>				  
				</div>
			</div>
		</div>
	<!--
            	<div class="kt-portlet__head kt-portlet__head--lg">
		    <div class="kt-portlet__head-label" style="width: 100%;">
		         <span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>
		         <div class="row" style="width: 100%;">
				  <div class="col-xs-12 col-md-9" style="text-align: left;">
					  <h3 class="kt-portlet__head-title" style="margin-top:6px;">
						Stripe Payment
					 </h3>
				  </div>
				  
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick="window.location.href ='http://7fulfillment.com/portal/wallet';" class="btn btn-outline-danger" style="width:100%;"> Back</button>
					</div>
					
				</div>
		    </div>
		</div>
		-->
                
                <div class="kt-widget14">   
                             
                   @if (session('status')=='Your Transaction has completed.')
			<div class="alert alert-success">
			{{ session('status') }}
			</div>
			
		  @elseif (session('status') !='')
			<div class="alert alert-danger">
			{{ session('status') }}
			</div>
		   @endif
		  
		   
		   <span>
		   
		   <p>Note : The Stripe charges a fees of 4.4%+0.3 for each transaction.</p></span>
			
                    <div class="col-md-12" style="">
                    
                    
			    <form class="form-horizontal" method="POST" id="payment-form" role="form" action="{{route('addmoney.stripe')}}">
			
			        {{ csrf_field() }}
			        
			        <div class='form-row'>
				            <div class='col-md-12 form-group'>
				                <label class='control-label'>Card Number</label>
				                <input autocomplete='off' class='form-control card-number' maxlength="16" type='text' name="card_no" required>
				            </div>
				        </div>
				        <div class='form-row'>
				            <div class='col-xs-4 col-md-4 form-group cvc required'>
				                <label class='control-label'>CVV</label>
				                <input autocomplete='off' class='form-control card-cvc' required placeholder="" maxlength="4" type='text' name="cvvNumber">
				            </div>
				            <div class='col-xs-4 col-md-4 form-group expiration required'>
				                <label class='control-label'>Exp Month</label>
								<select class='form-control card-expiry-month' placeholder='MM' maxlength="2" type='text' name="ccExpiryMonth" required >
									<option value="">MM</option>
									<option value="01">01</option>
									<option value="02">02</option>
									<option value="03">03</option>
									<option value="04">04</option>
									<option value="05">05</option>
									<option value="06">06</option>
									<option value="07">07</option>
									<option value="08">08</option>
									<option value="09">09</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
								</select>
				            </div>
				            <div class='col-xs-4 col-md-4 form-group expiration required'>
				                <label class='control-label'>Exp Year </label>
				                
				                <select class='form-control card-expiry-year' placeholder='YYYY' maxlength="4" type='text' name="ccExpiryYear" required >
				                <option value="">YYYY</option>
				                <option value="2019">2019</option>
				                <option value="2020">2020</option>
				                <option value="2021">2021</option>
				                <option value="2022">2022</option>
				                <option value="2023">2023</option>
				                <option value="2024">2024</option>
								<option value="2025">2025</option>
				                <option value="2026">2026</option>
				                <option value="2027">2027</option>
				                <option value="2028">2028</option>
				                <option value="2029">2029</option>
				                <option value="2030">2030</option>
								<option value="2031">2031</option>
				                <option value="2032">2032</option>
				                <option value="2033">2033</option>
				                <option value="2034">2034</option>
				                <option value="2035">2035</option>
				                <option value="2036">2036</option>
								<option value="2037">2037</option>
								<option value="2038">2038</option>
				                <option value="2039">2039</option>
				                <option value="2040">2040</option>
				                <option value="2041">2041</option>
				                <option value="2042">2042</option>
				                <option value="2043">2043</option>
								<option value="2044">2044</option>
								<option value="2045">2045</option>
				                <option value="2046">2046</option>
				                <option value="2047">2047</option>
				                <option value="2048">2048</option>
				                <option value="2049">2049</option>
				                <option value="2050">2050</option>
				                
				                </select>
				            </div>
			           </div>
			        <div class='form-row' style="margin-bottom:20px;">
			             <div class='col-md-12' >
			                <label>Amount</label>
			                
			                <input class='form-control' placeholder='' size="" type='number' name="amountAdd" id ="amountAdd" value="" required>
			                <input class='form-control' placeholder='' size="" type='hidden' name="amount" id ="amount" value="">
			                
			            </div>
			        </div>
						<div class='form-row'>
							<div class='col-xs-12 col-md-3'>
								<button class='form-control btn btn-primary submit-button' name='submit' type='submit'>Make Payment</button>
							</div>
							<div class="col-xs-12 col-md-3" style="text-align: center;">
								<button type="button" onclick="window.location.href ='{{url('/wallet')}}';" class="btn btn-outline-danger" style="width:100%;"> Back</button>
							</div>
						</div>

			        <div class='form-row'>
			            <div class='col-xs-12 col-md-3'>					
			                <img class="" alt="Pic" style="width:50%;" src="{{asset(config('app.asset_path').'/assets/media/images/secure-stripe.png')}}" />
			            </div>							
			        </div>
			        
			    </form>
<!--
                            <form class="form-horizontal" method="POST" id="myForm" role="form" action="{{route('addmoney.stripe2')}}">
				 	@csrf
			
				  <input type="text" id="amountInDollars" />
				  <input type="hidden" id="stripeToken" name="stripeToken" />
				  <input type="hidden" id="stripeEmail" name="stripeEmail" />
				  <input type="hidden" id="amountInCents" name="amountInCents" />
			   </form>

			   <input type="button" id="customButton" value="Pay">
-->
			</div>                                      
                </div>                   
            </div>
        </div>

        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
<script src="https://checkout.stripe.com/checkout.js"></script>
	<script>

		$(document).ready(function() {
			//alert('stripe');

		var handler = StripeCheckout.configure({
		           key: 'pk_test_YhTpLBSLMZdpxxpzbkD8wBXr00pZoLrYSC',
		           image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
		           token: function(token) {
		             $("#stripeToken").val(token.id);
		             $("#stripeEmail").val(token.email);
		             $("#amountInCents").val(Math.floor($("#amountInDollars").val() * 100));
		             $("#myForm").submit();
		           }
		         });


		         $('#customButton').on('click', function(e) {
		           var amountInCents = Math.floor($("#amountInDollars").val() * 100);
		           var displayAmount = parseFloat(Math.floor($("#amountInDollars").val() * 100) / 100).toFixed(2);
		           // Open Checkout with further options
		           handler.open({
		             name: 'Demo Site',
		             description: 'Custom amount ($' + displayAmount + ')',
		             amount: amountInCents,
		           });
		           e.preventDefault();
		         });

		         // Close Checkout on page navigation
		         $(window).on('popstate', function() {
		           handler.close();
		         });
		    });
	</script>

	<script>
 
	        $(document).ready(function() {
	        
		        $('#submit').click(function() {
		        
		        	var amountAdd = $('#amountAdd').val();
			         if(amountAdd == '') {
			          alert('Please fill the amount.');
			          return false;
			         }
		        });
		         
		        var amountToAdd =0;
		        var addFee = 0
		          var result = 0;
		         $('#amountAdd').on('input',function(e){
		         	 amountToAdd = Number($('#amountAdd').val());
		         	 addFee = Number(((((amountToAdd) *(4.4))/100)+0.3));        	
		         	 result = Number(amountToAdd) + Number(addFee);
			    $('#amount').val(result);
			    
			});
	          
	        });
	</script>
@endsection
