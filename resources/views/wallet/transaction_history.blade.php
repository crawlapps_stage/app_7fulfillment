@extends('layouts.app')
@section('page_title')
Wallet | Transaction History
@endsection

@section('page_css')
<style>

 .size_btn{
    height: 5rem !important;
    width: 12rem !important;
}
 .plus_icon{
    font-size:2.2rem !important;
}



</style>
@endsection

@section('content')
<!-- begin:: Content Head -->
	<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
   <div class="kt-portlet kt-portlet--mobile">
   <div class="kt-portlet__head kt-portlet__head--lg">
      <div class="kt-portlet__head-label" style="width: 100%;">
         <!--<span class="kt-portlet__head-icon">
         <i class="kt-font-brand flaticon2-line-chart"></i>
         </span>-->
         <div class="row" style="width: 100%;">
		 <!-- <div class="col-xs-12 col-md-9" style="text-align: left;">
			  <h3 class="kt-portlet__head-title"  style="margin-top:6px;">
				Transaction History
			 </h3>
		  </div>
		  -->
		  <div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <a href="{{url('/wallet')}}" class="kt-subheader__breadcrumbs-link">
						   Wallet                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Transaction History                   </a>
						   <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						</div>
					</div>
			<div class="col-xs-12 col-md-3" style="text-align: right;">
				<button type="button" onclick="window.location.href ='{{url('/wallet')}}';" class="btn btn-outline-danger" style="width:100%;"> Back</button>
			</div>
		</div>
      </div>
      <div class="kt-portlet__head-toolbar">
      
      
      </div>
   </div>
   <div class="kt-portlet__body">
    <!--begin: Search Form -->
    <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
        <div class="row align-items-center">
            <div class="col-xl-8 order-2 order-xl-1">
                <div class="row align-items-center">
                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                        <div class="kt-input-icon kt-input-icon--left">
                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
							<span><i class="la la-search"></i></span>
                            </span>
                        </div>
                    </div>                    
                </div>
            </div>            
        </div>
    </div>
    <!--end: Search Form -->
</div>
   <div class="kt-portlet__body kt-portlet__body--fit">
      <!--begin: Datatable -->
      <div class="my_datatable" id="kt_datatable"></div>
      <!--end: Datatable -->
   </div>
   </div>
</div>
<!-- end:: Content -->
@endsection
@section('page_script')

<script>


   var path = APP_URL+'/transaction/show';
   var datatable = $('.my_datatable').KTDatatable({
  // datasource definition
  data: {
    type: 'remote',
    source: {
      read: {
        url: path,
        method: 'GET',
        map: function(raw) {
          // sample data mapping
          var dataSet = raw;
          if (typeof raw.data !== 'undefined') {
            dataSet = raw.data;
          }
          return dataSet;
        },
      },
    },
    pageSize: 10,
    serverPaging: false,
    serverFiltering: false,
    serverSorting: false,
  },

  // layout definition
  layout: {
    scroll: true,
    footer: false,
  },

  // column sorting
  sortable: true,

  pagination: true,

  search: {
    input: $('#generalSearch'),
  },

  // columns definition
  columns: [
  /*
    {
      field: 'id',
      title: 'ID',     
      width: 40,      
      textAlign: 'center',
    },
    */ {
      field: 'txn_id',
      title: 'Transaction ID',
      textAlign: 'center',
    }, {
      field: 'amount',
      width: 180,
      title: 'Amount Including Fee ($)',            
      textAlign: 'center',
      template: function(row, index, datatable) {
      
      	const formatter = new Intl.NumberFormat('en-US', {
	  
	  minimumFractionDigits: 2
	})
	
	var amount =formatter.format(row.amount); 
      
	//var amount = parseFloat(Math.round(row.amount * 100) / 100).toFixed(2);
	    
	return amount;              
      },
    },
     {
      field: 'amount_without_fee',
      width: 110,
      title: 'Amount ($)',
      textAlign: 'center',     
      template: function(row, index, datatable) {
      
      	const formatter = new Intl.NumberFormat('en-US', {
	  
	  minimumFractionDigits: 2
	})
	
	var amount =formatter.format(row.amount_without_fee); 
      
	//var amount = parseFloat(Math.round(row.amount * 100) / 100).toFixed(2);
	    
	return amount;              
      },
    },
    {
      field: 'payment_method',
      title: 'Payment Method',
      width: 150,
      textAlign: 'center',
    }, 
    {
      field: 'payment_status',
      title: 'Status', 
      textAlign: 'center', 
      template: function(row, index, datatable) {
      if(row.payment_status == 'succeeded' || row.payment_status == 'Completed') {
      	return '<button style="pointer-events: none;" type="submit" class="btn btn btn-success">Success</button>';
      
      } else {
      	return '<button style="pointer-events: none;" type="submit" class="btn btn btn-danger">Pending</button>';
      }
        
      },
         
    }, 
    {
      field: 'created_at', 
      width: 150,
      title: 'Date',
      textAlign: 'center',
      template: function(row, index, datatable) {
      
	var dateTime = row.created_at;
	var date = dateTime.split(" ");
	var result = date[0];
	var resulted_date =  result.split("-");
	var date_to_show = resulted_date[2]+'-'+resulted_date[1]+'-'+resulted_date[0];    
	return date_to_show;              
      },      
    }],

});
</script>
@endsection