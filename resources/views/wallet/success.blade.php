@extends('layouts.app')
@section('page_title')
Wallet | Success
@endsection

@section('page_css')
<style>
</style>
@endsection

@section('content')
<!-- begin:: Content Head -->

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-4">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            	<div class="kt-portlet__head kt-portlet__head--lg">
		    <div class="kt-portlet__head-label" style="width: 100%;">
		         <span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>
		         <div class="row" style="width: 100%;">
				  <div class="col-xs-12 col-md-9" style="text-align: left;">
					  <h3 class="kt-portlet__head-title" style="margin-top:6px;">
						Wallet Transfer Success
					 </h3>
				  </div>
				  <!--
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick="window.location.href ='http://7fulfillment.com/portal/wallet';" class="btn btn-outline-danger" style="width:100%;"> Back</button>
					</div>
					-->
				</div>
		    </div>
		</div>
                <div class="kt-widget14">
                    <div class="kt-widget14__header kt-margin-b-30">
                        <!-- BEGIN PROFILE CONTENT -->
                        @if (session('status'))
			    <div class="alert alert-success">
			        {{ session('status') }}
			    </div>
			@endif                          
                        <!-- END PROFILE CONTENT -->
                    </div>
                </div>                    
            </div>
        </div>
        <!--end:: Widgets/Daily Sales-->
    </div>
    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
@endsection
	
    
   
