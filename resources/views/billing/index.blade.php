@extends('layouts.app')
@section('page_title')
Billing History
@endsection

@section('page_css')
<style>

</style>
@endsection

@section('content')


<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/admin/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Billing History                    </span>						   						   
						</div>
					
					</div>	
				</div>
			</div>
		</div>
		    <div class="kt-widget14">
	                @if (session('status')=='Sucess')
			<div class="alert alert-success">
			{{ session('status') }}
			 </div>
			@elseif (session('status') !='')
			<div class="alert alert-danger">
			{{ session('status') }}
			</div>
			@endif

			<div class="">
		    <!--begin: Search Form -->
		    <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
		        <div class="row align-items-center">
		            <div class="col-xl-8 order-2 order-xl-1">
		                <div class="row align-items-center">
		                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
		                        <div class="kt-input-icon kt-input-icon--left">
		                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
		                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
									<span><i class="la la-search"></i></span>
		                            </span>
		                        </div>
		                    </div>                    
		                </div>
		            </div>            
		        </div>
		    </div>
		    <!--end: Search Form -->
		</div>

			<!--begin: Datatable -->
			<div class="com_tran_datatable" id="Completed_datatable"></div>
			<!--end: Datatable -->		
                
        	</div>
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
<script>


   var path = APP_URL+'/billing-history/show';
   var datatable = $('.com_tran_datatable').KTDatatable({
  // datasource definition
  data: {
    type: 'remote',
    source: {
      read: {
        url: path,
        method: 'GET',
        map: function(raw) {
          // sample data mapping
          var dataSet = raw;
          if (typeof raw.data !== 'undefined') {
            dataSet = raw.data;
          }
          return dataSet;
        },
      },
    },
    pageSize: 10,
    serverPaging: false,
    serverFiltering: false,
    serverSorting: false,
  },

  // layout definition
  layout: {
    scroll: false,
    footer: false,
  },

  // column sorting
  sortable: true,

  pagination: true,

  search: {
    input: $('#generalSearch'),
  },

  // columns definition
  columns: [
  	
  	/*{
      field: 'number_of_parcel',
      title: 'Number Of parcel',     
      width: 150,      
      textAlign: 'center',
     
    },
    */
    {
      field: 'shipping_type',
      title: 'Shipping Type',     
      width: 180,      
      textAlign: 'center',
      template: function(row, index, datatable) {
      
        if(row.shipping_type == 'sourcing_invoice'){
          return "<strong>Sourcing Invoice</strong>"; 
        }
        if(row.shipping_type == 'shipping_invoice'){
          return "<strong>Shipping Invoice</strong>"; 
        }
        return "-";              
      },
    },
    /*{
      field: 'shipping_cost',
      title: 'Shipping Cost ($)',     
      width: 150,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var amount =formatter.format(row.shipping_cost); 
      
			return amount;              
      },
    },
    {
      field: 'handling_fee',
      title: 'Handling Fee ($)',     
      width: 150,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var amount =formatter.format(row.handling_fee); 
      
			return amount;              
      },
    },*/
    {
      field: 'total_product_cost',
      title: 'Total Cost ($)',     
      width: 150,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var amount =formatter.format(row.total_product_cost); 
      
			return amount;              
      },
    },

/*
    {
      field: 'product_cost',
      title: 'Product Cost ($)',     
      width: 180,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var amount =formatter.format(row.product_cost); 
      
			return amount;              
      },
    },
/*
    {
      field: 'total_cost',
      title: 'Total Cost ($)',     
      width: 180,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var amount =formatter.format(row.total_cost); 
      
			return amount;              
      },
    },
*/   
  {
      field: 'created_at',
      width: 150,
      title: 'Date',
      textAlign: 'center',
      template: function(row, index, datatable) {
      
	var dateTime		= 	row.created_at;
	var date		= 	dateTime.split(" ");
	var result 	 	= 	date[0];
	var resulted_date 	=  	result.split("-");
	var date_to_show 	= 	resulted_date[2]+'-'+resulted_date[1]+'-'+resulted_date[0];    
	return date_to_show;              
      },
    },
    {
      field: 'id',
      title: 'Actions', 
      textAlign: 'center', 
      template: function(row, index, datatable) {
      
      	return '<a href="{{url('/billing-history/view')}}/'+row.id+'" data-key="'+row.id+'" class="cstm_edit btn btn-label-primary btn-pill" title="View Invoice">\
                        Invoice\
                    </a>'; 
      },
         
    }
  
   ],

});
</script>
@endsection