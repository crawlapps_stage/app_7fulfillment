@extends('layouts.app')
@section('page_title')
Invoice View
@endsection

@section('page_css')
<style>
#inv_total_cost{
	display:none;
}
div#inv_total_cost label {
    font-weight: 600;
    //font-size: 14px;
}

#total_display_cost1{
	display:block;
}
div#total_display_cost1 label {
    font-weight: 600;
    //font-size: 14px;
}


#iprice_to_show{
	display:none;
}
div#price_to_show label {
    font-weight: 600;
    //font-size: 14px;
}

#pro_total_cost{
	display:none;
}

#ui-helper-center {
    text-align: center;
}

.cst_form {
margin-bottom: 5px !important;

}
</style>
@endsection
@section('content')
<!-- begin:: Content Head -->
<!-- <div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title"></h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>
    <div class="kt-subheader__toolbar ">
        <div class="kt-subheader__wrapper">     
        </div>
    </div>
</div> -->
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <a href="{{url('/billing-history')}}" class="kt-subheader__breadcrumbs-link">
						   Billing History                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   View Invoice                    </span>						   						   
						</div>
					
					</div>	
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick='window.location.href ="{{url('/billing-history')}}";' class="btn btn-outline-danger" style="width:100%;"> Back </button>
					</div>			  
				</div>
			</div>
		</div>
		  
            <div class="kt-widget14">
	            @if (session('status') != '')
					<div class="alert alert-success">
						{{ session('status') }}
		 			</div>
				@elseif (session('status-error') !='')
					<div class="alert alert-danger">
						{{ session('status-error') }}
					</div>
				@endif	
			
	            <div class="kt-widget14__header kt-margin-b-30"> 
		   			<form role="form">
		        		@csrf
		        	 
			            <div class="form-group cst_form">
			                <label class="control-label" style="font-weight:560;">User Name : </label>&nbsp;&nbsp;
			                <!--<input type="text" placeholder="User" name="uemail"  class="form-control" style="font-weight:460;" value="{{(isset($first_name->fname) && isset($last_name->lname)) ?  $first_name->fname." ".$last_name->lname : '' }} &nbsp;({{(isset($invoice->email)) ?  $invoice->email : '' }})" Readonly> -->
			                <span>{{(isset($first_name->fname) && isset($last_name->lname)) ?  $first_name->fname." ".$last_name->lname : '' }} &nbsp;({{(isset($invoice->email)) ?  $invoice->email : '' }})</span>
			            </div>
			            <input type="hidden" name="invoice_id" id="invoice_id" value="{{(isset($invoice->id)) ?  $invoice->id : '' }}">
			            <input type="hidden" name="product_invoice_id" id="product_invoice" value="{{(isset($invoice_product->id)) ?  $invoice_product->id : '' }}">
			            									            
			            @if((isset($product_details_arr)) && (!empty($product_details_arr)))
			            <?php 
			            	$count = count($product_details_arr);
			            	$serial_no = 1; 
			            ?>
			             
			            <div class="form-group cst_form">
					<table class="table table-bordered" id="ui-helper-center">
					  <thead>
					    <tr>
					      <!--<th scope="col">S No.</th>-->
					      <th scope="col">Product Name</th>
					      <th scope="col">Quantity</th>
					       @if($invoice->shipping_type == 'sourcing_invoice')
					      	<th scope="col">Price</th>
					      @endif
					    </tr>
					  </thead>
					
					  <tbody>
					  
					  @foreach($product_details_arr as $product_details_arr)
					 
					    <tr>
					      <!--<td scope="row">{{$serial_no}}</td>-->

					      <td>{{$product_details_arr["name"]}}</td>
					      
					      <td style="width: 235px;">
					      <span>{{(isset($product_details_arr["'quantity'"])) ?  $product_details_arr["'quantity'"] : '' }}</span>
					      <!--<input type="number" min="0" max="{{isset($product_details_arr['original_quantity'])+$product_details_arr["'quantity'"]}}" name="{{$product_details_arr['name']}}['quantity']" id="quantity_{{$product_details_arr["'quantity'"]}}" value="{{(isset($product_details_arr["'quantity'"])) ?  $product_details_arr["'quantity'"] : '' }}" class="form-control quantity_{{$serial_no}} cost_change" Readonly/>-->
					      
					      </td>
					      @if($invoice->shipping_type == 'sourcing_invoice')
					      <td style="width: 235px;">
					      <span>{{(isset($product_details_arr["'price'"])) ?  $product_details_arr["'price'"] : '' }}</span>
					      <!--<input type="text" name="{{$product_details_arr['name']}}['price']" id="price_{{$product_details_arr['name']}}" value="{{(isset($product_details_arr["'price'"])) ?  $product_details_arr["'price'"] : '' }}" class="form-control price_{{$serial_no}} cost_change" Readonly>-->
					       
					      </td>
					      @endif
					      
					    </tr>
					    <?php
					      	if($serial_no < $count){
					      		$serial_no++;
					      	}
					       ?>
					    
					    <?php
					      	$product_arr[] = $product_details_arr['name'];
					     ?>
					  
					  @endforeach
					  <input type="hidden" name="products" id="products" value="{{implode(',',$product_arr)}}">
					  
					  
					  </tbody>
					</table>
					</div>
					@else
					<!--
					<div class="form-group cst_form">
						<p>No Product Found.</p>
					 </div>
					-->					
					@endif					
			            @if($invoice->shipping_type == 'shipping_invoice')
			            <div class="form-group cst_form">
			                <label class="control-label" style="font-weight:560;">No. Of Parcel : </label>&nbsp;&nbsp;
			                <!--<input type="text" placeholder="No. Of Parcel" name="number_of_parcel" id="nparcel" value="{{(isset($invoice->number_of_parcel)) ?  $invoice->number_of_parcel : '' }}" class="form-control" Readonly>-->
			                <span>
			                	{{(isset($invoice->number_of_parcel)) ?  $invoice->number_of_parcel : '' }}
					</span> 
			            </div>
			           <?php /*  <div class="form-group cst_form">
			                <label class="control-label" style="font-weight:560;">Shipping Cost :</label>&nbsp;&nbsp;
			                <!--<input type="text" placeholder="Shipping Cost" name="shipping_cost" id="shipping_cost" value="{{(isset($invoice->shipping_cost)) ?  $invoice->shipping_cost : '' }}" class="form-control wrap_total" Readonly>-->
			                <span>
						{{(isset($invoice->shipping_cost)) ?  $invoice->shipping_cost : '' }}
					</span> 
			            </div> */ ?>
			             <div class="form-group cst_form">
			            	<label class="control-label" style="font-weight:560;">Shipping Cost (CNY) : </label>&nbsp;&nbsp;
							<span>
								{{(isset($invoice->shipping_cost_cny)) ?  $invoice->shipping_cost_cny : '' }}
							</span>
			            
			            </div>
			            <div class="form-group cst_form">
			            	<label class="control-label" style="font-weight:560;">Shipping Cost (USD) : </label>&nbsp;&nbsp;
							<span>
								{{(isset($invoice->shipping_cost_usd)) ?  $invoice->shipping_cost_usd : '' }}
							</span>
			             
			            </div>
			            <div class="form-group cst_form">
			                <label class="control-label" style="font-weight:560;">Handling Fee :</label>
			                <!--<input type="text" placeholder="Handling Fee" name="handling_fee" id="handling_fee" value="{{(isset($invoice->handling_fee)) ?  $invoice->handling_fee : '' }}" class="form-control wrap_total" Readonly>-->
			                <span>
			                	{{(isset($invoice->handling_fee)) ?  $invoice->handling_fee : '' }}
					</span> 
			            </div>
			            @endif	
			            <!--
				    <div class="form-group cst_form" id="inv_total_cost">
			                <label class="control-label">Total Cost :  </label>
			            </div>
-->

				    <div class="form-group cst_form" id="total_display_cost1">
			                <label class="control-label">Total Cost : {{(isset($invoice->total_product_cost)) ?  $invoice->total_product_cost : '' }} </label>
			            </div>
			            <div class="form-group" id="pro_total_cost">
			                
			            </div> 
			         </form>                
				</div>
        	</div>
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
<script>
	$('#uemail').select2({
 		 placeholder: 'Select User'
	});
	jQuery(document).ready(function(){
		//keypress input propertychange paste change keyup keydown
		jQuery('#shipping_cost, #handling_fee, #pro_cost').on('change blur focus focusout keypress keyup keydown input', function() {
			
			var flag 			=   true;
			var thisval 	    =   jQuery(this).val();

			if(!jQuery.isNumeric(thisval)){	
				jQuery(this).val('');
				flag = false;
			}
		});

		jQuery(document).on("change", ".wrap_total", function() {
    		var sum = 0;
    		jQuery(".wrap_total").each(function(){
        		sum += +jQuery(this).val();
    		});

			var html 		= 	'<label class="control-label">Total Cost :  $'+ sum +'</label>';
			jQuery('#inv_total_cost').html(html);
			jQuery('#inv_total_cost').css('display','none');
				
		});
		});
</script>

<script>
 
        $(document).ready(function() {
	         
	        var total_product_cost =0; 
	        var result = 0;
	        $('#nparcel').on('input',function(e){
	         	 total_product_cost = Number($('#nparcel').val());
	         	 result = Number((total_product_cost) *(0.88)); 
		         $('#handling_fee').val(result);
		    
		});
          
        });
</script>
   
   <script type="text/javascript">
    jQuery(document).on( "change", "#uemail", function(id) {

		var id = jQuery(this).children(":selected").attr("id");
                window.location.href = APP_URL +'/admin/invoices/create/'+id;		
        })
</script>
    
<script>
 
        $(document).ready(function() {
	         
	        var total_product_cost = 0; 
	        var result = 0;
                var price_cal=[];
                var resulted_price = 0;
                let final_price = 0;
	        
                $('#shipping_cost, #nparcel, .cost_change').on('input change',function(e){
                	
                	quantity_arr=[];
		         price_arr=[];
		         price_cal=[];
		         resulted_price=0;
		        final_price =0;
                	
                	jQuery('#invoice_total_product_cost_div').css('display','none');
		        
		        var count=$('#ui-helper-center tr').length;
		        var quantity_arr=[];
		        for(var i=1;i<count;i++) {
		        	quantity_arr.push($(".quantity_"+i).val());
		        }
			console.log(quantity_arr);
		
		        var price_arr=[];
		        for(var i=1;i<count;i++) {
		        	price_arr.push($(".price_"+i).val());
		        }
			console.log(price_arr);			
			
		        for(var i=0;i<(count-1);i++) {
		        	cal=quantity_arr[i]*price_arr[i];
		        	price_cal.push(cal);
		        }
		        console.log(price_cal);
	
	                for(var i=0;i<(count-1);i++) {
		        	resulted_price = Number(resulted_price) + Number(price_cal[i]);		         
		        }
		        console.log(resulted_price);
			final_price = Number(resulted_price) + Number($('#shipping_cost').val()) + Number($('#handling_fee').val());
			console.log(final_price);
		
			var html10 = '<label class="control-label total_display_cost">Total Cost :  $'+ final_price +'</label>';
			jQuery('#total_display_cost1').html(html10);
	
			if($('#nparcel').val() !='' || $('#shipping_cost').val() != '' ) {
				jQuery('#total_display_cost1').css('display','block');
			}
							
			console.log($('.total_display_cost').text());
		        
			var html1 = '<button type="submit" name="total_price_display" value="total_price_display" class="btn btn-primary" window.location.href ="{{url('admin/invoices/create/price')}}"; > Total Cost </button>';
			jQuery('#pro_total_cost').html(html1);
			jQuery('#pro_total_cost').css('display','none');	         	 		    
		});         
        });                
        
</script>
@endsection

