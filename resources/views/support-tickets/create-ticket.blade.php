@extends('layouts.app')
@section('page_title')
Create Ticket
@endsection
@section('page_css')
@endsection
@section('content')
<!-- begin:: Content Head -->

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Create Ticket                    </span>
						 
						</div>
					</div>	
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick='window.location.href ="{{url('/support-tickets')}}";' class="btn btn-outline-danger" style="width:100%;"> Back </button>
					</div>				  
				</div>
			</div>
		</div>
		
           
            <div class="kt-widget14">
	            @if (session('status') != '')
					<div class="alert alert-success">
						{{ session('status') }}
		 			</div>
				@elseif (session('status-error') !='')
					<div class="alert alert-danger">
						{{ session('status-error') }}
					</div>
				@endif

	            <div class="kt-widget14__header kt-margin-b-30"> 
		   			<form role="form" method="POST" action="{{url('/create-ticket/store')}}" name="create_ticket" id="create_ticket">
		        		@csrf
		        		
		        	    <input type="hidden" name="form_type" value="create_ticket">
		        	   
		        	      <?php /* <div class="form-group">
			                <label class="control-label">Select Invoice</label>
			                <select name="invoice_list" id="invoice_list" class="form-control" style="font-weight:600;"> 
			                	<option value="">Select Invoice</option>
			                	<?php /*foreach($all_user_invoices as $key => $invoice){ ?>
			                		<option value="<?php echo $invoice->id;?>" id="shipping_invoice<?php echo $invoice->id;?>" ><?php 
			                		 echo '$'.$invoice->total_product_cost;
			                		 if($invoice->shipping_type == 'sourcing_invoice'){
			                		 	echo ' (Sourcing Invoice)';
			                		 }else{
			                		 	echo ' (Shipping Invoice)';
			                		 }
			                		?></option>	
			                	<?php }?>
			                </select>	
			            </div> */ ?>
     					<div class="form-group">
			                <label class="control-label">Enter Order Number </label>
			                <input type="text" placeholder="Order Number" name="invoice_list" id="invoice_list" class="form-control" style="font-weight:600;" > 
			            </div>
			            <div class="form-group">
			                <label class="control-label">Subject</label>
			                <input type="text" placeholder="Subject" name="support_subject" id="support_subject" value="" class="form-control"> 
			            </div>
			            <div class="form-group">
			                <label class="control-label">Message</label>
			                <textarea name="support_message" style="width: 100%;height: 260px;border: 1px solid #ebedf2;"></textarea> 
			            </div>
			          
			             <div class="row">
							<div class="col-md-3" >
	                        	<button style="width:100%;" type="submit" class="btn btn-primary" name="addticket" id="addticket">Submit</button>
							</div>
	                    	<div  class="col-md-3" style="text-align: left;">
								<button style="width:100%;" type="button" onClick="window.location.href ='{{url('/support-tickets')}}';" class="btn btn-outline-danger">Cancel</button>
		                 	</div>
	                     </div>  
			         </form>                
				</div>
        	</div>
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>
<!-- end:: Content -->
@endsection
@section('page_script')
<script type="text/javascript">
	//$('#invoice_list').select2({
 	//	 placeholder: 'Select Invoice'
	//});
</script>
@endsection