@extends('layouts.app')
@section('page_title')
Support Tickets
@endsection
@section('page_css')
@endsection
@section('content')
<!-- begin:: Content Head -->
<!-- <div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title"></h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>
    <div class="kt-subheader__toolbar ">
        <div class="kt-subheader__wrapper">     
        </div>
    </div>
</div> -->
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Support Tickets                    </span>
						   <!--
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Wired Transfer                   </a>-->
						   <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						</div>
					</div>	
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick='window.location.href ="{{url('/create-ticket')}}";' class="btn btn-outline-danger" style="width:100%;"> Create Ticket</button>
					</div>			  
				</div>
			</div>
		</div>
		<!--
            	<div class="kt-portlet__head kt-portlet__head--lg">
		    <div class="kt-portlet__head-label" style="width: 100%;">
		         <span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>
		         <div class="row" style="width: 100%;">
				  <div class="col-xs-12 col-md-9" style="text-align: left;">
					  <h3 class="kt-portlet__head-title" style="margin-top:6px;">
						Support Tickets
					 </h3>
				  </div>
				  
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick="window.location.href ='http://7fulfillment.com/portal/wallet';" class="btn btn-outline-danger" style="width:100%;"> Back</button>
					</div>
					
				</div>
		    </div>
		</div>
		-->
                <div class="kt-widget14">
			<div class='row'>
		         </div>
                </div> 
                <div class="kt-widget14">
	                @if (session('status')=='Sucess')
			<div class="alert alert-success">
			{{ session('status') }}
			 </div>
			@elseif (session('status') !='')
			<div class="alert alert-danger">
			{{ session('status') }}
			</div>
			@endif	
			
			<!--begin: Datatable -->
			<div class="com_tran_datatable" id="Completed_datatable"></div>
			<!--end: Datatable -->		

        	</div>
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>
<!-- end:: Content -->
@endsection
@section('page_script')
<script>


   var path = APP_URL+'/support-ticket/show';
   var datatable = $('.com_tran_datatable').KTDatatable({
  // datasource definition
  data: {
    type: 'remote',
    source: {
      read: {
        url: path,
        method: 'GET',
        map: function(raw) {
          // sample data mapping
          var dataSet = raw;
          if (typeof raw.data !== 'undefined') {
            dataSet = raw.data;
          }
          return dataSet;
        },
      },
    },
    pageSize: 10,
    serverPaging: false,
    serverFiltering: false,
    serverSorting: false,
  },

  // layout definition
  layout: {
    scroll: false,
    footer: false,
  },

  // column sorting
  sortable: true,

  pagination: true,

  // columns definition
  columns: [
  	{
      field: 'ticket_id',
      title: 'Ticket Id',     
      width: 180,      
      textAlign: 'center',
    },
  	{
      field: 'order_id',
      title: 'Order Number',     
      width: 180,      
      textAlign: 'center',
    },
    {
      field: 'subject',
      title: 'Subject',     
      width: 180,      
      textAlign: 'center',
    },
    /*{
      field: 'message',
      title: 'Message',     
      width: 180,      
      textAlign: 'center',
    },*/
 	{
      field: 'status',
      title: 'Status',     
      width: 120,      
      textAlign: 'center',
       template: function(row, index, datatable) {
           var status = '<strong style="color:red;">Open</strong>'; 
          if(row.status == 1){
            status = '<strong style="color:green">Resolved</strong>';
          }
      
      return status;              
      },
    },
    
  	/*{
      field: 'number_of_parcel',
      title: 'Number Of parcel',     
      width: 120,      
      textAlign: 'center',
     
    },
    {
      field: 'shipping_cost',
      title: 'Shipping Cost ($)',     
      width: 120,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var amount =formatter.format(row.shipping_cost); 
      
			return amount;              
      },
    },
    {
      field: 'handling_fee',
      title: 'Handling Fee ($)',     
      width: 120,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var amount =formatter.format(row.handling_fee); 
      
			return amount;              
      },
    },*/
   /* {
      field: 'total_product_cost',
      title: 'Total Cost ($)',     
      width: 120,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var total_product_cost =formatter.format(row.total_product_cost); 
      
			return total_product_cost;              
      },
    },*/
    /*
    {
      field: 'product_cost',
      title: 'Product Cost ($)',     
      width: 180,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var amount =formatter.format(row.product_cost); 
      
			return amount;              
      },
    },
    
    
    {
      field: 'total_cost',
      title: 'Total Cost ($)',     
      width: 180,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var amount =formatter.format(row.total_cost); 
      
			return amount;              
      },
    },
    */   
  {
      field: 'created_at',
      width: 80,
      title: 'Date',
      textAlign: 'center',
      template: function(row, index, datatable) {
      
	var dateTime = row.created_at;
	var date = dateTime.split(" ");
	var result = date[0];
	var resulted_date =  result.split("-");
	var date_to_show = resulted_date[2]+'-'+resulted_date[1]+'-'+resulted_date[0];    
	return date_to_show;              
      },
    },
    {
      field: 'actions',      
      title: 'Actions', 
      textAlign: 'center', 
      template: function(row, index, datatable) {
        return '<a href="{{url('/support-ticket/view')}}/'+row.ticket_id+'" data-key="'+row.ticket_id+'" class="cstm_edit btn btn-label-primary btn-pill" title="View Ticket">\
                        View Ticket\
                    </a>';
                    
      },
         
    }
  
   ],

});
</script>
@endsection