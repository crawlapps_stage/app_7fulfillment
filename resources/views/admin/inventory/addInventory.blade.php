@extends('layouts.admin')
@section('page_title')
Add Inventory
@endsection

@section('page_css')
<style>
#inv_total_cost{
	display:none;
}
div#inv_total_cost label {
    font-weight: 600;
    font-size: 14px;
}
</style>
@endsection
@section('content')

<!-- begin:: Content Head -->
<!-- <div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title"></h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>
    <div class="kt-subheader__toolbar ">
        <div class="kt-subheader__wrapper">     
        </div>
    </div>
</div> -->
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/admin/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
<a href="{{url('/admin/inventory')}}" class="kt-subheader__breadcrumbs-link">
						   Inventory                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						    Add Inventory                    </span>						   						   
						</div>
					
					</div>	
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick='window.location.href ="{{url('admin/inventory')}}";' class="btn btn-outline-danger" style="width:100%;"> Back </button>
					</div>			  
				</div>
			</div>
		</div>
		  
            <div class="kt-widget14">
	            @if (session('status') != '')
					<div class="alert alert-success">
						{{ session('status') }}
		 			</div>
				@elseif (session('status-error') !='')
					<div class="alert alert-danger">
						{{ session('status-error') }}
					</div>
				@endif	
			
	            <div class="kt-widget14__header kt-margin-b-30"> 
		   			<form role="form" method="POST" action="{{url('admin/inventory/store')}}" name="storeProfile" id="storeProfile">
		        		@csrf
		        			        	    
			            <div class="form-group">
			                <label class="control-label">Product Name</label>
			                <input type="text" placeholder="Product Name" name="product_name" id="product_name" value="" class="form-control" required>
			                @if($errors->has('product_name'))
			                <span class="invalid-feedback" style="display: block;">
			                    <strong>{{ $errors->first('product_name') }}</strong>
                            <span>    
                            @endif
			            </div>
			            <div class="form-group">
			                <label class="control-label">Price</label>
			                <input type="number" step="0.01" placeholder="Enter Price" name="price" id="price" value="" class="form-control" required>
			                @if($errors->has('price'))
			                <span class="invalid-feedback" style="display: block;">
			                    <strong>{{ $errors->first('price') }}</strong>
                            <span>    
                            @endif
			            </div>
			            <div class="form-group">
			                <label class="control-label">Quantity</label>
			                <input type="number" placeholder="Quantity" name="quantity" id="quantity" value="" class="form-control" required>
			                @if($errors->has('quantity'))
			                <span class="invalid-feedback" style="display: block;">
			                    <strong>{{ $errors->first('quantity') }}</strong>
                            <span>    
                            @endif
			            </div>
			            
			            <div class="form-group">
			                <label class="control-label">Weight (gm)</label>
			                <input type="number" placeholder="Weight" name="weight" id="weight" value="" class="form-control" required>
			                @if($errors->has('weight'))
			                <span class="invalid-feedback" style="display: block;">
			                    <strong>{{ $errors->first('weight') }}</strong>
                            <span>    
                            @endif
			            </div>
			            
			            <div class="form-group">
			                <label class="control-label">SKU</label>
			                <input type="text" placeholder="Sku" name="sku" id="sku" value="" class="form-control" required>
			                @if($errors->has('sku'))
			                <span class="invalid-feedback" style="display: block;">
			                    <strong>{{ $errors->first('sku') }}</strong>
                            <span>    
                            @endif
			            </div>
			            <div class="form-group">
                            <label class="control-label">Shipping Provider</label>
                            <select class="form-control" id="shipping_provider" name="shipping_provider">
								<!--<option value="1" selected="&quot;selected&quot;">Activate</option>-->
								<option value="">Select Shipping Provider</option>
								@if($shipping_provider)
								@foreach($shipping_provider as $providers)
								<option value="{{$providers->id}}">{{$providers->name}}</option>
								@endforeach
								@else
								<option value="0">No Providers</option>
								@endif
							</select> 
                		</div>

                        <div class="form-group">
			                <label class="control-label">Select User</label>
			                <select name="uemail" id="uemail" class="form-control" style="font-weight:600;" required>
			                	<option value="">Select User</option>
			                	@if (!empty($user_details))
									@foreach($user_details as $user)
				                		<option value="{{$user['email']}}"> {{$user['name']}} <strong>({{$user['email']}})</strong></option>
		   							@endforeach
	   							@endif
			                </select>
			                @if($errors->has('uemail'))
    			                <span class="invalid-feedback" style="display: block;">
    			                    <strong>{{ $errors->first('uemail') }}</strong>
                                <span>    
                                @endif
			            </div>

<div class="row">
							<div class="col-md-3" >
	                        	<button style="width:100%;" type="submit" class="btn btn-primary" name="addinventory" id="addinventory">Save</button>
							</div>
	                    	<div  class="col-md-3" style="text-align: left;">
								<button style="width:100%;" type="button" onClick="window.location.href ='{{url('/admin/inventory')}}';" class="btn btn-outline-danger">Cancel</button>
		                 	</div>
	                     </div>
			            
			         </form>                
				</div>
        	</div>
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>

<!-- end:: Content -->
@endsection
@section('page_script')

<script>
    $(document).ready(function() {
        $("#addinventory").click(function() {
           
            var product_name = $("#product_name").val();
            var price = $("#price").val();
            var quantity = $("#quantity").val();
            var uemail = $("#uemail").val();
           
            if(product_name == '') {
              swal.fire('error',"Please fill the product name","error");
              return false;   
            }
            if(price == '') {
                swal.fire('error', "Please fill the price", "error");
                return false;
            }
            if(quantity == '') {
              swal.fire('error',"Please fill the quantity","error");
              return false;
            }
            
            if(uemail == '') {
              swal.fire('error',"PLease select the user email","error");
              return false;
            }   
        }); 
    }); 
</script>
<script>
	$('#uemail').select2({
 		 placeholder: 'Select User'
	});
	jQuery(document).ready(function(){
		//keypress input propertychange paste change keyup keydown
		jQuery('#shipping_cost, #handling_fee, #pro_cost').on('change blur focus focusout keypress keyup keydown input', function() {
			
			var flag 			=   true;
			var thisval 	    =   jQuery(this).val();

			if(!jQuery.isNumeric(thisval)){	
				jQuery(this).val('');
				flag = false;
			}
		});

		jQuery(document).on("change", ".wrap_total", function() {
    		var sum = 0;
    		jQuery(".wrap_total").each(function(){
        		sum += +jQuery(this).val();
    		});

			var html 		= 	'<label class="control-label">Total Cost :  $'+ sum +'</label>';
			jQuery('#inv_total_cost').html(html);
			jQuery('#inv_total_cost').css('display','block');	
		});
	});

</script>
@endsection