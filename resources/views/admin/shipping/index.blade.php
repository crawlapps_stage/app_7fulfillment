@extends('layouts.admin')
@section('page_title')
Shipping
@endsection

@section('page_css')
<style>

 .size_btn{
    height: 5rem !important;
    width: 12rem !important;
}
 .plus_icon{
    font-size:2.2rem !important;
}
.payment_mthd .row > div {
    margin-bottom: 15px;
}
.row.cstm_wallet > div {
    border:1px solid #eee;
    padding: 25px;
    display: table;
    margin: 0 auto;
}
.row.cstm_wallet {
    margin-top: 20px;
}
.row.cstm_wallet div:hover img {
    -moz-transform: scale(1.1);
    -webkit-transform: scale(1.1);
    transform: scale(1.1);
}
.row.cstm_wallet div img {
    -moz-transition: all 0.3s;
    -webkit-transition: all 0.3s;
    transition: all 0.3s;
    height: 90px !important;
    width: 160px !important;
}
.row.cstm_wallet > div:hover {
    box-shadow: 0px 0px 7px rgba(128,128,128,0.2);
}
</style>
@endsection

@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/admin/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Shipping                    </a>
						   <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						</div>
					</div>				  
				</div>
			</div>
		</div>
                <div class="kt-widget14">
					<div class='row'>
		    
					</div>
                </div> 
                <div class="kt-widget14">
	                @if (session('status')=='Sucess')
				    <div class="alert alert-success">
				        {{ session('status') }}
				    </div>
					@elseif (session('status') !='')
						<div class="alert alert-danger">
							{{ session('status') }}
						</div>
					@endif	
				<div class="row">
                	<div class='col-md-12' style="text-align:center;">
						<h3>Select Shipping Method</h3> 
					</div>
                </div>
                <div class="col-xl-12">
                    <div class="kt-widget14 payment_mthd" style="">
                    	<div class="row cstm_wallet">
							<div class='col-xs-12 col-md-3 col-lg-3' style="text-align:center;">
							    
							    <a href="{{url('admin/shipping/ePacket')}}"><img class="kt img-responsive" alt="Pic" style="height: 220px; width: 375px; margin: 0 39px;" src="{{asset(config('app.asset_path').'/assets/media/images/epacket.jpg')}}" /></a>
							    
                    		<!--<button type="button" onclick='window.location.href ="{{url('admin/shipping/ePacket')}}";' id="addStoreButton" class="btn btn-label-success btn-lg btn-upper" data-toggle="modal" data-target="#kt_modal_2">ePacket</button>-->
							</div>
                    		<div class='col-xs-12 col-md-3 col-lg-3' style="text-align:center;">
                    		    <a href="{{url('admin/shipping/eParcel')}}"><img class="kt img-responsive" alt="Pic" style="height: 220px; width: 375px; margin: 0 39px;" src="{{asset(config('app.asset_path').'/assets/media/images/eparcel.jpg')}}" /></a>
							</div>
                    		<div class='col-xs-12  col-md-3 col-lg-3' style="text-align:center;">
                    		    <a href="{{url('admin/shipping/Express-shipping')}}"><img class="kt img-responsive" alt="Pic" style="height: 220px; width: 375px; margin: 0 39px;" src="{{asset(config('app.asset_path').'/assets/media/images/express-shipping.jpg')}}" /></a>
							</div>
                    	</div>
                    
                   
                     </div>
                   <div class="kt-widget14">
                   <div class="kt-widget14">
                   
                </div>
                </div>                     
            </div>
        </div>

        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>
<!-- end:: Content -->
@endsection
@section('page_script')
@endsection
