@extends('layouts.admin')
@section('page_title')
Shipping | eparcel
@endsection

@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-xl-12">               
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head kt-portlet__head--lg">
        			<div class="kt-portlet__head-label" style="width: 100%;">
        				<div class="row" style="width: 100%;">
        					<div class="col-xs-12 col-md-9" style="text-align: left;">
        						<div class="kt-subheader__breadcrumbs">
        							<span class="kt-portlet__head-icon">
                    		         <i class="kt-font-brand flaticon2-line-chart"></i>
                    		        </span>						   
        						   <a href="{{url('/admin/home')}}" class="kt-subheader__breadcrumbs-link">
        						    Dashboard
        						   </a>
        						   <span class="kt-subheader__breadcrumbs-separator">
        						    - 
        						   </span>
        						   <a href="{{url('/admin/shipping')}}" class="kt-subheader__breadcrumbs-link">
        						    Shipping                    
        						   </a>
        						   <span class="kt-subheader__breadcrumbs-separator">
        						    - 
        						   </span>
        						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
        						    eParcel                   
        						   </span>
        						</div>
        					</div>
        					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick='window.location.href ="{{url('admin/shipping/eParcel/add')}}";' class="btn btn-outline-danger" style="width:100%;" title="Add eParcel"> Add eParcel</button>
						
					</div>
        				</div>
        			</div>
    		    </div>
    		    <div class="kt-portlet__body">
        		    @if (session('status')=='User updated!')
        			    <div class="alert alert-success fade show" role="alert">
        				  <div class="alert-text">{{ session('status') }}</div>
        				  <div class="alert-close">
        				      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        				          <span aria-hidden="true"><i class="la la-close"></i></span>
        				      </button>
        				  </div>
        				</div>
        			@elseif (session('status') !='')
        			    <div class="alert alert-danger fade show" role="alert">
        				  
        				  <div class="alert-text">{{ session('status') }}</div>
        				  <div class="alert-close">
        				      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        				          <span aria-hidden="true"><i class="la la-close"></i></span>
        				      </button>
        				  </div>
        				</div>
        			@endif
        		    <!--begin: Search Form -->
        		    <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
        		        <div class="row align-items-center">
        		            <div class="col-xl-8 order-2 order-xl-1">
        		                <div class="row align-items-center">
        		                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
        		                        <div class="kt-input-icon kt-input-icon--left">
        		                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
        		                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
        									<span><i class="la la-search"></i></span>
        		                            </span>
        		                        </div>
        		                    </div>                    
        		                </div>
        		            </div>            
        		        </div>
        		    </div>
        		    <!--end: Search Form -->
    		    </div>
    		    
    		    <div class="kt-portlet__body kt-portlet__body--fit">
                    <!--begin: Datatable -->
                    <div class="admin_user_datatable" id="kt_datatable"></div>
                    <!--end: Datatable -->
                </div>
			</div>
        </div>       
    </div>
</div>
@endsection
@section('page_script')

<script>

 var path = APP_URL+'/admin/shipping/eParcel/show';
   
   var datatable = $('.admin_user_datatable').KTDatatable({
  // datasource definition
  data: {
    type: 'remote',
    source: {
      read: {
        url: path,
        method: 'GET',
        map: function(raw) {
          // sample data mapping
          var dataSet = raw;
          if (typeof raw.data !== 'undefined') {
            dataSet = raw.data;
          }
          return dataSet;
        },
      },
    },
    pageSize: 100,
    serverPaging: false,
    serverFiltering: false,
    serverSorting: false,
  },

  // layout definition
  layout: {
    scroll: false,
    footer: false,
  },

  // column sorting
  sortable: true,

  pagination: true,

  search: {
    input: $('#generalSearch'),
  },

  // columns definition
  columns: [
    
    {
      field: 'name',
      title: 'Country',     
      width: 150,      
      textAlign: 'center',  
    },
    {
      field: 'cny_per_piece',
      title: 'Cny/piece',     
      width: 150,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      	const formatter = new Intl.NumberFormat('en-US', {
	    minimumFractionDigits: 2
	    })
	
	    var cny_per_piece =formatter.format(row.cny_per_piece);
	    return cny_per_piece;              
      },
    }, 
    {
      field: 'cny_per_gram',
      width: 180,
      title: 'Cny/gram',
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      	const formatter = new Intl.NumberFormat('en-US', {
	  
	  minimumFractionDigits: 2
	})
	
	var cny_per_gram =formatter.format(row.cny_per_gram);   
	return cny_per_gram;              
      },
    },
    {
      field: 'actions',
      title: 'Actions', 
      textAlign: 'center', 
      template: function(row, index, datatable) {
        return '<a href="{{url('/admin/shipping/eParcel/edit')}}/'+row.id+'" data-key="'+row.id+'" class="cstm_edit btn btn-hover-brand btn-icon btn-pill" title="Edit details">\
                    <i class="la la-edit"></i>\
                </a>\
                <a href="#" data-key="'+row.id+'" class="cstm_delet btn btn-hover-danger btn-icon btn-pill" title="Delete" onclick="('+row.id+')">\
                    <i class="la la-trash"></i>\
                </a>';             
      },
         
    }],

});

$(document).ready(function() {
    $(document).on('click','.cstm_delet', function(id){
        var id =$(this).attr("data-key");
            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then(function(result){
                if (result.value) {
                    var path = APP_URL + "/admin/shipping/eParcel/delete";
                    $.ajax({
                        method: "POST",
        				url: path,
        				data: { 
        					id: id
        				},
        				headers: {
        					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        				}
                    })
                    .done(function(msg){
                        swal.fire(
                        'Deleted!',
                        msg,
                        'success'
                    )
                        datatable.reload();
                    });
                    
                    // result.dismiss can be 'cancel', 'overlay',
                    // 'close', and 'timer'
                } else if (result.dismiss === 'cancel') {
                    txt = "You pressed Cancel!";
			        swal.fire("Cancelled", txt, "error")    
                }
            });
    });
});

</script>
@endsection