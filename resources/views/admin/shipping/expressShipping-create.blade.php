@extends('layouts.admin')
@section('page_title')
Shipping | Add Express Shipping
@endsection
@section('page_css')
<style>
</style>
@endsection
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head kt-portlet__head--lg">
        			<div class="kt-portlet__head-label" style="width: 100%;">
        				<div class="row" style="width: 100%;">
        					<div class="col-xs-12 col-md-9" style="text-align: left;">
        						<div class="kt-subheader__breadcrumbs">
        						    <span class="kt-portlet__head-icon">
                    		            <i class="kt-font-brand flaticon2-line-chart"></i>
                    		        </span>						   
        						    <a href="{{url('/admin/home')}}" class="kt-subheader__breadcrumbs-link">
        						        Dashboard                    
        						    </a>
        						    <span class="kt-subheader__breadcrumbs-separator">
        						        -
        						    </span>
        						    <a href="{{url('/admin/shipping')}}" class="kt-subheader__breadcrumbs-link">
        						        Shipping                    
        						    </a>
        						    <span class="kt-subheader__breadcrumbs-separator">
        						        - 
        						    </span>
        						    <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                                        Add Express Shipping
        						    </span>		   
        						</div>
        					</div>				  
        				</div>
        			</div>
    		    </div>
                <div class="kt-widget14">
                    <div class="kt-widget14__header kt-margin-b-30">
                        <!-- BEGIN PROFILE CONTENT -->
                        @if (session('status'))
					    <div class="alert alert-success">
					        {{ session('status') }}
					    </div>
						@endif

                        <form role="form" method="POST" action="{{url('admin/shipping/Express-shipping/store')}}" name="storeProfile" id="storeProfile">
                            @csrf                           
                            <input type="hidden"  name="id" id="id" value="{{isset($provider->id) ?  $provider->id : '' }}">
            
                            <div class="form-group">
                                <label class="control-label">Country</label>
                                <select class="form-control" id="country" name="country" required>
                                    <option value="">Select Country</option>
                                    @if(isset($countries) && $countries)
                                    @foreach($countries as $country)
                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                    @else
                                    <option value="0">No Country</option>
                                    @endif
                                </select>
                                @if ($errors->has('country'))
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('country') }}</strong>
                                </span> 
                                @endif 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Price Less Than 500</label>
                                <input type="number" step=".0001" placeholder="Enter Price Less Than 500" name="price_less_than_500" id="price_less_than_500" value="{{ isset($provider->price_less_than_500) ?  $provider->price_less_than_500 : '' }}" class="form-control" required>
                                @if ($errors->has('price_less_than_500'))
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('price_less_than_500') }}</strong>
                                </span> 
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">Price More Than 500</label>
                                <input type="number" step=".0001" placeholder="Enter Price More Than 500" name="price_more_than_500" id="price_more_than_500" value="{{ isset($provider->price_more_than_500) ?  $provider->price_more_than_500 : '' }}" class="form-control" required>
                                @if ($errors->has('price_more_than_500'))
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('price_more_than_500') }}</strong>
                                </span> 
                                @endif
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="col-md-12" style="text-align: center;">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <button style="width:100%;" type="submit" class="btn btn-primary" name="addExpressShipping" id="addExpressShipping">Save Changes</button>
                                            </div>
                                            <div class="col-md-4">
                                                <button style="width:100%;" type="button" onClick="window.location.href ='{{url('/admin/shipping/Express-shipping')}}';" class="btn btn-outline-danger">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>    
                        <!-- END PROFILE CONTENT -->
                    </div>
                </div>                    
            </div>
        </div>
        <!--end:: Widgets/Daily Sales-->
    </div>
    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
<script>
    $(document).ready(function() {
        $("#addExpressShipping").click(function() {
            var country = $("#country").val();
            var price_less_than_500 = $("#price_less_than_500").val();
            var price_more_than_500 = $("#price_more_than_500").val();

            if(country == '') {
                swal.fire('error', 'Please select a country.','error');
                return false;
            }
            if(price_less_than_500 == '') {
                swal.fire('error', 'Please fill the Price Less Than 500 field.','error');
                return false;
            }
            if(price_more_than_500 == '') {
                swal.fire('error', 'Please fill the Price More Than 500 field.','error');
                return false;
            }
        });
    });
</script>
@endsection