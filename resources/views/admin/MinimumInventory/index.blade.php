@extends('layouts.admin')
@section('page_title')
Minimum Inventory
@endsection

@section('page_css')
<style>
</style>
@endsection
@section('content')

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/admin/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						  Minimum Inventory                    </span>						   						   
						</div>
					
					</div>	
					<!--<div class="col-xs-12 col-md-3" style="text-align: right;">-->
					<!--	<button type="button" onclick='window.location.href ="{{url('admin/home')}}";' class="btn btn-outline-danger" style="width:100%;"> Back </button>-->
					<!--</div>			  -->
				</div>
			</div>
		</div>
		  
            <div class="kt-widget14">
	                @if (session('status') != '')
			<div class="alert alert-success">
			{{ session('status') }}
			 </div>
			@elseif (session('status-error') !='')
			<div class="alert alert-danger">
			{{ session('status-error') }}
			</div>
			@endif	
			
           <div class="kt-widget14__header kt-margin-b-30"> 
	   			<form role="form" method="POST" action="{{url('/admin/inventory/limitinventory/store')}}" name="limitinventorystore" id="limitinventorystore">
	        		@csrf
	        	
	        	   
		            <div class="form-group">
		                <label class="control-label">Inventory Minimum Quantity </label>
		                <input type="text" placeholder="Enter Quantity" name="inventory_minimum_quantity" id="inventory_minimum_quantity" value="{{$inventory_minimum_quantity }}" class="form-control">
		                @if($errors->has('inventory_minimum_quantity'))
		                <span class="invalid-feedback" style="display: block;">
		                    <strong>{{$errors->first('inventory_minimum_quantity')}}</strong>
		                </span>
		                @endif
		            </div>
		           
                     <div class="row">
						<div class="col-md-3" >
                        	<button style="width:100%;" type="submit" class="btn btn-primary" name="addinventorylimits" id="addinventorylimits">Save Changes</button>
						</div>
                    	<div  class="col-md-3" style="text-align: left;">
							<button style="width:100%;" type="button" onClick="window.location.href ='{{url('/admin/home')}}';" class="btn btn-outline-danger">Cancel</button>
	                 	</div>
                     </div>  
		         </form>                
			</div>
                		                   
           		
        	</div>
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
<script>
    $(document).ready(function() {
        $("#addinventorylimits").click(function() {
            var inventory_minimum_quantity = $("#inventory_minimum_quantity").val();
            
            if(inventory_minimum_quantity == '') {
                swal.fire('error',"Please fill the inventory minimum quantity field","error");
                return false;
            }
        });
    });
</script>
@endsection