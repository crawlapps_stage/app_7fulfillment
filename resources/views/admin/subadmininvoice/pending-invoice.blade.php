@extends('layouts.admin')
@section('page_title')
Pending Invoices
@endsection

@section('page_css')
<style>

</style>
@endsection

@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/admin/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Subadmin Invoices                   </span>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Pending Subadmin Invoices                     </span>
						</div>
					</div>				  
				</div>
			</div>
		</div>
		
		<div class="kt-portlet__body">
		    <!--begin: Search Form -->
		    <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
		        <div class="row align-items-center">
		            <div class="col-xl-8 order-2 order-xl-1">
		                <div class="row align-items-center">
		                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
		                        <div class="kt-input-icon kt-input-icon--left">
		                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
		                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
									<span><i class="la la-search"></i></span>
		                            </span>
		                        </div>
		                    </div>                    
		                </div>
		            </div>            
		        </div>
		    </div>
		    <!--end: Search Form -->
		</div>
                
                <div class="kt-widget14">   
			<!--begin: Datatable -->
			<div class="pen_photography_datatable" id="Pending_datatable"></div>
			<!--end: Datatable -->									
                </div>                   
            </div>
        </div>

        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>

<!-- end:: Content -->
@endsection
@section('page_script')

<script>

/*********** This code is working **/
	jQuery(document).ready(function(){
		jQuery(document).on( "click", 'td[data-field="product_link"] span a', function() {	
			window.open(jQuery(this).attr('href'));
			return false;	
		});
	});
/************************************/
</script>

<script>


   var path = APP_URL+'/admin/subadmin-invoice/pending-invoice/show';
   
   
   var datatable = $('.pen_photography_datatable').KTDatatable({
  // datasource definition
  data: {
    type: 'remote',
    source: {
      read: {
        url: path,
        method: 'GET',
        map: function(raw) {
          // sample data mapping
          var dataSet = raw;
          if (typeof raw.data !== 'undefined') {
            dataSet = raw.data;
          }
          return dataSet;
        },
      },
    },
    pageSize: 10,
    serverPaging: false,
    serverFiltering: false,
    serverSorting: false,
  },

  // layout definition
  layout: {
    scroll: false,
    footer: false,
  },

  // column sorting
  sortable: true,

  pagination: true,

  search: {
    input: $('#generalSearch'),
  },
  //sort: { sort: "desc", field: "created_at" },

  // columns definition
  columns: [
  
    {
      field: 'fname',
      title: 'Username',     
      width: 150,      
      textAlign: 'center',
      template: function(row, index, datatable) {
      
    //   	return "<strong>"+row.fname+" "+row.lname+"</strong>"+"\n"+ "("+row.email+")";
    
     var link_name = row.fname+' '+row.lname;
    var link_href =  '<a href="mailto:'+row.email+'">'+row.email+'</a>';
    return '<strong>'+link_name+'</strong>'+'\n'+ link_href;
    // return '<a href="'+row.product_link+'" target="_blank">'+row.product_link+'</a>';
      	
      },
    },
    {
      field: 'product_link',
      title: 'Product Link ',     
      width: 180,      
      textAlign: 'center',
      template: function(row, index, datatable) {
        return '<a href="'+row.product_link+'" target="_blank">'+row.product_link+'</a>';                
      },
    }, {
      field: 'product_price',
      width: 120,
      title: 'Product Price ($)',
      textAlign: 'center',
      template: function(row, index, datatable) {
      
      	const formatter = new Intl.NumberFormat('en-US', {
	  
	  minimumFractionDigits: 2
	})
	
	var product_price =formatter.format(row.product_price); 
      
	//var amount = parseFloat(Math.round(row.amount * 100) / 100).toFixed(2);
	    
	return product_price;              
      },
    },
    {
      field: 'total_price',
      title: 'Total Price ($)',
      width: 200,
      textAlign: 'center',
      template: function(row, index, datatable) {
      
      	const formatter = new Intl.NumberFormat('en-US', {
	  
	  minimumFractionDigits: 2
	})
	
	var total_price =formatter.format(row.total_price); 
      
	//var amount = parseFloat(Math.round(row.amount * 100) / 100).toFixed(2);
	    
	return total_price;              
      },
    },
    {
      field: 'id',
      title: 'Action',
      textAlign: 'center',
      
      template: function(row, index, datatable) {
        return '</a>\
                    <a href="#" data-key="'+row.id+'" class="cstm_approve btn btn-success background_gradient btn-view" title="Complete" onclick="('+row.id+')">\
                       Complete\
                    </a>';                
      },
    },    
    {
      field: 'created_at',
      width: 150,
      title: 'Date',
      type: 'date',
      
      textAlign: 'center',
      template: function(row, index, datatable) {
      
	var dateTime = row.created_at;
	var date = dateTime.split(" ");
	var result = date[0];
	var resulted_date =  result.split("-");
	var date_to_show = resulted_date[2]+'-'+resulted_date[1]+'-'+resulted_date[0];    
	return date_to_show;
	//return row.created_at;              
      },
    }],

});


jQuery(document).ready(function(){

	jQuery(document).on( "click", ".cstm_approve", function(id) {
	
		var r = confirm("Do you want to mark this request completed!");
		if (r == true) {
		
			var id = $(this).attr("data-key");
						
			var path = APP_URL + "/admin/photography/pending-photography";  
			$.ajax({
			
				method: "POST",
				url: path,
				data: { 
					id: id
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			})
			.done(function( msg ) {
			
			
				alert( msg );
				datatable.reload();
			});
		} else {
			txt = "You pressed Cancel!";
			alert(txt);
		}	
	});
});

</script>

@endsection