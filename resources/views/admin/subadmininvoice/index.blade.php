@extends('layouts.admin')
@section('page_title')
Subadmin Invoice
@endsection
@section('page_css')
@endsection
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/admin/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Subadmin Invoice                   </span>						   						   
						</div>
					</div>				  
				</div>
			</div>
		</div>
		
                <div class="kt-widget14">
			<div class='row'>
		         </div>
                </div> 
                <div class="kt-widget14">
	                @if (session('status')=='Sucess')
			<div class="alert alert-success">
			{{ session('status') }}
			 </div>
			@elseif (session('status') !='')
			<div class="alert alert-danger">
			{{ session('status') }}
			</div>
			@endif	
			<div class="row">
	                	<div class='col-md-12' style="text-align:center;">
					<h3>Subadmin Invoice coming soon.</h3> 
				</div>
                	</div>
                	<div class="container">                    
                   		<div class="kt-widget14">
                   			<div class="kt-widget14">                  
                			</div>
                		</div>                     
           		</div>
        	</div>
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>
<!-- end:: Content -->
@endsection
@section('page_script')
@endsection