@extends('layouts.admin')
@section('page_title')
View Ticket
@endsection
@section('page_css')
<style>
	ul.ticket_chat_list {
    	list-style: none;
    	padding-left: 0;
	}
	ul.ticket_chat_list {
	    float: left;
	    width: 100%;
	    max-height: 600px;
	    overflow-y: scroll;
	}
	li.user_reply {
	    border: 1px solid #5867dd;
	    margin-bottom: 20px;
	    padding: 10px;
	    border-radius: 15px;
	    width: 65%;
	    display: block;
	    float: left;
	    clear: both;
	    background: #f7f7f7;
	}
	li.admin_reply {
	    border: 1px solid #2b2c2d;
	    margin-bottom: 20px;
	    padding: 10px;
	    border-radius: 15px;
	    width: 65%;
	    display: block;
	    float: right;
	    clear: both;
	    background: #80808036;
	}
	.ticket_subject {
	    font-size: 18px;
	    font-weight: 400;
	    margin-bottom: 20px;
	    color: #000;
	    padding: 10px;
    	margin-top: 12px;
    	background: #efefef;
	}
	.inner_img img {
	    border: 1px solid #00000047;
	    border-radius: 45%;
	}
	.img_content {
	    display: inline-block;
	    width: 6%;
	    float: left;
	}
	.msg_content {
   	 	display: inline-block;
    	width: 100%;
	}
	.msg_content .time {
    	text-align: right;
	    color: #000;
	    font-weight: 400;
	    display: inline;
	    float: right;
	    position: relative;
	    top: -22px;
	    width:100%;
	}
	.tic_details div {
	    font-size: 16px;
	    font-weight: 600;
	    color: #5867dd;
	}
	.order_id {
    	display: inline-block;
    	text-align: left;
	}
	.ticket_info {
    	display: inline-block;
    	text-align: right;
    	float: right;
	}
	span.time_show {
    	padding-left: 4px;
	}
	.img_content2 {
    	display: inline-block;
    	float: left;
    	width: 100%;
	}
	.user_reply span.reply_name {
    	font-size: 12px;
    	font-weight: 600;
    	color: #5867dd;
	}
	.admin_reply span.reply_name {
    	font-size: 12px;
    	font-weight: 600;
    	color: #000;
	}
	.msg {
    	font-size: 14px;
	    color: #000;
	    width: 100%;
	    position: relative;
	    top: -18px;
	}
</style>
@endsection
@section('content')
<!-- begin:: Content Head -->

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   View Ticket                    </span>
						 
						</div>
					</div>	
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick='window.location.href ="{{url('/admin/all-tickets')}}";' class="btn btn-outline-danger" style="width:100%;"> Back </button>
					</div>				  
				</div>
			</div>
		</div>
		
           
            <div class="kt-widget14">
	            @if (session('status') != '')
					<div class="alert alert-success">
						{{ session('status') }}
		 			</div>
				@elseif (session('status-error') !='')
					<div class="alert alert-danger">
						{{ session('status-error') }}
					</div>
				@endif
				<?php //print_r($ticket_details);
					//echo ;//print_r($chat_details);
				?>
	            <div class="kt-widget14__header kt-margin-b-30">
	            	<div class="tic_details">
	            		<div class="order_id">Order ID: <?php echo $ticket_details->order_id;?></div>
	            		<div class="ticket_info">Ticket ID: <?php echo $ticket_details->ticket_id;?></div>	
	            	</div> 
	            
	            	<div class="ticket_subject"><?php echo $ticket_details->subject;?></div>
	            	<ul class="ticket_chat_list">
	            		<?php 
	            		foreach($chat_details as $key => $sing_chat){
	            			$classname = '';
	            			$namereply = '' ;
	            			if($sing_chat->reply_by_user == 1){
	            				$classname = 'user_reply';
	            				$namereply = '<span class="reply_name">You:</span>';
	            			}else{
	            				$classname = 'admin_reply';
	            				$namereply = '<span class="reply_name">Admin:</span>';
	            			}
	            		?>
	            			<li class="<?php echo $classname;?>">
	            				<div class="inner_loop">
	            					<div class="img_content2">
	            						<div class="inner_img2">
	            							<!-- <img src="{{url('uploads/')}}/demo-img.png" height="60px"> -->
	            							<?php echo $namereply; ?>
	            						</div>
	            					</div>	
	            					<div class="msg_content">
	            						<div class="time"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect id="bound" x="0" y="0" width="24" height="24"/>
        <path d="M12,22 C7.02943725,22 3,17.9705627 3,13 C3,8.02943725 7.02943725,4 12,4 C16.9705627,4 21,8.02943725 21,13 C21,17.9705627 16.9705627,22 12,22 Z" id="Mask" fill="#000000" opacity="0.3"/>
        <path d="M11.9630156,7.5 L12.0475062,7.5 C12.3043819,7.5 12.5194647,7.69464724 12.5450248,7.95024814 L13,12.5 L16.2480695,14.3560397 C16.403857,14.4450611 16.5,14.6107328 16.5,14.7901613 L16.5,15 C16.5,15.2109164 16.3290185,15.3818979 16.1181021,15.3818979 C16.0841582,15.3818979 16.0503659,15.3773725 16.0176181,15.3684413 L11.3986612,14.1087258 C11.1672824,14.0456225 11.0132986,13.8271186 11.0316926,13.5879956 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" id="Path-107" fill="#000000"/>
    </g>
</svg><span class="time_show"><?php echo $sing_chat->created_at;?></span> </div>
	            						<div class="msg"><?php echo $sing_chat->message;?></div>
	            					</div>
	            				</div>
	            			</li>	
	            		<?php 
	            		}
	            		?>
	            	</ul>
	            	<form role="form" method="POST" action="{{url('admin/update-ticket/store')}}" name="edit_ticket" id="edit_ticket">
		        		@csrf
		        		
		        	    <input type="hidden" name="ticket_id" value="<?php echo $ticket_details->ticket_id;?>">
		        	   
		        	      <div class="form-group">
			               	
			            </div>
			               <div class="form-group" style="width:100%;">
			                <label class="control-label" style="width: 100%;">Status</label>
			                <select name="ticket_status" style="width: 100%;" id="ticket_status">
			                	<option value="0" <?php if($ticket_details->status == 0){echo 'selected="selected"';}?> >Open</option>
			                	<option value="1" <?php if($ticket_details->status == 1){echo 'selected="selected"';}?> >Resolved</option>
			                </select> 
			            </div>
      					<div class="form-group" style="width:100%;">
			                <label class="control-label" style="width: 100%;">Message</label>
			                <textarea name="support_message" style="width: 100%;height: 260px;border: 1px solid #ebedf2;"></textarea> 
			            </div>

			          
			             <div class="row">
							<div class="col-md-3" >
	                        	<button style="width:100%;" type="submit" class="btn btn-primary" name="addinvoices" id="addinvoices">Submit</button>
							</div>
	                    	<div  class="col-md-3" style="text-align: left;">
								<button style="width:100%;" type="button" onClick="window.location.href ='{{url('/admin/all-tickets')}}';" class="btn btn-outline-danger">Cancel</button>
		                 	</div>
	                     </div>  
			         </form>                
				</div>
        	</div>
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>
<!-- end:: Content -->
@endsection
@section('page_script')
<script>
	jQuery('#ticket_status').select2({
		placeholder:'Status'
	});
</script>
@endsection