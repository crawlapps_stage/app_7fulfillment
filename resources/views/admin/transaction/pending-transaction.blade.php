@extends('layouts.admin')
@section('page_title')
Pending Transactions
@endsection

@section('page_css')
<style>

</style>
@endsection

@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/admin/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Transactions                    </span>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Pending Transactions                    </span>
						   <!--
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Wired Transfer                   </a>-->
						   <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						</div>
					</div>				  
				</div>
			</div>
		</div>
		<!--
            <div class="kt-portlet__head kt-portlet__head--lg">
		    <div class="kt-portlet__head-label" style="width: 100%;">
		         <span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>
		         <div class="row" style="width: 100%;">
				  <div class="col-xs-12 col-md-9" style="text-align: left;">
					  <h3 class="kt-portlet__head-title" style="margin-top:6px;">
						Pending Transactions
					 </h3>
				  </div>
				  
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick="window.location.href ='http://7fulfillment.com/portal/wallet';" class="btn btn-outline-danger" style="width:100%;"> Back</button>
					</div>
					
				</div>
		    </div>
		</div>
		-->
		
		<div class="kt-portlet__body">
		    <!--begin: Search Form -->
		    <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
		        <div class="row align-items-center">
		            <div class="col-xl-8 order-2 order-xl-1">
		                <div class="row align-items-center">
		                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
		                        <div class="kt-input-icon kt-input-icon--left">
		                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
		                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
									<span><i class="la la-search"></i></span>
		                            </span>
		                        </div>
		                    </div>                    
		                </div>
		            </div>            
		        </div>
		    </div>
		    <!--end: Search Form -->
		</div>
                
                <div class="kt-widget14">   
			<!--begin: Datatable -->
			<div class="pen_tran_datatable" id="Pending_datatable"></div>
			<!--end: Datatable -->									
                </div>                   
            </div>
        </div>

        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>

<!-- end:: Content -->
@endsection
@section('page_script')

<script>

/*********** This code is working **/
	jQuery(document).ready(function(){
		jQuery(document).on( "click", 'td[data-field="proof_of_payment"] span a', function() {	
			window.open(jQuery(this).attr('href'));
			return false;	
		});
	});
/************************************/
</script>

<script>
/*********** This code is not working **/
	/*
	jQuery(document).ready(function(){
	
		jQuery('td[data-field="proof_of_payment"] span a').click(function(){
			window.open(jQuery(this).attr('href'));
			return false;
		});
	});
	*/
/************************************/
</script>



<script>


   var path = APP_URL+'/admin/transaction/pending-transaction/show';
   
   
   var datatable = $('.pen_tran_datatable').KTDatatable({
  // datasource definition
  data: {
    type: 'remote',
    source: {
      read: {
        url: path,
        method: 'GET',
        map: function(raw) {
          // sample data mapping
          var dataSet = raw;
          if (typeof raw.data !== 'undefined') {
            dataSet = raw.data;
          }
          return dataSet;
        },
      },
    },
    pageSize: 10,
    serverPaging: false,
    serverFiltering: false,
    serverSorting: false,
  },

  // layout definition
  layout: {
    scroll: false,
    footer: false,
  },

  // column sorting
  sortable: true,

  pagination: true,

  search: {
    input: $('#generalSearch'),
  },
  //sort: { sort: "desc", field: "created_at" },

  // columns definition
  columns: [
  /*
  {
      field: 'fname',
      title: 'Name',     
      width: 180,      
      textAlign: 'center',
      template: function(row, index, datatable) {
      	
	return row.fname+' '+row.lname;             
      },
    },
    {
      field: 'email',
      title: 'Email',     
      width: 180,      
      textAlign: 'center',
    },
    */
    {
      field: 'fname',
      title: 'Username',     
      width: 150,      
      textAlign: 'center',
      template: function(row, index, datatable) {
      
      	return "<strong>"+row.fname+" "+row.lname+"</strong>"+"\n"+ "("+row.email+")";              
      },
    },
    {
      field: 'txn_id',
      title: 'Transaction ID',     
      width: 180,      
      textAlign: 'center',
    }, {
      field: 'amount_without_fee',
      width: 120,
      title: 'Amount ($)',
      textAlign: 'center',
      template: function(row, index, datatable) {
      
      	const formatter = new Intl.NumberFormat('en-US', {
	  
	  minimumFractionDigits: 2
	})
	
	var amount =formatter.format(row.amount_without_fee); 
      
	//var amount = parseFloat(Math.round(row.amount * 100) / 100).toFixed(2);
	    
	return amount;              
      },
    },
    {
      field: 'amount',
      title: 'Amount Including Fee ($)',
      width: 200,
      textAlign: 'center',
      template: function(row, index, datatable) {
      
      	const formatter = new Intl.NumberFormat('en-US', {
	  
	  minimumFractionDigits: 2
	})
	
	var amount =formatter.format(row.amount); 
      
	//var amount = parseFloat(Math.round(row.amount * 100) / 100).toFixed(2);
	    
	return amount;              
      },
    },{
      field: 'proof_of_payment',
      title: 'View Proof',
      textAlign: 'center',
      template: function(row, index, datatable) {
      
      	
      
      	var proof_of_payment = '<a href="{{ asset(config('app.asset_path').'/image')}}/'+row.proof_of_payment+'" target="_blank">'+row.proof_of_payment+'</a>';
    	
        if(row.proof_of_payment == null){
      		return 'No Proof';
      	}
      	else {
      		return proof_of_payment;
      	}	    
	             
      },
    },
     {
      field: 'payment_method',
      width: 150,
      title: 'Payment Method',
      textAlign: 'center',
    },
    {
      field: 'id',
      title: 'Approve Transaction',
      textAlign: 'center',
      
      template: function(row, index, datatable) {
        return '</a>\
                    <a href="#" data-key="'+row.id+'" class="cstm_approve btn btn-success background_gradient btn-view" title="Approve" onclick="('+row.id+')">\
                       Approve\
                    </a>';
                    
      },
      
      /*
      template: function(row, index, datatable) {      	     	
      	return '\
                 <a href="{{url('/admin/transaction/pending-transaction')}}/'+row.id+'" class="btn btn-success background_gradient btn-view" title="Approve">\
                    Approve\
                    </a>\
                 ';        
      },
      */
    },    
    {
      field: 'created_at',
      width: 150,
      title: 'Date',
      type: 'date',
      
      textAlign: 'center',
      template: function(row, index, datatable) {
      
	var dateTime = row.created_at;
	var date = dateTime.split(" ");
	var result = date[0];
	var resulted_date =  result.split("-");
	var date_to_show = resulted_date[2]+'-'+resulted_date[1]+'-'+resulted_date[0];    
	return date_to_show;
	//return row.created_at;              
      },
    }],

});


jQuery(document).ready(function(){

	jQuery(document).on( "click", ".cstm_approve", function(id) {
	
		var r = confirm("Are you really want to Approve Payment!");
		if (r == true) {
		
			var id = $(this).attr("data-key");
						
			var path = APP_URL + "/admin/transaction/pending-transaction";  
			$.ajax({
			
				method: "POST",
				url: path,
				data: { 
					id: id
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			})
			.done(function( msg ) {
			
			
				alert( msg );
				datatable.reload();
			});
		} else {
			txt = "You pressed Cancel!";
			alert(txt);
		}	
	});
});


/*
function approve(id){


  var path = APP_URL + "/location-licensee/create";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
     var res = $.parseJSON(result);
     if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
     $('.sweet-overlay').remove();
     $('.showSweetAlert ').remove();
     $("#ResponseSuccessModal").modal('show');
     $("#ResponseSuccessModal #ResponseHeading").text(res.message);
   } 
 },
 error: function(){
  alert("Error");
}
}); 
}
*/

</script>

@endsection
