@extends('layouts.admin')
@section('page_title')
Dashboard
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">        
        <div class="col-md-12">
			<div id="profile_login">
				<div class="profile-body">
				<h5 style="text-align:center">Welcome to 7Fulfillment! Admin </h5>
				
            <!--
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                </div>
                -->
                </div>
            </div>
            
        </div>       
    </div>
</div>
@endsection
@section('page_script')
<script>
 
	        $(document).ready(function() {
	            var url = window.location.href;
	            if(url == 'https://7fulfillment.com/portal/admin/home'){
	                window.location.href = "https://app.7fulfillment.com/admin/home"; 
	            }
	        });
	</script>
@endsection
