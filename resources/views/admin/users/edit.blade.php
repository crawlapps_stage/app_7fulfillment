@extends('layouts.admin')
@section('page_title')
Admin | Edit User
@endsection

@section('page_css')
<style>

</style>
@endsection

@section('content')
<!-- begin:: Content Head -->
	
	<!--<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	    <div class="kt-subheader__main">
	        <h3 class="kt-subheader__title">Profile</h3>
	        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
	        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
	            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
	            <span class="kt-input-icon__icon kt-input-icon__icon--right">
	                <span><i class="flaticon2-search-1"></i></span>
	            </span>
	        </div>
	    </div>
	    <div class="kt-subheader__toolbar">
	        <div class="kt-subheader__wrapper">     
	        </div>
	    </div>
	</div>
	-->
	
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/admin/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <a href="{{url('/admin/users')}}" class="kt-subheader__breadcrumbs-link">
						   Users                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Edit User                    </span>						   						   
						</div>
					</div>				  
				</div>
			</div>
		</div>
		<!--
            	<div class="kt-portlet__head kt-portlet__head--lg">
		    <div class="kt-portlet__head-label" style="width: 100%;">
		         <span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>
		         <div class="row" style="width: 100%;">
				  <div class="col-xs-12 col-md-9" style="text-align: left;">
					  <h3 class="kt-portlet__head-title" style="margin-top:6px;">
						Admin Profile
					 </h3>
				  </div>
				  
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick="window.location.href ='http://7fulfillment.com/portal/wallet';" class="btn btn-outline-danger" style="width:100%;"> Back</button>
					</div>
					
				</div>
		    </div>
		</div>
		-->
                <div class="kt-widget14">
                    <div class="kt-widget14__header kt-margin-b-30">
                        <!-- BEGIN PROFILE CONTENT -->
                        @if (session('status'))
					    <div class="alert alert-success">
					        {{ session('status') }}
					    </div>
						@endif
			
                        <form role="form" method="POST" action="{{url('admin/user/store')}}" name="storeProfile" id="storeProfile">
                        	@csrf                        	
							<input type="hidden"  name="id" id="id" value="{{isset($user->id) ?  $user->id : '' }}">

<div class="row">
							  	<div class="col-md-12">
									<div class="row">
									    <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">First Name</label>
                                <input type="text" placeholder="First Name" name="fname" id="fname" class="form-control" value="{{isset($user->fname) ?  $user->fname : '' }}"> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Last Name</label>
                                <input type="text" placeholder="Last Name" name="lname" id="lname" value="{{ isset($user->lname) ?  $user->lname : '' }}" class="form-control"> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Email</label>
                                <input type="email" placeholder="Email" name="email" id="email" value="{{ isset($user->email) ?  $user->email : '' }}" class="form-control"> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Skype</label>
                                <input type="text" placeholder="Skype" name="skype" id="skype" value="{{ isset($user->skype) ?  $user->skype : '' }}" class="form-control"> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Phone</label>
                                <input type="text" placeholder="Phone Number" name="phone" maxlength="12" id="phone" value="{{ isset($user->phone) ?  $user->phone : '' }}" class="form-control"> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Country</label>
                                <select class="form-control" id="country" name="country" value="{{isset($user->country) ?  'selected="selected"' : '' }}"  >
									<option value="Afghanistan" {{($user->country == 'Afghanistan') ?  'selected="selected"' : '' }}>Afghanistan</option>
									<option value="Albania" {{($user->country == 'Albania') ?  'selected="selected"' : '' }}>Albania</option>
									<option value="Algeria" {{($user->country == 'Algeria') ?  'selected="selected"' : '' }}>Algeria</option>
									<option value="American Samoa" {{($user->country == 'American Samoa') ?  'selected="selected"' : '' }}>American Samoa</option>
									<option value="Andorra" {{($user->country == 'Andorra') ?  'selected="selected"' : '' }}>Andorra</option>
									<option value="Angola" {{($user->country == 'Angola') ?  'selected="selected"' : '' }}>Angola</option>
									<option value="Anguilla" {{($user->country == 'Anguilla') ?  'selected="selected"' : '' }}>Anguilla</option>
									<option value="Antartica" {{($user->country == 'Antartica') ?  'selected="selected"' : '' }}>Antarctica</option>
									<option value="Antigua and Barbuda" {{($user->country == 'Antigua and Barbuda') ?  'selected="selected"' : '' }}>Antigua and Barbuda</option>
									<option value="Argentina" {{($user->country == 'Argentina') ?  'selected="selected"' : '' }}>Argentina</option>
									<option value="Armenia" {{($user->country == 'Armenia') ?  'selected="selected"' : '' }}>Armenia</option>
									<option value="Aruba" {{($user->country == 'Aruba') ?  'selected="selected"' : '' }}>Aruba</option>
									<option value="Australia" {{($user->country == 'Australia') ?  'selected="selected"' : '' }}>Australia</option>
									<option value="Austria" {{($user->country == 'Austria') ?  'selected="selected"' : '' }}>Austria</option>
									<option value="Azerbaijan" {{($user->country == 'Azerbaijan') ?  'selected="selected"' : '' }}>Azerbaijan</option>
									<option value="Bahamas" {{($user->country == 'Bahamas') ?  'selected="selected"' : '' }}>Bahamas</option>
									<option value="Bahrain" {{($user->country == 'Bahrain') ?  'selected="selected"' : '' }}>Bahrain</option>
									<option value="Bangladesh" {{($user->country == 'Bangladesh') ?  'selected="selected"' : '' }}>Bangladesh</option>
									<option value="Barbados" {{($user->country == 'Barbados') ?  'selected="selected"' : '' }}>Barbados</option>
									<option value="Belarus" {{($user->country == 'Belarus') ?  'selected="selected"' : '' }}>Belarus</option>
									<option value="Belgium" {{($user->country == 'Belgium') ?  'selected="selected"' : '' }}>Belgium</option>
									<option value="Belize" {{($user->country == 'Belize') ?  'selected="selected"' : '' }}>Belize</option>
									<option value="Benin" {{($user->country == 'Benin') ?  'selected="selected"' : '' }}>Benin</option>
									<option value="Bermuda" {{($user->country == 'Bermuda') ?  'selected="selected"' : '' }}>Bermuda</option>
									<option value="Bhutan" {{($user->country == 'Bhutan') ?  'selected="selected"' : '' }}>Bhutan</option>
									<option value="Bolivia" {{($user->country == 'Bolivia') ?  'selected="selected"' : '' }}>Bolivia</option>
									<option value="Bosnia and Herzegowina" {{($user->country == 'Bosnia and Herzegowina') ?  'selected="selected"' : '' }}>Bosnia and Herzegowina</option>
									<option value="Botswana" {{($user->country == 'Botswana') ?  'selected="selected"' : '' }}>Botswana</option>
									<option value="Bouvet Island" {{($user->country == 'Bouvet Island') ?  'selected="selected"' : '' }}>Bouvet Island</option>
									<option value="Brazil" {{($user->country == 'Brazil') ?  'selected="selected"' : '' }}>Brazil</option>
									<option value="British Indian Ocean Territory" {{($user->country == 'British Indian Ocean Territory') ?  'selected="selected"' : '' }}>British Indian Ocean Territory</option>
									<option value="Brunei Darussalam" {{($user->country == 'Brunei Darussalam') ?  'selected="selected"' : '' }}>Brunei Darussalam</option>
									<option value="Bulgaria" {{($user->country == 'Bulgaria') ?  'selected="selected"' : '' }}>Bulgaria</option>
									<option value="Burkina Faso" {{($user->country == 'Burkina Faso') ?  'selected="selected"' : '' }}>Burkina Faso</option>
									<option value="Burundi" {{($user->country == 'Burundi') ?  'selected="selected"' : '' }}>Burundi</option>
									<option value="Cambodia" {{($user->country == 'Cambodia') ?  'selected="selected"' : '' }}>Cambodia</option>
									<option value="Cameroon" {{($user->country == 'Cameroon') ?  'selected="selected"' : '' }}>Cameroon</option>
									<option value="Canada" {{($user->country == 'Canada') ?  'selected="selected"' : '' }}>Canada</option>
									<option value="Cape Verde" {{($user->country == 'Cape Verde') ?  'selected="selected"' : '' }}>Cape Verde</option>
									<option value="Cayman Islands" {{($user->country == 'Cayman Islands') ?  'selected="selected"' : '' }}>Cayman Islands</option>
									<option value="Central African Republic" {{($user->country == 'Central African Republic') ?  'selected="selected"' : '' }}>Central African Republic</option>
									<option value="Chad" {{($user->country == 'Chad') ?  'selected="selected"' : '' }}>Chad</option>
									<option value="Chile" {{($user->country == 'Chile') ?  'selected="selected"' : '' }}>Chile</option>
									<option value="China" {{($user->country == 'China') ?  'selected="selected"' : '' }}>China</option>
									<option value="Christmas Island" {{($user->country == 'Christmas Island') ?  'selected="selected"' : '' }}>Christmas Island</option>
									<option value="Cocos Islands" {{($user->country == 'Cocos Islands') ?  'selected="selected"' : '' }}>Cocos (Keeling) Islands</option>
									<option value="Colombia" {{($user->country == 'Colombia') ?  'selected="selected"' : '' }}>Colombia</option>
									<option value="Comoros" {{($user->country == 'Comoros') ?  'selected="selected"' : '' }}>Comoros</option>
									<option value="Congo" {{($user->country == 'Congo') ?  'selected="selected"' : '' }}>Congo</option>
									
									<option value="Cook Islands" {{($user->country == 'Cook Islands') ?  'selected="selected"' : '' }}>Cook Islands</option>
									<option value="Costa Rica" {{($user->country == 'Costa Rica') ?  'selected="selected"' : '' }}>Costa Rica</option>
									<option value="Cota DIvoire" {{($user->country == 'Cota DIvoire') ?  'selected="selected"' : '' }}>Cote dIvoire</option>
									<option value="Croatia" {{($user->country == 'Croatia') ?  'selected="selected"' : '' }}>Croatia (Hrvatska)</option>
									<option value="Cuba" {{($user->country == 'Cuba') ?  'selected="selected"' : '' }}>Cuba</option>
									<option value="Cyprus" {{($user->country == 'Cyprus') ?  'selected="selected"' : '' }}>Cyprus</option>
									<option value="Czech Republic" {{($user->country == 'Czech Republic') ?  'selected="selected"' : '' }}>Czech Republic</option>
									<option value="Denmark" {{($user->country == 'Denmark') ?  'selected="selected"' : '' }}>Denmark</option>
									<option value="Djibouti" {{($user->country == 'Djibouti') ?  'selected="selected"' : '' }}>Djibouti</option>
									<option value="Dominica" {{($user->country == 'Dominica') ?  'selected="selected"' : '' }}>Dominica</option>
									<option value="Dominican Republic" {{($user->country == 'Dominican Republic') ?  'selected="selected"' : '' }}>Dominican Republic</option>
									<option value="East Timor" {{($user->country == 'East Timor') ?  'selected="selected"' : '' }}>East Timor</option>
									<option value="Ecuador" {{($user->country == 'Ecuador') ?  'selected="selected"' : '' }}>Ecuador</option>
									<option value="Egypt" {{($user->country == 'Egypt') ?  'selected="selected"' : '' }}>Egypt</option>
									<option value="El Salvador" {{($user->country == 'El Salvador') ?  'selected="selected"' : '' }}>El Salvador</option>
									<option value="Equatorial Guinea" {{($user->country == 'Equatorial Guinea') ?  'selected="selected"' : '' }}>Equatorial Guinea</option>
									<option value="Eritrea" {{($user->country == 'Eritrea') ?  'selected="selected"' : '' }}>Eritrea</option>
									<option value="Estonia" {{($user->country == 'Estonia') ?  'selected="selected"' : '' }}>Estonia</option>
									<option value="Ethiopia" {{($user->country == 'Ethiopia') ?  'selected="selected"' : '' }}>Ethiopia</option>
									<option value="Falkland Islands" {{($user->country == 'Falkland Islands') ?  'selected="selected"' : '' }}>Falkland Islands (Malvinas)</option>
									<option value="Faroe Islands" {{($user->country == 'Faroe Islands') ?  'selected="selected"' : '' }}>Faroe Islands</option>
									<option value="Fiji" {{($user->country == 'Fiji') ?  'selected="selected"' : '' }}>Fiji</option>
									<option value="Finland" {{($user->country == 'Finland') ?  'selected="selected"' : '' }}>Finland</option>
									<option value="France" {{($user->country == 'France') ?  'selected="selected"' : '' }}>France</option>
									
									<option value="Gabon" {{($user->country == 'Gabon') ?  'selected="selected"' : '' }}>Gabon</option>
									<option value="Gambia" {{($user->country == 'Gambia') ?  'selected="selected"' : '' }}>Gambia</option>
									<option value="Georgia" {{($user->country == 'Georgia') ?  'selected="selected"' : '' }}>Georgia</option>
									<option value="Germany" {{($user->country == 'Germany') ?  'selected="selected"' : '' }}>Germany</option>
									<option value="Ghana" {{($user->country == 'Ghana') ?  'selected="selected"' : '' }}>Ghana</option>
									<option value="Gibraltar" {{($user->country == 'Gibraltar') ?  'selected="selected"' : '' }}>Gibraltar</option>
									<option value="Greece" {{($user->country == 'Greece') ?  'selected="selected"' : '' }}>Greece</option>
									<option value="Greenland" {{($user->country == 'Greenland') ?  'selected="selected"' : '' }}>Greenland</option>
									<option value="Grenada" {{($user->country == 'Grenada') ?  'selected="selected"' : '' }}>Grenada</option>
									<option value="Guadeloupe" {{($user->country == 'Guadeloupe') ?  'selected="selected"' : '' }}>Guadeloupe</option>
									<option value="Guam" {{($user->country == 'Guam') ?  'selected="selected"' : '' }}>Guam</option>
									<option value="Guatemala" {{($user->country == 'Guatemala') ?  'selected="selected"' : '' }}>Guatemala</option>
									<option value="Guinea" {{($user->country == 'Guinea') ?  'selected="selected"' : '' }}>Guinea</option>
									<option value="Guinea-Bissau" {{($user->country == 'Guinea-Bissau') ?  'selected="selected"' : '' }}>Guinea-Bissau</option>
									<option value="Guyana" {{($user->country == 'Guyana') ?  'selected="selected"' : '' }}>Guyana</option>
									<option value="Haiti" {{($user->country == 'Haiti') ?  'selected="selected"' : '' }}>Haiti</option>
									
									
									<option value="Honduras" {{($user->country == 'Honduras') ?  'selected="selected"' : '' }}>Honduras</option>
									<option value="Hong Kong" {{($user->country == 'Hong Kong') ?  'selected="selected"' : '' }}>Hong Kong</option>
									<option value="Hungary" {{($user->country == 'Hungary') ?  'selected="selected"' : '' }}>Hungary</option>
									<option value="Iceland" {{($user->country == 'Iceland') ?  'selected="selected"' : '' }}>Iceland</option>
									<option value="India" {{($user->country == 'India') ?  'selected="selected"' : '' }}>India</option>
									<option value="Indonesia" {{($user->country == 'Indonesia') ?  'selected="selected"' : '' }}>Indonesia</option>
									<option value="Iran" {{($user->country == 'Iran') ?  'selected="selected"' : '' }}>Iran (Islamic Republic of)</option>
									<option value="Iraq" {{($user->country == 'Iraq') ?  'selected="selected"' : '' }}>Iraq</option>
									<option value="Ireland" {{($user->country == 'Ireland') ?  'selected="selected"' : '' }}>Ireland</option>
									<option value="Israel" {{($user->country == 'Israel') ?  'selected="selected"' : '' }}>Israel</option>
									<option value="Italy" {{($user->country == 'Italy') ?  'selected="selected"' : '' }}>Italy</option>
									<option value="Jamaica" {{($user->country == 'Jamaica') ?  'selected="selected"' : '' }}>Jamaica</option>
									<option value="Japan"{{($user->country == 'Japan') ?  'selected="selected"' : '' }}>Japan</option>
									<option value="Jordan" {{($user->country == 'Jordan') ?  'selected="selected"' : '' }}>Jordan</option>
									<option value="Kazakhstan"{{($user->country == 'Kazakhstan') ?  'selected="selected"' : '' }}>Kazakhstan</option>
									<option value="Kenya"{{($user->country == 'Kenya') ?  'selected="selected"' : '' }}>Kenya</option>
									<option value="Kiribati"{{($user->country == 'Kiribati') ?  'selected="selected"' : '' }}>Kiribati</option>
									<option value="Democratic People Republic of Korea" {{($user->country == 'Democratic People Republic of Korea') ?  'selected="selected"' : '' }}>Korea, Democratic People Republic of</option>
									<option value="Korea" {{($user->country == 'Korea') ?  'selected="selected"' : '' }}>Korea, Republic of</option>
									<option value="Kuwait" {{($user->country == 'Kuwait') ?  'selected="selected"' : '' }}>Kuwait</option>
									<option value="Kyrgyzstan" {{($user->country == 'Kyrgyzstan') ?  'selected="selected"' : '' }}>Kyrgyzstan</option>
									<option value="Lao" {{($user->country == 'Lao') ?  'selected="selected"' : '' }}>Lao People Democratic Republic</option>
									<option value="Latvia" {{($user->country == 'Latvia') ?  'selected="selected"' : '' }}>Latvia</option>
									<option value="Lebanon" {{($user->country == 'Lebanon') ?  'selected="selected"' : '' }}>Lebanon</option>
									<option value="Lesotho" {{($user->country == 'Lesotho') ?  'selected="selected"' : '' }}>Lesotho</option>
									<option value="Liberia" {{($user->country == 'Liberia') ?  'selected="selected"' : '' }}>Liberia</option>
									<option value="Libyan Arab Jamahiriya" {{($user->country == 'Libyan Arab Jamahiriya') ?  'selected="selected"' : '' }}>Libyan Arab Jamahiriya</option>
									<option value="Liechtenstein" {{($user->country == 'Liechtenstein') ?  'selected="selected"' : '' }}>Liechtenstein</option>
									<option value="Lithuania" {{($user->country == 'Lithuania') ?  'selected="selected"' : '' }}>Lithuania</option>
									<option value="Luxembourg" {{($user->country == 'Luxembourg') ?  'selected="selected"' : '' }}>Luxembourg</option>
									<option value="Macau" {{($user->country == 'Macau') ?  'selected="selected"' : '' }}>Macau</option>
									<option value="Macedonia" {{($user->country == 'Macedonia') ?  'selected="selected"' : '' }}>Macedonia, The Former Yugoslav Republic of</option>
									<option value="Madagascar" {{($user->country == 'Madagascar') ?  'selected="selected"' : '' }}>Madagascar</option>
									<option value="Malawi" {{($user->country == 'Malawi') ?  'selected="selected"' : '' }}>Malawi</option>
									<option value="Malaysia" {{($user->country == 'Malaysia') ?  'selected="selected"' : '' }}>Malaysia</option>
									<option value="Maldives" {{($user->country == 'Maldives') ?  'selected="selected"' : '' }}>Maldives</option>
									<option value="Mali" {{($user->country == 'Mali') ?  'selected="selected"' : '' }}>Mali</option>
									<option value="Malta" {{($user->country == 'Malta') ?  'selected="selected"' : '' }}>Malta</option>
									<option value="Marshall Islands" {{($user->country == 'Marshall Islands') ?  'selected="selected"' : '' }}>Marshall Islands</option>
									<option value="Martinique" {{($user->country == 'Martinique') ?  'selected="selected"' : '' }}>Martinique</option>
									<option value="Mauritania" {{($user->country == 'Mauritania') ?  'selected="selected"' : '' }}>Mauritania</option>
									<option value="Mauritius" {{($user->country == 'Mauritius') ?  'selected="selected"' : '' }}>Mauritius</option>
									<option value="Mayotte" {{($user->country == 'Mayotte') ?  'selected="selected"' : '' }}>Mayotte</option>
									<option value="Mexico" {{($user->country == 'Mexico') ?  'selected="selected"' : '' }}>Mexico</option>
									<option value="Micronesia" {{($user->country == 'Micronesia') ?  'selected="selected"' : '' }}>Micronesia, Federated States of</option>
									<option value="Moldova" {{($user->country == 'Moldova') ?  'selected="selected"' : '' }}>Moldova, Republic of</option>
									<option value="Monaco" {{($user->country == 'Monaco') ?  'selected="selected"' : '' }}>Monaco</option>
									<option value="Mongolia" {{($user->country == 'Mongolia') ?  'selected="selected"' : '' }}>Mongolia</option>
									<option value="Montserrat" {{($user->country == 'Montserrat') ?  'selected="selected"' : '' }}>Montserrat</option>
									<option value="Morocco" {{($user->country == 'Morocco') ?  'selected="selected"' : '' }}>Morocco</option>
									<option value="Mozambique" {{($user->country == 'Mozambique') ?  'selected="selected"' : '' }}>Mozambique</option>
									<option value="Myanmar" {{($user->country == 'Myanmar') ?  'selected="selected"' : '' }}>Myanmar</option>
									<option value="Namibia" {{($user->country == 'Namibia') ?  'selected="selected"' : '' }}>Namibia</option>
									<option value="Nauru"> {{($user->country == 'Nauru') ?  'selected="selected"' : '' }}Nauru</option>
									<option value="Nepal" {{($user->country == 'Nepal') ?  'selected="selected"' : '' }}>Nepal</option>
									<option value="Netherlands" {{($user->country == 'Netherlands') ?  'selected="selected"' : '' }}>Netherlands</option>
									<option value="Netherlands Antilles" {{($user->country == 'Netherlands Antilles') ?  'selected="selected"' : '' }}>Netherlands Antilles</option>
									<option value="New Caledonia" {{($user->country == 'New Caledonia') ?  'selected="selected"' : '' }}>New Caledonia</option>
									<option value="New Zealand" {{($user->country == 'New Zealand') ?  'selected="selected"' : '' }}>New Zealand</option>
									<option value="Nicaragua" {{($user->country == 'Nicaragua') ?  'selected="selected"' : '' }}>Nicaragua</option>
									<option value="Niger" {{($user->country == 'Niger') ?  'selected="selected"' : '' }}>Niger</option>
									<option value="Nigeria" {{($user->country == 'Nigeria') ?  'selected="selected"' : '' }}>Nigeria</option>
									<option value="Niue" {{($user->country == 'Niue') ?  'selected="selected"' : '' }}>Niue</option>
									<option value="Norfolk Island" {{($user->country == 'Norfolk Island') ?  'selected="selected"' : '' }}>Norfolk Island</option>
									<option value="Northern Mariana Islands" {{($user->country == 'Northern Mariana Islands') ?  'selected="selected"' : '' }}>Northern Mariana Islands</option>
									<option value="Norway" {{($user->country == 'Norway') ?  'selected="selected"' : '' }}>Norway</option>
									<option value="Oman" {{($user->country == 'Oman') ?  'selected="selected"' : '' }}>Oman</option>
									<option value="Pakistan" {{($user->country == 'Pakistan') ?  'selected="selected"' : '' }}>Pakistan</option>
									<option value="Palau" {{($user->country == 'Palau') ?  'selected="selected"' : '' }}>Palau</option>
									<option value="Panama" {{($user->country == 'Panama') ?  'selected="selected"' : '' }}>Panama</option>
									<option value="Papua New Guinea" {{($user->country == 'Papua New Guinea') ?  'selected="selected"' : '' }}>Papua New Guinea</option>
									<option value="Paraguay" {{($user->country == 'Paraguay') ?  'selected="selected"' : '' }}>Paraguay</option>
									<option value="Peru" {{($user->country == 'Peru') ?  'selected="selected"' : '' }}>Peru</option>
									<option value="Philippines" {{($user->country == 'Philippines') ?  'selected="selected"' : '' }}>Philippines</option>
									<option value="Pitcairn" {{($user->country == 'Pitcairn') ?  'selected="selected"' : '' }}>Pitcairn</option>
									<option value="Poland" {{($user->country == 'Poland') ?  'selected="selected"' : '' }}>Poland</option>
									<option value="Portugal" {{($user->country == 'Portugal') ?  'selected="selected"' : '' }}>Portugal</option>
									<option value="Puerto Rico" {{($user->country == 'Puerto Rico') ?  'selected="selected"' : '' }}>Puerto Rico</option>
									<option value="Qatar" {{($user->country == 'Qatar') ?  'selected="selected"' : '' }}>Qatar</option>
									<option value="Reunion" {{($user->country == 'Reunion') ?  'selected="selected"' : '' }}>Reunion</option>
									<option value="Romania" {{($user->country == 'Romania') ?  'selected="selected"' : '' }}>Romania</option>
									<option value="Russia" {{($user->country == 'Russia') ?  'selected="selected"' : '' }}>Russian Federation</option>
									<option value="Rwanda" {{($user->country == 'Rwanda') ?  'selected="selected"' : '' }}>Rwanda</option>
									<option value="Saint Kitts and Nevis" {{($user->country == 'Saint Kitts and Nevis') ?  'selected="selected"' : '' }}>Saint Kitts and Nevis</option> 
									<option value="Saint LUCIA" {{($user->country == 'Saint LUCIA') ?  'selected="selected"' : '' }}>Saint LUCIA</option>
									<option value="Saint Vincent" {{($user->country == 'Saint Vincent') ?  'selected="selected"' : '' }}>Saint Vincent and the Grenadines</option>
									<option value="Samoa" {{($user->country == 'Samoa') ?  'selected="selected"' : '' }}>Samoa</option>
									<option value="San Marino" {{($user->country == 'San Marino') ?  'selected="selected"' : '' }}>San Marino</option>
									<option value="Sao Tome and Principe" {{($user->country == 'Sao Tome and Principe') ?  'selected="selected"' : '' }}>Sao Tome and Principe</option> 
									<option value="Saudi Arabia" {{($user->country == 'Saudi Arabia') ?  'selected="selected"' : '' }}>Saudi Arabia</option>
									<option value="Senegal" {{($user->country == 'Senegal') ?  'selected="selected"' : '' }}>Senegal</option>
									<option value="Seychelles" {{($user->country == 'Seychelles') ?  'selected="selected"' : '' }}>Seychelles</option>
									<option value="Sierra" {{($user->country == 'Sierra') ?  'selected="selected"' : '' }}>Sierra Leone</option>
									<option value="Singapore" {{($user->country == 'Singapore') ?  'selected="selected"' : '' }}>Singapore</option>
									<option value="Slovakia" {{($user->country == 'Slovakia') ?  'selected="selected"' : '' }}>Slovakia (Slovak Republic)</option>
									<option value="Slovenia" {{($user->country == 'Slovenia') ?  'selected="selected"' : '' }}>Slovenia</option>
									<option value="Solomon Islands" {{($user->country == 'Solomon Islands') ?  'selected="selected"' : '' }}>Solomon Islands</option>
									<option value="Somalia" {{($user->country == 'Somalia') ?  'selected="selected"' : '' }}>Somalia</option>
									<option value="South Africa" {{($user->country == 'South Africa') ?  'selected="selected"' : '' }}>South Africa</option>
									<option value="South Georgia" {{($user->country == 'South Georgia') ?  'selected="selected"' : '' }}>South Georgia and the South Sandwich Islands</option>
									<option value="Span" {{($user->country == 'Span') ?  'selected="selected"' : '' }}>Spain</option>
									<option value="SriLanka" {{($user->country == 'SriLanka') ?  'selected="selected"' : '' }}>Sri Lanka</option>
									<option value="St. Helena" {{($user->country == 'St. Helena') ?  'selected="selected"' : '' }}>St. Helena</option>
									<option value="St. Pierre and Miguelon" {{($user->country == 'St. Pierre and Miguelon') ?  'selected="selected"' : '' }}>St. Pierre and Miquelon</option>
									<option value="Sudan" {{($user->country == 'Sudan') ?  'selected="selected"' : '' }}>Sudan</option>
									<option value="Suriname" {{($user->country == 'Suriname') ?  'selected="selected"' : '' }}>Suriname</option>
									<option value="Svalbard" {{($user->country == 'Svalbard') ?  'selected="selected"' : '' }}>Svalbard and Jan Mayen Islands</option>
									<option value="Swaziland" {{($user->country == 'Swaziland') ?  'selected="selected"' : '' }}>Swaziland</option>
									<option value="Sweden" {{($user->country == 'Sweden') ?  'selected="selected"' : '' }}>Sweden</option>
									<option value="Switzerland" {{($user->country == 'Switzerland') ?  'selected="selected"' : '' }}>Switzerland</option>
									<option value="Syria" {{($user->country == 'Syria') ?  'selected="selected"' : '' }}>Syrian Arab Republic</option>
									<option value="Taiwan" {{($user->country == 'Taiwan') ?  'selected="selected"' : '' }}>Taiwan, Province of China</option>
									<option value="Tajikistan" {{($user->country == 'Tajikistan') ?  'selected="selected"' : '' }}>Tajikistan</option>
									<option value="Tanzania" {{($user->country == 'Tanzania') ?  'selected="selected"' : '' }}>Tanzania, United Republic of</option>
									<option value="Thailand" {{($user->country == 'Thailand') ?  'selected="selected"' : '' }}>Thailand</option>
									<option value="Togo" {{($user->country == 'Togo') ?  'selected="selected"' : '' }}>Togo</option>
									<option value="Tokelau" {{($user->country == 'Tokelau') ?  'selected="selected"' : '' }}>Tokelau</option>
									<option value="Tonga" {{($user->country == 'Tonga') ?  'selected="selected"' : '' }}>Tonga</option>
									<option value="Trinidad and Tobago" {{($user->country == 'Trinidad and Tobago') ?  'selected="selected"' : '' }}>Trinidad and Tobago</option>
									<option value="Tunisia" {{($user->country == 'Tunisia') ?  'selected="selected"' : '' }}>Tunisia</option>
									<option value="Turkey" {{($user->country == 'Turkey') ?  'selected="selected"' : '' }}>Turkey</option>
									<option value="Turkmenistan" {{($user->country == 'Turkmenistan') ?  'selected="selected"' : '' }}>Turkmenistan</option>
									<option value="Turks and Caicos" {{($user->country == 'Turks and Caicos') ?  'selected="selected"' : '' }}>Turks and Caicos Islands</option>
									<option value="Tuvalu" {{($user->country == 'Tuvalu') ?  'selected="selected"' : '' }}>Tuvalu</option>
									<option value="Uganda" {{($user->country == 'Uganda') ?  'selected="selected"' : '' }}>Uganda</option>
									<option value="Ukraine" {{($user->country == 'Ukraine') ?  'selected="selected"' : '' }}>Ukraine</option>
									<option value="United Arab Emirates" {{($user->country == 'United Arab Emirates') ?  'selected="selected"' : '' }}>United Arab Emirates</option>
									<option value="United Kingdom" {{($user->country == 'United Kingdom') ?  'selected="selected"' : '' }}>United Kingdom</option>
									<option value="United States" {{($user->country == 'United States') ?  'selected="selected"' : '' }}>United States</option>
									<option value="United States Minor Outlying Islands" {{($user->country == 'United States Minor Outlying Islands') ?  'selected="selected"' : '' }}>United States Minor Outlying Islands</option>
									<option value="Uruguay" {{($user->country == 'Uruguay') ?  'selected="selected"' : '' }}>Uruguay</option>
									<option value="Uzbekistan" {{($user->country == 'Uzbekistan') ?  'selected="selected"' : '' }}>Uzbekistan</option>
									<option value="Vanuatu" {{($user->country == 'Vanuatu') ?  'selected="selected"' : '' }}>Vanuatu</option>
									<option value="Venezuela" {{($user->country == 'Venezuela') ?  'selected="selected"' : '' }}>Venezuela</option>
									<option value="Vietnam" {{($user->country == 'Vietnam') ?  'selected="selected"' : '' }}>Viet Nam</option>
									<option value="Virgin Islands (British)" {{($user->country == 'Virgin Islands (British)') ?  'selected="selected"' : '' }}>Virgin Islands (British)</option>
									<option value="Virgin Islands (U.S)" {{($user->country == 'Virgin Islands (U.S)') ?  'selected="selected"' : '' }}>Virgin Islands (U.S.)</option>
									<option value="Wallis and Futana Islands" {{($user->country == 'Wallis and Futana Islands') ?  'selected="selected"' : '' }}>Wallis and Futuna Islands</option>
									<option value="Western Sahara" {{($user->country == 'Western Sahara') ?  'selected="selected"' : '' }}>Western Sahara</option>
									<option value="Yemen" {{($user->country == 'Yemen') ?  'selected="selected"' : '' }}>Yemen</option>
									<option value="Yugoslavia" {{($user->country == 'Yugoslavia') ?  'selected="selected"' : '' }}>Yugoslavia</option>
									<option value="Zambia" {{($user->country == 'Zambia') ?  'selected="selected"' : '' }}>Zambia</option>
									<option value="Zimbabwe" {{($user->country == 'Zimbabwe') ?  'selected="selected"' : '' }}>Zimbabwe</option>
								</select> 
                            </div>
</div>
										<div class="col-md-4">
                            
                             <div class="form-group">
                                <label class="control-label">Wallet Amount</label>
                                <input type="text" placeholder="Amount" name="walletAmount" id="walletAmount" value="{{ isset($amount->amount) ?  $amount->amount : '' }}" class="form-control"> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">State</label>
                                <input type="text" placeholder="State" name="state" id="state" value="{{ isset($user->state) ?  $user->state : '' }}" class="form-control"> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">City</label>
                                <input type="text" placeholder="City" name="city" id="city" value="{{ isset($user->city) ?  $user->city : '' }}" class="form-control"> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Business Name</label>
                                <input type="text" placeholder="Business Name" name="business_name" id="business_name" value="{{ isset($user->business_name) ?  $user->business_name : '' }}" class="form-control"> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Business Address</label>
                                <input type="text" placeholder="Business Address" name="business_address" id="business_address" value="{{ isset($user->business_address) ?  $user->business_address : '' }}" class="form-control"> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Business Phone</label>
                                <input type="text" placeholder="Business Phone" name="business_phone" id="business_phone" value="{{ isset($user->business_phone) ?  $user->business_phone : '' }}" class="form-control"> 
                            </div>
</div>
									    <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Which platform do you use?</label>
                                <select class="form-control" name="platform" value="" required autofocus>                    				
									<option value="Shopify" {{ (isset($user->platform) && $user->platform == 'Shopify') ? "selected=\"selected\"" : "" }}>Shopify</option>
									<option value="WooCommerce" {{ (isset($user->platform) && $user->platform == 'WooCommerce') ? "selected=\"selected\"" : "" }}>WooCommerce</option>
                                </select>                                        
                            </div>
                            <div class="form-group">
                                <label class="control-label">How long have you been running your store?</label>
                                <select class="form-control" name="storeruntime" value="" required autofocus>
		                            <option value="Just started" {{ (isset($user->storeruntime) && $user->storeruntime == 'Just started') ?  'selected="selected"' : '' }}>Just started</option>
		                            <option value="Less a year" {{ (isset($user->storeruntime) && $user->storeruntime == 'Less a year') ?  'selected="selected"' : '' }}>Less a year</option>
		                            <option value="1 - 3 years" {{ (isset($user->storeruntime) && $user->storeruntime == '1 - 3 years') ?  'selected="selected"' : '' }}>1 - 3 years</option>
		                            <option value="4 - 10 years" {{ (isset($user->storeruntime) && $user->storeruntime == '4 - 10 years') ?  'selected="selected"' : '' }}>4 - 10 years</option>
		                            <option value="10+ years" {{ (isset($user->storeruntime) && $user->storeruntime == '10+ years') ?  'selected="selected"' : '' }}>10+ years</option>
                            	</select>                                        
                            </div>
                            <div class="form-group">
                                <label class="control-label">How many orders have you been getting on average everyday in the past 30 days?</label>
                                 <select class="form-control" name="ordersno" value="" required autofocus>                            
		                            <option value="Less than 50" {{ (isset($user->ordersno) && $user->ordersno == 'Less than 50') ?  'selected="selected"' : '' }}>Less than 50</option>
		                            <option value="51 - 100" {{ (isset($user->ordersno) && $user->ordersno == '51 - 100') ?  'selected="selected"' : '' }}>51 - 100</option>
		                            <option value="101 - 200" {{ (isset($user->ordersno) && $user->ordersno == '101 - 200') ?  'selected="selected"' : '' }}>101 - 200</option>
		                            <option value="201 - 300" {{ (isset($user->ordersno) && $user->ordersno == '201 - 300') ?  'selected="selected"' : '' }}>201 - 300</option>
		                            <option value="301 - 400" {{ (isset($user->ordersno) && $user->ordersno == '301 - 400') ?  'selected="selected"' : '' }}>301 - 400</option>
		                            <option value="401 - 500" {{ (isset($user->ordersno) && $user->ordersno == '401 - 500') ?  'selected="selected"' : '' }}>401 - 500</option>
		                            <option value="Just started" {{ (isset($user->ordersno) && $user->ordersno == '500+') ?  'selected="selected"' : '' }}>500+</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">How many products do you want us to process for you?</label>
                                <select class="form-control" id="productsno" name="productsno" value="" required autofocus>
                                	<option value="Only 1" {{ (isset($user->productsno) && $user->productsno == 'Only 1') ?  'selected="selected"' : '' }}>Only 1</option>
		                            <option value="2 - 10" {{ (isset($user->productsno) && $user->productsno == '2 - 10') ?  'selected="selected"' : '' }}>2 - 10</option>
		                            <option value="11 - 20" {{ (isset($user->productsno) && $user->productsno == '11 - 20') ?  'selected="selected"' : '' }}>11 - 20</option>
		                            <option value="21 - 30" {{ (isset($user->productsno) && $user->productsno == '21 - 30') ?  'selected="selected"' : '' }}>21 - 30</option>
		                            <option value="30+" {{ (isset($user->productsno) && $user->productsno == '30+') ?  'selected="selected"' : '' }}>30+</option>              
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">How many orders do you want us to process for you everyday?</label>
                                <select class="form-control" name="ordersperday" value="" required autofocus>
                                	<option value="Less than 50" {{ (isset($user->ordersperday) && $user->ordersperday == 'Less than 50') ?  'selected="selected"' : '' }}>Less than 50</option>
		                            <option value="51 - 100" {{ (isset($user->ordersperday) && $user->ordersperday == '51 - 100') ?  'selected="selected"' : '' }}>51 - 100</option>
		                            <option value="101 - 200" {{ (isset($user->ordersperday) && $user->ordersperday == '101 - 200') ?  'selected="selected"' : '' }}>101 - 200</option>
		                            <option value="201 - 300" {{ (isset($user->ordersperday) && $user->ordersperday == '201 - 300') ?  'selected="selected"' : '' }}>201 - 300</option>
		                            <option value="301 - 400" {{ (isset($user->ordersperday) && $user->ordersperday == '301 - 400') ?  'selected="selected"' : '' }}>301 - 400</option>
		                            <option value="401 - 500" {{ (isset($user->ordersperday) && $user->ordersperday == '401 - 500') ?  'selected="selected"' : '' }}>401 - 500</option>
		                            <option value="Just started" {{ (isset($user->ordersperday) && $user->ordersperday == '500+') ?  'selected="selected"' : '' }}>500+</option>
                                </select>
                            </div>

                            </div>
									</div>
								</div>
							 </div>
							 <div class="row">
							   	<div class="col-md-12">
							   		<div class="form-group">
		                                <label class="control-label">Activate</label>
		                                <select class="form-control" id="approve" name="approve">
											<option value="1" {{($user->approve == '1') ?  'selected="selected"' : '' }}>Activate</option>
											<option value="0" {{($user->approve == '0') ?  'selected="selected"' : '' }}>Deactivate</option>
										</select> 
                            		</div>
							   	</div>
							 </div>                            
                            
                            <input type="hidden" name="approveValue" id="approveValue" value="{{$user->approve}}">

<div class="kt-portlet__foot">
					<div class="kt-form__actions">
							   	<div class="col-md-12" style="text-align: center;">
									<div class="row">
										<div class="col-md-4">
											<button style="width:100%;" type="submit" class="btn btn-primary" name="addProfile" id="addProfile">Save Changes</button>
										</div>
										<div class="col-md-4">
										    <button style="width:100%;" type="button" onClick="window.location.href ='{{url('/admin/users')}}';" class="btn btn-outline-danger">Cancel</button>
										</div>
									</div>
								</div>
							</div>
</div>
<!--
                            <div class="row">
							   	<div class="col-md-12" style="text-align: center;">
									<div class="row">
										<div class="col-md-4">
											<button style="width:100%;" type="submit" class="btn btn-primary" name="addProfile" id="addProfile">Save Changes</button>
										</div>
										<div class="col-md-4">
										    <button style="width:100%;" type="button" onClick="window.location.href ='{{url('/admin/users')}}';" class="btn btn-outline-danger">Cancel</button>
										</div>
									</div>
</div>
</div>
-->			
								
                            <!--
                            <div class="row">
								<div class="col-md-4" >
	                            	<button style="width:100%;" type="submit" class="btn btn-primary" name="addProfile" id="addProfile">Save Changes</button>
								</div>
	                            <div  class="col-md-4" style="text-align: left;">
									<button style="width:100%;" type="button" onClick="window.location.href ='{{url('/admin/users')}}';" class="btn btn-outline-danger">Cancel</button>
		                 		</div>
                            </div>
                        	-->

                        </form>    
                        <!-- END PROFILE CONTENT -->
                    </div>
                </div>                    
            </div>
        </div>
        <!--end:: Widgets/Daily Sales-->
    </div>
    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
@endsection