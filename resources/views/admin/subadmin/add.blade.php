@extends('layouts.admin')
@section('page_title')
@if(!isset($subadmin))
Admin | Add SubAdmin
@else
Admin | Edit SubAdmin
@endif
@endsection
@section('page_css')
<style>

</style>
@endsection

@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head kt-portlet__head--lg">
        			<div class="kt-portlet__head-label" style="width: 100%;">
        				<div class="row" style="width: 100%;">
        					<div class="col-xs-12 col-md-9" style="text-align: left;">
        						<div class="kt-subheader__breadcrumbs">
        						    <span class="kt-portlet__head-icon">
                    		            <i class="kt-font-brand flaticon2-line-chart"></i>
                    		        </span>						   
        						    <a href="{{url('/admin/home')}}" class="kt-subheader__breadcrumbs-link">
        						        Dashboard                    
        						    </a>
        						    <span class="kt-subheader__breadcrumbs-separator">
        						        -
        						    </span>
        						    <a href="{{url('/admin/subadmin')}}" class="kt-subheader__breadcrumbs-link">
        						        SubAdmin                    
        						    </a>
        						    <span class="kt-subheader__breadcrumbs-separator">
        						        - 
        						    </span>
        						    <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
        						        @if(!isset($subadmin))
                                            Add SubAdmin
                                        @else
                                            Edit SubAdmin
                                        @endif
        						    </span>		   
        						</div>
        					</div>				  
        				</div>
        			</div>
    		    </div>
                <div class="kt-widget14">
                    <div class="kt-widget14__header kt-margin-b-30">
                        <!-- BEGIN PROFILE CONTENT -->
                        @if (session('status'))
					    <div class="alert alert-success">
					        {{ session('status') }}
					    </div>
						@endif
			
                        <form role="form" method="POST" action="{{url('admin/subadmin/store')}}" name="storeProfile" id="storeProfile">
                        	@csrf                        	
							<input type="hidden"  name="id" id="id" value="{{isset($subadmin->id) ?  $subadmin->id : '' }}">
		    
                            <div class="form-group">
                                <label class="control-label">First Name</label>
                                <input type="text" placeholder="First Name" name="fname" id="fname" class="form-control" value="{{isset($subadmin->fname) ?  $subadmin->fname : '' }}" required>
                                 @if ($errors->has('fname'))
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('fname') }}</strong>
                                </span> 
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">Last Name</label>
                                <input type="text" placeholder="Last Name" name="lname" id="lname" value="{{ isset($subadmin->lname) ?  $subadmin->lname : '' }}" class="form-control" required>
                                @if ($errors->has('lname'))
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('lname') }}</strong>
                                </span> 
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">Email</label>
                                <input type="email" placeholder="Email" name="email" id="email" value="{{ isset($subadmin->email) ?  $subadmin->email : '' }}" class="form-control" required>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span> 
                                @endif
                            </div>
                            
                            @if(!isset($subadmin))
                            
                            <div class="form-group">
                                <label class="control-label">Password</label>
                                <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" placeholder="Password" name="password" value="{{ isset($subadmin->password) ?  $subadmin->password : '' }}" required> 
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span> 
                                @endif
                            </div>
                            
                            <div class="form-group space_captcha">
                                <label class="control-label">Confirm Password</label>
                                <input class="form-control" type="password" placeholder="Confirm Password" name="password_confirmation" value="{{ isset($subadmin->password_confirmation) ?  $subadmin->password_confirmation : '' }}" required>
                            </div>
                            @endif
                            <div class="kt-portlet__foot">
                            	<div class="kt-form__actions">
                            	   	<div class="col-md-12" style="text-align: center;">
                            			<div class="row">
                            				<div class="col-md-4">
                            					<button style="width:100%;" type="submit" class="btn btn-primary" name="addProfile" id="addProfile">Save Changes</button>
                            				</div>
                            				<div class="col-md-4">
                            				    <button style="width:100%;" type="button" onClick="window.location.href ='{{url('/admin/subadmin')}}';" class="btn btn-outline-danger">Cancel</button>
                            				</div>
                            			</div>
                            		</div>
                            	</div>
                            </div>
                        </form>    
                        <!-- END PROFILE CONTENT -->
                    </div>
                </div>                    
            </div>
        </div>
        <!--end:: Widgets/Daily Sales-->
    </div>
    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
@endsection