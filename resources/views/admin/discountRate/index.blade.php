@extends('layouts.admin')
@section('page_title')
Discount Rate
@endsection
@section('page_css')
@endsection
@section('content')
<!-- begin:: Content Head -->
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/admin/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Discount Rate                    </span>						   						   
						</div>
					</div>				  
				</div>
			</div>
		</div>
		
            <div class="kt-widget14">
    			<div class='row'>
    		    </div>
            </div> 
            <div class="kt-widget14">
	            @if (session('status')=='Discount Rate successfully saved!')
	            <div class="alert alert-success fade show" role="alert">
                    <div class="alert-text">{{ session('status') }}</div>
                    <div class="alert-close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="la la-close"></i></span>
                        </button>
                    </div>
                </div>
    			@elseif (session('status') !='')
    			<div class="alert alert-danger fade show" role="alert">
                    <div class="alert-text">{{ session('status') }}</div>
                    <div class="alert-close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="la la-close"></i></span>
                        </button>
                    </div>
                </div>
    			@endif	
    			<div class="row">
    	            <div class='col-md-12' style="text-align:center;">
    					<form class="kt-form" method="post" action="{{url('/admin/discount-rate/store')}}">
    					    @csrf
            				<div class="kt-portlet__body">
            					<div class="kt-section kt-section--first">
            						<div class="kt-section__body">
            							<div class="form-group row">
            								<label class="col-lg-4 col-form-label">Discount  </label>
            								<div class="col-lg-4">
            									<div class="input-group">
            										
            										<input type="number" name="discount_value" id="discount_value" step="0.01" value="{{isset($discount_rate->discount_value) ? $discount_rate->discount_value : ''}}" class="form-control" placeholder="99.99">
                                                    <div class="input-group-prepend"><span class="input-group-text" id="basic-addon2">%</span></div>
            									</div>	
            								</div>
            							</div>		
            						</div>
            		            </div>
            	            </div>
            	            <div class="kt-portlet__foot">
            					<div class="kt-form__actions">
            						<div class="row">
            							<div class="col-lg-3"></div>
            							<div class="col-lg-6">
            								<button type="submit" class="btn btn-success">Submit</button>
            								<button type="reset" class="btn btn-secondary">Cancel</button>
            							</div>
            						</div>
            					</div>
            				</div>
            			</form>
    				</div>
                </div>
                <div class="container">                    
               		<div class="kt-widget14">
               			<div class="kt-widget14">                  
            			</div>
            		</div>                     
           		</div>
        	</div>
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>
<!-- end:: Content -->
@endsection
@section('page_script')
@endsection