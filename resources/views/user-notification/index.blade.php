@extends('layouts.app')
@section('page_title')
User Notification
@endsection

@section('page_css')
<style>

.tw-button tw-bg-white tw-absolute tw-px-xs tw-py-1 tw-text-3xs tw-text-grey-darker {
    top: -44px !important;
    color: #fff !important;
    width: 45px;
    font-size: 9px !important;
}

.tw-button {
    border-radius: 18px !important;
}

.tw-absolute {
    position: relative !important;
}

.tw-bg-white {
    background-color: #6994ca !important;
}

.tw-py-1 {
    padding-top: .25rem;
    padding-bottom: .25rem;
}

.tw-px-xs {
    padding-left: .4rem;
    padding-right: .5rem;
}

.tw-text-3xs {
    font-size: .7rem;
}
</style>
@endsection

@section('content')


<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   User Notitifcation         </span>						   						   
						</div>
					
					</div>	
				</div>
			</div>
		</div>
		<!--
		<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				Notifications
			</h3>
		</div>
		
		<div class="kt-portlet__head-toolbar">
			<ul class="nav nav-pills nav-pills-sm nav-pills-label nav-pills-bold" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" data-toggle="tab" href="#kt_widget6_tab1_content" role="tab">
						Latest
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#kt_widget6_tab2_content" role="tab">
						Week
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#kt_widget6_tab3_content" role="tab">
						Month
					</a>
				</li>
			</ul>
		</div>
		
	</div>-->
		 <div class="kt-widget14">
		 
			<div class="tab-content">
				@foreach($unread_notifications as $unread_notification)
				<div class="tab-pane active" id="kt_widget6_tab1_content" aria-expanded="true">
					<div class="kt-notification">
						<a href="{{url('/usernotifications/'.$unread_notification->id)}}" class="kt-notification__item">
							<div class="kt-notification__item-icon">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect id="bound" x="0" y="0" width="24" height="24"/>
        <path d="M5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" id="Combined-Shape" fill="#000000"/>
    </g>
</svg>                        
							</div>
							<div class="kt-notification__item-details">
								<div class="kt-notification__item-title">
									{{ Illuminate\Support\Str::limit($unread_notification->messages, 20) }}
									
								</div>
								<div class="kt-notification__item-time">
									{{$unread_notification->created_at->diffForHumans()}}
								</div>
								<!--<span class="tw-button tw-bg-white tw-absolute tw-px-xs tw-py-1 tw-text-3xs tw-text-grey-darker" style="top: -44px;right: 46px;color: rgb(46, 68, 105);width: 45px;">New</span>-->
							</div>
						</a> 
					</div>
				</div>
				@endforeach
				
				@foreach($read_notifications as $read_notification)
				<div class="tab-pane active" id="kt_widget6_tab1_content" aria-expanded="true">
					<div class="kt-notification">
						<a href="{{url('/usernotifications/'.$read_notification->id)}}" class="kt-notification__item">
							<div class="kt-notification__item-icon">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect id="bound" x="0" y="0" width="24" height="24"/>
        <path d="M12.7037037,14 L15.6666667,10 L13.4444444,10 L13.4444444,6 L9,12 L11.2222222,12 L11.2222222,14 L6,14 C5.44771525,14 5,13.5522847 5,13 L5,3 C5,2.44771525 5.44771525,2 6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,13 C19,13.5522847 18.5522847,14 18,14 L12.7037037,14 Z" id="Combined-Shape" fill="#000000" opacity="0.3"/>
        <path d="M9.80428954,10.9142091 L9,12 L11.2222222,12 L11.2222222,16 L15.6666667,10 L15.4615385,10 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 L9.80428954,10.9142091 Z" id="Combined-Shape" fill="#000000"/>
    </g>
</svg>                       
							</div>
							<div class="kt-notification__item-details">
								<div class="kt-notification__item-title">
									{{ Illuminate\Support\Str::limit($read_notification->messages, 20) }}
								</div>
								<div class="kt-notification__item-time">
									{{$read_notification->updated_at->diffForHumans()}}
								</div>
							</div>
						</a> 
					</div>
				</div>
				@endforeach
			</div>
		</div>
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
<script>


   var path = APP_URL+'/billing-history/show';
   var datatable = $('.com_tran_datatable').KTDatatable({
  // datasource definition
  data: {
    type: 'remote',
    source: {
      read: {
        url: path,
        method: 'GET',
        map: function(raw) {
          // sample data mapping
          var dataSet = raw;
          if (typeof raw.data !== 'undefined') {
            dataSet = raw.data;
          }
          return dataSet;
        },
      },
    },
    pageSize: 10,
    serverPaging: false,
    serverFiltering: false,
    serverSorting: false,
  },

  // layout definition
  layout: {
    scroll: false,
    footer: false,
  },

  // column sorting
  sortable: true,

  pagination: true,

  search: {
    input: $('#generalSearch'),
  },

  // columns definition
  columns: [
  	
  	{
      field: 'number_of_parcel',
      title: 'Number Of parcel',     
      width: 150,      
      textAlign: 'center',
     
    },
    {
      field: 'shipping_cost',
      title: 'Shipping Cost ($)',     
      width: 150,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var amount =formatter.format(row.shipping_cost); 
      
			return amount;              
      },
    },
    {
      field: 'handling_fee',
      title: 'Handling Fee ($)',     
      width: 150,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var amount =formatter.format(row.handling_fee); 
      
			return amount;              
      },
    },
{
      field: 'total_product_cost',
      title: 'Total Cost ($)',     
      width: 150,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var amount =formatter.format(row.total_product_cost); 
      
			return amount;              
      },
    },

/*
    {
      field: 'product_cost',
      title: 'Product Cost ($)',     
      width: 180,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var amount =formatter.format(row.product_cost); 
      
			return amount;              
      },
    },
/*
    {
      field: 'total_cost',
      title: 'Total Cost ($)',     
      width: 180,      
      textAlign: 'center',
       template: function(row, index, datatable) {
      
      		const formatter = new Intl.NumberFormat('en-US', {
	  
	  		minimumFractionDigits: 2
			})
	
			var amount =formatter.format(row.total_cost); 
      
			return amount;              
      },
    },
*/   
  {
      field: 'created_at',
      width: 150,
      title: 'Date',
      textAlign: 'center',
      template: function(row, index, datatable) {
      
	var dateTime		= 	row.created_at;
	var date		= 	dateTime.split(" ");
	var result 	 	= 	date[0];
	var resulted_date 	=  	result.split("-");
	var date_to_show 	= 	resulted_date[2]+'-'+resulted_date[1]+'-'+resulted_date[0];    
	return date_to_show;              
      },
    },
    {
      field: 'id',
      title: 'Actions', 
      textAlign: 'center', 
      template: function(row, index, datatable) {
      
      	return '<a href="{{url('/billing-history/view')}}/'+row.id+'" data-key="'+row.id+'" class="cstm_edit btn btn-label-primary btn-pill" title="View Invoice">\
                        Invoice\
                    </a>'; 
      },
         
    }
  
   ],

});
</script>
@endsection