@extends('layouts.app')
@section('page_title')
Dashboard
@endsection

@section('page_css')
<style>

.kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-text {
   
    padding: 0.35rem 0 0 9rem !important;
    color: #444444 !important;
    font-size: 1.1rem !important;
}

.kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-cricle {
    
    left: 8rem !important;
      
}
.kt-timeline-v2:before {
    
    left: 8.70rem !important;   
}

.kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-time {
    
    font-size: 1.2rem !important;    
}

.kt-widget17 .kt-widget17__stats .kt-widget17__items .kt-widget17__item {
    
    cursor: auto !important;    
}

.kt-widget17 .kt-widget17__stats .kt-widget17__items .kt-widget17__item .kt-widget17__desc {
    
    color: #66676b !important;
}
</style>
@endsection

@section('content')
<!--
<div class="container">
    <div class="row justify-content-center">        
        <div class="col-md-12">
		<div id="profile_login">
			<div class="profile-body">
				<h5>Welcome to 7Fulfillment!<br>
				Connect your store to get started </h5>
				<p>By connecting your store will be able to access all the features of our platform, let’s get started!</p>
				<a class="btn btn-primary btn-pill" href="{{url('/store')}}">Connect you store</a>            
                	</div>
            	</div>            
        </div>       
    </div>
</div>
-->

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="row">
		<div class="col-xl-6">
				
			<div class="kt-portlet kt-portlet--fit kt-portlet--head-lg kt-portlet--head-overlay kt-portlet--skin-solid kt-portlet--height-fluid">
				<div class="kt-portlet__head kt-portlet__head--noborder kt-portlet__space-x">
					<div class="kt-portlet__head-label">
					   <h3 class="kt-portlet__head-title">
				            
				       	</h3>
				       	
					</div>		
				</div>
				
				<div class="kt-portlet__body kt-portlet__body--fit">
					<div class="kt-widget17">
						<div class="kt-widget17__visual kt-widget17__visual--chart kt-portlet-fit--top kt-portlet-fit--sides" style="background-color: #fd397a">
							<div class="kt-widget17__chart" style="height:320px; background-color: #5d78ff;"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
								<div class="kt-widget20__content kt-portlet__space-x">
									
									<span class="kt-widget20__desc">
										<h5>Welcome to 7Fulfillment!<br>
											Connect your store to get started </h5>
											<p>By connecting your store will be able to access all the features of our platform, let’s get started!</p>
											<!--<a class="btn btn-primary btn-pill" href="{{url('/store')}}">Connect you store</a>-->
									</span>
								</div>
							</div>
						</div>
						<div class="kt-widget17__stats">
							<div class="kt-widget17__items">
								<div class="kt-widget17__item">
									<span class="kt-widget17__icon">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                           <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                           <circle id="Oval-47" fill="#000000" opacity="0.3" cx="20.5" cy="12.5" r="1.5"></circle>
                           <rect id="Rectangle-162" fill="#000000" opacity="0.3" transform="translate(12.000000, 6.500000) rotate(-15.000000) translate(-12.000000, -6.500000) " x="3" y="3" width="18" height="7" rx="1"></rect>
                           <path d="M22,9.33681558 C21.5453723,9.12084552 21.0367986,9 20.5,9 C18.5670034,9 17,10.5670034 17,12.5 C17,14.4329966 18.5670034,16 20.5,16 C21.0367986,16 21.5453723,15.8791545 22,15.6631844 L22,18 C22,19.1045695 21.1045695,20 20,20 L4,20 C2.8954305,20 2,19.1045695 2,18 L2,6 C2,4.8954305 2.8954305,4 4,4 L20,4 C21.1045695,4 22,4.8954305 22,6 L22,9.33681558 Z" id="Combined-Shape" fill="#000000"></path>
                        </g>
                     </svg>						</span> 
									<span class="kt-widget17__subtitle">
										Wallet Balance
									</span> 
									<span class="kt-widget17__desc" style="font-size: 16px;">
										<b>{{isset($balance_history->amount) ? '$ '.number_format($balance_history->amount,2) : '$ 0' }}</b>
									</span>  
								</div>

								<div class="kt-widget17__item">
									<span class="kt-widget17__icon">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect id="bound" x="0" y="0" width="24" height="24"/>
        <circle id="Combined-Shape" fill="#000000" cx="9" cy="15" r="6"/>
        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" id="Combined-Shape" fill="#000000" opacity="0.3"/>
    </g>
</svg>						</span>  
									<span class="kt-widget17__subtitle">
										Total Points
									</span> 
									<span class="kt-widget17__desc" style="font-size: 16px;">
										<b>{{isset($total_points) ? number_format($total_points) : '0' }}</b>
									</span>  
								</div>					
							</div>
							<div class="kt-widget17__items">
								<div class="kt-widget17__item">
									<span class="kt-widget17__icon">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--warning">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect id="bound" x="0" y="0" width="24" height="24"/>
        <path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero"/>
    </g>
</svg>						</span>  
									<span class="kt-widget17__subtitle">
										Total Parcel Shipped
									</span> 
									<span class="kt-widget17__desc" style="font-size: 16px;">
										<b>{{isset($total_parcels_shipped) ? number_format($total_parcels_shipped) : '0' }}</b>
									</span>  
								</div>	

								<div class="kt-widget17__item">
									<span class="kt-widget17__icon">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--danger">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                           <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                           <path d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z" id="Path-30" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                           <path d="M3.28077641,9 L20.7192236,9 C21.2715083,9 21.7192236,9.44771525 21.7192236,10 C21.7192236,10.0817618 21.7091962,10.163215 21.6893661,10.2425356 L19.5680983,18.7276069 C19.234223,20.0631079 18.0342737,21 16.6576708,21 L7.34232922,21 C5.96572629,21 4.76577697,20.0631079 4.43190172,18.7276069 L2.31063391,10.2425356 C2.17668518,9.70674072 2.50244587,9.16380623 3.03824078,9.0298575 C3.11756139,9.01002735 3.1990146,9 3.28077641,9 Z M12,12 C11.4477153,12 11,12.4477153 11,13 L11,17 C11,17.5522847 11.4477153,18 12,18 C12.5522847,18 13,17.5522847 13,17 L13,13 C13,12.4477153 12.5522847,12 12,12 Z M6.96472382,12.1362967 C6.43125772,12.2792385 6.11467523,12.8275755 6.25761704,13.3610416 L7.29289322,17.2247449 C7.43583503,17.758211 7.98417199,18.0747935 8.51763809,17.9318517 C9.05110419,17.7889098 9.36768668,17.2405729 9.22474487,16.7071068 L8.18946869,12.8434035 C8.04652688,12.3099374 7.49818992,11.9933549 6.96472382,12.1362967 Z M17.0352762,12.1362967 C16.5018101,11.9933549 15.9534731,12.3099374 15.8105313,12.8434035 L14.7752551,16.7071068 C14.6323133,17.2405729 14.9488958,17.7889098 15.4823619,17.9318517 C16.015828,18.0747935 16.564165,17.758211 16.7071068,17.2247449 L17.742383,13.3610416 C17.8853248,12.8275755 17.5687423,12.2792385 17.0352762,12.1362967 Z" id="Combined-Shape" fill="#000000"></path>
                        </g>
                     </svg>						</span>  
									<span class="kt-widget17__subtitle">
										<a class="" href="{{url('/store')}}">Connect your store</a>
									</span>
									<!-- 
									<span class="kt-widget17__desc">
										34 Upgraded Boxes
									</span>
									-->  
								</div>				
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>

		<div class="col-xl-6">		
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Recent Activities
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						
					</div>
				</div>
				<div class="kt-portlet__body">
						<!--Begin::Timeline 3 -->
						<div class="kt-timeline-v2">
						
						
							<div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">
							@if(isset($recent_activities))
							@foreach($recent_activities as $activities)
								<div class="kt-timeline-v2__item">
									<span class="kt-timeline-v2__item-time">
									
									{{date( "d-M-Y", strtotime($activities->created_at))}}</span>
									<div class="kt-timeline-v2__item-cricle">
										<i class="fa fa-genderless kt-font-danger"></i>
									</div>
									<div class="kt-timeline-v2__item-text  kt-padding-top-5">
										{{$activities->message}}                                           	                                	               
									</div>
								</div>
							@endforeach
							@endif
							<!--
								<div class="kt-timeline-v2__item">
									<span class="kt-timeline-v2__item-time">10:00</span>
									<div class="kt-timeline-v2__item-cricle">
										<i class="fa fa-genderless kt-font-danger"></i>
									</div>
									<div class="kt-timeline-v2__item-text  kt-padding-top-5">
										Lorem ipsum dolor sit amit,consectetur eiusmdd tempor<br>
										incididunt ut labore et dolore magna                                           	                                	               
									</div>
								</div>
								<div class="kt-timeline-v2__item">
									<span class="kt-timeline-v2__item-time">12:45</span>
									<div class="kt-timeline-v2__item-cricle">
										<i class="fa fa-genderless kt-font-success"></i>
									</div>
									<div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold">
										AEOL Meeting With 
									</div>
									<div class="kt-list-pics kt-list-pics--sm kt-padding-l-20">
										<a href="#"><img src="./assets/media/users/100_4.jpg" title=""></a>
										<a href="#"><img src="./assets/media/users/100_13.jpg" title=""></a>
										<a href="#"><img src="./assets/media/users/100_11.jpg" title=""></a>
										<a href="#"><img src="./assets/media/users/100_14.jpg" title=""></a>
									</div>
								</div>
								<div class="kt-timeline-v2__item">
									<span class="kt-timeline-v2__item-time">14:00</span>
									<div class="kt-timeline-v2__item-cricle">
										<i class="fa fa-genderless kt-font-brand"></i>
									</div>
									<div class="kt-timeline-v2__item-text kt-padding-top-5">
										Make Deposit <a href="#" class="kt-link kt-link--brand kt-font-bolder">USD 700</a> To ESL.
									</div>
								</div>
								<div class="kt-timeline-v2__item">
									<span class="kt-timeline-v2__item-time">16:00</span>
									<div class="kt-timeline-v2__item-cricle">
										<i class="fa fa-genderless kt-font-warning"></i>
									</div>
									<div class="kt-timeline-v2__item-text kt-padding-top-5">
										Lorem ipsum dolor sit amit,consectetur eiusmdd tempor<br>
										incididunt ut labore et dolore magna elit enim at minim<br>
										veniam quis nostrud                                                            	                                
									</div>
								</div>
								<div class="kt-timeline-v2__item">
									<span class="kt-timeline-v2__item-time">17:00</span>
									<div class="kt-timeline-v2__item-cricle">
										<i class="fa fa-genderless kt-font-info"></i>
									</div>
									<div class="kt-timeline-v2__item-text kt-padding-top-5">
										Placed a new order in <a href="#" class="kt-link kt-link--brand kt-font-bolder">SIGNATURE MOBILE</a> marketplace.
									</div>
								</div>
								<div class="kt-timeline-v2__item">
									<span class="kt-timeline-v2__item-time">16:00</span>
									<div class="kt-timeline-v2__item-cricle">
										<i class="fa fa-genderless kt-font-brand"></i>
									</div>
									<div class="kt-timeline-v2__item-text kt-padding-top-5">
										Lorem ipsum dolor sit amit,consectetur eiusmdd tempor<br>
										incididunt ut labore et dolore magna elit enim at minim<br>
										veniam quis nostrud                                                            	                                
									</div>
								</div>
								<div class="kt-timeline-v2__item">
									<span class="kt-timeline-v2__item-time">17:00</span>
									<div class="kt-timeline-v2__item-cricle">
										<i class="fa fa-genderless kt-font-danger"></i>
									</div>
									<div class="kt-timeline-v2__item-text kt-padding-top-5">
										Received a new feedback on <a href="#" class="kt-link kt-link--brand kt-font-bolder">FinancePro App</a> product.
									</div>
								</div>
								<div class="kt-timeline-v2__item">
									<span class="kt-timeline-v2__item-time">15:30</span>
									<div class="kt-timeline-v2__item-cricle">
										<i class="fa fa-genderless kt-font-danger"></i>
									</div>
									<div class="kt-timeline-v2__item-text kt-padding-top-5">
										New notification message has been received on <a href="#" class="kt-link kt-link--brand kt-font-bolder">LoopFin Pro</a> product.
									</div>
								</div>
								-->
							</div>
						</div>
						<!--End::Timeline 3 -->
				</div>
			</div>
			<!--End::Portlet-->	
		</div>
	</div>
</div>
@endsection

@section('page_script')
<script>
 
	        $(document).ready(function() {
	           // var url = window.location.href;
	           // if(url == 'https://7fulfillment.com/portal/home'){
	           //     window.location.href = "https://app.7fulfillment.com/home"; 
	           // }
	        });
	</script>
@endsection
