<style>
.icon_sidebar_first a::before {
   content: url({{asset(config('app.asset_path').'/assets/media/sidebar/icon1.jpg')}});
   position: relative;
   top: 2px;
   left: -3px;
}
.icon_sidebar_second a::before {
   content: url({{asset(config('app.asset_path').'/assets/media/sidebar/icon2.jpg')}});
   position: relative;
   top: 2px;
   left: -3px;
}

.icon_sidebar_third a::before {
   content: url({{asset(config('app.asset_path').'/assets/media/sidebar/icon3.jpg')}});
   position: relative;
   top: 2px;
   left: -3px;
}
</style>
<!-- begin:: Aside -->
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

	<!-- begin:: Aside -->
	<div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
		<div class="kt-aside__brand-logo">
			<a href="index.html">
				<img alt="Logo" src="{{asset(config('app.asset_path').'/assets/media/logos/logo-light.png')}}" />
			</a>
		</div>
		<div class="kt-aside__brand-tools">
			<button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
				<span></span>
				<span></span>
			</button>
		</div>
	</div>

	<!-- end:: Aside -->

	<!-- begin:: Aside Menu -->
	<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
		<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
			<ul class="kt-menu__nav ">
				<li class="kt-menu__item  kt-menu__item icon_sidebar_first" aria-haspopup="true"><a href="{{url('/home')}}" class="kt-menu__link "><span class="kt-menu__link-icon"></span><span class="kt-menu__link-text">Dashboard</span></a>
				</li>
				
				
				<li class="kt-menu__item  kt-menu__item icon_sidebar_second" aria-haspopup="true"><a href="{{url('/store')}}" class="kt-menu__link "><span class="kt-menu__link-icon"></span><span class="kt-menu__link-text">Store Management</span></a>
				</li>
				
				<li class="kt-menu__item  kt-menu__item icon_sidebar_third" aria-haspopup="true"><a href="{{url('/wallet')}}" class="kt-menu__link "><span class="kt-menu__link-icon"></span><span class="kt-menu__link-text">Wallet</span></a>
				</li>
			</ul>
		</div>
	</div>

	<!-- end:: Aside Menu -->
</div>

<!-- end:: Aside -->
