<!DOCTYPE html>

<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 7
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

      <!-- begin::Head -->
      <head>
            <meta charset="utf-8" />
            <title>7fulfillment | Login</title>
            <meta name="description" content="Login page example">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

            <!--begin::Fonts -->
            <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
            <script>
                  WebFont.load({
                        google: {
                              "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
                        },
                        active: function() {
                              sessionStorage.fonts = true;
                        }
                  });
            </script>

            <!--end::Fonts -->

            <!--begin::Page Custom Styles(used by this page) -->
            <link href="{{asset(config('app.asset_path').'/assets/app/custom/login/login-v2.default.css')}}" rel="stylesheet" type="text/css" />

            <!--end::Page Custom Styles -->

            <!--begin:: Global Mandatory Vendors -->
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet" type="text/css" />

            <!--end:: Global Mandatory Vendors -->

            <!--begin:: Global Optional Vendors -->
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/tether/dist/css/tether.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/select2/dist/css/select2.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/nouislider/distribute/nouislider.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/dropzone/dist/dropzone.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/summernote/dist/summernote.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/animate.css/animate.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/toastr/build/toastr.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/morris.js/morris.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/socicon/css/socicon.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/custom/vendors/line-awesome/css/line-awesome.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/custom/vendors/flaticon/flaticon.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/custom/vendors/flaticon2/flaticon.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/custom/vendors/fontawesome5/css/all.min.css')}}" rel="stylesheet" type="text/css" />

            <!--end:: Global Optional Vendors -->

            <!--begin::Global Theme Styles(used by all pages) -->
            <link href="{{asset(config('app.asset_path').'/assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />

            <!--end::Global Theme Styles -->

            <!--begin::Layout Skins(used by all pages) -->
            <link href="{{asset(config('app.asset_path').'/assets/demo/default/skins/header/base/light.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/demo/default/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/demo/default/skins/brand/dark.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/demo/default/skins/aside/dark.css')}}" rel="stylesheet" type="text/css" />

            <!--end::Layout Skins -->
            <link rel="shortcut icon" href="{{asset(config('app.asset_path').'/assets/media/logos/favicon.ico')}}" />
<style>
.kt-login.kt-login--v2 .kt-login__wrapper .kt-login__container .kt-form .form-control {
    height: 46px;
    border-radius: 46px;
    border: none;
    padding-left: 1.5rem;
    padding-right: 1.5rem;
    margin-top: 1.5rem;
    background: rgba(67, 34, 167, 0.4);
    color: #fff !important;
}
.kt-grid__item.kt-grid__item--fluid.kt-grid.kt-grid--hor {
    background-size: cover;
}
.space_captcha{
margin-bottom:15px !important;
}
.label1 {                
      height: 25px;
      padding-left: 1.5rem;
      color: #fff !important;
}
button.btn.btn-secondary.btn-wide {
    background-color: white;
}
</style>
      </head>

      <!-- end::Head -->

      <!-- begin::Body -->
      <body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

            <!-- begin:: Page -->
            <div class="kt-grid kt-grid--ver kt-grid--root">
                  <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
                        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url(//app.7fulfillment.com/public/assets/media//bg/bg-1.jpg);">
                              <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                                    <div class="kt-login__container">
                                          <div class="kt-login__logo">
                                                <a href="#">
                                                      <img src="{{asset(config('app.asset_path').'/assets/media/logos/logo-mini-2-md.png')}}">
                                                </a>
                                          </div>
                                          
                                          <div class="kt-login__signin">
                                                <div class="kt-login__head">
                                                      
                                                      <h3 class="kt-login__title">SignUp - Step 2</h3>
                                                </div>
                                                <div style="margin-top: 25px; margin-bottom: 0px; text-align:center;">
                                                      @if (session('status'))
                                                      <div class="alert alert-success">
                                                            {{ session('status') }}
                                                       </div>
                                                      @endif
                                                </div>
    <form action="{{url('/step2')}}" method="post" class="kt-login__form kt-form">
        {{ csrf_field() }}

        <div class="form-group">
            <input class="form-control{{ $errors->has('fname') ? ' is-invalid' : '' }}" type="text" id="fname" name="fname" placeholder="First name" value="{{ isset($product1->fname) ?  $product1->fname : '' }}" required autofocus>
            @if ($errors->has('fname'))
                    <span class="invalid-feedback" style="display: block;">
                        <strong>{{ $errors->first('fname') }}</strong>
                    </span>
                @endif
            
        </div>

        <div class="form-group">
            <input class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}" type="text" id="lname" placeholder="Last name" name="lname" value="{{ isset($product1->lname) ?  $product1->lname : '' }}" required autofocus>
            
            @if ($errors->has('lname'))
                    <span class="invalid-feedback" style="display: block;">
                        <strong>{{ $errors->first('lname') }}</strong>
                    </span>
                @endif
        
            
        </div>

        <div class="form-group">
            <input class="form-control{{ $errors->has('skype') ? ' is-invalid' : '' }}" type="text" id="skype" placeholder="Skype" name="skype" value="{{ isset($product1->skype) ?  $product1->skype : '' }}" required autofocus>
            
            @if ($errors->has('skype'))
                    <span class="invalid-feedback" style="display: block;">
                        <strong>{{ $errors->first('skype') }}</strong>
                    </span>
                @endif
            
        </div>
        
        <div class="form-group">
            <input class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" type="number" id="phone" placeholder="Phone No." name="phone" value="{{ isset($product1->phone) ?  $product1->phone : '' }}" required autofocus>
            
            @if ($errors->has('phone'))
                    <span class="invalid-feedback" style="display: block;">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif  
        </div>
                                        <div class="form-group">
                                
                                <select class="form-control" id="country" name="country">
<option value="Afghanistan" {{(isset($product1->country) && $product1->country == 'Afghanistan') ?  'selected="selected"' : '' }}>Afghanistan</option>
        
        <option value="Albania" {{(isset($product1->country) && $product1->country == 'Albania') ?  'selected="selected"' : '' }}>Albania</option>
        <option value="Algeria" {{(isset($product1->country) && $product1->country == 'Algeria') ?  'selected="selected"' : '' }}>Algeria</option>
        <option value="American Samoa" {{(isset($product1->country) && $product1->country == 'American Samoa') ?  'selected="selected"' : '' }}>American Samoa</option>
        <option value="Andorra" {{(isset($product1->country) && $product1->country == 'Andorra') ?  'selected="selected"' : '' }}>Andorra</option>
        <option value="Angola" {{(isset($product1->country) && $product1->country == 'Angola') ?  'selected="selected"' : '' }}>Angola</option>
        <option value="Anguilla" {{(isset($product1->country) && $product1->country == 'Anguilla') ?  'selected="selected"' : '' }}>Anguilla</option>
        <option value="Antartica" {{(isset($product1->country) && $product1->country == 'Antartica') ?  'selected="selected"' : '' }}>Antarctica</option>
        <option value="Antigua and Barbuda" {{(isset($product1->country) && $product1->country == 'Antigua and Barbuda') ?  'selected="selected"' : '' }}>Antigua and Barbuda</option>
        <option value="Argentina" {{(isset($product1->country) && $product1->country == 'Argentina') ?  'selected="selected"' : '' }}>Argentina</option>
        <option value="Armenia" {{(isset($product1->country) && $product1->country == 'Armenia') ?  'selected="selected"' : '' }}>Armenia</option>
        <option value="Aruba" {{(isset($product1->country) && $product1->country == 'Aruba') ?  'selected="selected"' : '' }}>Aruba</option>
        <option value="Australia" {{(isset($product1->country) && $product1->country == 'Australia') ?  'selected="selected"' : '' }}>Australia</option>
        <option value="Austria" {{(isset($product1->country) && $product1->country == 'Austria') ?  'selected="selected"' : '' }}>Austria</option>
        <option value="Azerbaijan" {{(isset($product1->country) && $product1->country == 'Azerbaijan') ?  'selected="selected"' : '' }}>Azerbaijan</option>
        <option value="Bahamas" {{(isset($product1->country) && $product1->country == 'Bahamas') ?  'selected="selected"' : '' }}>Bahamas</option>
        <option value="Bahrain" {{(isset($product1->country) && $product1->country == 'Bahrain') ?  'selected="selected"' : '' }}>Bahrain</option>
        <option value="Bangladesh" {{(isset($product1->country) && $product1->country == 'Bangladesh') ?  'selected="selected"' : '' }}>Bangladesh</option>
        <option value="Barbados" {{(isset($product1->country) && $product1->country == 'Barbados') ?  'selected="selected"' : '' }}>Barbados</option>
        <option value="Belarus" {{(isset($product1->country) && $product1->country == 'Belarus') ?  'selected="selected"' : '' }}>Belarus</option>
        <option value="Belgium" {{(isset($product1->country) && $product1->country == 'Belgium') ?  'selected="selected"' : '' }}>Belgium</option>
        <option value="Belize" {{(isset($product1->country) && $product1->country == 'Belize') ?  'selected="selected"' : '' }}>Belize</option>
        <option value="Benin" {{(isset($product1->country) && $product1->country == 'Benin') ?  'selected="selected"' : '' }}>Benin</option>
        <option value="Bermuda" {{(isset($product1->country) && $product1->country == 'Bermuda') ?  'selected="selected"' : '' }}>Bermuda</option>
        <option value="Bhutan" {{(isset($product1->country) && $product1->country == 'Bhutan') ?  'selected="selected"' : '' }}>Bhutan</option>
        <option value="Bolivia" {{(isset($product1->country) && $product1->country == 'Bolivia') ?  'selected="selected"' : '' }}>Bolivia</option>
        <option value="Bosnia and Herzegowina" {{(isset($product1->country) && $product1->country == 'Bosnia and Herzegowina') ?  'selected="selected"' : '' }}>Bosnia and Herzegowina</option>
        <option value="Botswana" {{(isset($product1->country) && $product1->country == 'Botswana') ?  'selected="selected"' : '' }}>Botswana</option>
        <option value="Bouvet Island" {{(isset($product1->country) && $product1->country == 'Bouvet Island') ?  'selected="selected"' : '' }}>Bouvet Island</option>
        <option value="Brazil" {{(isset($product1->country) && $product1->country == 'Brazil') ?  'selected="selected"' : '' }}>Brazil</option>
        <option value="British Indian Ocean Territory" {{(isset($product1->country) && $product1->country == 'British Indian Ocean Territory') ?  'selected="selected"' : '' }}>British Indian Ocean Territory</option>
        <option value="Brunei Darussalam" {{(isset($product1->country) && $product1->country == 'Brunei Darussalam') ?  'selected="selected"' : '' }}>Brunei Darussalam</option>
        <option value="Bulgaria" {{(isset($product1->country) && $product1->country == 'Bulgaria') ?  'selected="selected"' : '' }}>Bulgaria</option>
        <option value="Burkina Faso" {{(isset($product1->country) && $product1->country == 'Burkina Faso') ?  'selected="selected"' : '' }}>Burkina Faso</option>
        <option value="Burundi" {{(isset($product1->country) && $product1->country == 'Burundi') ?  'selected="selected"' : '' }}>Burundi</option>
        <option value="Cambodia" {{(isset($product1->country) && $product1->country == 'Cambodia') ?  'selected="selected"' : '' }}>Cambodia</option>
        <option value="Cameroon" {{(isset($product1->country) && $product1->country == 'Cameroon') ?  'selected="selected"' : '' }}>Cameroon</option>
        <option value="Canada" {{(isset($product1->country) && $product1->country == 'Canada') ?  'selected="selected"' : '' }}>Canada</option>
        <option value="Cape Verde" {{(isset($product1->country) && $product1->country == 'Cape Verde') ?  'selected="selected"' : '' }}>Cape Verde</option>
        <option value="Cayman Islands" {{(isset($product1->country) && $product1->country == 'Cayman Islands') ?  'selected="selected"' : '' }}>Cayman Islands</option>
        <option value="Central African Republic" {{(isset($product1->country) && $product1->country == 'Central African Republic') ?  'selected="selected"' : '' }}>Central African Republic</option>
        <option value="Chad" {{(isset($product1->country) && $product1->country == 'Chad') ?  'selected="selected"' : '' }}>Chad</option>
        <option value="Chile" {{(isset($product1->country) && $product1->country == 'Chile') ?  'selected="selected"' : '' }}>Chile</option>
        <option value="China" {{(isset($product1->country) && $product1->country == 'China') ?  'selected="selected"' : '' }}>China</option>
        <option value="Christmas Island" {{(isset($product1->country) && $product1->country == 'Christmas Island') ?  'selected="selected"' : '' }}>Christmas Island</option>
        <option value="Cocos Islands" {{(isset($product1->country) && $product1->country == 'Cocos Islands') ?  'selected="selected"' : '' }}>Cocos (Keeling) Islands</option>
        <option value="Colombia" {{(isset($product1->country) && $product1->country == 'Colombia') ?  'selected="selected"' : '' }}>Colombia</option>
        <option value="Comoros" {{(isset($product1->country) && $product1->country == 'Comoros') ?  'selected="selected"' : '' }}>Comoros</option>
        <option value="Congo" {{(isset($product1->country) && $product1->country == 'Congo') ?  'selected="selected"' : '' }}>Congo</option>
        
        <option value="Cook Islands" {{(isset($product1->country) && $product1->country == 'Cook Islands') ?  'selected="selected"' : '' }}>Cook Islands</option>
        <option value="Costa Rica" {{(isset($product1->country) && $product1->country == 'Costa Rica') ?  'selected="selected"' : '' }}>Costa Rica</option>
        <option value="Cota DIvoire" {{(isset($product1->country) && $product1->country == 'Cota DIvoire') ?  'selected="selected"' : '' }}>Cote dIvoire</option>
        <option value="Croatia" {{(isset($product1->country) && $product1->country == 'Croatia') ?  'selected="selected"' : '' }}>Croatia (Hrvatska)</option>
        <option value="Cuba" {{(isset($product1->country) && $product1->country == 'Cuba') ?  'selected="selected"' : '' }}>Cuba</option>
        <option value="Cyprus" {{(isset($product1->country) && $product1->country == 'Cyprus') ?  'selected="selected"' : '' }}>Cyprus</option>
        <option value="Czech Republic" {{(isset($product1->country) && $product1->country == 'Czech Republic') ?  'selected="selected"' : '' }}>Czech Republic</option>
        <option value="Denmark" {{(isset($product1->country) && $product1->country == 'Denmark') ?  'selected="selected"' : '' }}>Denmark</option>
        <option value="Djibouti" {{(isset($product1->country) && $product1->country == 'Djibouti') ?  'selected="selected"' : '' }}>Djibouti</option>
        <option value="Dominica" {{(isset($product1->country) && $product1->country == 'Dominica') ?  'selected="selected"' : '' }}>Dominica</option>
        <option value="Dominican Republic" {{(isset($product1->country) && $product1->country == 'Dominican Republic') ?  'selected="selected"' : '' }}>Dominican Republic</option>
        <option value="East Timor" {{(isset($product1->country) && $product1->country == 'East Timor') ?  'selected="selected"' : '' }}>East Timor</option>
        <option value="Ecuador" {{(isset($product1->country) && $product1->country == 'Ecuador') ?  'selected="selected"' : '' }}>Ecuador</option>
        <option value="Egypt" {{(isset($product1->country) && $product1->country == 'Egypt') ?  'selected="selected"' : '' }}>Egypt</option>
        <option value="El Salvador" {{(isset($product1->country) &&$product1->country == 'El Salvador') ?  'selected="selected"' : '' }}>El Salvador</option>
        <option value="Equatorial Guinea" {{(isset($product1->country) &&$product1->country == 'Equatorial Guinea') ?  'selected="selected"' : '' }}>Equatorial Guinea</option>
        <option value="Eritrea" {{(isset($product1->country) && $product1->country == 'Eritrea') ?  'selected="selected"' : '' }}>Eritrea</option>
        <option value="Estonia" {{(isset($product1->country) && $product1->country == 'Estonia') ?  'selected="selected"' : '' }}>Estonia</option>
        <option value="Ethiopia" {{(isset($product1->country) && $product1->country == 'Ethiopia') ?  'selected="selected"' : '' }}>Ethiopia</option>
        <option value="Falkland Islands" {{(isset($product1->country) && $product1->country == 'Falkland Islands') ?  'selected="selected"' : '' }}>Falkland Islands (Malvinas)</option>
        <option value="Faroe Islands" {{(isset($product1->country) && $product1->country == 'Faroe Islands') ?  'selected="selected"' : '' }}>Faroe Islands</option>
        <option value="Fiji" {{(isset($product1->country) && $product1->country == 'Fiji') ?  'selected="selected"' : '' }}>Fiji</option>
        <option value="Finland" {{(isset($product1->country) &&$product1->country == 'Finland') ?  'selected="selected"' : '' }}>Finland</option>
        <option value="France" {{(isset($product1->country) && $product1->country == 'France') ?  'selected="selected"' : '' }}>France</option>
        
        <option value="Gabon" {{(isset($product1->country) && $product1->country == 'Gabon') ?  'selected="selected"' : '' }}>Gabon</option>
        <option value="Gambia" {{(isset($product1->country) && $product1->country == 'Gambia') ?  'selected="selected"' : '' }}>Gambia</option>
        <option value="Georgia" {{(isset($product1->country) && $product1->country == 'Georgia') ?  'selected="selected"' : '' }}>Georgia</option>
        <option value="Germany" {{(isset($product1->country) && $product1->country == 'Germany') ?  'selected="selected"' : '' }}>Germany</option>
        <option value="Ghana" {{(isset($product1->country) && $product1->country == 'Ghana') ?  'selected="selected"' : '' }}>Ghana</option>
        <option value="Gibraltar" {{(isset($product1->country) && $product1->country == 'Gibraltar') ?  'selected="selected"' : '' }}>Gibraltar</option>
        <option value="Greece" {{(isset($product1->country) && $product1->country == 'Greece') ?  'selected="selected"' : '' }}>Greece</option>
        <option value="Greenland" {{(isset($product1->country) && $product1->country == 'Greenland') ?  'selected="selected"' : '' }}>Greenland</option>
        <option value="Grenada" {{(isset($product1->country) && $product1->country == 'Grenada') ?  'selected="selected"' : '' }}>Grenada</option>
        <option value="Guadeloupe" {{(isset($product1->country) && $product1->country == 'Guadeloupe') ?  'selected="selected"' : '' }}>Guadeloupe</option>
        <option value="Guam" {{(isset($product1->country) && $product1->country == 'Guam') ?  'selected="selected"' : '' }}>Guam</option>
        <option value="Guatemala" {{(isset($product1->country) && $product1->country == 'Guatemala') ?  'selected="selected"' : '' }}>Guatemala</option>
        <option value="Guinea" {{(isset($product1->country) && $product1->country == 'Guinea') ?  'selected="selected"' : '' }}>Guinea</option>
        <option value="Guinea-Bissau" {{(isset($product1->country) &&$product1->country == 'Guinea-Bissau') ?  'selected="selected"' : '' }}>Guinea-Bissau</option>
        <option value="Guyana" {{(isset($product1->country) && $product1->country == 'Guyana') ?  'selected="selected"' : '' }}>Guyana</option>
        <option value="Haiti" {{(isset($product1->country) && $product1->country == 'Haiti') ?  'selected="selected"' : '' }}>Haiti</option>
        
        
        <option value="Honduras" {{(isset($product1->country) && $product1->country == 'Honduras') ?  'selected="selected"' : '' }}>Honduras</option>
        <option value="Hong Kong" {{(isset($product1->country) && $product1->country == 'Hong Kong') ?  'selected="selected"' : '' }}>Hong Kong</option>
        <option value="Hungary" {{(isset($product1->country) && $product1->country == 'Hungary') ?  'selected="selected"' : '' }}>Hungary</option>
        <option value="Iceland" {{(isset($product1->country) && $product1->country == 'Iceland') ?  'selected="selected"' : '' }}>Iceland</option>
        <option value="India" {{(isset($product1->country) && $product1->country == 'India') ?  'selected="selected"' : '' }}>India</option>
        <option value="Indonesia" {{(isset($product1->country) && $product1->country == 'Indonesia') ?  'selected="selected"' : '' }}>Indonesia</option>
        <option value="Iran" {{(isset($product1->country) && $product1->country == 'Iran') ?  'selected="selected"' : '' }}>Iran (Islamic Republic of)</option>
        <option value="Iraq" {{(isset($product1->country) && $product1->country == 'Iraq') ?  'selected="selected"' : '' }}>Iraq</option>
        <option value="Ireland" {{(isset($product1->country) && $product1->country == 'Ireland') ?  'selected="selected"' : '' }}>Ireland</option>
        <option value="Israel" {{(isset($product1->country) && $product1->country == 'Israel') ?  'selected="selected"' : '' }}>Israel</option>
        <option value="Italy" {{(isset($product1->country) && $product1->country == 'Italy') ?  'selected="selected"' : '' }}>Italy</option>
        <option value="Jamaica" {{(isset($product1->country) && $product1->country == 'Jamaica') ?  'selected="selected"' : '' }}>Jamaica</option>
        <option value="Japan"{{(isset($product1->country) && $product1->country == 'Japan') ?  'selected="selected"' : '' }}>Japan</option>
        <option value="Jordan" {{(isset($product1->country) && $product1->country == 'Jordan') ?  'selected="selected"' : '' }}>Jordan</option>
        <option value="Kazakhstan"{{(isset($product1->country) && $product1->country == 'Kazakhstan') ?  'selected="selected"' : '' }}>Kazakhstan</option>
        <option value="Kenya"{{(isset($product1->country) && $product1->country == 'Kenya') ?  'selected="selected"' : '' }}>Kenya</option>
        <option value="Kiribati"{{(isset($product1->country) && $product1->country == 'Kiribati') ?  'selected="selected"' : '' }}>Kiribati</option>
        <option value="Democratic People Republic of Korea" {{(isset($product1->country) && $product1->country == 'Democratic People Republic of Korea') ?  'selected="selected"' : '' }}>Korea, Democratic People Republic of</option>
        <option value="Korea" {{(isset($product1->country) && $product1->country == 'Korea') ?  'selected="selected"' : '' }}>Korea, Republic of</option>
        <option value="Kuwait" {{(isset($product1->country) && $product1->country == 'Kuwait') ?  'selected="selected"' : '' }}>Kuwait</option>
        <option value="Kyrgyzstan" {{(isset($product1->country) && $product1->country == 'Kyrgyzstan') ?  'selected="selected"' : '' }}>Kyrgyzstan</option>
        <option value="Lao" {{(isset($product1->country) && $product1->country == 'Lao') ?  'selected="selected"' : '' }}>Lao People Democratic Republic</option>
        <option value="Latvia" {{(isset($product1->country) && $product1->country == 'Latvia') ?  'selected="selected"' : '' }}>Latvia</option>
        <option value="Lebanon" {{(isset($product1->country) && $product1->country == 'Lebanon') ?  'selected="selected"' : '' }}>Lebanon</option>
        <option value="Lesotho" {{(isset($product1->country) && $product1->country == 'Lesotho') ?  'selected="selected"' : '' }}>Lesotho</option>
        <option value="Liberia" {{(isset($product1->country) && $product1->country == 'Liberia') ?  'selected="selected"' : '' }}>Liberia</option>
        <option value="Libyan Arab Jamahiriya" {{(isset($product1->country) && $product1->country == 'Libyan Arab Jamahiriya') ?  'selected="selected"' : '' }}>Libyan Arab Jamahiriya</option>
        <option value="Liechtenstein" {{(isset($product1->country) && $product1->country == 'Liechtenstein') ?  'selected="selected"' : '' }}>Liechtenstein</option>
        <option value="Lithuania" {{(isset($product1->country) && $product1->country == 'Lithuania') ?  'selected="selected"' : '' }}>Lithuania</option>
        <option value="Luxembourg" {{(isset($product1->country) && $product1->country == 'Luxembourg') ?  'selected="selected"' : '' }}>Luxembourg</option>
        <option value="Macau" {{(isset($product1->country) && $product1->country == 'Macau') ?  'selected="selected"' : '' }}>Macau</option>
        <option value="Macedonia" {{(isset($product1->country) && $product1->country == 'Macedonia') ?  'selected="selected"' : '' }}>Macedonia, The Former Yugoslav Republic of</option>
        <option value="Madagascar" {{(isset($product1->country) && $product1->country == 'Madagascar') ?  'selected="selected"' : '' }}>Madagascar</option>
        <option value="Malawi" {{(isset($product1->country) && $product1->country == 'Malawi') ?  'selected="selected"' : '' }}>Malawi</option>
        <option value="Malaysia" {{(isset($product1->country) && $product1->country == 'Malaysia') ?  'selected="selected"' : '' }}>Malaysia</option>
        <option value="Maldives" {{(isset($product1->country) && $product1->country == 'Maldives') ?  'selected="selected"' : '' }}>Maldives</option>
        <option value="Mali" {{(isset($product1->country) && $product1->country == 'Mali') ?  'selected="selected"' : '' }}>Mali</option>
        <option value="Malta" {{(isset($product1->country) && $product1->country == 'Malta') ?  'selected="selected"' : '' }}>Malta</option>
        <option value="Marshall Islands" {{(isset($product1->country) && $product1->country == 'Marshall Islands') ?  'selected="selected"' : '' }}>Marshall Islands</option>
        <option value="Martinique" {{(isset($product1->country) && $product1->country == 'Martinique') ?  'selected="selected"' : '' }}>Martinique</option>
        <option value="Mauritania" {{(isset($product1->country) && $product1->country == 'Mauritania') ?  'selected="selected"' : '' }}>Mauritania</option>
        <option value="Mauritius" {{(isset($product1->country) && $product1->country == 'Mauritius') ?  'selected="selected"' : '' }}>Mauritius</option>
        <option value="Mayotte" {{(isset($product1->country) && $product1->country == 'Mayotte') ?  'selected="selected"' : '' }}>Mayotte</option>
        <option value="Mexico" {{(isset($product1->country) && $product1->country == 'Mexico') ?  'selected="selected"' : '' }}>Mexico</option>
        <option value="Micronesia" {{(isset($product1->country) && $product1->country == 'Micronesia') ?  'selected="selected"' : '' }}>Micronesia, Federated States of</option>
        <option value="Moldova" {{(isset($product1->country) && $product1->country == 'Moldova') ?  'selected="selected"' : '' }}>Moldova, Republic of</option>
        <option value="Monaco" {{(isset($product1->country) && $product1->country == 'Monaco') ?  'selected="selected"' : '' }}>Monaco</option>
        <option value="Mongolia" {{(isset($product1->country) && $product1->country == 'Mongolia') ?  'selected="selected"' : '' }}>Mongolia</option>
        <option value="Montserrat" {{(isset($product1->country) && $product1->country == 'Montserrat') ?  'selected="selected"' : '' }}>Montserrat</option>
        <option value="Morocco" {{(isset($product1->country) && $product1->country == 'Morocco') ?  'selected="selected"' : '' }}>Morocco</option>
        <option value="Mozambique" {{(isset($product1->country) && $product1->country == 'Mozambique') ?  'selected="selected"' : '' }}>Mozambique</option>
        <option value="Myanmar" {{(isset($product1->country) && $product1->country == 'Myanmar') ?  'selected="selected"' : '' }}>Myanmar</option>
        <option value="Namibia" {{(isset($product1->country) && $product1->country == 'Namibia') ?  'selected="selected"' : '' }}>Namibia</option>
        <option value="Nauru" {{(isset($product1->country) && $product1->country == 'Nauru') ?  'selected="selected"' : '' }}Nauru</option>
        <option value="Nepal" {{(isset($product1->country) && $product1->country == 'Nepal') ?  'selected="selected"' : '' }}>Nepal</option>
        <option value="Netherlands" {{(isset($product1->country) && $product1->country == 'Netherlands') ?  'selected="selected"' : '' }}>Netherlands</option>
        <option value="Netherlands Antilles" {{(isset($product1->country) && $product1->country == 'Netherlands Antilles') ?  'selected="selected"' : '' }}>Netherlands Antilles</option>
        <option value="New Caledonia" {{(isset($product1->country) && $product1->country == 'New Caledonia') ?  'selected="selected"' : '' }}>New Caledonia</option>
        <option value="New Zealand" {{(isset($product1->country) && $product1->country == 'New Zealand') ?  'selected="selected"' : '' }}>New Zealand</option>
        <option value="Nicaragua" {{(isset($product1->country) && $product1->country == 'Nicaragua') ?  'selected="selected"' : '' }}>Nicaragua</option>
        <option value="Niger" {{(isset($product1->country) && $product1->country == 'Niger') ?  'selected="selected"' : '' }}>Niger</option>
        <option value="Nigeria" {{(isset($product1->country) && $product1->country == 'Nigeria') ?  'selected="selected"' : '' }}>Nigeria</option>
        <option value="Niue" {{(isset($product1->country) && $product1->country == 'Niue') ?  'selected="selected"' : '' }}>Niue</option>
        <option value="Norfolk Island" {{(isset($product1->country) && $product1->country == 'Norfolk Island') ?  'selected="selected"' : '' }}>Norfolk Island</option>
        <option value="Northern Mariana Islands" {{(isset($product1->country) && $product1->country == 'Northern Mariana Islands') ?  'selected="selected"' : '' }}>Northern Mariana Islands</option>
        <option value="Norway" {{(isset($product1->country) && $product1->country == 'Norway') ?  'selected="selected"' : '' }}>Norway</option>
        <option value="Oman" {{(isset($product1->country) && $product1->country == 'Oman') ?  'selected="selected"' : '' }}>Oman</option>
        <option value="Pakistan" {{(isset($product1->country) && $product1->country == 'Pakistan') ?  'selected="selected"' : '' }}>Pakistan</option>
        <option value="Palau" {{(isset($product1->country) && $product1->country == 'Palau') ?  'selected="selected"' : '' }}>Palau</option>
        <option value="Panama" {{(isset($product1->country) && $product1->country == 'Panama') ?  'selected="selected"' : '' }}>Panama</option>
        <option value="Papua New Guinea" {{(isset($product1->country) && $product1->country == 'Papua New Guinea') ?  'selected="selected"' : '' }}>Papua New Guinea</option>
        <option value="Paraguay" {{(isset($product1->country) && $product1->country == 'Paraguay') ?  'selected="selected"' : '' }}>Paraguay</option>
        <option value="Peru" {{(isset($product1->country) && $product1->country == 'Peru') ?  'selected="selected"' : '' }}>Peru</option>
        <option value="Philippines" {{(isset($product1->country) && $product1->country == 'Philippines') ?  'selected="selected"' : '' }}>Philippines</option>
        <option value="Pitcairn" {{(isset($product1->country) && $product1->country == 'Pitcairn') ?  'selected="selected"' : '' }}>Pitcairn</option>
        <option value="Poland" {{(isset($product1->country) && $product1->country == 'Poland') ?  'selected="selected"' : '' }}>Poland</option>
        <option value="Portugal" {{(isset($product1->country) && $product1->country == 'Portugal') ?  'selected="selected"' : '' }}>Portugal</option>
        <option value="Puerto Rico" {{(isset($product1->country) && $product1->country == 'Puerto Rico') ?  'selected="selected"' : '' }}>Puerto Rico</option>
        <option value="Qatar" {{(isset($product1->country) && $product1->country == 'Qatar') ?  'selected="selected"' : '' }}>Qatar</option>
        <option value="Reunion" {{(isset($product1->country) && $product1->country == 'Reunion') ?  'selected="selected"' : '' }}>Reunion</option>
        <option value="Romania" {{(isset($product1->country) && $product1->country == 'Romania') ?  'selected="selected"' : '' }}>Romania</option>
        <option value="Russia" {{(isset($product1->country) && $product1->country == 'Russia') ?  'selected="selected"' : '' }}>Russian Federation</option>
        <option value="Rwanda" {{(isset($product1->country) && $product1->country == 'Rwanda') ?  'selected="selected"' : '' }}>Rwanda</option>
        <option value="Saint Kitts and Nevis" {{(isset($product1->country) && $product1->country == 'Saint Kitts and Nevis') ?  'selected="selected"' : '' }}>Saint Kitts and Nevis</option> 
        <option value="Saint LUCIA" {{(isset($product1->country) && $product1->country == 'Saint LUCIA') ?  'selected="selected"' : '' }}>Saint LUCIA</option>
        <option value="Saint Vincent" {{(isset($product1->country) &&$product1->country == 'Saint Vincent') ?  'selected="selected"' : '' }}>Saint Vincent and the Grenadines</option>
        <option value="Samoa" {{(isset($product1->country) && $product1->country == 'Samoa') ?  'selected="selected"' : '' }}>Samoa</option>
        <option value="San Marino" {{(isset($product1->country) && $product1->country == 'San Marino') ?  'selected="selected"' : '' }}>San Marino</option>
        <option value="Sao Tome and Principe" {{(isset($product1->country) && $product1->country == 'Sao Tome and Principe') ?  'selected="selected"' : '' }}>Sao Tome and Principe</option> 
        <option value="Saudi Arabia" {{(isset($product1->country) && $product1->country == 'Saudi Arabia') ?  'selected="selected"' : '' }}>Saudi Arabia</option>
        <option value="Senegal" {{(isset($product1->country) && $product1->country == 'Senegal') ?  'selected="selected"' : '' }}>Senegal</option>
        <option value="Seychelles" {{(isset($product1->country) && $product1->country == 'Seychelles') ?  'selected="selected"' : '' }}>Seychelles</option>
        <option value="Sierra" {{(isset($product1->country) && $product1->country == 'Sierra') ?  'selected="selected"' : '' }}>Sierra Leone</option>
        <option value="Singapore" {{(isset($product1->country) && $product1->country == 'Singapore') ?  'selected="selected"' : '' }}>Singapore</option>
        <option value="Slovakia" {{(isset($product1->country) && $product1->country == 'Slovakia') ?  'selected="selected"' : '' }}>Slovakia (Slovak Republic)</option>
        <option value="Slovenia" {{(isset($product1->country) && $product1->country == 'Slovenia') ?  'selected="selected"' : '' }}>Slovenia</option>
        <option value="Solomon Islands" {{(isset($product1->country) && $product1->country == 'Solomon Islands') ?  'selected="selected"' : '' }}>Solomon Islands</option>
        <option value="Somalia" {{(isset($product1->country) && $product1->country == 'Somalia') ?  'selected="selected"' : '' }}>Somalia</option>
        <option value="South Africa" {{(isset($product1->country) && $product1->country == 'South Africa') ?  'selected="selected"' : '' }}>South Africa</option>
        <option value="South Georgia" {{(isset($product1->country) && $product1->country == 'South Georgia') ?  'selected="selected"' : '' }}>South Georgia and the South Sandwich Islands</option>
        <option value="Span" {{(isset($product1->country) && $product1->country == 'Span') ?  'selected="selected"' : '' }}>Spain</option>
        <option value="SriLanka" {{(isset($product1->country) && $product1->country == 'SriLanka') ?  'selected="selected"' : '' }}>Sri Lanka</option>
        <option value="St. Helena" {{(isset($product1->country) && $product1->country == 'St. Helena') ?  'selected="selected"' : '' }}>St. Helena</option>
        <option value="St. Pierre and Miguelon" {{(isset($product1->country) && $product1->country == 'St. Pierre and Miguelon') ?  'selected="selected"' : '' }}>St. Pierre and Miquelon</option>
        <option value="Sudan" {{(isset($product1->country) && $product1->country == 'Sudan') ?  'selected="selected"' : '' }}>Sudan</option>
        <option value="Suriname" {{(isset($product1->country) && $product1->country == 'Suriname') ?  'selected="selected"' : '' }}>Suriname</option>
        <option value="Svalbard" {{(isset($product1->country) && $product1->country == 'Svalbard') ?  'selected="selected"' : '' }}>Svalbard and Jan Mayen Islands</option>
        <option value="Swaziland" {{(isset($product1->country) && $product1->country == 'Swaziland') ?  'selected="selected"' : '' }}>Swaziland</option>
        <option value="Sweden" {{(isset($product1->country) && $product1->country == 'Sweden') ?  'selected="selected"' : '' }}>Sweden</option>
        <option value="Switzerland" {{(isset($product1->country) && $product1->country == 'Switzerland') ?  'selected="selected"' : '' }}>Switzerland</option>
        <option value="Syria" {{(isset($product1->country) && $product1->country == 'Syria') ?  'selected="selected"' : '' }}>Syrian Arab Republic</option>
        <option value="Taiwan" {{(isset($product1->country) && $product1->country == 'Taiwan') ?  'selected="selected"' : '' }}>Taiwan, Province of China</option>
        <option value="Tajikistan" {{(isset($product1->country) && $product1->country == 'Tajikistan') ?  'selected="selected"' : '' }}>Tajikistan</option>
        <option value="Tanzania" {{(isset($product1->country) && $product1->country == 'Tanzania') ?  'selected="selected"' : '' }}>Tanzania, United Republic of</option>
        <option value="Thailand" {{(isset($product1->country) && $product1->country == 'Thailand') ?  'selected="selected"' : '' }}>Thailand</option>
        <option value="Togo" {{(isset($product1->country) && $product1->country == 'Togo') ?  'selected="selected"' : '' }}>Togo</option>
        <option value="Tokelau" {{(isset($product1->country) && $product1->country == 'Tokelau') ?  'selected="selected"' : '' }}>Tokelau</option>
        <option value="Tonga" {{(isset($product1->country) && $product1->country == 'Tonga') ?  'selected="selected"' : '' }}>Tonga</option>
        <option value="Trinidad and Tobago" {{(isset($product1->country) && $product1->country == 'Trinidad and Tobago') ?  'selected="selected"' : '' }}>Trinidad and Tobago</option>
        <option value="Tunisia" {{(isset($product1->country) && $product1->country == 'Tunisia') ?  'selected="selected"' : '' }}>Tunisia</option>
        <option value="Turkey" {{(isset($product1->country) && $product1->country == 'Turkey') ?  'selected="selected"' : '' }}>Turkey</option>
        <option value="Turkmenistan" {{(isset($product1->country) && $product1->country == 'Turkmenistan') ?  'selected="selected"' : '' }}>Turkmenistan</option>
        <option value="Turks and Caicos" {{(isset($product1->country) && $product1->country == 'Turks and Caicos') ?  'selected="selected"' : '' }}>Turks and Caicos Islands</option>
        <option value="Tuvalu" {{(isset($product1->country) && $product1->country == 'Tuvalu') ?  'selected="selected"' : '' }}>Tuvalu</option>
        <option value="Uganda" {{(isset($product1->country) && $product1->country == 'Uganda') ?  'selected="selected"' : '' }}>Uganda</option>
        <option value="Ukraine" {{(isset($product1->country) && $product1->country == 'Ukraine') ?  'selected="selected"' : '' }}>Ukraine</option>
        <option value="United Arab Emirates" {{(isset($product1->country) && $product1->country == 'United Arab Emirates') ?  'selected="selected"' : '' }}>United Arab Emirates</option>
        <option value="United Kingdom" {{(isset($product1->country) && $product1->country == 'United Kingdom') ?  'selected="selected"' : '' }}>United Kingdom</option>
        <option value="United States" {{(isset($product1->country) && $product1->country == 'United States') ?  'selected="selected"' : '' }}>United States</option>
        <option value="United States Minor Outlying Islands" {{(isset($product1->country) && $product1->country == 'United States Minor Outlying Islands') ?  'selected="selected"' : '' }}>United States Minor Outlying Islands</option>
        <option value="Uruguay" {{(isset($product1->country) && $product1->country == 'Uruguay') ?  'selected="selected"' : '' }}>Uruguay</option>
        <option value="Uzbekistan" {{(isset($product1->country) && $product1->country == 'Uzbekistan') ?  'selected="selected"' : '' }}>Uzbekistan</option>
        <option value="Vanuatu" {{(isset($product1->country) && $product1->country == 'Vanuatu') ?  'selected="selected"' : '' }}>Vanuatu</option>
        <option value="Venezuela" {{(isset($product1->country) && $product1->country == 'Venezuela') ?  'selected="selected"' : '' }}>Venezuela</option>
        <option value="Vietnam" {{(isset($product1->country) && $product1->country == 'Vietnam') ?  'selected="selected"' : '' }}>Viet Nam</option>
        <option value="Virgin Islands (British)" {{(isset($product1->country) && $product1->country == 'Virgin Islands (British)') ?  'selected="selected"' : '' }}>Virgin Islands (British)</option>
        <option value="Virgin Islands (U.S)" {{(isset($product1->country) && $product1->country == 'Virgin Islands (U.S)') ?  'selected="selected"' : '' }}>Virgin Islands (U.S.)</option>
        <option value="Wallis and Futana Islands" {{(isset($product1->country) && $product1->country == 'Wallis and Futana Islands') ?  'selected="selected"' : '' }}>Wallis and Futuna Islands</option>
        <option value="Western Sahara" {{(isset($product1->country) && $product1->country == 'Western Sahara') ?  'selected="selected"' : '' }}>Western Sahara</option>
        <option value="Yemen" {{(isset($product1->country) && $product1->country == 'Yemen') ?  'selected="selected"' : '' }}>Yemen</option>
        <option value="Yugoslavia" {{(isset($product1->country) && $product1->country == 'Yugoslavia') ?  'selected="selected"' : '' }}>Yugoslavia</option>
        <option value="Zambia" {{(isset($product1->country) && $product1->country == 'Zambia') ?  'selected="selected"' : '' }}>Zambia</option>
        <option value="Zimbabwe" {{(isset($product1->country) &&$product1->country == 'Zimbabwe') ?  'selected="selected"' : '' }}>Zimbabwe</option>
    </select>

        @if ($errors->has('country'))
                <span class="invalid-feedback" style="display: block;">
                    <strong>{{ $errors->first('country') }}</strong>
                </span>
            @endif  
    </div>

        <div class="form-group">
            <input class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" type="text" id="state" placeholder="State" name="state" value="{{ isset($product1->state) ?  $product1->state : '' }}" required autofocus>
            
            @if ($errors->has('state'))
                    <span class="invalid-feedback" style="display: block;">
                        <strong>{{ $errors->first('state') }}</strong>
                    </span>
                @endif        
        </div>

        <div class="form-group">
            <input class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" type="text" id="city" placeholder="City" name="city" value="{{ isset($product1->city) ?  $product1->city : '' }}" required autofocus>
            
            @if ($errors->has('city'))
                    <span class="invalid-feedback" style="display: block;">
                        <strong>{{ $errors->first('city') }}</strong>
                    </span>
                @endif            
        </div>
<!--
        <div class="kt-login__actions">

            <button type="button" onclick="window.location.href ='{{url('/step1')}}';" class="btn btn-pill kt-login__btn-primary">
                        {{ __('Previous') }}
                    </button>&nbsp;&nbsp;
            <button type="submit" class="btn btn-pill kt-login__btn-secondary">Next</button>&nbsp;&nbsp;
<button type="button" onclick="window.location.href ='{{url('/')}}';" class="btn btn-pill kt-login__btn-primary">
                        Cancel
                    </button>
      </div>
-->

<div class="kt-login__actions">
<button type="button" onclick="window.location.href ='{{url('/')}}';" class="btn btn-secondary btn-wide">
                        Cancel
                    </button>&nbsp;&nbsp;
            <button type="button" onclick="window.location.href ='{{url('/step1')}}';" class="btn btn-primary btn-wide">
                        {{ __('Previous') }}
                    </button>&nbsp;&nbsp;
            <button type="submit" class="btn btn-success btn-wide">Next</button>&nbsp;&nbsp;

     
        </div>
        <!-- <a type="button" href="{{url('/products/create-step1')}}" class="btn btn-warning">Previous</a>
        <button type="submit" class="btn btn-primary">Next</button> -->
        <!--@if ($errors->any())-->
        <!--    <div class="alert alert-danger">-->
        <!--        <ul>-->
        <!--            @foreach ($errors->all() as $error)-->
        <!--                <li>{{ $error }}</li>-->
        <!--            @endforeach-->
        <!--        </ul>-->
        <!--    </div>-->
        <!--@endif-->
    </form>
</div>
    </div>
                              </div>
                        </div>
                  </div>
            </div>

            <!-- end:: Page -->

            <!-- begin::Global Config(global config for global JS sciprts) -->
            <script>
                  var KTAppOptions = {
                        "colors": {
                              "state": {
                                    "brand": "#5d78ff",
                                    "dark": "#282a3c",
                                    "light": "#ffffff",
                                    "primary": "#5867dd",
                                    "success": "#34bfa3",
                                    "info": "#36a3f7",
                                    "warning": "#ffb822",
                                    "danger": "#fd3995"
                              },
                              "base": {
                                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                              }
                        }
                  };
            </script>

            <!-- end::Global Config -->

            <!--begin:: Global Mandatory Vendors -->
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/popper.js/dist/umd/popper.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/js-cookie/src/js.cookie.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/moment/min/moment.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/sticky-js/dist/sticky.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/wnumb/wNumb.js')}}" type="text/javascript"></script>

            <!--end:: Global Mandatory Vendors -->

            <!--begin:: Global Optional Vendors -->
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/jquery-form/dist/jquery.form.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/block-ui/jquery.blockUI.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/components/vendors/bootstrap-datepicker/init.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/components/vendors/bootstrap-timepicker/init.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/components/vendors/bootstrap-switch/init.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/select2/dist/js/select2.full.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/ion-rangeslider/js/ion.rangeSlider.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/typeahead.js/dist/typeahead.bundle.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/handlebars/dist/handlebars.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/nouislider/distribute/nouislider.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/owl.carousel/dist/owl.carousel.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/autosize/dist/autosize.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/clipboard/dist/clipboard.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/dropzone/dist/dropzone.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/summernote/dist/summernote.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/markdown/lib/markdown.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/components/vendors/bootstrap-markdown/init.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-notify/bootstrap-notify.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/components/vendors/bootstrap-notify/init.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/jquery-validation/dist/jquery.validate.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/jquery-validation/dist/additional-methods.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/components/vendors/jquery-validation/init.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/toastr/build/toastr.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/raphael/raphael.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/morris.js/morris.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/chart.js/dist/Chart.bundle.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/waypoints/lib/jquery.waypoints.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/counterup/jquery.counterup.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/es6-promise-polyfill/promise.min.js')}}" type="text/javascript"></script>
            <script src="../assets/vendors/general/sweetalert2/dist/sweetalert2.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/components/vendors/sweetalert2/init.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/jquery.repeater/src/lib.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/jquery.repeater/src/jquery.input.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/jquery.repeater/src/repeater.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/dompurify/dist/purify.js')}}" type="text/javascript"></script>

            <!--end:: Global Optional Vendors -->

            <!--begin::Global Theme Bundle(used by all pages) -->
            <script src="{{asset(config('app.asset_path').'/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>

            <!--end::Global Theme Bundle -->

            <!--begin::Page Scripts(used by this page) -->
            <script src="{{asset(config('app.asset_path').'/assets/app/custom/login/login-general.js')}}" type="text/javascript"></script>

            <!--end::Page Scripts -->

            <!--begin::Global App Bundle(used by all pages) -->
            <script src="{{asset(config('app.asset_path').'/assets/app/bundle/app.bundle.js')}}" type="text/javascript"></script>

            <!--end::Global App Bundle -->
      </body>

      <!-- end::Body -->
</html>
