<!DOCTYPE html>

<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 7
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

      <!-- begin::Head -->
      <head>
            <meta charset="utf-8" />
            <title>7fulfillment | Login</title>
            <meta name="description" content="Login page example">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

            <!--begin::Fonts -->
            <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
            <script>
                  WebFont.load({
                        google: {
                              "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
                        },
                        active: function() {
                              sessionStorage.fonts = true;
                        }
                  });
            </script>

            <!--end::Fonts -->

            <!--begin::Page Custom Styles(used by this page) -->
            <link href="{{asset(config('app.asset_path').'/assets/app/custom/login/login-v2.default.css')}}" rel="stylesheet" type="text/css" />

            <!--end::Page Custom Styles -->

            <!--begin:: Global Mandatory Vendors -->
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet" type="text/css" />

            <!--end:: Global Mandatory Vendors -->

            <!--begin:: Global Optional Vendors -->
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/tether/dist/css/tether.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/select2/dist/css/select2.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/nouislider/distribute/nouislider.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/dropzone/dist/dropzone.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/summernote/dist/summernote.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/animate.css/animate.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/toastr/build/toastr.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/morris.js/morris.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/general/socicon/css/socicon.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/custom/vendors/line-awesome/css/line-awesome.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/custom/vendors/flaticon/flaticon.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/custom/vendors/flaticon2/flaticon.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/vendors/custom/vendors/fontawesome5/css/all.min.css')}}" rel="stylesheet" type="text/css" />

            <!--end:: Global Optional Vendors -->

            <!--begin::Global Theme Styles(used by all pages) -->
            <link href="{{asset(config('app.asset_path').'/assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />

            <!--end::Global Theme Styles -->

            <!--begin::Layout Skins(used by all pages) -->
            <link href="{{asset(config('app.asset_path').'/assets/demo/default/skins/header/base/light.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/demo/default/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/demo/default/skins/brand/dark.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{asset(config('app.asset_path').'/assets/demo/default/skins/aside/dark.css')}}" rel="stylesheet" type="text/css" />

            <!--end::Layout Skins -->
            <link rel="shortcut icon" href="{{asset(config('app.asset_path').'/assets/media/logos/favicon.ico')}}" />
<style>
.kt-login.kt-login--v2 .kt-login__wrapper .kt-login__container .kt-form .form-control {
    height: 46px;
    border-radius: 46px;
    border: none;
    padding-left: 1.5rem;
    padding-right: 1.5rem;
    margin-top: 1.5rem;
    background: rgba(67, 34, 167, 0.4);
    color: #fff !important;
}
.kt-grid__item.kt-grid__item--fluid.kt-grid.kt-grid--hor {
    background-size: cover;
}
.space_captcha{
margin-bottom:15px !important;
}
.label1 {                
      height: 25px;
      padding-left: 1.5rem;
      color: #fff !important;
}
button.btn.btn-secondary.btn-wide {
    background-color: white;
}
</style>
      </head>

      <!-- end::Head -->

      <!-- begin::Body -->
      <body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

            <!-- begin:: Page -->
            <div class="kt-grid kt-grid--ver kt-grid--root">
                  <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
                        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url(//app.7fulfillment.com/public/assets/media//bg/bg-1.jpg);">
                              <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                                    <div class="kt-login__container">
                                          <div class="kt-login__logo">
                                                <a href="#">
                                                      <img src="{{asset(config('app.asset_path').'/assets/media/logos/logo-mini-2-md.png')}}">
                                                </a>
                                          </div>
                                          
                                          <div class="kt-login__signin">
                                                <div class="kt-login__head">
                                                      
                                                      <h3 class="kt-login__title">SignUp - Step 4</h3>
                                                </div>
                                                <div style="margin-top: 25px; margin-bottom: 0px; text-align:center;">
                                                      @if (session('status'))
                                                      <div class="alert alert-success">
                                                            {{ session('status') }}
                                                       </div>
                                                      @endif
                                                </div>

                                                <!--@if ($errors->any())-->
                                                <!--    <div class="alert alert-danger">-->
                                                <!--        <ul>-->
                                                <!--            @foreach ($errors->all() as $error)-->
                                                <!--                <li>{{ $error }}</li>-->
                                                <!--            @endforeach-->
                                                <!--        </ul>-->
                                                <!--    </div>-->
                                                <!--@endif-->
   
    <form action="{{ route('register') }}" method="post" class="kt-login__form kt-form">
        {{ csrf_field() }}                

      <div class="form-group">
            <label for="exampleSelect1" class="label1">Which platform do you use?</label>
            <select class="form-control{{ $errors->has('platform') ? ' is-invalid' : '' }}" name="platform" value="" required autofocus>
<option  value="">Select a platform</option>
                  <option {{ (isset($product3->platform) && $product3->platform == 'Shopify') ? "selected=\"selected\"" : "" }}>Shopify</option>
                  <option {{ (isset($product3->platform) && $product3->platform == 'WooCommerce') ? "selected=\"selected\"" : "" }}>WooCommerce</option>
            </select>
            @if ($errors->has('platform'))
                  <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('platform') }}</strong>
                  </span>
            @endif
      </div>

      <div class="form-group">
            <label class="label1">How long have you been running your store?</label>
            <select class="form-control{{ $errors->has('storeruntime') ? ' is-invalid' : '' }}" name="storeruntime" value="" required autofocus>
<option  value="">Select an option</option>

<option value="Just started" {{ (isset($product3->storeruntime) && $product3->storeruntime == 'Just started') ?  'selected="selected"' : '' }}>Just started</option>
<option value="Less a year" {{ (isset($product3->storeruntime) && $product3->storeruntime == 'Less a year') ?  'selected="selected"' : '' }}>Less a year</option>
<option value="1 - 3 years" {{ (isset($product3->storeruntime) && $product3->storeruntime == '1 - 3 years') ?  'selected="selected"' : '' }}>1 - 3 years</option>
<option value="4 - 10 years" {{ (isset($product3->storeruntime) && $product3->storeruntime == '4 - 10 years') ?  'selected="selected"' : '' }}>4 - 10 years</option>
<option value="10+ years" {{ (isset($product3->storeruntime) && $product3->storeruntime == '10+ years') ?  'selected="selected"' : '' }}>10+ years</option>

<!--
                  <option {{ (isset($product3->storeruntime) && $product3->storeruntime == 'Just started') ? "selected=\"selected\"" : "" }}>Just started</option>
                  <option {{ (isset($product3->storeruntime) && $product3->storeruntime == 'Less a year') ? "selected=\"selected\"" : "" }}>Less a year</option>
                  <option {{ (isset($product3->storeruntime) && $product3->storeruntime == '1 - 3 years') ? "selected=\"selected\"" : "" }}>1 - 3 years</option>
                  <option {{ (isset($product3->storeruntime) && $product3->storeruntime == '4 - 10 years') ? "selected=\"selected\"" : "" }}>4 - 10 years</option>
                  <option {{ (isset($product3->storeruntime) && $product3->storeruntime == '10+ years') ? "selected=\"selected\"" : "" }}>10+ years</option>
-->
            </select>
            @if ($errors->has('storeruntime'))
                  <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('storeruntime') }}</strong>
                  </span>
            @endif
      </div>

      <div class="form-group">
            <label class="label1">How many orders have you been getting on average everyday in the past 30 days?</label>
            <select class="form-control{{ $errors->has('ordersno') ? ' is-invalid' : '' }}" name="ordersno" value="" required autofocus>
<option  value="">Select an option</option>

<option value="Less than 50" {{ (isset($product3->ordersno) && $product3->ordersno == 'Less than 50') ?  'selected="selected"' : '' }}>Less than 50</option>
<option value="51 - 100" {{ (isset($product3->ordersno) && $product3->ordersno == '51 - 100') ?  'selected="selected"' : '' }}>51 - 100</option>
<option value="101 - 200" {{ (isset($product3->ordersno) && $product3->ordersno == '101 - 200') ?  'selected="selected"' : '' }}>101 - 200</option>
<option value="201 - 300" {{ (isset($product3->ordersno) && $product3->ordersno == '201 - 300') ?  'selected="selected"' : '' }}>201 - 300</option>
<option value="301 - 400" {{ (isset($product3->ordersno) && $product3->ordersno == '301 - 400') ?  'selected="selected"' : '' }}>301 - 400</option>
<option value="401 - 500" {{ (isset($product3->ordersno) && $product3->ordersno == '401 - 500') ?  'selected="selected"' : '' }}>401 - 500</option>
<option value="Just started" {{ (isset($product3->ordersno) && $product3->ordersno == '500+') ?  'selected="selected"' : '' }}>500+</option>

<!--
                  <option {{ (isset($product3->ordersno) && $product3->ordersno == 'Less than 50') ? "selected=\"selected\"" : "" }}>Less than 50</option>
                  <option {{ (isset($product3->ordersno) && $product3->ordersno == '51 - 100') ? "selected=\"selected\"" : "" }}>51 - 100</option>
                  <option {{ (isset($product3->ordersno) && $product3->ordersno == '101 - 200') ? "selected=\"selected\"" : "" }}>101 - 200</option>
                  <option {{ (isset($product3->ordersno) && $product3->ordersno == '201 - 300') ? "selected=\"selected\"" : "" }}>201 - 300</option>
                  <option {{ (isset($product3->ordersno) && $product3->ordersno == '301 - 400') ? "selected=\"selected\"" : "" }}>301 - 400</option>
                  <option {{ (isset($product3->ordersno) && $product3->ordersno == '401 - 500') ? "selected=\"selected\"" : "" }}>401 - 500</option>
                  <option {{ (isset($product3->ordersno) && $product3->ordersno == '500+') ? "selected=\"selected\"" : "" }}>500+</option>
-->
            </select>
            @if ($errors->has('ordersno'))
                  <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('ordersno') }}</strong>
                  </span>
            @endif
      </div>

      <div class="form-group">
            <label class="label1">How many products do you want us to process for you?</label>
            <select class="form-control{{ $errors->has('productsno') ? ' is-invalid' : '' }}" name="productsno" value="" required autofocus>
<option  value="">Select an option</option>

<option value="Only 1" {{ (isset($product3->productsno) && $product3->productsno == 'Only 1') ?  'selected="selected"' : '' }}>Only 1</option>
<option value="2 - 10" {{ (isset($product3->productsno) && $product3->productsno == '2 - 10') ?  'selected="selected"' : '' }}>2 - 10</option>
<option value="11-20" {{ (isset($product3->productsno) && $product3->productsno == '11-20') ?  'selected="selected"' : '' }}>11-20</option>
<option value="21-30" {{ (isset($product3->productsno) && $product3->productsno == '21-30') ?  'selected="selected"' : '' }}>21-30</option>
<option value="30+" {{ (isset($product3->productsno) && $product3->productsno == '30+') ?  'selected="selected"' : '' }}>30+</option>

<!--
                  <option {{ (isset($product3->productsno) && $product3->productsno == 'Only 1') ? "selected=\"selected\"" : "" }}>Only 1</option>
                  <option {{ (isset($product3->productsno) && $product3->productsno == '2 - 10') ? "selected=\"selected\"" : "" }}>2 - 10</option>
                  <option {{ (isset($product3->productsno) && $product3->productsno == '11-20') ? "selected=\"selected\"" : "" }}>11-20</option>
                  <option {{ (isset($product3->productsno) && $product3->productsno == '21-30') ? "selected=\"selected\"" : "" }}>21-30</option>
                  <option {{ (isset($product3->productsno) && $product3->productsno == '30+') ? "selected=\"selected\"" : "" }}>30+</option>
-->                
            </select>
            @if ($errors->has('productsno'))
                  <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('productsno') }}</strong>
                  </span>
            @endif
      </div>

      <div class="form-group">
            <label class="label1">How many orders do you want us to process for you everyday?</label>
            <select class="form-control{{ $errors->has('ordersperday') ? ' is-invalid' : '' }}" name="ordersperday" value="" required autofocus>
<option  value="">Select an option</option>

<option value="Less than 50" {{ (isset($product3->ordersperday) && $product3->ordersperday == 'Less than 50') ?  'selected="selected"' : '' }}>Less than 50</option>
<option value="51 - 100" {{ (isset($product3->ordersperday) && $product3->ordersperday == '51 - 100') ?  'selected="selected"' : '' }}>51 - 100</option>
<option value="101 - 200" {{ (isset($product3->ordersperday) && $product3->ordersperday == '101 - 200') ?  'selected="selected"' : '' }}>101 - 200</option>
<option value="201 - 300" {{ (isset($product3->ordersperday) && $product3->ordersperday == '201 - 300') ?  'selected="selected"' : '' }}>201 - 300</option>
<option value="301 - 400" {{ (isset($product3->ordersperday) && $product3->ordersperday == '301 - 400') ?  'selected="selected"' : '' }}>301 - 400</option>
<option value="401 - 500" {{ (isset($product3->ordersperday) && $product3->ordersperday == '401 - 500') ?  'selected="selected"' : '' }}>401 - 500</option>
<option value="Just started" {{ (isset($product3->ordersperday) && $product3->ordersperday == '500+') ?  'selected="selected"' : '' }}>500+</option>

<!--
                  <option {{ (isset($product3->ordersperday) && $product3->ordersperday == 'Less than 50') ? "selected=\"selected\"" : "" }}>Less than 50</option>
                  <option {{ (isset($product3->ordersperday) && $product3->ordersperday == '51 - 100') ? "selected=\"selected\"" : "" }}>51 - 100</option>
                  <option {{ (isset($product3->ordersperday) && $product3->ordersperday == '101 - 200') ? "selected=\"selected\"" : "" }}>101 - 200</option>
                  <option {{ (isset($product3->ordersperday) && $product3->ordersperday == '201 - 300') ? "selected=\"selected\"" : "" }}>201 - 300</option>
                  <option {{ (isset($product3->ordersperday) && $product3->ordersperday == '301 - 400') ? "selected=\"selected\"" : "" }}>301 - 400</option>
                  <option {{ (isset($product3->ordersperday) && $product3->ordersperday == '401 - 500') ? "selected=\"selected\"" : "" }}>401 - 500</option>
                  <option {{ (isset($product3->ordersperday) && $product3->ordersperday == '500+') ? "selected=\"selected\"" : "" }}>500+</option>
-->
            </select>
            @if ($errors->has('ordersperday'))
                  <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('ordersperday') }}</strong>
                  </span>
            @endif
      </div>

      <div class="input-group" style="padding-top: 5px; padding-left: 15px;">
          
          <div class="input-group">
          
                {!! NoCaptcha::renderJs() !!}
                {!! NoCaptcha::display() !!}
                
                
          </div>
          <div class="form-group">
          @if ($errors->has('g-recaptcha-response'))
                  <span class="invalid-feedback" style="display: block;">
                      <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                  </span>
              @endif
      </div>
        
        </div>

        <div class="row kt-login__extra">
            <div class="col kt-align-left">
                  <label class="kt-checkbox">
                        <input type="checkbox" name="agree">I Agree the <a href="#" class="kt-link kt-login__link kt-font-bold">terms and conditions</a>.
                        <span></span>
                  </label>
                  @if ($errors->has('agree'))
                        <span class="invalid-feedback" style="display: block;">
                            <strong>{{ $errors->first('agree') }}</strong>
                        </span>
                    @endif
                  <span class="form-text text-muted"></span>
            </div>
        </div>

      <!-- <div class="kt-form__actions">

            <button type="button" onclick="window.location.href ='{{url('/products/create-step3')}}';" class="btn btn-primary kt-login__btn-primary">Previous</button>
            &nbsp;&nbsp;

            <button type="submit" class="btn btn-primary kt-login__btn-primary">Submit</button>            
      </div> -->

   <!--   <div class="kt-login__actions">
            <button type="button" onclick="window.location.href ='{{url('/step3')}}';" class="btn btn-pill kt-login__btn-primary">
                        {{ __('Previous') }}
                    </button>&nbsp;&nbsp;
            <button type="submit" class="btn btn-pill kt-login__btn-secondary">Submit Application</button>&nbsp;&nbsp;
<button type="button" onclick="window.location.href ='{{url('/')}}';" class="btn btn-pill kt-login__btn-primary">
                        Cancel
                    </button>
      </div>
      -->
<div class="kt-login__actions">
<button type="button" onclick="window.location.href ='{{url('/')}}';" class="btn btn-secondary btn-wide">
                        Cancel
                    </button>&nbsp;&nbsp;
 <button type="button" onclick="window.location.href ='{{url('/step3')}}';" class="btn btn-primary btn-wide">
                        {{ __('Previous') }}
                    </button>&nbsp;&nbsp;
            <button type="submit" class="btn btn-success btn-wide">Submit Application</button>&nbsp;&nbsp;
         
     
        </div> 
        
        <!-- <a type="button" href="{{url('/products/create-step3')}}" class="btn btn-warning">Previous</a>
        <button type="submit" class="btn btn-primary">Submit</button> -->
       
    </form>
    </div>
    </div>
                              </div>
                        </div>
                  </div>
            </div>

            <!-- end:: Page -->

            <!-- begin::Global Config(global config for global JS sciprts) -->
            <script>
                  var KTAppOptions = {
                        "colors": {
                              "state": {
                                    "brand": "#5d78ff",
                                    "dark": "#282a3c",
                                    "light": "#ffffff",
                                    "primary": "#5867dd",
                                    "success": "#34bfa3",
                                    "info": "#36a3f7",
                                    "warning": "#ffb822",
                                    "danger": "#fd3995"
                              },
                              "base": {
                                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                              }
                        }
                  };
            </script>

            <!-- end::Global Config -->

            <!--begin:: Global Mandatory Vendors -->
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/popper.js/dist/umd/popper.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/js-cookie/src/js.cookie.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/moment/min/moment.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/sticky-js/dist/sticky.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/wnumb/wNumb.js')}}" type="text/javascript"></script>

            <!--end:: Global Mandatory Vendors -->

            <!--begin:: Global Optional Vendors -->
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/jquery-form/dist/jquery.form.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/block-ui/jquery.blockUI.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/components/vendors/bootstrap-datepicker/init.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/components/vendors/bootstrap-timepicker/init.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/components/vendors/bootstrap-switch/init.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/select2/dist/js/select2.full.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/ion-rangeslider/js/ion.rangeSlider.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/typeahead.js/dist/typeahead.bundle.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/handlebars/dist/handlebars.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/nouislider/distribute/nouislider.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/owl.carousel/dist/owl.carousel.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/autosize/dist/autosize.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/clipboard/dist/clipboard.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/dropzone/dist/dropzone.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/summernote/dist/summernote.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/markdown/lib/markdown.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/components/vendors/bootstrap-markdown/init.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/bootstrap-notify/bootstrap-notify.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/components/vendors/bootstrap-notify/init.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/jquery-validation/dist/jquery.validate.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/jquery-validation/dist/additional-methods.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/components/vendors/jquery-validation/init.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/toastr/build/toastr.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/raphael/raphael.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/morris.js/morris.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/chart.js/dist/Chart.bundle.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/waypoints/lib/jquery.waypoints.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/counterup/jquery.counterup.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/es6-promise-polyfill/promise.min.js')}}" type="text/javascript"></script>
            <script src="../assets/vendors/general/sweetalert2/dist/sweetalert2.min.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/custom/components/vendors/sweetalert2/init.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/jquery.repeater/src/lib.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/jquery.repeater/src/jquery.input.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/jquery.repeater/src/repeater.js')}}" type="text/javascript"></script>
            <script src="{{asset(config('app.asset_path').'/assets/vendors/general/dompurify/dist/purify.js')}}" type="text/javascript"></script>

            <!--end:: Global Optional Vendors -->

            <!--begin::Global Theme Bundle(used by all pages) -->
            <script src="{{asset(config('app.asset_path').'/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>

            <!--end::Global Theme Bundle -->

            <!--begin::Page Scripts(used by this page) -->
            <script src="{{asset(config('app.asset_path').'/assets/app/custom/login/login-general.js')}}" type="text/javascript"></script>

            <!--end::Page Scripts -->

            <!--begin::Global App Bundle(used by all pages) -->
            <script src="{{asset(config('app.asset_path').'/assets/app/bundle/app.bundle.js')}}" type="text/javascript"></script>

            <!--end::Global App Bundle -->
      </body>

      <!-- end::Body -->
</html>
