@extends('layouts.app')
@section('page_title')
Store | Store Management
@endsection

@section('page_css')
<style>

</style>
@endsection

@section('content')
<!-- begin:: Content Head -->

   <!--  <div class="kt-subheader__main">
        <h3 class="kt-subheader__title"></h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
            <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div> -->
        <!-- <div class="kt-subheader__toolbar ">
            <div class="kt-subheader__wrapper">     
            </div>
        </div> -->

<!-- <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-xl-4">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-widget14">
                    <div class="kt-widget14__header kt-margin-b-30">
                       <h2>Connect a new store </h2>
                            <form class="kt-form">
                                <div class="kt-portlet__body">
                                    <div class="row">
                                    <div class="col-xl-8">
                                        <div class="form-group">
                                            <label>Store name</label>
                                            <input type="text" class="form-control" aria-describedby="emailHelp" placeholder=".myshopify.com" >
                                        </div> 
                                      </div>
                                            <div class="kt-portlet__foot">
                                                <div class="col-xl-4">
                                                    <button type="reset" class="btn btn-success">Submit</button>
                                                </div>     
                                         </div>
                                     </div>
                                </div>
                         </form>
                    </div>
                </div>                    
            </div>
        </div>
    </div>
</div>
 -->
 <div class="container">
          <h2>Connect a new store</h2>
          <form action="{{url('/storemanagement')}}">
                <div class="form-group">
                  <label for="">Store name</label>
                  <input type="text" class="form-control" id="storename" placeholder="Enter Store name" name="storename">
                   </div>
                   <!-- <div class="form-group" >
                    <label for="sel1">Select list (select one):</label>
                      <select class="form-control" id="sel1">
                        <option>Default</option>
                        <option>demo</option>
                        <option>demo2</option>
                      </select>
                  </div> -->
                <div class="kt-portlet__foot">
                    <button type="submit" class="btn btn-primary">Add Store</button>
                </div>
          
          </form>
     </div>
<!-- end:: Content -->
@endsection
@section('page_script')
@endsection