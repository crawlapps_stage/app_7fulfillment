@extends('layouts.app')
@section('page_title')
Store
@endsection
@section('page_css')
<style>
   .size_btn{
   height: 5rem !important;
   width: 12rem !important;
   }
   .plus_icon{
   font-size:2.2rem !important;
   }
   div#all_store table {
   border: 1px solid #fff;
   }
   div#all_store table tr:nth-child(odd) {
   background: #f7f7f7;
   }
   div#all_store h3{
   color:#000;
   margin-bottom: 16px;
   }
   div#all_store {
   padding: 25px;
   }
</style>
@endsection
@section('content')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
   <!--Begin::Dashboard 1-->
   <!--Begin::Section-->
   <div class="row">
      <div class="col-xl-12">
         <!--begin:: Widgets/Daily Sales-->
         <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__head kt-portlet__head--lg">
               <div class="kt-portlet__head-label" style="width: 100%;">
                  <div class="row" style="width: 100%;">
                     <div class="col-xs-12 col-md-9" style="text-align: left;">
                        <div class="kt-subheader__breadcrumbs">
                           <span class="kt-portlet__head-icon">
                           <i class="kt-font-brand flaticon2-line-chart"></i>
                           </span>						   
                           <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
                           Dashboard                    </a>
                           <span class="kt-subheader__breadcrumbs-separator"> - </span>
                           <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                           Store                    </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
           
            <div class="kt-widget14">
               <div class="kt-widget14__header kt-margin-b-30">
                  <h2 id="jsChange">Authorize Shopify Store</h2>
               </div>
               <div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="" data-placement="left" data-original-title="Quick actions">
                  <button type="button" id="addStoreButton" class="btn btn-outline-brand btn-sm" data-toggle="modal" data-target="#kt_modal_2"><i class="fa fa-plus"></i>Add Store</button>
                  <!--<a href="{{url('/storemanagement')}}" class="btn btn-brand btn-icon size_btn"  aria-haspopup="true" aria-expanded="false">-->
                  <!--<i class="flaticon2-plus plus_icon"></i>-->
                  <!--</a>-->
               </div>
               <!--<div class="kt-portlet__body">-->
               <div id="all_store">
               <!--begin::Add Store Modal-->
               <div class="modal fade" id="kt_modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" style="display: none;" name="addStoreModal" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                        <div class="modal-header">
                           <h5 class="modal-title" id="exampleModalLongTitle">Add Store</h5>
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           </button>
                        </div>
                        <div class="modal-body">
                           <form method="post" id ="addStoreForm" action="{{url('/authenticate')}}">
                              {{ csrf_field() }}
                              <div class="form-group">
                                 <label>Store</label>
                                 <input type="text" class="form-control" name="shop" id="store" placeholder="eg - mystore.myshopify.com
                                    ">
                              </div>
                              <!--<div class="form-group">-->
                              <!--    <label>Dropdown</label>-->
                              <!--    <select class="form-control" id="dropdown" name="dropdown">-->
                              <!--        <option value="">Please select a value</option>-->
                              <!--    </select>-->
                              <!--</div>-->
                              <div class="form-group">
                                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 <button type="submit" name="addStore" id="addStore" class="btn btn-primary">Connect Store</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
               <!--end::Add Store Modal-->
              
            </div>
               <!--begin: Search Form -->
               <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                  <div class="row align-items-center">
                     <div class="col-xl-8 order-2 order-xl-1">
                        <div class="row align-items-center">
                           <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                              <div class="kt-input-icon kt-input-icon--left">
                                 <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                 <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                 <span><i class="la la-search"></i></span>
                                 </span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--end: Search Form -->
               
               <div class="kt-portlet__body kt-portlet__body--fit">
                  <!--begin: Datatable -->
                  <div class="my_datatable" id="kt_datatable"></div>
                  <!--end: Datatable -->
               </div>
               <!--</div>-->
            </div>
         </div>
      </div>
      <!--end:: Widgets/Daily Sales-->
   </div>
   <!--End::Section-->
   <!--End::Dashboard 1-->
</div>
<!-- end:: Content -->
@endsection
@section('page_script')

<!--</script>-->
<script>
   var path = APP_URL+'/store/show';
   var datatable = $('.my_datatable').KTDatatable({
   // datasource definition
   data: {
    type: 'remote',
    source: {
      read: {
        url: path,
        method: 'GET',
        map: function(raw) {
          // sample data mapping
          var dataSet = raw;
          if (typeof raw.data !== 'undefined') {
            dataSet = raw.data;
          }
          return dataSet;
        },
      },
    },
    pageSize: 10,
    serverPaging: false,
    serverFiltering: false,
    serverSorting: false,
   },
   
   // layout definition
   layout: {
    scroll: true,
    footer: false,
   },
   
   // column sorting
   sortable: true,
   
   pagination: true,
   
   search: {
    input: $('#generalSearch'),
   },
   
   // columns definition
   columns: [
   /*
    {
      field: 'id',
      title: 'ID',     
      width: 40,      
      textAlign: 'center',
    },
    */ {
      field: 'shopify_domain',
      title: 'Store Url',
      textAlign: 'center',
    },
    {
      field: 'created_at', 
      width: 150,
      title: 'Date',
      textAlign: 'center',
      template: function(row, index, datatable) {
      
   var dateTime = row.created_at;
   var date = dateTime.split(" ");
   var result = date[0];
   var resulted_date =  result.split("-");
   var date_to_show = resulted_date[2]+'-'+resulted_date[1]+'-'+resulted_date[0];    
   return date_to_show;              
      },      
    },
    {
      field: 'actions',
      title: 'Actions', 
      textAlign: 'center', 
      template: function(row, index, datatable) {
        return '<a href="#" data-key="'+row.id+'" class="cstm_delet btn btn-hover-danger btn-icon btn-pill" title="Delete" onclick="('+row.id+')">\
                        <i class="la la-trash"></i>\
                    </a>';
                    
      },
         
    }],
   
   });
   
   jQuery(document).ready(function(){

	jQuery(document).on( "click", ".cstm_delet", function(id) {
	
		var r = confirm("Are you really want to delete!");
		if (r == true) {
		
			var id = $(this).attr("data-key");			
			var path = APP_URL + "/store/delete";  
			$.ajax({
			
				method: "POST",
				url: path,
				data: { 
					id: id
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			})
			.done(function( msg ) {
			
			
				alert( msg );
				datatable.reload();
			});
		} else {
			txt = "You pressed Cancel!";
			alert(txt);
		}	
	});
});
</script>
<script>
   $(document).ready(function() {
       
       //var message = $('#jsChange');
       //message.text('Sample Store');
       
       $('#addStoreButton').click(function(e) {
           $('#store').val('');
           $('#dropdown').val('');
       });
       
       $('#addStore').click(function(e) {
           var store = $('#store').val();
           var dropdown = $('#dropdown').val();
           
           if(store == '') {
               swal.fire("Error!", "Store field is required", "error");
               return false;    
           }
           // if(dropdown == '') {
           //     swal.fire("Error!", "Dropdown field is required", "error");
           //     return false;    
           // }
           
           // $.ajax({
           //     method: "POST",
           //     url: $('#addStoreForm').attr('action'),
           //     headers: {
           //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           //     },
           //     data: { 
           //         store: store,
           //         dropdown: dropdown
           //     }
           // });
           // .done(function( msg ) {
           //     console.log(msg);
           //     //alert( "Data Saved: " + msg );
           //     swal.fire("Success", "Data Saved" + msg.data, "success");
           // })
           // .fail(function( msg ) {
           //     //alert( "Request failed: " + msg );
           //     swal.fire("Request failed!" + msg , "", "error");
           // });
       });
   });
</script>
@endsection