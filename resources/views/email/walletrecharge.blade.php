<!-- Mail send to customer wired transfer-->
@if($mail_send == 'wired')
	<div>
		<h4> Hello {{$customer_name}},</h4>
		<p>{{$mail_body}}</p>
		<p><b> Transaction Details : </b></p>
		<p><b>Payment Method</b>: {{$payment_method}} </p>
		<p><b>Amount Without Fee</b> :$ {{$amount_without_fee}}</p>
		<p><b>Amount With Fee</b> :$ {{$amount_with_fee}}</p>
		<p><b>Transaction ID</b> : {{$txn_id}}</p>
		<p><b>Status</b> : {{$payment_status}}</p>
		<p><b>Bank Name</b> : {{$bank_name}}</p>
		
		<p>Thanks,</p>
		<p>Regards</p>
		<p>7Fullfillment Team</p>

	</div>
@endif

<!-- Mail send to customer paypal-->
@if($mail_send == 'paypal')
	<div>
		<h4> Hello {{$customer_name}},</h4>
		<p>{{$mail_body}}</p>
		<p><b> Transaction Details : </b></p>
		<p><b>Payment Method</b>: {{$payment_method}} </p>
		<p><b>Amount Without Fee</b> : {{$amount_without_fee}}</p>
		<p><b>Amount With Fee</b> : {{$amount_with_fee}}</p>
		<p><b>Transaction ID</b> : {{$txn_id}}</p>
		<p><b>Status</b> : {{$payment_status}}</p>
		
		
		<p>Thanks,</p>
		<p>Regards</p>
		<p>7Fullfillment Team</p>

	</div>
@endif 

<!-- Mail send to customer paypal-->
@if($mail_send == 'stripe')
	<div>
		<h4> Hello {{$customer_name}},</h4>
		<p>{{$mail_body}}</p>
		<p><b> Transaction Details : </b></p>
		<p><b>Payment Method</b>: {{$payment_method}} </p>
		<p><b>Amount Without Fee</b> : ${{$amount_without_fee}}</p>
		<p><b>Amount With Fee</b> : ${{$amount_with_fee}}</p>
		<p><b>Transaction ID</b> : {{$txn_id}}</p>
		<p><b>Status</b> : {{$payment_status}}</p>
		
		
		<p>Thanks,</p>
		<p>Regards</p>
		<p>7Fullfillment Team</p>

	</div>
@endif 


<!-- Mail send to admin -->
@if($mail_send == 'admin')
	<div>
		<h4> Hello Admin,</h4>
		<p>{{$mail_body}}</p>
		<p></b> Transaction Details : </b></p>
		<p><b>Payment Method</b>: {{$payment_method}} </p>
		<p><b>Amount Without Fee</b> : ${{$amount_without_fee}}</p>
		<p><b>Amount With Fee</b> : ${{$amount_with_fee}}</p>
		<p><b>Transaction ID</b> : {{$txn_id}}</p>
		<p><b>Status</b> : {{$payment_status}}</p>
		@if($bank_name != '')
			<p><b>Bank Name</b> : {{$bank_name}}</p>
		@endif
		<p>Thanks,</p>
		<p>Regards</p>
		<p>7Fullfillment Team</p>

	</div>
@endif

<!-- Mail send to customer payoneer transfer-->
@if($mail_send == 'payoneer')
	<div>
		<h4> Hello {{$customer_name}},</h4>
		<p>{{$mail_body}}</p>
		<p><b> Transaction Details : </b></p>
		<p><b>Payment Method</b>: {{$payment_method}} </p>
		<p><b>Amount Without Fee</b> :$ {{$amount_without_fee}}</p>
		<p><b>Amount With Fee</b> :$ {{$amount_with_fee}}</p>
		<p><b>Transaction ID</b> : {{$txn_id}}</p>
		<p><b>Status</b> : {{$payment_status}}</p>
		
		<p>Thanks,</p>
		<p>Regards</p>
		<p>7Fullfillment Team</p>

	</div>
@endif
<!-- Mail send to customer paypal-->

