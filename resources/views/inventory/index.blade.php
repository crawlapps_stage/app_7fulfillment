@extends('layouts.app')
@section('page_title')
Inventory
@endsection
@section('page_css')
@endsection
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-xl-12">               
            <div class="kt-portlet kt-portlet--height-fluid">
            
            <div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Inventory                    </span>
						   <!--
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Wired Transfer                   </a>-->
						   <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						</div>
					</div>
				</div>
			</div>
		</div>				
		<div class="kt-portlet__body">		
		    <!--begin: Search Form -->
		    <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
		        <div class="row align-items-center">
		            <div class="col-xl-8 order-2 order-xl-1">
		                <div class="row align-items-center">
		                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
		                        <div class="kt-input-icon kt-input-icon--left">
		                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
		                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
									<span><i class="la la-search"></i></span>
		                            </span>
		                        </div>
		                    </div>                    
		                </div>
		            </div>            
		        </div>
		    </div>
		    <!--end: Search Form -->
		</div>

               <div class="kt-portlet__body kt-portlet__body--fit">
               
                <!--begin: Datatable -->
                <div class="inventory_datatable" id="kt_datatable"></div>
                <!--end: Datatable -->
             </div>
             
             <div class="kt-portlet__body kt-portlet__body--fit">
               
                <!--begin: Datatable -->
                <div class="inventory_activity_datatable" id="kt1_datatable"></div>
                <!--end: Datatable -->
             </div>
			     </div>
        </div>       
    </div>
</div>
@endsection
@section('page_script')

<script>
 
 var path = APP_URL+'/inventory/show';
   
   var datatable = $('.inventory_datatable').KTDatatable({
  // datasource definition
  data: {
    type: 'remote',
    source: {
      read: {
        url: path,
        method: 'GET',
        map: function(raw) {
          // sample data mapping
          var dataSet = raw;
          if (typeof raw.data !== 'undefined') {
            dataSet = raw.data;
          }
          return dataSet;
        },
      },
    },
    pageSize: 10,
    serverPaging: false,
    serverFiltering: false,
    serverSorting: false,
  },

  // layout definition
  layout: {
    scroll: false,
    footer: false,
  },

  // column sorting
  sortable: true,

  pagination: true,

  search: {
    input: $('#generalSearch'),
  },

  // columns definition
  columns: [
  /*
   {
      field: 'S_No',
      title: '#',     
      width: 45,      
      textAlign: 'center',
       template: function(row, index, datatable) {
           
	return '#';              
      },      
    },
    */
    {
      field: 'product_name',
      title: 'Product Name',     
      width: 150,      
      textAlign: 'center',      
    },
    {
      field: 'price',
      width: 150,
      title: 'Price',
      textAlign: 'center',
      template: function(row, index, datatable) {
      
        const formatter = new Intl.NumberFormat('en-US', {
	  
	        minimumFractionDigits: 2
	    })
	
	    var price =formatter.format(row.price);
	    return price;              
      },
    }, {
      field: 'quantity',
      width: 150,
      title: 'Quantity',
      textAlign: 'center',
    },
    {
      field: 'weight',
      width: 150,
      title: 'Weight (gm)',
      textAlign: 'center',
      template: function(row, index, datatable) {
      
        const formatter = new Intl.NumberFormat('en-US', {
	  
	        minimumFractionDigits: 2
	    })
	
	    var weight =formatter.format(row.weight);
	    return weight;              
      },
    },
    {
      field: 'sku',
      width: 150,
      title: 'SKU',
      textAlign: 'center',
    },
    {
      field: 'name',
      width: 150,
      title: 'Shipping Provider',
      textAlign: 'center',
      template : function(row, index, datatable){
          if(row.name != '') {
              return row.name;
              
          } else {
                return '-'; 
          }   
      },
    }],

});
</script>

<script>
 
 var path = APP_URL+'/inventory/activity';
   
   var datatable = $('.inventory_activity_datatable').KTDatatable({
  // datasource definition
  data: {
    type: 'remote',
    source: {
      read: {
        url: path,
        method: 'GET',
        map: function(raw) {
          // sample data mapping
          var dataSet = raw;
          if (typeof raw.data !== 'undefined') {
            dataSet = raw.data;
          }
          return dataSet;
        },
      },
    },
    pageSize: 10,
    serverPaging: false,
    serverFiltering: false,
    serverSorting: false,
  },

  // layout definition
  layout: {
    scroll: false,
    footer: false,
  },

  // column sorting
  sortable: true,

  pagination: true,

  search: {
    input: $('#generalSearch'),
  },

  // columns definition
  columns: [
  /*
   {
      field: 'S_No',
      title: '#',     
      width: 45,      
      textAlign: 'center',
       template: function(row, index, datatable) {
           
	return '#';              
      },      
    },
    */
    {
      field: 'product_name',
      title: 'Product Name',     
      width: 150,      
      textAlign: 'center',      
    }, 
    {
      field: 'activity',
      width: 260,
      title: 'Activity',
      textAlign: 'center',
    }, 
    {
      field: 'created_at',
      width: 150,
      type : 'Date',
      title: 'Date',
      textAlign: 'center',
      template: function(row, index, datatable) {
      
	var dateTime = row.created_at;
	var date = dateTime.split(" ");
	var result = date[0];
	var resulted_date =  result.split("-");
	var date_to_show = resulted_date[2]+'-'+resulted_date[1]+'-'+resulted_date[0];    
	return date_to_show;              
      },
    }],

});
</script>
@endsection