@extends('layouts.app')
@section('page_title')
7fulfillment | Change Password
@endsection

@section('page_css')
<style>

</style>
@endsection

@section('content')
<!-- begin:: Content Head -->
<!-- <div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">Change Password</h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>
    <div class="kt-subheader__toolbar">
        <div class="kt-subheader__wrapper">     
        </div>
    </div>
</div> -->
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Change Password                    </span>
						   <!--
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Wired Transfer                   </a>-->
						   <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						</div>
					</div>				  
				</div>
			</div>
		</div>
		<!--
            	<div class="kt-portlet__head kt-portlet__head--lg">
		    <div class="kt-portlet__head-label" style="width: 100%;">
		         <span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>
		         <div class="row" style="width: 100%;">
				  <div class="col-xs-12 col-md-9" style="text-align: left;">
					  <h3 class="kt-portlet__head-title" style="margin-top:6px;">
						Change Password
					 </h3>
				  </div>
				  
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick="window.location.href ='http://7fulfillment.com/portal/wallet';" class="btn btn-outline-danger" style="width:100%;"> Back</button>
					</div>
					
				</div>
		    </div>
		</div>
		-->
                <div class="kt-widget14">
                    <div class="kt-widget14__header kt-margin-b-30">
                        <!-- BEGIN PROFILE CONTENT -->
                        @if (session('status')=='Password updated!')
			    <div class="alert alert-success fade show" role="alert">
  
				  <div class="alert-text">{{ session('status') }}</div>
				  <div class="alert-close">
				      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				          <span aria-hidden="true"><i class="la la-close"></i></span>
				      </button>
				  </div>
				</div>
				
				
			
			@elseif (session('status') !='')
			    <div class="alert alert-danger fade show" role="alert">
				  
				  <div class="alert-text">{{ session('status') }}</div>
				  <div class="alert-close">
				      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				          <span aria-hidden="true"><i class="la la-close"></i></span>
				      </button>
				  </div>
				</div>
			@endif
			
                        <form role="form" method="POST" action="{{url('store/changePassword')}}" name="storechangePassword" id="storechangePassword">
                        	@csrf
                            <input type="hidden" name="opassword" id="opassword"  value="{{Auth::user()->password}}" class="form-control"> 
                            
                            <div class="form-group">
                                <label class="control-label">Current Password</label>
                                <input type="password" placeholder="Current Password" name="cpassword" id="cpassword"  value="" class="form-control"> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">New Password</label>
                                <input type="password" placeholder="New Password" name="npassword" id="npassword"  value="" class="form-control"> 
                            </div>
                              <div class="form-group">
                                <label class="control-label">Confirm New Password</label>
                                <input type="password" placeholder="Confirm New Password" name="confirmpassword" id="confirmpassword"  value="" class="form-control"> 
                            </div>                       
                             <div class="row">
							<div class="col-xs-12 col-md-4" >
                            	<button style="width:100%;" type="submit" class="btn btn-primary" name="addchangePassword" id="addchangePassword">Save Changes</button>
							</div>
							<div class="col-xs-12 col-md-4" >
                            	<button style="width:100%;" type="button" onclick="window.location.href ='{{url('/profile')}}';" class="btn btn-outline-danger">Cancel
                            	</button>
							</div>
                            </div>
	
                        </form>    
                        <!-- END PROFILE CONTENT -->
                    </div>
                </div>                    
            </div>
        </div>
        <!--end:: Widgets/Daily Sales-->
    </div>
    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
@endsection