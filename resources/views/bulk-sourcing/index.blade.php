@extends('layouts.app')
@section('page_title')
Bulk Sourcing
@endsection
@section('page_css')
@endsection
@section('content')
<!-- begin:: Content Head -->
<!-- <div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title"></h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
            <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                <span><i class="flaticon2-search-1"></i></span>
            </span>
        </div>
    </div>
    <div class="kt-subheader__toolbar ">
        <div class="kt-subheader__wrapper">     
        </div>
    </div>
</div> -->
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            	<div class="kt-portlet__head kt-portlet__head--lg">
        			<div class="kt-portlet__head-label" style="width: 100%;">
        				<div class="row" style="width: 100%;">
        					<div class="col-xs-12 col-md-9" style="text-align: left;">
        						<div class="kt-subheader__breadcrumbs">
        							<span class="kt-portlet__head-icon">
                                        <i class="kt-font-brand flaticon2-line-chart"></i>
                                    </span>						   
                                    <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
                                        Dashboard
                                    </a>
        						    <span class="kt-subheader__breadcrumbs-separator">
        						        - 
        						    </span>
        						    <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
        						        Bulk Sourcing
        						    </span>
        						  
        						</div>
        					</div>				  
        				</div>
        				
                        <div class="col-xs-12 col-md-3" style="text-align: right;">
                            <button type="button" onclick="window.location.href =APP_URL+'/create-quote';" class="btn btn-outline-danger" style="width:100%;"> Request Quote</button>
                        </div>
                        
        			</div>
        		</div>
        		<div class="kt-portlet__body">
                	<!--begin: Search Form -->
                	<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                        <div class="row align-items-center">
                        	<div class="col-xl-8 order-2 order-xl-1">
                        		<div class="row align-items-center">				
                        			<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                        				<div class="kt-input-icon kt-input-icon--left">
                        					<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                        					<span class="kt-input-icon__icon kt-input-icon__icon--left">
                        						<span><i class="la la-search"></i></span>
                        					</span>
                        				</div>
                        			</div>
                                </div>
                        	</div>
                        </div>
                    </div>
                    <!--end: Search Form -->   
	            </div>
	            <div class="kt-portlet__body kt-portlet__body--fit">
                    <!--begin: Datatable -->
                    <div class="photography_datatable" id="kt_datatable"></div>
                    <!--end: Datatable -->
                </div>
		        
                <div class="kt-widget14">
    			    <div class='row'>
    		        </div>
                </div> 
                <div class="kt-widget14">
                    @if (session('status')=='Sucess')
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @elseif (session('status') !='')
                    <div class="alert alert-danger">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div class="container">                    
                        <div class="kt-widget14">
                            <div class="kt-widget14">                  
                            </div>
                        </div>                     
                    </div>
            	</div>
                <!--end:: Widgets/Daily Sales-->
            </div>
            <!--End::Section-->
            <!--End::Dashboard 1-->
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection
@section('page_script')
<script>

/*********** This code is working **/
	jQuery(document).ready(function(){
		jQuery(document).on( "click", 'td[data-field="q_link"] span a', function() {	
			window.open(jQuery(this).attr('href'));
			return false;	
		});
	});
/************************************/
</script>
<script>

	    var path = APP_URL+'/bulk-sourcing/show';

		var datatable = $('.photography_datatable').KTDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: {
					read: {
						url: path,
						method: 'GET',
						// sample custom headers
						//headers: {'X-CSRF-TOKEN': 'some value', 'x-test-header': 'the value'},
						map: function(raw) {
							// sample data mapping
							var dataSet = raw;
							console.log(dataSet);
							if (typeof raw.data !== 'undefined') {
								dataSet = raw.data;
							}
							return dataSet;
						},
					},
				},
				pageSize: 10,
				serverPaging: false,
				serverFiltering: false,
				serverSorting: false,
			},

			// layout definition
			layout: {
				scroll: false,
				footer: false,
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch'),
			},

			// columns definition
			columns: [
				// {
				// 	field: 'id',
				// 	title: 'ID',
				// 	width: 80,
				// 	type: 'number',
				// 	textAlign: 'center',
				// }, 
				{
					field: 'q_link',
					title: 'Link',
					width: 180,
					textAlign: 'center',
					 template: function(row, index, datatable) {
        return '<a href="'+row.q_link+'" target="_blank">'+row.q_link+'</a>';                
      },
				}, {
					field: 'pieces',
					title: 'Pieces',
					width: 180,
					textAlign: 'center',
				},  {
					field: 'shipping_location',
					title: 'Shipping Location',
					width: 200,
					textAlign: 'center',
				},  {
					field: 'time_frame',
					title: 'Time Frame',
					width: 200,
					textAlign: 'center',
				},
				// {
				// 	field: 'status',
				// 	title: 'Status',
				// 	width: 180,
				// 	textAlign: 'center',
				// 	template: function(row) {
				// 	    var status = row.status;
				// 	    if(status == 0) {
				// 	        return '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--danger">Pending</span>';
				// 	    } else {
				// 	        return '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--success">Approved</span>';
				// 	    }	
				// 	},
				// },
				
				 {
					field: 'quote',
					title: 'Quote ($)',
					width: 200,
					textAlign: 'center',
					template: function(row) {
					    var status = row.status;
					    if(status == 0) {
					        return 'Waiting for admin..';
					    } else {
					        const formatter = new Intl.NumberFormat('en-US', {
	  
                        	  minimumFractionDigits: 2
                        	})
                        	
                        	var quote =formatter.format(row.quote);
					        return quote;
					    }	
					},
				},
				{
                  field: 'id',
                  title: 'Action',
                  textAlign: 'center',
                  
                  template: function(row, index, datatable) {
                      var status = row.status;
                      var quote_approve = row.quote_approve;
					    if(status == 1 && quote_approve == 0) {
					        
					        return '</a>\
                    <a href="#" data-key="'+row.id+'" class="cstm_approve btn btn-success background_gradient btn-view" title="Approve Quote" onclick="('+row.id+')">\
                       Approve\
                    </a>';
                            
        				} 
        				else if(status == 1 && quote_approve == 1) {
            				    return '</a>\
                        <a href="#" data-key="'+row.id+'" class="btn btn-success background_gradient btn-view" title="Quote Approved" onclick="('+row.id+')">\
                           Approved\
                        </a>';
        				}
                               
                  },
                }
				
				,{
					field: 'created_at',
					title: 'Date',
					type: 'date',
					format: 'MM/DD/YYYY',
					template: function(row, index, datatable) {
      
                    	var dateTime = row.created_at;
                    	var date = dateTime.split(" ");
                    	var result = date[0];
                    	var resulted_date =  result.split("-");
                    	var date_to_show = resulted_date[2]+'-'+resulted_date[1]+'-'+resulted_date[0];    
                    	return date_to_show;              
                    },
				 }
				//, {
				// 	field: 'Actions',
				// 	title: 'Actions',
				// 	sortable: false,
				// 	width: 110,
				// 	overflow: 'visible',
				// 	autoHide: false,
				// 	template: function() {
				// 		return '\
				// 		<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Edit details">\
				// 			<i class="flaticon2-paper"></i>\
				// 		</a>\
				// 		<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Delete">\
				// 			<i class="flaticon2-trash"></i>\
				// 		</a>\
				// 	';
				// 	},
				// }
				],

		});
		
		
		jQuery(document).ready(function(){

	jQuery(document).on( "click", ".cstm_approve", function(id) {
	
		var r = confirm("Do you want to approve this quote");
		if (r == true) {
		
			var id = $(this).attr("data-key");
						
			var path = APP_URL + "/bulk-sourcing/approve-quote";  
			$.ajax({
			
				method: "POST",
				url: path,
				data: { 
					id: id
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			})
			.done(function( msg ) {
			
			
				alert( msg );
				datatable.reload();
			});
		} else {
			txt = "You pressed Cancel!";
			alert(txt);
		}	
	});
});

    // $('#kt_form_status').on('change', function() {
    //   datatable.search($(this).val().toLowerCase(), 'Status');
    // });

    // $('#kt_form_type').on('change', function() {
    //   datatable.search($(this).val().toLowerCase(), 'Type');
    // });

    // $('#kt_form_status,#kt_form_type').selectpicker();

	

</script>
@endsection