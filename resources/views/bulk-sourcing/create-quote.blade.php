@extends('layouts.app')
@section('page_title')
Request Quote
@endsection

@section('page_css')
<style>

</style>
@endsection
@section('content')

<!-- begin:: Content Head -->

<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 1-->
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Daily Sales-->
            <div class="kt-portlet kt-portlet--height-fluid">
            
            	<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label" style="width: 100%;">
				
				<div class="row" style="width: 100%;">
					<div class="col-xs-12 col-md-9" style="text-align: left;">
						<div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>						   
						   <a href="{{url('/admin/home')}}" class="kt-subheader__breadcrumbs-link">
						   Dashboard                    </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
<a href="{{url('/bulk-sourcing')}}" class="kt-subheader__breadcrumbs-link">
						   bulk-sourcing                   </a>
						   <span class="kt-subheader__breadcrumbs-separator"> - </span>
						   <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						    Request Quote                    </span>						   						   
						</div>
					
					</div>	
					<div class="col-xs-12 col-md-3" style="text-align: right;">
						<button type="button" onclick='window.location.href ="{{url('/bulk-sourcing')}}";' class="btn btn-outline-danger" style="width:100%;"> Back </button>
					</div>			  
				</div>
			</div>
		</div>
		  
            <div class="kt-widget14">
	            @if (session('status') != '')
					<div class="alert alert-success">
						{{ session('status') }}
		 			</div>
				@elseif (session('status-error') !='')
					<div class="alert alert-danger">
						{{ session('status-error') }}
					</div>
				@endif	
			
	            <div class="kt-widget14__header kt-margin-b-30"> 
		   			<form role="form" method="POST" action="{{url('/create-quote/store')}}" name="storeProfile" id="storeProfile">
		        		@csrf
		        			        	    
			            <div class="form-group">
			                <label class="control-label">Link</label>
			                <input type="text" placeholder="Link" name="q_link" id="q_link" value="" class="form-control" required> 
			                @if ($errors->has('q_link'))
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('q_link') }}</strong>
                                </span>
                            @endif
			            </div>
			            <div class="form-group">
			                <label class="control-label">Pieces</label>
			                <input type="text" placeholder="Pieces" name="pieces" id="pieces" value="" class="form-control" required>
			                @if ($errors->has('pieces'))
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('pieces') }}</strong>
                                </span>
                            @endif
			            </div>
			            <div class="form-group">
			                <label class="control-label">Where to ship</label>
			                <input type="text" placeholder="Where to ship" name="shipping_location" id="shipping_location" value="" class="form-control" required>
			                @if ($errors->has('shipping_location'))
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('shipping_location') }}</strong>
                                </span>
                            @endif
			            </div>
			            <div class="form-group">
                            <label class="label1">Time Frame</label>
                            <select class="form-control{{ $errors->has('time_frame') ? ' is-invalid' : '' }}" name="time_frame" value="" required autofocus>
                                <option  value="">Select an option</option>
                                
                                <option value="1 week" {{ (isset($product3->storeruntime) && $product3->storeruntime == 'Just started') ?  'selected="selected"' : '' }}>1 week</option>
                                <option value="2 weeks" {{ (isset($product3->storeruntime) && $product3->storeruntime == 'Less a year') ?  'selected="selected"' : '' }}>2 weeks</option>
                                <option value="3 weeks" {{ (isset($product3->storeruntime) && $product3->storeruntime == '1 - 3 years') ?  'selected="selected"' : '' }}>3 weeks</option>
                                <option value="4 weeks" {{ (isset($product3->storeruntime) && $product3->storeruntime == '4 - 10 years') ?  'selected="selected"' : '' }}>4 weeks</option>
                            </select>
                            @if ($errors->has('time_frame'))
                                  <span class="invalid-feedback" style="display: block;">
                                  <strong>{{ $errors->first('time_frame') }}</strong>
                                  </span>
                            @endif
                        </div>
			            <!--<div class="form-group">-->
	              <!--          	Note : Fee for 20 photographs is $100 which will be deducted along with the product cost on making the photography request.-->
	                        	
	              <!--      </div>-->

<div class="row">
							<div class="col-md-3" >
	                        	<button style="width:100%;" type="submit" class="btn btn-primary" name="addquote" id="addquote">Send Request</button>
							</div>
	                    	<div  class="col-md-3" style="text-align: left;">
								<button style="width:100%;" type="button" onClick="window.location.href ='{{url('/bulk-sourcing')}}';" class="btn btn-outline-danger">Cancel</button>
		                 	</div>
	                     </div>
			            
			         </form>                
				</div>
        	</div>
        <!--end:: Widgets/Daily Sales-->
    </div>

    <!--End::Section-->
    
    <!--End::Dashboard 1-->
</div>
</div>
</div>

<!-- end:: Content -->
@endsection
@section('page_script')
<script>
	$('#uemail').select2({
 		 placeholder: 'Select User'
	});
	jQuery(document).ready(function(){
		//keypress input propertychange paste change keyup keydown
		jQuery('#shipping_cost, #handling_fee, #pro_cost').on('change blur focus focusout keypress keyup keydown input', function() {
			
			var flag 			=   true;
			var thisval 	    =   jQuery(this).val();

			if(!jQuery.isNumeric(thisval)){	
				jQuery(this).val('');
				flag = false;
			}
		});

		jQuery(document).on("change", ".wrap_total", function() {
    		var sum = 0;
    		jQuery(".wrap_total").each(function(){
        		sum += +jQuery(this).val();
    		});

			var html 		= 	'<label class="control-label">Total Cost :  $'+ sum +'</label>';
			jQuery('#inv_total_cost').html(html);
			jQuery('#inv_total_cost').css('display','block');	
		});
	});

</script>
@endsection