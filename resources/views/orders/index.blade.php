@extends('layouts.app')
@section('page_title')
    Orders
@endsection
@section('page_css')
@endsection
@section('content')
    <!-- begin:: Content Head -->
    <!-- <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title"></h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
            </div>
        </div>
        <div class="kt-subheader__toolbar ">
            <div class="kt-subheader__wrapper">
            </div>
        </div>
    </div> -->
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <!--Begin::Dashboard 1-->
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin:: Widgets/Daily Sales-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label" style="width: 100%;">

                            <div class="row" style="width: 100%;">
                                <div class="col-xs-12 col-md-9" style="text-align: left;">
                                    <div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>
                                        <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
                                            Dashboard                    </a>
                                        <span class="kt-subheader__breadcrumbs-separator"> - </span>
                                        <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Orders                    </span>
                                        <!--
                                        <span class="kt-subheader__breadcrumbs-separator"> - </span>
                                        <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                                        Wired Transfer                   </a>-->
                                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="kt-widget14">
                        <div class="">
                            <!--begin: Search Form -->
                            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                                <div class="row align-items-center">
                                    <div class="col-xl-8 order-2 order-xl-1">
                                        <div class="row align-items-center">
                                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                                <div class="kt-input-icon kt-input-icon--left">
                                                    <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
									<span><i class="la la-search"></i></span>
		                            </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Search Form -->
                        </div>

                        <!--begin: Datatable -->
                        <div class="com_tran_datatable" id="Completed_datatable"></div>
                        <!--end: Datatable -->
                    </div>
                    <div class="kt-widget14">
                        @if (session('status')=='Sucess')
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @elseif (session('status') !='')
                            <div class="alert alert-danger">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="container">
                            <div class="kt-widget14">
                                <div class="kt-widget14">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end:: Widgets/Daily Sales-->
                </div>

                <!--End::Section-->

                <!--End::Dashboard 1-->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
@endsection
@section('page_script')
    <script>
        var path = APP_URL+'/orders/show';
        var datatable = $('.com_tran_datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: path,
                        method: 'GET',
                        map: function(raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false,
            },

            // layout definition
            layout: {
                scroll: false,
                footer: false,
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
                input: $('#generalSearch'),
            },

            // columns definition
            columns: [

                {
                    field: 'store_name',
                    title: 'Store Name',
                    width: 150,
                    textAlign: 'center',

                },

                {
                    field: 'order_number',
                    title: 'Shopify Order No<br>[Payment satus]',
                    width: 180,
                    textAlign: 'center',
                    template: function(row, index, datatable) {
                        var result = row.order_number+'<br> ['+ row.payment_status +']';
                        return result;
                    },
                },
                {
                    field: 'no_of_items',
                    title: 'No of Items',
                    width: 150,
                    textAlign: 'center',
                },
                {
                    field: 'buyer',
                    title: 'Buyer [Country]',
                    width: 180,
                    textAlign: 'center',
                },
                {
                    field: 'order_time',
                    title: 'Order Date',
                    width: 150,
                    textAlign: 'center',
                    template: function(row, index, datatable) {
      
                        var dateTime = row.order_time;
                        var date = dateTime.split(" ");
                        var result = date[0];
                        var resulted_date =  result.split("-");
                        var date_to_show = resulted_date[2]+'-'+resulted_date[1]+'-'+resulted_date[0];    
                        return date_to_show;              
                    },
                },
                {
                    field: 'shipping_status',
                    title: 'Shipping Status',
                    width: 150,
                    textAlign: 'center',
                    template: function(row, index, datatable) {
      
                        var shipping_status = row.shipping_status;
                        var shipping_status_c = '';
                        
                        if(shipping_status == null || shipping_status == ''){
                            shipping_status_c = '-';
                        }else{
                            shipping_status_c = '<span style="color:green;font-weight:600;">'+shipping_status+'</span>';
                        }
                        return shipping_status_c;              
                    },
                },
                {
                    field: 'id',
                    title: 'Actions',
                    textAlign: 'center',
                    template: function(row, index, datatable) {

                        return '<a href="{{route('orders.details')}}/'+row.id+'" data-key="'+row.id+'" class="cstm_edit btn btn-label-primary btn-pill" title="View in detail">\
                        View\
                    </a>';
                    },

                }

            ],

        });
    </script>
@endsection
