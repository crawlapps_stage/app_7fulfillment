@extends('layouts.app')
@section('page_title')
    Order Details
@endsection
@section('page_css')
<style>
.kt-datatable__table{
    min-height:auto !important;
}
.remove_padding{
    padding-top:0px;
}
</style>
@endsection
@section('content')

    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <!--Begin::Dashboard 1-->
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin:: Widgets/Daily Sales-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label" style="width: 100%;">

                            <div class="row" style="width: 100%;">
                                <div class="col-xs-12 col-md-9" style="text-align: left;">
                                    <div class="kt-subheader__breadcrumbs">
							<span class="kt-portlet__head-icon">
		         <i class="kt-font-brand flaticon2-line-chart"></i>
		         </span>
                                        <a href="{{url('/home')}}" class="kt-subheader__breadcrumbs-link">
                                            Dashboard                    </a>
                                        <span class="kt-subheader__breadcrumbs-separator"> - </span>
                                        <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
						   Orders                    </span>

                                        <!--
                                        <span class="kt-subheader__breadcrumbs-separator"> - </span>
                                        <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                                        Wired Transfer                   </a>-->
                                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="kt-widget14">
                    {{--<div class="">
                    <!--begin: Search Form -->
                    <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="row align-items-center">
                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end: Search Form -->
                </div>--}}
                    <div class="">
                         @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @elseif (session('status-error') !='')
                            <div class="alert alert-danger">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>
                    <!--begin: Datatable -->
                        <div class="com_tran_datatable" id="Completed_datatable"></div>
                        <!--end: Datatable -->
                    </div>
                    <div class="kt-widget14 remove_padding">
                       

                        <div class="">
                            <div class="kt-widget14 remove_padding">
                                <div class="kt-widget14 remove_padding">
                                    <div class="final_report d-none">
                                        <div class="">
                                            <h3><label class="control-label" style="font-weight:560;">No. Of Parcel : </label>
                                                <span id="num_parcel"></span></h3>
                                        </div>
                                        <div class="num_parcel">
                                            <h3><label class="control-label" style="font-weight:560;">Shipping Cost : </label>
                                                <span id="total_shipping"></span></h3>
                                        </div>
                                        <div class="num_parcel">
                                            <h3><label class="control-label" style="font-weight:560;">Product Fee : </label>
                                                <span id="product_fee"></span></h3>
                                        </div>
                                        <div class="">
                                            <h3><label class="control-label" style="font-weight:560;">Handling Fee : </label>
                                                <span id="handling_fee"></span></h3>
                                        </div>
                                        <div class="">
                                            <h3><label class="control-label" style="font-weight:560;">Total Cost : <span id="total_cost"></span></label></h3>
                                        </div>
                                        <div class="pay_shipping_section">
                                            <?php $order_id = $id; ?>
                                            
                                            <form role="form" method="post" action="{{url('/order-details/'.$id.'/payShipping')}}">
                                             @csrf
                                               <!-- <input type="hidden" name="num_parcel_vgs" value="">
                                                <input type="hidden" name="total_shipping_vgs" value="">
                                                <input type="hidden" name="product_fee_vgs" value="">
                                                <input type="hidden" name="handling_fee_vgs" value="">
                                                <input type="hidden" name="total_cost_vgs" value="">
                                                <input type="hidden" name="order_id" value="<?php //echo $order_id;?>">
                                                -->
                                                <input type="submit" class="btn btn-primary" name="paynow" id="paynow" value = "Pay Now">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end:: Widgets/Daily Sales-->
                </div>

                <!--End::Section-->

                <!--End::Dashboard 1-->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
@endsection
@section('page_script')
    <script>
        var no_of_percel = 0;
        var total_shipping = 0;
        var product_fee = 0;
        var handling_fee = 0;
        var total_cost = 0;
        var path = APP_URL+'/order-details/{{$id}}/show';
        var datatable = $('.com_tran_datatable').KTDatatable({

            data: {
                type: 'remote',
                source: {
                    read: {
                        url: path,
                        method: 'GET',
                        map: function(raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                                $(".final_report").removeClass('d-none');
                                $("#num_parcel").text(raw.num_parcel);
                                $("#product_fee").text(raw.product_fee);
                                $("#total_shipping").text(raw.total_shipping);
                                $("#handling_fee").text(raw.handling_fee);
                                $("#total_cost").text(raw.total_cost);

                                 var button_status;
                                 button_status = raw.button_hide;
                                 if(button_status == 'yes'){
                                    var pay_status = '';
                                    pay_status = '<span class="shipping_paid btn btn-primary">Paid</span>';
                                    jQuery('.pay_shipping_section').html(pay_status);
                                 }
                                //vgs shipping paid
                                /*$('input[name="num_parcel_vgs"]').val(raw.num_parcel);
                                $('input[name="total_shipping_vgs"]').val(raw.total_shipping);
                                $('input[name="product_fee_vgs"]').val(raw.product_fee);
                                $('input[name="handling_fee_vgs"]').val(raw.handling_fee);
                                $('input[name="total_cost_vgs"]').val(raw.total_cost);*/
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false,
            },


            // layout definition
            layout: {
                scroll: false,
                footer: false,
            },


            // column sorting
            sortable: true,

            pagination: false,


            search: false,

            // columns definition
            columns: [
                {
                    field: 'sku',
                    title: 'SKU',
                    width: 150,
                    textAlign: 'center',


                },
                {
                    field: 'product_name',
                    title: 'Product name',
                    width: 180,
                    textAlign: 'center',
                },
                {
                    field: 'product_price',
                    title: 'Product Price',
                    width: 150,
                    textAlign: 'center',
                },
                {
                    field: 'qty',
                    title: 'Quantity',
                    width: 150,
                    textAlign: 'center',
                },
                {
                    field: 'weight',
                    title: 'Weight(gm)',
                    width: 150,
                    textAlign: 'center',

                },
                {
                    field: 'shipping_provider',
                    title: 'Shipping provider',
                    width: 150,
                    textAlign: 'center',
                },
            ],
        });
    </script>
@endsection
