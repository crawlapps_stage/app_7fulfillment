<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/webhooks/', 'WebHookController@get');
Route::get('/webhooks/delete', 'WebHookController@destroy');


Route::get('/', function () {
    if(Auth::check())
    {
        if(Auth::user()->role == 0) 
        {
            return redirect('/admin/home');
        }
        else if(Auth::user()->role == 2) 
        {    
            return redirect('/subadmin/home');
        }
        else if(Auth::user()->role == 1) 
        {
            return redirect('/home');
        }
        
        else 
        {
            return view('login');
        }
    } 
    else 
    {
        return view('login'); 
    }
})->name('loginurl');

//Route::get('/', 'HomeController@login')->name('loginurl');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/******************************** Mail ******************************************/

/* this route works after the link is clicked in mail to verify the user*/
Route::get('/user/verify/{email}', 'Auth\RegisterController@verifyUser')->name('verifyUser');
/************************************************************************/

Route::get('/verifyEmailFirst','\App\Http\Controllers\Auth\RegisterController@verifyEmailFirst')->name('verifyEmailFirst');
Route::get('verify/{email}/verifyToken','\App\Http\Controllers\Auth\RegisterController@sendEmailDone')->name('sendEmailDone');
Route::get('sendbasicemail','MailController@basic_email')->name('basic_email');
Route::get('sendhtmlemail','\App\Http\Controllers\Auth\RegisterController@html_email')->name('sendhtmlemail');
Route::get('verify','MailController@verifyAccount')->name('verifyAccount');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('customlogout');

/******************************** User Profile ***********************************/
Route::get('/profile', 'UserController@index')->name('userProfile');
Route::get('store/profile', 'UserController@storeProfile')->name('userProfile.getStoreProfile');
Route::post('store/profile', 'UserController@storeProfile')->name('userProfile.storeProfile');
Route::get('/changePassword', 'UserController@changePasswordView')->name('userProfile.changePasswordView');
Route::post('store/changePassword', 'UserController@storechangePassword')->name('userProfile.storechangePassword');

/******************************** Store Management *******************************/
Route::get('/store', 'StoreController@index')->name('user.store.index');
Route::get('/storemanagement', 'StoreManagementController@storeManagement')->name('user.store.storeManagement');
Route::post('storemanagement', 'StoreManagementController@storeShopurl')->name('user.store.storeShopurl');
Route::get('/store/show', 'StoreController@show')->name('user.store.show');
Route::post('/store/delete','StoreController@destroy')->name('user.store.destroy');

Route::post('/shopify', 'StoreController@getPermission')->name('user.store.getPermission');
Route::get('oauth/authorize', 'StoreController@getResponse')->name('user.store.getResponse');

/******************************* Wallet ******************************************/

Route::get('/wallet', 'WalletController@index')->name('wallet.index');
Route::get('/stripe/view', 'WalletController@stripeViewIndex')->name('wallet.stripeViewIndex');
Route::get('/stripe/view', 'WalletController@payWithStripe')->name('addmoney.paywithstripe');
Route::post('addmoney/stripe', 'WalletController@postPaymentWithStripe')->name('addmoney.stripe');

Route::post('addmoney/stripe2', 'WalletController@postPaymentWithStripe2')->name('addmoney.stripe2');

Route::post('wallet/paymentmethod', 'WalletController@paymentmethod')->name('wallet.paymentmethod');
Route::get('/paypal_integration_php/success.php', 'WalletController@paypalsuccess')->name('wallet.paypalsuccess');
Route::get('wallet/success', 'WalletController@walletsuccess')->name('wallet.walletsuccess');
Route::get('/paypal_integration_php/cancel', function () {
return view('wallet.cancel');
});

/******************************* Wallet Payment Methods **************************/

Route::get('/wallet/paypal', 'WalletController@walletpaypal')->name('wallet.paypal');
Route::get('/wallet/stripe', 'WalletController@walletstripe')->name('wallet.stripe');
Route::get('/wallet/wired', 'WalletController@walletwired')->name('wallet.wired');
Route::post('wallet/wired-payment', 'WalletController@walletwiredpayment')->name('wallet.wiredpayment');
Route::get('/wallet/transaction-history', 'TransactionController@index')->name('wallet.transactionhistory.index');
Route::get('/transaction/show', 'TransactionController@show')->name('wallet.transactionhistory.show');

Route::get('/wallet/payoneer', 'WalletController@walletpayoneer')->name('wallet.payoneer');
Route::post('wallet/payoneer-payment', 'WalletController@payoneerpayment')->name('wallet.payoneerpayment');

/******************************* Billing History *********************************/
Route::get('/billing-history','BillingController@index')->name('billing.index');
Route::get('/billing-history/show','BillingController@show')->name('billing.show');//dkk
Route::get('/billing-history/view/{id}','BillingController@view')->name('billing.view');

/******************************* User Orders *************************************/
Route::get('/orders', 'OrdersController@index')->name('orders.index');
Route::get('/orders/show','OrdersController@show')->name('orders.show');

/******************************* User Order Details *************************************/
Route::get('/order-details/{order?}','OrderLineItemController@index')->name('orders.details');
Route::get('/order-details/{order?}/show','OrderLineItemController@show')->name('orders.details.show');
Route::post('/order-details/{order?}/payShipping','OrderLineItemController@payShipping')->name('orders.details.payShipping');//dk 12

/******************************* Forgot Password *********************************/
Route::post('/forgot/password', 'UserController@forgotPassword')->name('forgotPassword');
/* this route works after the link is clicked in mail to change Paswword*/
Route::get('forgotPassword/change/{email}', 'UserController@resetPassword')->name('forgotPassword.resetPassword');
Route::post('resetPassword', 'UserController@changeresetPassword')->name('forgotPassword.changeresetPassword');

/******************************* Countries *********************************/
Route::get('/countries','CountriesController@countries')->name('countries');

/******************************* Inventory **********************************/
Route::get('/inventory', 'InventoryController@index')->name('inventory.index');
Route::get('/inventory/show', 'InventoryController@show')->name('inventory.show');
Route::get('/inventory/activity', 'InventoryController@activity')->name('inventory.acitivity');

/******************************* Support Tickets **********************************/
Route::get('/support-tickets', 'SupportTicketsController@index')->name('support-tickets.index');
Route::get('/create-ticket', 'TicketCreateController@index')->name('support-tickets.create-ticket');//create ticket form
Route::post('/create-ticket/store', 'TicketCreateController@store')->name('TicketCreate.store');//store ticket
Route::get('/support-ticket/show', 'SupportTicketsController@show')->name('SupportTickets.show');//list all ticket
Route::get('/support-ticket/view/{id}', 'SupportTicketsController@view')->name('SupportTickets.view');// view single ticket
Route::post('/edit-ticket/store', 'SupportTicketsController@store')->name('SupportTickets.store');//store ticket edit

//list all ticket
/******************************* Photography **********************************/
Route::get('/photography', 'PhotographyController@index')->name('photography.index');
Route::get('/photography/show','PhotographyController@show')->name('phtography.show');
Route::get('/create-photography','PhotographyController@createPhotography')->name('phtography.createPhotography');
Route::post('/create-photography/store','PhotographyController@store')->name('phtography.store');

/******************************* Bulk-sourcing **********************************/
Route::get('/bulk-sourcing', 'BulkSourcingController@index')->name('bulk-sourcing.index');
Route::get('/bulk-sourcing/show','BulkSourcingController@show')->name('phtography.show');
Route::get('/create-quote','BulkSourcingController@createQuote')->name('phtography.createPhotography');
Route::post('/create-quote/store','BulkSourcingController@store')->name('phtography.store');
Route::post('/bulk-sourcing/approve-quote','BulkSourcingController@approveQuote')->name('pendingphotography.approveRequest');

/******************************* 7Priority **********************************/
Route::get('/7Priority', 'PriorityController@index')->name('7Priority.index');
Route::get('/7Priority/show', 'PriorityController@show')->name('7Priority.show');

/******************************* Meetups **********************************/
Route::get('/meetups', 'MeetupsController@index')->name('meetups.index');

/******************************* Conferences **********************************/
Route::get('/conferences', 'ConferencesController@index')->name('conferences.index');

/******************************* Users Notifications ************************************/ 
Route::get('/usernotifications', 'UserNotificationsController@index')->name('UserNotifications.index');
Route::get('/usernotifications/{id}', 'UserNotificationsController@message')->name('UserNotifications.message');

/******************************* Signup Step *************************************/

Route::get('/step1', '\App\Http\Controllers\Auth\RegisterController@createStep1');
Route::post('/step1', '\App\Http\Controllers\Auth\RegisterController@postCreateStep1');
Route::get('/step2', '\App\Http\Controllers\Auth\RegisterController@createStep2');
Route::post('/step2', '\App\Http\Controllers\Auth\RegisterController@postCreateStep2');
Route::get('/step3', '\App\Http\Controllers\Auth\RegisterController@createStep3');
Route::post('/step3', '\App\Http\Controllers\Auth\RegisterController@postCreateStep3');
Route::get('/step4', '\App\Http\Controllers\Auth\RegisterController@createStep4');
Route::post('/step4', '\App\Http\Controllers\Auth\RegisterController@store');
Route::post('/signup/store', '\App\Http\Controllers\Auth\RegisterController@signupstore');

/********************************* Route Group **************************************/

Route::middleware('isAdmin')->prefix('admin')->name('admin.')->group(function () {

   /****************************** Admin Dashboard********************************/
   Route::get('home', 'Admin\Dashboard\AdminController@index')->name('home');

   /****************************** Admin users ***********************************/ 
   Route::get('users', 'Admin\User\UserController@index')->name('user');
   Route::get('users/show', 'Admin\User\UserController@show')->name('user.show');
   Route::get('/user/edit/{id}','Admin\User\UserController@edit')->name('user.edit');
   Route::post('/user/store','Admin\User\UserController@store')->name('user.store');
   Route::post('/user/delete','Admin\User\UserController@destroy')->name('user.destroy');
   
   /****************************** Sub-Admin ***********************************/ 
   Route::get('subadmin', 'Admin\SubAdmin\SubAdminController@index')->name('subadmin');
   Route::get('subadmin/add', 'Admin\SubAdmin\SubAdminController@add')->name('subadmin.add');
   Route::post('/subadmin/store','Admin\SubAdmin\SubAdminController@store')->name('subadmin.store');
   Route::get('/subadmin/show', 'Admin\SubAdmin\SubAdminController@show')->name('subadmin.show');
   Route::get('/subadmin/edit/{id}','Admin\SubAdmin\SubAdminController@edit')->name('subadmin.edit');
   Route::post('/subadmin/delete','Admin\SubAdmin\SubAdminController@destroy')->name('subadmin.destroy');

   /***************************** Admin Completed transaction *********************/ 
   Route::get('transaction/completed-transaction', 'Admin\Transaction\CompletedtransactionController@index')->name('completedtransaction.index');
   Route::get('transaction/completed-transaction/show', 'Admin\Transaction\CompletedtransactionController@show')->name('completedtransactions.show');

   /**************************** Admin Pending transaction ************************/ 
   Route::get('transaction/pending-transaction', 'Admin\Transaction\PendingtransactionController@index')->name('pendingtransaction.index');
   Route::get('transaction/pending-transaction/show', 'Admin\Transaction\PendingtransactionController@show')->name('pendingtransaction.show');
   //Route::get('transaction/pending-transaction/{id}','Admin\Transaction\PendingtransactionController@approve')->name('pendingtransaction.approve');
   Route::post('transaction/pending-transaction','Admin\Transaction\PendingtransactionController@approvePayment')->name('pendingtransaction.approvePayment');

   /*************************** Admin Invoices ************************************/ 
   Route::get('invoices', 'Admin\Invoice\InvoiceController@index')->name('invoice.index');
   Route::get('invoices/show', 'Admin\Invoice\InvoiceController@show')->name('invoice.show');//dkk
   Route::get('invoices/create', 'Admin\Invoice\InvoiceCreateController@index')->name('invoiceCreate.index');//dkk
   Route::post('invoices/create/store', 'Admin\Invoice\InvoiceCreateController@store')->name('invoiceCreate.store');//dkk 
   Route::get('/invoices/create/{id}', 'Admin\Invoice\InvoiceCreateController@invoiceProduct')->name('invoice.invoiceProduct'); 
   Route::get('/invoices/edit/{id}','Admin\Invoice\InvoiceCreateController@edit')->name('invoice.edit');
   Route::get('/invoices/view/{id}','Admin\Invoice\InvoiceCreateController@view')->name('invoice.view');
   Route::post('/invoices/pending-invoices','Admin\Invoice\InvoiceCreateController@approveRequest')->name('pendinginvoices.approveRequest');
   
   /*************************** Subadmin Invoices ************************************/ 
   Route::get('subadmin-invoices', 'Admin\SubadminInvoice\SubadminInvoiceController@index')->name('invoice.index');
   
//   Route::get('invoices/show', 'Admin\Invoice\InvoiceController@show')->name('invoice.show');//dkk
//   Route::get('invoices/create', 'Admin\Invoice\InvoiceCreateController@index')->name('invoiceCreate.index');//dkk
//   Route::post('invoices/create/store', 'Admin\Invoice\InvoiceCreateController@store')->name('invoiceCreate.store');//dkk 
//   Route::get('/invoices/create/{id}', 'Admin\Invoice\InvoiceCreateController@invoiceProduct')->name('invoice.invoiceProduct'); 
//   Route::get('/invoices/edit/{id}','Admin\Invoice\InvoiceCreateController@edit')->name('invoice.edit');
//   Route::get('/invoices/view/{id}','Admin\Invoice\InvoiceCreateController@view')->name('invoice.view');

   //Route::get('/invoices/products/{email}', 'Admin\Invoice\InvoiceCreateController@invoiceProduct')->name('invoice.invoiceProduct');
   //Route::get('/invoices/products/{id}', 'Admin\Invoice\InvoiceCreateController@invoiceProduct')->name('invoice.invoiceProduct');   
   //Route::get('/invoices/products/{id}', 'Admin\Invoice\InvoiceCreateController@search_overall_stock_details')->name('invoice.search_overall_stock_details');

  /******************************* Admin Support Tickets **********************************/
  Route::get('all-tickets', 'Admin\AdminSupportTickets\AdminSupportTicketsController@index')->name('support-tickets.index');
  Route::get('all-tickets/show', 'Admin\AdminSupportTickets\AdminSupportTicketsController@show')->name('AdminSupportTicketsController.show');
  Route::get('support-ticket/view/{id}', 'Admin\AdminSupportTickets\AdminSupportTicketsController@view')->name('AdminSupportTicketsController.view');// view single ticket
  Route::post('update-ticket/store', 'Admin\AdminSupportTickets\AdminSupportTicketsController@store')->name('AdminSupportTicketsController.store');//store ticket edit

   /*************************** Wallet Limit ************************************/ 
   Route::get('wallet/limitamount', 'Admin\WalletLimit\WalletLimitAmountController@index')->name('walletLimit.index');//dkk
   Route::post('wallet/limitamount/store', 'Admin\WalletLimit\WalletLimitAmountController@store')->name('walletLimit.store');//dkk

   /*************************** Admin Profile *************************************/
   Route::get('profile', 'Admin\AdminProfile\ProfileController@index')->name('profile.index');
   Route::get('store/profile', 'Admin\AdminProfile\ProfileController@storeProfile')->name('profile.getStoreProfile');
   Route::post('store/profile', 'Admin\AdminProfile\ProfileController@storeProfile')->name('profile.storeProfile');
   Route::get('changePassword', 'Admin\AdminProfile\ProfileController@changePasswordView')->name('profile.changePasswordView');
   Route::post('store/changePassword', 'Admin\AdminProfile\ProfileController@storechangePassword')->name('profile.storechangePassword');

   /*************************** Admin Orders **************************************/
   Route::get('orders', 'Admin\Orders\OrdersController@index')->name('orders.index');

   /****************************** Admin Inventory ***********************************/ 
   Route::get('/inventory', 'Admin\Inventory\InventoryController@index')->name('inventory.index');
   Route::get('/inventory/add', 'Admin\Inventory\InventoryController@addInventory')->name('inventory.addInventory');
   Route::post('/inventory/store','Admin\Inventory\InventoryController@store')->name('inventory.store');
   Route::get('inventory/show', 'Admin\Inventory\InventoryController@show')->name('inventory.show');
   Route::get('/inventory/edit/{id}','Admin\Inventory\InventoryController@edit')->name('inventory.edit');
   Route::post('/inventory/delete','Admin\Inventory\InventoryController@destroy')->name('inventory.destroy'); 
   Route::post('/inventory/getproduct_by_userid','Admin\Inventory\InventoryController@getproduct_by_userid')->name('inventory.getproduct'); 

   /*************************** Admin Users Notifications ************************************/ 
   Route::get('usernotifications', 'Admin\UserNotifications\UserNotificationsController@index')->name('UserNotifications.index');//dkk
   Route::post('usernotifications/store', 'Admin\UserNotifications\UserNotificationsController@store')->name('UserNotifications.store');//dkk
   
   /******************************* Photography **********************************/
    Route::get('/photography/completed-photography', 'Admin\Photography\CompletedPhotographyController@index')->name('completedphotography.index');
    Route::get('/photography/completed-photography/show', 'Admin\Photography\CompletedPhotographyController@show')->name('completedphotography.show');
    
    Route::get('/photography/pending-photography', 'Admin\Photography\PendingPhotographyController@index')->name('pendingphotography.index');
    Route::get('/photography/pending-photography/show', 'Admin\Photography\PendingPhotographyController@show')->name('pendingphotography.show');
    Route::post('/photography/pending-photography','Admin\Photography\PendingPhotographyController@approveRequest')->name('pendingphotography.approveRequest');
    
    /******************************* Bulk-sourcing **********************************/
    
    Route::get('/bulk-sourcing/completed-quote', 'Admin\BulkSourcing\CompletedBulkSourcingController@index')->name('completedphotography.index');
    Route::get('/bulk-sourcing/completed-quote/show', 'Admin\BulkSourcing\CompletedBulkSourcingController@show')->name('completedphotography.show');
    
    Route::get('/bulk-sourcing/pending-quote', 'Admin\BulkSourcing\PendingBulkSourcingController@index')->name('pendingphotography.index');
    Route::get('/bulk-sourcing/pending-quote/show', 'Admin\BulkSourcing\PendingBulkSourcingController@show')->name('pendingphotography.show');
    //Route::post('/bulk-sourcing/pending-quote','Admin\BulkSourcing\PendingBulkSourcingController@approveQuote')->name('pendingphotography.approveRequest');
    Route::get('/bulk-sourcing/make-quote/{id}','Admin\BulkSourcing\PendingBulkSourcingController@makeQuote')->name('pendingphotography.approveRequest');
    Route::post('/bulk-sourcing/make-quote/store','Admin\BulkSourcing\PendingBulkSourcingController@makeQuoteStore')->name('pendingphotography.approveRequest');
    
    /*************************** Minimum Inventory ************************************/ 
    Route::get('/inventory/limitinventory', 'Admin\MinimumInventory\MinimumInventoryController@index')->name('minimumInventory.index');
    Route::post('/inventory/limitinventory/store', 'Admin\MinimumInventory\MinimumInventoryController@store')->name('minimumInventory.store');
    
    /*************************** Shipping ************************************/ 
    Route::get('/shipping', 'Admin\Shipping\ShippingController@index')->name('shipping.index');
    
    Route::get('/shipping/ePacket', 'Admin\Shipping\ePacketController@index')->name('shipping.ePacket.index');
    Route::get('/shipping/ePacket/add', 'Admin\Shipping\ePacketController@add')->name('shipping.ePacket.add');
    Route::get('/shipping/ePacket/show', 'Admin\Shipping\ePacketController@show')->name('shipping.ePacket.show');
    Route::get('/shipping/ePacket/edit/{id}', 'Admin\Shipping\ePacketController@edit')->name('shipping.ePacket.edit');
    Route::post('/shipping/ePacket/store', 'Admin\Shipping\ePacketController@store')->name('shipping.ePacket.store');
    Route::post('/shipping/ePacket/delete', 'Admin\Shipping\ePacketController@delete')->name('shipping.ePacket.delete');
    
    Route::get('/shipping/eParcel', 'Admin\Shipping\eParcelController@index')->name('shipping.eParcel.index');
    Route::get('/shipping/eParcel/add', 'Admin\Shipping\eParcelController@add')->name('shipping.eParcel.add');
    Route::get('/shipping/eParcel/show', 'Admin\Shipping\eParcelController@show')->name('shipping.eParcel.show');
    Route::get('/shipping/eParcel/edit/{id}', 'Admin\Shipping\eParcelController@edit')->name('shipping.eParcel.edit');
    Route::post('/shipping/eParcel/store', 'Admin\Shipping\eParcelController@store')->name('shipping.eParcel.store');
    Route::post('/shipping/eParcel/delete', 'Admin\Shipping\eParcelController@delete')->name('shipping.eParcel.delete');
    
    Route::get('/shipping/Express-shipping', 'Admin\Shipping\ExpressShippingController@index')->name('expressShipping.index');
    Route::get('/shipping/Express-shipping/add', 'Admin\Shipping\ExpressShippingController@add')->name('expressShipping.add');
    Route::get('/shipping/Express-shipping/show', 'Admin\Shipping\ExpressShippingController@show')->name('expressShipping.show');
    Route::get('/shipping/Express-shipping/edit/{id}', 'Admin\Shipping\ExpressShippingController@edit')->name('expressShipping.edit');
    Route::post('/shipping/Express-shipping/store', 'Admin\Shipping\ExpressShippingController@store')->name('expressShipping.store');
    Route::post('/shipping/Express-shipping/delete', 'Admin\Shipping\ExpressShippingController@delete')->name('expressShipping.delete');
    
    /*************************** Exchange-Rate ************************************/ 
    Route::get('/exchange-rate', 'Admin\ExchangeRate\ExchangeRateController@index')->name('exchangeRate.index');
    Route::post('/exchange-rate/store', 'Admin\ExchangeRate\ExchangeRateController@store')->name('exchangeRate.store');
    
    /*************************** Handling Fee ************************************/ 
    Route::get('/handling-fee', 'Admin\HandlingFee\HandlingFeeController@index')->name('handlingFee.index');
    Route::post('/handling-fee/store', 'Admin\HandlingFee\HandlingFeeController@store')->name('handlingFee.store');

    /*************************** Discount-Rate ************************************/ 
    Route::get('/discount-rate', 'Admin\DiscountRate\DiscountRateController@index')->name('discount.index');
    Route::post('/discount-rate/store', 'Admin\DiscountRate\DiscountRateController@store')->name('discount.store');

});

/********************************* End Route Group **************************************/

/********************************* Sub Admin Route Group **************************************/

Route::middleware('isAdmin')->prefix('subadmin')->name('subadmin.')->group(function () {

   /****************************** Sub-Admin Dashboard********************************/
   Route::get('home', 'SubAdmin\Dashboard\SubAdminController@index')->name('home');
   
   /****************************** Sub-Admin Inventory ***********************************/ 
   Route::get('/inventory', 'SubAdmin\Inventory\SubAdminInventoryController@index')->name('inventory.index');
   Route::get('/inventory/add', 'SubAdmin\Inventory\SubAdminInventoryController@addInventory')->name('inventory.addInventory');
   Route::post('/inventory/store','SubAdmin\Inventory\SubAdminInventoryController@store')->name('inventory.store');
   Route::get('inventory/show', 'SubAdmin\Inventory\SubAdminInventoryController@show')->name('inventory.show');
   Route::get('/inventory/edit/{id}','SubAdmin\Inventory\SubAdminInventoryController@edit')->name('inventory.edit');
   Route::post('/inventory/delete','SubAdmin\Inventory\SubAdminInventoryController@destroy')->name('inventory.destroy'); 
   
   Route::post('/inventory/getproduct_by_userid','SubAdmin\SubAdminInventoryController\InventoryController@getproduct_by_userid')->name('inventory.getproduct');

   /*************************** Admin Invoices ************************************/ 
   Route::get('invoices', 'SubAdmin\Invoice\SubAdminInvoiceController@index')->name('invoice.index');
   Route::get('invoices/show', 'SubAdmin\Invoice\SubAdminInvoiceController@show')->name('invoice.show');
   Route::get('invoices/create', 'SubAdmin\Invoice\SubAdminInvoiceCreateController@index')->name('invoiceCreate.index');
   Route::post('invoices/create/store', 'SubAdmin\Invoice\SubAdminInvoiceCreateController@store')->name('invoiceCreate.store');
   Route::get('/invoices/create/{id}', 'SubAdmin\Invoice\SubAdminInvoiceCreateController@invoiceProduct')->name('invoice.invoiceProduct'); 
   Route::get('/invoices/edit/{id}','SubAdmin\Invoice\SubAdminInvoiceCreateController@edit')->name('invoice.edit');
   Route::get('/invoices/view/{id}','SubAdmin\Invoice\SubAdminInvoiceCreateController@view')->name('invoice.view');

   /*************************** Sub-Admin Profile *************************************/
   Route::get('profile', 'SubAdmin\Profile\ProfileController@index')->name('profile.index');
   Route::get('store/profile', 'SubAdmin\Profile\ProfileController@storeProfile')->name('profile.getStoreProfile');
   Route::post('store/profile', 'SubAdmin\Profile\ProfileController@storeProfile')->name('profile.storeProfile');
   Route::get('changePassword', 'SubAdmin\Profile\ProfileController@changePasswordView')->name('profile.changePasswordView');
   Route::post('store/changePassword', 'SubAdmin\Profile\ProfileController@storechangePassword')->name('profile.storechangePassword');
   
   /******************************* Sub-Admin Photography **********************************/
    Route::get('/photography/completed-photography', 'SubAdmin\Photography\CompletedPhotographyController@index')->name('completedphotography.index');
    Route::get('/photography/completed-photography/show', 'SubAdmin\Photography\CompletedPhotographyController@show')->name('completedphotography.show');
    
    Route::get('/photography/pending-photography', 'SubAdmin\Photography\PendingPhotographyController@index')->name('pendingphotography.index');
    Route::get('/photography/pending-photography/show', 'SubAdmin\Photography\PendingPhotographyController@show')->name('pendingphotography.show');
    Route::post('/photography/pending-photography','SubAdmin\Photography\PendingPhotographyController@approveRequest')->name('pendingphotography.approveRequest');
    
    /******************************* Sub-Admin Bulk-sourcing **********************************/
    
    Route::get('/bulk-sourcing/completed-quote', 'SubAdmin\BulkSourcing\CompletedBulkSourcingController@index')->name('completedphotography.index');
    Route::get('/bulk-sourcing/completed-quote/show', 'SubAdmin\BulkSourcing\CompletedBulkSourcingController@show')->name('completedphotography.show');
    Route::get('/bulk-sourcing/pending-quote', 'SubAdmin\BulkSourcing\PendingBulkSourcingController@index')->name('pendingphotography.index');
    Route::get('/bulk-sourcing/pending-quote/show', 'SubAdmin\BulkSourcing\PendingBulkSourcingController@show')->name('pendingphotography.show');
    Route::get('/bulk-sourcing/make-quote/{id}','SubAdmin\BulkSourcing\PendingBulkSourcingController@makeQuote')->name('pendingphotography.approveRequest');
    Route::post('/bulk-sourcing/make-quote/store','SubAdmin\BulkSourcing\PendingBulkSourcingController@makeQuoteStore')->name('pendingphotography.approveRequest');
    
    /*************************** Sub-Admin Users Notifications ************************************/
    
    Route::get('usernotifications', 'SubAdmin\UserNotifications\UserNotificationsController@index')->name('UserNotifications.index');
    Route::post('usernotifications/store', 'SubAdmin\UserNotifications\UserNotificationsController@store')->name('UserNotifications.store');

});

/********************************* End Route Group **************************************/

Route::group(['prefix'=>'admin','as'=>'admin.'], function(){
   
   //Route::get('/', ['as' => 'index', 'uses' => 'AdminController@index']);       
   
});




