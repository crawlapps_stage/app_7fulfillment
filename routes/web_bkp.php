<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->name('loginurl');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/******************** Mail ***************************/

/* this route works after the link is clicked in mail to verify the user*/
Route::get('/user/verify/{email}', 'Auth\RegisterController@verifyUser');
/************************************************************************/

Route::get('/verifyEmailFirst','\App\Http\Controllers\Auth\RegisterController@verifyEmailFirst')->name('verifyEmailFirst');
Route::get('verify/{email}/verifyToken','\App\Http\Controllers\Auth\RegisterController@sendEmailDone')->name('sendEmailDone');

Route::get('sendbasicemail','MailController@basic_email');
Route::get('sendhtmlemail','\App\Http\Controllers\Auth\RegisterController@html_email')->name('sendhtmlemail');
Route::get('verify','MailController@verifyAccount')->name('verifyAccount');

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

/************************* User Profile ***********/
Route::get('/profile', 'UserController@index');
Route::get('store/profile', 'UserController@storeProfile');

Route::post('store/profile', 'UserController@storeProfile');

Route::get('/changePassword', 'UserController@changePasswordView');

Route::post('store/changePassword', 'UserController@storechangePassword');

/*********************** Store Management ***********/
Route::get('/store', 'StoreController@index');
Route::get('/storemanagement', 'StoreManagementController@storeManagement');
Route::post('storemanagement', 'StoreManagementController@storeShopurl');

/*********************** Wallet ***********/

Route::get('/wallet', 'WalletController@index');
Route::get('/stripe/view', 'WalletController@stripeViewIndex');


Route::get('/stripe/view', 'WalletController@payWithStripe')->name('addmoney.paywithstripe');
Route::post('addmoney/stripe', 'WalletController@postPaymentWithStripe')->name('addmoney.stripe');

Route::post('wallet/paymentmethod', 'WalletController@paymentmethod')->name('wallet.paymentmethod');

Route::get('/paypal_integration_php/success.php', 'WalletController@paypalsuccess');

Route::get('wallet/success', 'WalletController@walletsuccess');
Route::get('/paypal_integration_php/cancel', function () {
return view('wallet.cancel');
});


/************************Wallet Payment Methods *****************************/

Route::get('/wallet/paypal', 'WalletController@walletpaypal')->name('wallet.paypal');
Route::get('/wallet/stripe', 'WalletController@walletstripe')->name('wallet.stripe');
Route::get('/wallet/wired', 'WalletController@walletwired')->name('wallet.wired');

Route::post('wallet/wired-payment', 'WalletController@walletwiredpayment')->name('wallet.wiredpayment');

Route::get('/wallet/transaction-history', 'TransactionController@index');

Route::get('/transaction/show', 'TransactionController@show');


/******************************Admin********************************/


Route::get('admin/home', 'Admin\Dashboard\AdminController@index')->name('admin.home');


/********************** users ******************************/ 
Route::get('admin/users', 'Admin\User\UserController@index')->name('admin.user');


/********************** users ******************************/ 
Route::get('admin/users/show', 'Admin\User\UserController@show')->name('admin.user.show');
Route::get('/user/delete','Admin\User\UserController@destroy');



/********************** Completed transaction ******************************/ 
Route::get('admin/transaction/completed-transaction', 'Admin\Transaction\CompletedtransactionController@index')->name('admin.completedtransaction');

Route::get('admin/transaction/completed-transaction/show', 'Admin\Transaction\CompletedtransactionController@show')->name('admin.completedtransactions');

Route::group(['prefix'=>'admin','as'=>'admin.'], function(){
   
   //Route::get('/', ['as' => 'index', 'uses' => 'AdminController@index']);
       
   
});

/********************** Pending transaction ******************************/ 
Route::get('admin/transaction/pending-transaction', 'Admin\Transaction\PendingtransactionController@index')->name('admin.pendingtransaction');
Route::get('admin/transaction/pending-transaction/show', 'Admin\Transaction\PendingtransactionController@show')->name('admin.pendingtransaction.show');

Route::get('/admin/transaction/pending-transaction/{id}','Admin\Transaction\PendingtransactionController@approve');

Route::group(['prefix'=>'admin','as'=>'admin.'], function(){
   
   //Route::get('/', ['as' => 'index', 'uses' => 'AdminController@index']);
       
   
});

/********************** Invoices ******************************/ 
Route::get('admin/invoices', 'Admin\Invoice\InvoiceController@index')->name('admin.invoice');
Route::group(['prefix'=>'admin','as'=>'admin.'], function(){
   
   //Route::get('/', ['as' => 'index', 'uses' => 'AdminController@index']);
       
   
});

Route::group(['prefix'=>'admin','as'=>'admin.'], function(){
    
    //Route::get('/', ['as' => 'index', 'uses' => 'AdminController@index']);
        
    
});

/***************** Billing HIstory *****************/
Route::get('/billing-history','BillingController@index')->name('billing.index');


/************************ Admin Profile ******************************/
Route::get('/admin/profile', 'Admin\AdminProfile\ProfileController@index');
Route::get('admin/store/profile', 'Admin\AdminProfile\ProfileController@storeProfile');

Route::post('admin/store/profile', 'Admin\AdminProfile\ProfileController@storeProfile');

Route::get('/admin/changePassword', 'Admin\AdminProfile\ProfileController@changePasswordView');

Route::post('admin/store/changePassword', 'Admin\AdminProfile\ProfileController@storechangePassword');


/***************** Forgot Password ********************/

Route::post('/forgot/password', 'UserController@forgotPassword')->name('forgotPassword');
/* this route works after the link is clicked in mail to change Paswword*/
Route::get('forgotPassword/change/{email}', 'UserController@resetPassword');
Route::post('resetPassword', 'UserController@changeresetPassword');
/************************************************************************/


/******************************** User Orders ******************************/

Route::get('/orders', 'OrdersController@index')->name('orders.index');


/******************************** Admin Orders ******************************/

Route::get('/admin/orders', 'Admin\Orders\OrdersController@index')->name('admin.orders.index');







